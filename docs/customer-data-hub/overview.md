# Overview

## About

The Customer Data Hub on the Trusted Twin platform allows Data Providers to make their customer data accessible to Data Users. The Customer Data Hub allows for granular consent and data access management and use of privacy enhancing technologies.

## Definitions

### Data Providers

Data Providers are organizations or departments within an organization that provide customer data to Data Users on the Customer Data Hub. They manage the access management to these customer data.

### Data Users

Data Users are organizations or departments within an organization that access or request to access customer data on the Customer Data Hub that was made accessible by Data Providers.

### Customer data

Customer data is data pertaining to the end customer (a person who buys and uses a product or service). The end customer consents to the access to these data.

### Consent management

Consent management is a process in which customers define what information they are willing to permit someone to access.

### Data access management 

Data access management is a process defining who has access to which data assets. It allows to protect the privacy of customers to which the data pertains and to ensure that the organization complies with data protection laws.
