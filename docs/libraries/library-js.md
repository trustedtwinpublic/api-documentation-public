---
tags:
  - sdk
  - sdks
  - javascript
  - js
---

# JavaScript library

## Introduction

The Trusted Twin JavaScript library makes it easy to use the Trusted Twin user infrastructure API in applications written in server-side JavaScript. The library can be used in the browser or Node.js environment. It has Promise-based methods that wrap individual REST API endpoint calls. In TypeScript the definition should be automatically resolved via `package.json`.

## Requirements

The Trusted Twin JavaScript library requires:

### Environment

- Node.js
- Webpack
- Browserify

### Language level

- ES5 - you must have a Promises/A+ library installed
- ES6

### Module system

- CommonJS
- ES6 module system

## Installation

To install the library:

<ExtendedCodeGroup title="Installation" copyable>
<CodeBlock title="json">
```json
npm install @trustedtwin/js-client@2.13.00 --save
```
</CodeBlock>
</ExtendedCodeGroup>


<ExtendedCodeGroup title="Installation" copyable>
<CodeBlock title="json">

```js
import {
  Configuration,
  ConfigurationParameters,
  TwinsApi,
  IdentitiesApi,
  LedgerApi,
  DocsApi,
  RolesApi,
  UsersApi,
  SecretsApi,
  IndexesApi,
  TimerseriesApi,
  StickersApi,
  NotificationsApi,
  WebhooksApi,
} from "@trustedtwin/js-client";
```

</CodeBlock>
</ExtendedCodeGroup>

## Authorization

On the Trusted Twin platform, [User authentication](../account-and-access/authentication.md) is based on User Secrets. The User Secret is the API key on the Trusted Twin platform as it is required to access Trusted Twin API methods. To configure the client service provide the API key in the object constructor:

<ExtendedCodeGroup title="Authorization" copyable>
<CodeBlock title="js">

```js
export const CreateApiClient = (apiKey: string) => {
  const configParams: ConfigurationParameters = {
    basePath: "/api", // if you use client proxy to bypass CORS on the REST server
    apiKey,
  };
  const apiConfig = new Configuration(configParams);

  return new TwinsApi(apiConfig);
}
```
</CodeBlock>
</ExtendedCodeGroup>

## Responses

To retrieve only data returned by an endpoint, use regular API methods:

<ExtendedCodeGroup title="Example request" copyable>
<CodeBlock title="js">

```js
const twinAlive = await twinsApi.createTwin();
setTwinId(twinAlive?.creationCertificate?.uuid);
```
</CodeBlock>
</ExtendedCodeGroup>

If you want to read the response metadata, use methods with the `Raw` suffix:

<ExtendedCodeGroup title="Example request" copyable>
<CodeBlock title="js">

```js
const response = await twinsApi.createTwinRaw();
const {headers, ok, redirected, status, statusText, type, url} = response.raw;
```
</CodeBlock>
</ExtendedCodeGroup>

## Resources

For more information, see the [Trusted Twin JavaScript](https://github.com/TrustedTwin) GitHub repository.

You can explore an [example web application using the Trusted Twin REST API JavaScript Client](https://github.com/TrustedTwin/trusted-twin-js-example). 
