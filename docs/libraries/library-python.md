---
tags:
  - sdk
  - sdks
  - python
---

# Python library

## Introduction

The Trusted Twin Python library makes it easy to use the Trusted Twin user infrastructure API in Python applications. The library version is consistent with the Swagger version of the Trusted Twin API. 

The Trusted Twin Python library offers a synchronous (`tt_api`) and an asynchronous(`tt_api_async`) client type.

## Requirements

The Trusted Twin Python library requires Python 3.6 or above and the `requests` package.

<ExtendedCodeGroup title="Library requirements" copyable>
<CodeBlock title="python">
```bash
requests==2.31.0
```
</CodeBlock>
</ExtendedCodeGroup>

The asynchronous client type requires the `aiohhtp` package.

<ExtendedCodeGroup title="Asynchronous client type requirements" copyable>
<CodeBlock title="python">
```bash
aiohttp==3.8.4
```
</CodeBlock>
</ExtendedCodeGroup>

## Installation

You can install the Trusted Twin Python library using the pip package manager for Python.    
    
For synchronous client:

<ExtendedCodeGroup title="Synchronous client" copyable>
<CodeBlock title="python">
```bash
pip install trustedtwin
```
</CodeBlock>
</ExtendedCodeGroup>
    
For asynchronous client:

<ExtendedCodeGroup title="Asynchronous client" copyable>
<CodeBlock title="python">
```bash
pip install trustedtwin[async]
```
</CodeBlock>
</ExtendedCodeGroup>


## Authorization

On the Trusted Twin platform, [User authentication](../account-and-access/authentication.md) is based on User Secrets:

- If you have a User Secret, use it to authenticate REST requests:

<ExtendedCodeGroup title="Request authentication" copyable>
<CodeBlock title="python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(tt_auth=$USER_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>

- If you have a User Secret PIN, you can generate a User Secret (see documentation about the [create_user_secret](../reference/secrets/create-user-secret.md) endpoint for more details):

<ExtendedCodeGroup title="Create User Secret" copyable>
<CodeBlock title="python">

```python
from trustedtwin.tt_api import TTRESTService

status, response = TTRESTService().create_user_secret($ACCOUNT_UUID, $PIN)
user_secret = response['secret']

TT_SERVICE = TTRESTService(tt_auth=$USER_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>


## Example calls

### Synchronous client

<ExtendedCodeGroup title="Example call" copyable>
<CodeBlock title="python">

```python
import json 
from trustedtwin.tt_api import TTRESTService

status, response = TTRESTService().create_user_secret($ACCOUNT_UUID, $PIN)
resp = json.loads(response)

TT_SERVICE = TTRESTService(tt_auth=resp['secret'])

_body = {
    'description': {
        'custom_name': 'custom_value'
    }
}

status, response = TT_SERVICE.create_twin(body=_body)
resp = json.loads(response)
```
</CodeBlock>
</ExtendedCodeGroup>

### Asynchronous client

<ExtendedCodeGroup title="Example call" copyable>
<CodeBlock title="python">

```python
import json 
from trustedtwin.tt_api_async import TTAsyncRESTService

status, response = await TTAsyncRESTService().create_user_secret($ACCOUNT_UUID, $PIN)
resp = json.loads(response)

TT_SERVICE = TTAsyncRESTService(tt_auth=resp['secret'])

_body = {
    'description': {
        'custom_name': 'custom_value'
    }
}

status, response = await TT_SERVICE.create_twin(body=_body)
resp = json.loads(response)
```
</CodeBlock>
</ExtendedCodeGroup>


