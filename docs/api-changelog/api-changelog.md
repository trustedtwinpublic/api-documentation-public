---
tags:
  - changelog
  - api changelog
  - api version
  - api versions
  - new features
  - changes API
meta:
  - name: API changelog
    content: The Trusted Twin API changelog lists changes to the Trusted Twin API.
---

# Versions

The Trusted Twin API changelog lists changes to the Trusted Twin API.

## 3.17.00 (01-08-2024)

- Minor system improvements.

## 3.16.00 (04-06-2024)

- You can now grant and remove access to your account's notification topics to foreign accounts:
  - [update_notifications_account_access](../reference/notifications/update-notifications-account-access.md)
- You can now retrieve a list of accounts that have access to your account's notification topics:
  - [get_notifications_access](../reference/notifications/get-notifications-access.md)
- You can now use templates in the `"publish"` property of a Sticker (see the list of templates available for Stickers in the [Templates](../overview/templates.md) section). 
- You can now add more than one topic in the `"publish"` property for a given Sticker event (`"on_put"`, `"on_remove"`, `"on_expire"`).

## 3.15.00 (09-05-2024)

- Minor improvements to the [Usage](../reference/usage/index.md) functionality.

## 3.14.00 (15-04-2024)

- Changes to the [resolve_twin_identity](../reference/identities/resolve-twin-identity.md) endpoint:
  - the `"context"` query string parameter now takes also a list of account [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids) as value.
- Changes to the [get_stickers](../reference/stickers/get-stickers.md) endpoint:
  - the `"context"` query string parameter now takes also a list of account [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids) as value.
- Minor fixes to the [get_twin_identities](../reference/identities/get-twin-identities.md) endpoint.

## 3.13.01 (27-03-2024)

- Minor fixes to the [role](../reference/roles/index.md) functionality.
- Minor fixes to the [user](../reference/users/index.md) functionality.
- Minor fixes to the [batches](../reference/batches/index.md) functionality.

## 3.13.00 (14-03-2024)

- Added the following endpoint to the [Database](../reference/databases/index.md) advanced services category:
  - [update_database_ip_access](../reference/databases/update-database-ip-access.md).
- Improvements to the [get_batches](../reference/batches/get-batches.md) endpoint:
  - you can now use the `ge`, `le`, `limit` and `cursor` parameters when listing batches.

## 3.12.00 (07-02-2024)

- Changes to the `"write"` permission in databases:
  - the `"write"` permission does not include the `"read"` permission. If you had `"write"` permissions set for your database, these have been changed to `["read", "write"]` to preserve the permission levels.
- Improvements to the advanced services ([Timeseries](../reference/timeseries/index.md) and [Indexes](../reference/indexes/index.md)):
  - You can add more than one database to a single account.
- Added the [Database](../reference/databases/index.md) advanced services category and the following endpoints:
  - [get_databases](../reference/databases/get-databases.md)
  - [get_database](../reference/databases/get-database.md)
  - [update_database](../reference/databases/update-database.md)
  - [update_database_user_access](../reference/databases/update-database-user-access.md)
- Minor changes to the [create_timeseries_table](../reference/timeseries/create-timeseries-table.md) endpoint:
  - When creating a Timeseries table, you need to define which database it should be created in. In other case, the Timeseries table will be created in the [default database](../reference/databases/index.md#default-database).
- Minor changes to the [create_indexes_table](../reference/indexes/create-indexes-table.md) endpoint:
  - When creating an Indexes table, you need to define which database it should be created in. In other case, the Indexes table will be created in the [default database](../reference/databases/index.md#default-database).
- Minor changes to the [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md) endpoint:
  - The `"status"`, `"database_size"`, and `"database_uuid"` attributes are no longer available in the response to the [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md) endpoint. These attributes are available in the response to the [get_database](../reference/databases/get-database.md) for the given endpoint.
  - The `"users`" property is no longer available in the response to the [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md) endpoint. All information related to database access is available through the [get_database_access](../reference/databases/get-database-access.md) endpoint
- Minor changes to the [get_indexes_tables](../reference/indexes/get-indexes-tables.md) endpoint:
  - The `"status"`, `"database_size"`, and `"database_uuid"` attributes are no longer available in the response to the [get_indexes_tables](../reference/indexes/get-indexes-tables.md) endpoint. These attributes are available in the response to the [get_database](../reference/databases/get-database.md) for the given endpoint.
  - The `"users`" property is no longer available in the response to the [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md) endpoint. All information related to database access is available through the [get_database_access](../reference/databases/get-database-access.md) endpoint.
  Minor changes to the `"database_uuid"` attribute:
  - the name of the `"database_uuid"` attribute is now `"uuid"`.
- Minor improvements to the [Stickers](../reference/stickers/index.md) functionality:
  - the `"note"` maximum length is now 1024 characters (extended from 512 characters).

## 3.11.00 (10-01-2024)

- Added the `"details"` query string parameter to the [resolve_twin_identity](../reference/identities/resolve-twin-identity.md) endpoint.
- Added the `"auth_type"` and `"cause"` variables to [rules](../overview/rules.md).
- Changed the `"auth_type"` values from `"USR"` and `"TKN"` to `"secret"` and `"token"`.

## 3.10.00 (20-12-2023)

- Added the [Resource Access Log](../accounting/resource-access-log.md) functionality.
- Added endpoints allowing to retrieve and update the configuration of the [account](../reference/account/index.md) of the calling user:
  - [get_account](../reference/account/get-account.md)
  - [update_account](../reference/account/update-account.md)

## 3.09.00 (15-11-2023)

- Improvements to the [Stickers](../reference/stickers/index.md) functionality.
- Minor fixes to the [Identity](../reference/identities/index.md) functionality.

## 3.08.00 (2-11-2023)

- Added the `"operation"` variable to [rules](../overview/rules.md).
- Minor improvements to the [scan_twins](../reference/twins/scan-twins.md) functionality.

## 3.07.00 (19-10-2023)

- Improvements to advanced database services ([Timeseries](../reference/timeseries/index.md), [Indexes](../reference/indexes/index.md)):
  - When inserting records, when a match is found on a timestamp and in dimensions columns in an existing record, the existing record is updated with new measurement values instead of inserting a new one.
- Fixes to advanced database services ([Timeseries](../reference/timeseries/index.md), [Indexes](../reference/indexes/index.md)).
- New [built-in functions in rules](../overview/rules.md#built-in-functions).
- Improvements to existing [built-in functions in rules](../overview/rules.md#built-in-functions).
- Minor improvements to the [scan_twins](../reference/twins/scan-twins.md) functionality.

## 3.06.00 (20-9-2023)

- Minor improvements to the [batch](../reference/batches/index.md) functionality.     
- Minor improvement to the [role](../reference/roles/index.md) and [user](../reference/users/index.md) objects.

## 3.05.00 (6-09-2023)

- Added the [batch](../reference/batches/index.md) functionality and the following endpoints:
  - [create_batch](../reference/batches/create-batch.md)
  - [get_batch](../reference/batches/get-batch.md)
  - [get_batches](../reference/batches/get-batches.md)
  - [update_batch](../reference/batches/update-batch.md)
  - [delete_batch](../reference/batches/delete-batch.md)
- Minor improvements to the [role](../reference/roles/index.md) and [user](../reference/users/index.md) objects.

## 3.04.00 (10-08-2023)

- Minor system improvements.

## 3.03.00 (12-07-2023)

- Added the [scan_twins](../reference/twins/scan-twins.md) endpoint.
- Minor improvements to the [Timeseries](../reference/timeseries/index.md) service.

## 3.02.00 (26-06-2023)

- Added the `"activity"` field to the [user](../reference/users/index.md) object. The `"activity"` field lets you enable the [User Activity Log](../accounting/user-activity-log.md) for a given user.
- Minor improvements to the [Doc](../reference/docs/index.md) object.
- Minor improvements to the [History](../reference/history/index.md) service.
- Minor fixes to the [role](../reference/roles/index.md) object.

## 3.01.00 (15-06-2023)

- Improvement to the [Notifications](../reference/notifications/index.md) service:
  - you can now set notifications events to be triggered upon deletion of a Ledger Entry. 
- Minor changes to the [Notifications](../reference/notifications/index.md) service:
  - the `"topic"` value must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\\-]{3,48}$`. 
- Minor changes to the [Stickers](../reference/stickers/index.md) service:
  - `"color"` value must the [regular expression](https://regexr.com/) `^[0-9A-Za-z\\-]{3,48}$`.
- Minor improvements to the [Stickers](../reference/stickers/index.md) functionality.
- Minor fixes to the [Notifications](../reference/notifications/index.md) functionality.

## 3.00.01 (13-04-2023)

- Minor improvements to the [Notifications](../reference/notifications/index.md) service.

## 3.00.00 (03-04-2023)

- Added [built-in functions](../overview/rules.md#built-in-functions).
- Changes to the [Ledger](../reference/ledgers/index.md) object:
  - the `"entry_created_ts"`, `"entry_updated_ts"` and `"value_changed_ts"` are now only available at the Entry level,
  - the `"history"`, `"timeseries"`, `"publish"`, and `"value"` attributes are returned only if they have a value.
- Minor improvements to the [Notifications](../reference/notifications/index.md) and [Stickers](../reference/stickers/index.md) services.
- Minor changes to the [role](../reference/roles/index.md) object.

## 2.13.00 (20-01-2023)

- Added the [get_users](../reference/users/get-users.md) endpoint.
- Added the [get_twin_ledger_entry_value](../reference/ledgers/get-twin-ledger-entry-value.md) endpoint.

## 2.12.00 (22-12-2022)

- Added the [Notifications](../reference/notifications/index.md) functionality to the [Stickers](../reference/stickers/index.md) service.<
- Added the `"geography"` and `"geometry"` types for the `"properties"` column in the [Indexes](../reference/indexes/index.md) service and for the `"measurement"` column in the  [Timeseries](../reference/timeseries/index.md) service.
- Minor changes to the [Indexes](../reference/indexes/index.md) service.
- Minor changes to the [Timeseries](../reference/indexes/index.md) service.
- Minor changes to the [Ledger Entry](../reference/ledgers/index.md).
- Minor improvements to the [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md) endpoint.

## 2.11.00 (8-12-2022)

- Minor system improvements.

## 2.10.00 (21-11-2022)

- Added the `"identity"` field to [role](../reference/roles/index.md) [rules](../overview/rules.md).
- Minor fixes to the [User Token](../reference/token/index.md) functionality.

## 2.09.00 (10-11-2022)

- Improvements to [update_twin_ledger_entry](../reference/ledgers/update-twin-ledger-entry.md):
  - you can now update the `"history"`, `"publish"`, and `"timeseries"` attributes.
Minor improvements to the [History](../reference/history/index.md) service:
  - if `"history"` attribute is set to `null` (through [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md) and [update_twin_ledger_entry](../reference/ledgers/update-twin-ledger-entry.md) endpoint), the History service is not enabled.
- Minor improvements to the [Notifications](../reference/notifications/index.md) service:
  - if not provided, the default value of the `"expires"` parameter is 604800 seconds (7 days).

## 2.08.00 (20-10-2022)

- Added the [update_twin_ledger_entry_value](../reference/ledgers/update-twin-ledger-entry-value.md) endpoint.
- Minor improvements to the [Timeseries](../reference/timeseries/index.md) database.
- Minor fixes to the [Notifications](../reference/notifications/index.md) service.

## 2.07.00 (13-10-2022)

- Added the [Stickers](../reference/stickers/index.md) service and the following endpoints:
  - [put_sticker](../reference/stickers/put-sticker.md)
  - [get_sticker](../reference/stickers/get-sticker.md)
  - [get_stickers](../reference/stickers/get-stickers.md)
  - [list_stickers](../reference/stickers/remove-sticker.md)
  - [remove_sticker](../reference/stickers/remove-sticker.md)
- Added the [User Token](../reference/token/index.md) functionality and the following endpoints:
  - [create_user_token](../reference/token/create-user-token.md)
  - [refresh_user_token](../reference/token/refresh-user-token.md)
- Added [Trace](../reference/trace/index.md) functionality and the following endpoint:
  - [trace](../reference/trace/trace.md)
- Improvements to [update_timeseries_table](../reference/timeseries/update-timeseries-table.md):
  - you can now update the names of the `"measurements"` and `"dimensions"` columns,
  - you can now add new `"measurements"` and `"dimensions"` columns to an existing Timeseries table.
- Improvements to [update_indexes_table](../reference/indexes/update-indexes-table.md):
  - you can now update the names of `"properties"` columns, 
  - you can now add new `"properties"` columns to an existing Indexes table,
  - you can now update the [templates](../overview/templates.md) of existing properties.
- Minor improvements to the [Doc](../reference/docs/index.md) object.

## 2.06.00 (15-09-2022)

- Major system refactor.      
- Added the [`X-TrustedTwin` custom header](../account-and-access/custom-headers.md#x-trustedtwin).     
- The [Timeseries](../reference/timeseries/index.md) and [Indexes](../reference/indexes/index.md) databases are now also accessible through domain names (database_id.database.trustedtwin.com).

## 2.05.02 (21-07-2022)

- Added the following endpoints related to [usage](../reference/usage/index.md):
  - [get_account_usage](../reference/usage/get-account-usage.md)
  - [get_user_usage](../reference/usage/get-user-usage.md)

## 2.05.01 (18-07-2022)

- Minor fixes to the [Ledger](../reference/ledgers/index.md) Entry.

## 2.05.00 (12-07-2022)

- Added the [Notifications](../reference/notifications/index.md) service and the following endpoints:
  - [webhook_subscribe](../reference/notifications/webhook-subscribe.md)
  - [webhook_confirm_subscription](../reference/notifications/webhook-confirm-subscription.md)
  - [webhook_refresh_subscription](../reference/notifications/webhook-refresh-subscription.md)
  - [webhook_unsubscribe](../reference/notifications/webhook-unsubscribe.md)
- Added the [who_am_i](../reference/whoami/index.md) endpoint.
- Added the `"include"` field to the [Ledger](../reference/ledgers/index.md) Entry.
- Added the `"timestamp"` field to the [Timeseries](../reference/timeseries/index.md) database. 

## 2.04.00 (21-06-2022)

- Minor fixes to the [History](../reference/history/index.md) service.

## 2.03.02 (01-06-2022)

- Minor improvements to the [Indexes](../reference/indexes/index.md) database.

## 2.03.01 (25-05-2022)

- Minor fixes to the [Indexes](../reference/indexes/index.md) database.

## 2.03.00 (20-05-2022)

- Added the [Indexes*](../reference/indexes/index.md) service and the following endpoints:
  - [update_indexes_access](../reference/indexes/update-indexes-access.md)
  - [create_indexes_table](../reference/indexes/create-indexes-table.md)
  - [get_indexes_table](../reference/indexes/get-indexes-table.md)
  - [get_indexes_tables](../reference/indexes/get-indexes-tables.md)
  - [update_indexes_table](../reference/indexes/update-indexes-table.md)
  - [truncate_indexes_table](../reference/indexes/truncate-indexes-table.md)
  - [delete_indexes_table](../reference/indexes/delete-indexes-table.md)

::: footnote *
The [Indexes*](../reference/indexes/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::

## 2.02.00 (28-04-2022)

- Added the [History](../reference/history/index.md) service and the following endpoint:
  - Added the [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md) endpoint.
- Added the [get_log](../reference/log/get-log.md) endpoint.
- Minor fixes to the [Timeseries](../reference/timeseries/index.md) database.     

## 2.01.02 (13-04-2022)

- Minor fixes to the [Timeseries](../reference/timeseries/index.md) database.

## 2.01.01 (12-04-2022)

- Minor improvements to the [Timeseries](../reference/timeseries/index.md) database.

## 2.01.00 (11-04-2022)

- Added the [Timeseries*](../reference/timeseries/index.md) database and the following endpoints:
  - [update_timeseries_access](../reference/timeseries/update-timeseries-access.md)
  - [create_timeseries_table](../reference/timeseries/create-timeseries-table.md)
  - [get_timeseries_table](../reference/timeseries/get-timeseries-table.md)
  - [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md)
  - [update_timeseries_table](../reference/timeseries/update-timeseries-table.md)
  - [truncate_timeseries_table](../reference/timeseries/truncate-timeseries-table.md)
  - [delete_timeseries_table](../reference/timeseries/delete-timeseries-table.md)
- Added the `"history"` and `"timeseries"` fields to the [Ledger](../reference/ledgers/index.md) Entry.

::: footnote *
The [Timeseries*](../reference/timeseries/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::

## 2.00.01 (06-04-2022)

- Minor improvements to the [Doc](../reference/docs/index.md) object.
