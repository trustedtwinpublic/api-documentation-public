---
tags:
  - postman
---

# Postman quickstart guide

This tutorial shows how to quickly get set up with [Postman](https://www.postman.com/) and use it to explore the Trusted Twin API.

## Introduction

[Postman](https://www.postman.com/) is an API platform used for API testing. You can use Postman to send HTTP requests and obtain responses via a graphical user interface for free.

### Prerequisites

In order to use the Trusted Twin API collections in Postman, you will need to download the Postman app or use Postman in a web browser.

To download Postman:
1. Go to [https://www.postman.com/downloads](https://www.postman.com/downloads/).
2. Choose your operating system.
3. Download Postman.
4. Open the downloaded file and follow the installation steps.

To access Postman via a web browser:
1. Go to [https://go.postman.co/home](https://go.postman.co/home).
2. Sign up to use the web browser version of Postman.

## Trusted Twin Postman collections

There are two Trusted Twin API collections available.

### Trusted Twin API demo 

The [Trusted Twin API demo collection](https://postman.com/trustedtwin/workspace/trusted-twin-api/documentation/19109389-cb7dbe63-9140-4709-b68e-38cde84e68ac) allows you to send requests for Trusted Twin objects such as the Twin, Ledger, and Identity. To explore the full range of the Trusted Twin API, use the collection [Trusted Twin API full version](https://postman.com/trustedtwin/workspace/trusted-twin-api/documentation/19109389-06396a65-2be2-4346-ab27-35ee5f958d30). 

In order to use this collection, you will need to [create a free Trusted Twin Account](../account-and-access/account-creation.md) through the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/).
      
[Go to the Trusted Twin API demo collection.](https://postman.com/trustedtwin/workspace/trusted-twin-api/documentation/19109389-cb7dbe63-9140-4709-b68e-38cde84e68ac) 

In order to use the collection, you need to fork it:
1. On the collection overview tab, click on the **fork** icon in the upper-right corner.
2. Enter a *Fork label* and *Location* (workspace) of the collection. Click on the **Fork collection** button.

### Trusted Twin API full version

This collection contains all requests available on the Trusted Twin platform. Please note that the [Timeseries](../reference/timeseries/index.md) and [Indexes](../reference/indexes/index.md) services need to be enabled on your account separately. Please contact <hello@trustedtwin.com> for more details.

In order to use this collection, you will need to [create a free Trusted Twin Account](../account-and-access/account-creation.md) through the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/).
        
[Go to the Trusted Twin API full version collection](https://postman.com/trustedtwin/workspace/trusted-twin-api/documentation/19109389-06396a65-2be2-4346-ab27-35ee5f958d30).    

In order to use the collection, you need to fork it:
1. On the collection overview tab, click on the **fork** icon in the upper-right corner.
2. Enter a *Fork label* and *Location* (workspace) of the collection. Click on the **Fork collection** button.

## Authentication

On the Trusted Twin platform, the User Secret is the API key required to access Trusted Twin API methods. It should be passed in the HTTP Authorization header of each request. For more details, see the [User authentication](../account-and-access/authentication.md) section.   

In order to generate the User Secret (API key), [create a free Trusted Twin Account](../account-and-access/account-creation.md) through the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/).

To configure the User Secret (API key) for all your requests in Postman:
1. In your collection, go to the **Authorization tab**.
2. In the *Type* section, select **API Key** from the dropdown menu.
3. In the *Key* section, type in *Authorization*.
4. In the *Value* section, type in your User Secret.
5. In the *Add to* section, leave the default setting (*Header*).

## Send a request to the Trusted Twin API

To send a request to the Trusted Twin API:

1. On the left-hand side, click on the name of the Trusted Twin collection to view folders holding endpoints related to each object.
2. Click on the folder to view the list of endpoints.
3. Select an endpoint. Depending on the endpoint, you might need to include parameters in the path, or body of the request:
- If there is a required parameter in the path of the request, it will be wrapped in {{ }}, for example:

<ExtendedCodeGroup title="Example request" copyable>
<CodeBlock title="cURL">
```bash
GET https://rest.trustedtwin.com/twins/{{twin}}
```
</CodeBlock>
</ExtendedCodeGroup>

- If there is a required or optional request body, a draft of the request body will be included in the `Body` >> `raw` section.

To obtain information about optional query string parameters as well as documentation on the respective endpoint, click on the `documentation` icon on the right-hand side and click on the provided link to be taken to the relevant documentation section.
