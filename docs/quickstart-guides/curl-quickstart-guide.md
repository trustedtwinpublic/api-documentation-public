# cURL quickstart guide

This guide shows how to use the Trusted Twin API and the Client URL (or cURL) tool to create, access, update, and delete REST resources on the Trusted Twin platform.     

We are going to guide you through creating, retrieving, and updating a Twin, Identities, and Ledger Entries, as well as retrieving the history of Ledger Entry changes. You are going to create a Digital Twin that represents an electric scooter in a scooter-sharing system.

In our guide we start with an introduction to cURL. If you are familiar with cURL, you can skip the introduction and go straight to the [Authentication](./curl-quickstart-guide.md#authentication) section.

## About cURL

Client URL (or cURL) is a command-line tool for transferring data to and from a server using URL syntax. Please note that cURL is an open-source command-line tool. It is not supported by Trusted Twin.

### Prerequisites

cURl is pre-installed on most Linux distributions, macOS systems, and latest Windows versions. 

To check whether cURL is installed on your system:

- Open your console.
- Type in:

<ExtendedCodeGroup title="cURL" copyable>
<CodeBlock title="cURL">
```bash
curl
```
</CodeBlock>
</ExtendedCodeGroup>

- Press `*Enter*`.

If cURL is installed, the system will print:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
try 'curl --help' or 'curl --manual' for more information
```
</CodeBlock>
</ExtendedCodeGroup>

If cURL is not installed, the system will print:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
curl command not found
```
</CodeBlock>
</ExtendedCodeGroup>

#### Installing cURL - Linux

To install cURL on Ubuntu and Debian:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
$ sudo apt update
$ sudo apt install curl
```
</CodeBlock>
</ExtendedCodeGroup>

To install cURL on CentOS and Fedora:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
$ sudo yum install curl
```
</CodeBlock>
</ExtendedCodeGroup>

#### Installing cURL - Mac OSX

To install cURL on MacOSX:
- Open Terminal (`Command+Space`, type `Terminal`, press `*Enter*`).
- Type in:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" /dev/null
```
</CodeBlock>
</ExtendedCodeGroup>

- Press `*Enter*`.
- Run:

<ExtendedCodeGroup title="cURL installation" copyable>
<CodeBlock title="cURL">
```bash
rew install curl
```
</CodeBlock>
</ExtendedCodeGroup>

#### Installing cURL - Microsoft Windows

You can download and install official cURL builds for Microsoft Windows [here](https://curl.se/windows/).

### Sending API requests

To request data from the Trusted Twin API, you must make an HTTP request with a header.

### API request structure

This is an example of an API request in the Trusted Twin environment:

<ExtendedCodeGroup title="Example request" copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
        "brand": "e_scooters"
        "scooter_ID": "ouccv096875434";
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane"
        "frame_material"" "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg" 
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

#### Basic API request

The basic API request is as follows:

<ExtendedCodeGroup title="Example cURL request" copyable>
<CodeBlock title="cURL">
```bash
curl --location --request 
GET https://rest.trustedtwin.com/twins
```
</CodeBlock>
</ExtendedCodeGroup>

It consists of the following components:

- Verb: The verb specifies the action to be performed on a resource or a collection of resources:

<EndpointsTable>
| Method | Operation           |
|:-------|:--------------------|
| POST   | Create a resource   |
| GET    | Retrieve a resource |
| PATCH  | Update a resource   |
| DELETE | Delete a resource   |
</EndpointsTable>

- APIName: Trusted Twin API is using the REST API. 
- BaseURL: All Trusted Twin API calls should be made to the `rest.trustedtwin.com` base domain.
- Endpoint: Enables the API to gain access to resources.

### Body of the request

The request body is used to send data via the REST API.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="cURL">
```bash
--data-raw '{
    "description": {
        "brand": "e_scooters"
        "scooter_ID": "ouccv096875434";
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane"
        "frame_material"" "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg" 
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

#### Authentication

The Trusted Twin API authenticates requests through [User Secrets](../reference/secrets/index.md). The User Secret is a string composed of 64 random characters used to authenticate the identity of a user.

You can go through our cURL guide by [creating a free Trusted Twin Account](../account-and-access/account-creation.md) through the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/).

<ExtendedCodeGroup title="Authorization" copyable>
<CodeBlock title="cURL">
```bash
curl --header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>
</ExtendedCodeGroup>

## Example scenario

In our example, we are going to create a Digital Twin representing an electric scooter used in a scooter-sharing system. The scooter-sharing system is a short-range shared-transport service where electric scooters are available for short-term rentals.     
   
Here is a list of parties who use or are involved in the production, operating and/or maintenance, and, last but not least, the use of a single scooter:
- The users who need to install an application in order to use the scooter. 
- The application for the scooter as well as the scooters' operating system has been developed and is maintained by company A.
- The scooters are produced by company B.
- The scooters are operated by company C.
- The scooters are sustained by mechanics from a contractor company (company D) hired by the scooter company.
- The daily operations and maintenance (for example, charging scooters) are managed by a contractor company E.
    
As we can see, there are many parties who contribute their data to the scooter and who will require access to other people's data. However, all of these data cannot be stored on the infrastructure of any particular company or person as there are multiple owners of data. Each of the owners of the data should remain authority over their data. Therefore, we are going to build a Digital Twin and attach data from different parties to it. The object which will tie together this knowledge will be the Twin.

### Create a Twin

We are going to create a Twin object which represents an electric scooter. The Twin has one owner, but all users involved in the process can attach data to the Twin and retain full authority over the data they attach. 
    
The Twin object has an optional description field. The owner of the Twin (in our case it is company C who operates the scooters) can include information about the Twin in form of key-value pairs (the value needs to be JSON compliant). The contents of the description field are visible to all users who know the unique ID of the Twin (Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids)). Therefore, no confidential data should be added to the description of the Twin.      
     
In our case, the description contains technical information about the scooter such as the brand, scooter_id, model, colour etc. of the scooter: 

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg" 
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

In order to create a Twin, we send the following POST request:

<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
        "brand": "e_scooters"
        "scooter_ID": "ouccv096875434";
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane"
        "frame_material"" "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg" 
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns details of the created Twin.

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "owner": "9c8d5504-cacd-4dfb-94d2-e34b1cd03c89",
    "status": "alive",
    "updated_ts": 1654698840.952,
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg"
    },
    "creation_certificate": {
        "uuid": "955cccda-9e35-48aa-9512-abe3ab1ccfcb",
        "creator": "9c8d5504-cacd-4dfb-94d2-e34b1cd03c89",
        "created_ts": 1654698840.952
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Retrieve a Twin

To retrieve the details of the Twin, send a GET request to the https://rest.trustedtwin.com/twins/{twin} endpoint including the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin you want to retrieve:

<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/955cccda-9e35-48aa-9512-abe3ab1ccfcb' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns the details of the Twin:

<ExtendedCodeGroup title="JSON response" copyable>
<CodeBlock title="json">

```json
{
    "owner": "893bdefe-f963-44c3-a6c0-d599abc561fe",
    "status": "alive",
    "updated_ts": 1654698840.952,
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg"
    },
    "creation_certificate": {
        "uuid": "955cccda-9e35-48aa-9512-abe3ab1ccfcb",
        "creator": "893bdefe-f963-44c3-a6c0-d599abc561fe",
        "created_ts": 1654698840.952
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Update a Twin

If we are the owner of a Twin, we can update the description of the Twin. Please note that the if the "description" field with attributes is provided in the request body, the "description" field from the request body will replace the current "description" field.     
      
The description of a Twin can be updated only if the Twin has the status `"alive"`. If a Twin has been terminated, the owner of the Twin can no longer changed the description.      
     
However, after a Twin has been terminated, all other users still have access to the data they have attached to the Twin and can attach more data. For example, the owner of the Twin terminates the Twin when the scooter is retired from use. However, other users still have access to the data they have attached to the Twin so that they can access and view their data and add new data (for example, the mechanics from company D could attach documentation stating the detailed reason of why the scooter has been retired).   
      
In our example, we want to change the description to:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg",
        "warranty_check": "01.06.2022"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

To update a Twin, send a PATCH request to the `https://rest.trustedtwin.com/twins/{twin}` endpoint including the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin you want to update:

<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/955cccda-9e35-48aa-9512-abe3ab1ccfcb' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg",
        "warranty_check": "01.06.2022"
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns the details of the updated Twin:

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "owner": "893bdefe-f963-44c3-a6c0-d599abc561fe",
    "status": "alive",
    "updated_ts": 1654699296.734,
    "description": {
        "brand": "e_scooters",
        "scooter_id": "ouccv096875434",
        "production_year": 2021,
        "model": "bna56",
        "colour": "bright red",
        "wheel_size": "11 inches",
        "wheel_material": "polyurethane",
        "frame_material": "aluminium",
        "weight": "22.2 kg",
        "max_load": "120 kg",
        "warranty_check": "01.06.2022"
    },
    "creation_certificate": {
        "uuid": "955cccda-9e35-48aa-9512-abe3ab1ccfcb",
        "creator": "893bdefe-f963-44c3-a6c0-d599abc561fe",
        "created_ts": 1654698840.952
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Create a Ledger Entry

The scooters as well require continuous measurement and sharing of data:
- the scooters are dock-less (they don't have fixed parking spots). As a results, their location needs to be measured and communicated regularly.
- they operate between geo-fences delimiting the available area of usage (there is a warning preceding the locking of the scooter). 
- In certain areas there are as well geo-speed limits.
- the scooters charged electrically in selected spots. 
- the scooters are charged by company E. However, users are encouraged to charge the scooters by receiving a small fee and loyalty points which they can redeem when they next hire a scooter.     
     
We can see that there are many measurements related to an e-scooter where the values need to be stored as well as updated. On the Trusted Twin platform, you can store measurements in [Ledger](../reference/ledgers/index.md) Entries.     
     
In our example, we want to focus on two measurements from the above list. We are going to store measurements related to the distance traveled with an e-scooter as well as the battery level of an e-scooter. Therefore, our Ledger Entries will look as follows:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "distance_traveled": {
            "value": 214,
            "visibility": null,
            "history": "1M"
        },
        "battery_level": {
            "value": 55.5,
            "visibility": null,
            "history": "1M"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


We can add these Entries to a Ledger by sending a POST request to the `https://rest.trustedtwin.com/twins/{twin}/ledgers/personal` endpoint including the Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin:


<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="json">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/955cccda-9e35-48aa-9512-abe3ab1ccfcb/ledgers/personal ' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "entries": {
        "distance_traveled": {
            "value": 214,
            "visibility": null,
            "history": "1M"
        },
        "battery_level": {
            "value": 55.5,
            "visibility": null,
            "history": "1M"
        }
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns:

<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="json">

```json
{
    "entry_created_ts": 1660836476.698,
    "entry_updated_ts": 1660836476.698,
    "value_changed_ts": 1660836476.698,
    "entries": {
        "distance_traveled": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660836476.698,
            "value_changed_ts": 1660836476.698,
            "value": 214,
            "visibility": null,
            "history": "1M"
        },
        "battery_level": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660836476.698,
            "value_changed_ts": 1660836476.698,
            "value": 55.5,
            "visibility": null,
            "history": "1M"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Get Ledger Entry

In order to retrieve current measurements, we need to send a GET request to the `https://rest.trustedtwin.com/twins/{twin}/ledgers/personal` endpoint and include the Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids):

<ExtendedCodeGroup title="cURL request" copyable>
<CodeBlock title="json">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/955cccda-9e35-48aa-9512-abe3ab1ccfcb/ledgers/personal ' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Example response" copyable>
<CodeBlock title="json">
```json
{
    "entry_created_ts": 1660836476.698,
    "entry_updated_ts": 1660836476.698,
    "value_changed_ts": 1660836476.698,
    "entries": {
        "battery_level": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660836476.698,
            "value_changed_ts": 1660836476.698,
            "value": 55.5,
            "visibility": null,
            "history": "1M"
        },
        "distance_traveled": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660836476.698,
            "value_changed_ts": 1660836476.698,
            "value": 214,
            "visibility": null,
            "history": "1M"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Update Ledger Entry

In order to update the values, the new values need to be added to the body of the request:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "distance_traveled": {
            "value": 245
        },
        "battery_level": {
            "value": 40
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

To update the values of our measurements, we need to send a PATCH request to the `https://rest.trustedtwin.com/twins/{twin}/ledgers/personal` endpoint and include the Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids):

<ExtendedCodeGroup title="Example cURL request" copyable>
<CodeBlock title="json">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/955cccda-9e35-48aa-9512-abe3ab1ccfcb/ledgers/personal' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "entries": {
        "distance_traveled": {
            "value": 245
        },
        "battery_level": {
            "value": 40
        }
    }
}'
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns the details of the update Ledger Entries:

<ExtendedCodeGroup title="Example response" copyable>
<CodeBlock title="json">
```json
{
    "entry_created_ts": 1660836476.698,
    "entry_updated_ts": 1660838145.426,
    "value_changed_ts": 1660838145.426,
    "entries": {
        "battery_level": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660838145.426,
            "value_changed_ts": 1660838145.426,
            "value": 40,
            "visibility": null,
            "history": "1M"
        },
        "distance_traveled": {
            "entry_created_ts": 1660836476.698,
            "entry_updated_ts": 1660838145.426,
            "value_changed_ts": 1660838145.426,
            "value": 245,
            "visibility": null,
            "history": "1M"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>





