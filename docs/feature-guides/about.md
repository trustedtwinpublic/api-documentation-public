# About

This section contains guides on selected Trusted Twin features.

<TableWrapper>
| Guide                                               | Contents                                                                                                               |
|:----------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------|
| [Custom headers](../feature-guides/working-with-custom-headers.md)  | Learn how to enhance the rule functionality through variables passed by the user in a request to the Trusted Twin API. |
| [Rules (Entry visibility)](../feature-guides/working-with-rules.md) | Learn how to manage visibility of data from the resource perspective on the example of Ledger Entries.                   |
| [Rules (Twin rule)](../feature-guides/working-with-twin-rules.md)   | Learn how to manage visibility of data from the user perspective on the example of the `"twin"` rule in a role.              |
</TableWrapper>
