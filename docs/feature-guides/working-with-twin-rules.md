---
tags:
  - rules
  - access rules
---

# Rules (Twin visibility)

## About this guide

This guide describes how to use the `"twin"` [rule](../overview/rules.md) in role to control access to Twins. It is divided into the following sections:

<TableWrapper>
| Section                     | Contents |
|:----------------------------|:---------|
| [Rules](../feature-guides/working-with-twin-rules.md#rules)             | Introduction to [rules](../overview/rules.md) on the Trusted Twin platform. |
| [Preparation](../feature-guides/working-with-twin-rules.md#preparation) | In this section we create an account, a role, a user and a few Twins. |
| [Scenarios](../feature-guides/working-with-twin-rules.md#scenarios)     | Example scenarios illustrating the use of the `"twin"` rule. | 
| [Resources](../feature-guides/working-with-twin-rules.md#resources)     | Further resources related to this guide. |
</TableWrapper>

In this guide we use cURL to provide examples. For more information about cURL, please see our [cURL quickstart guide](../quickstart-guides/curl-quickstart-guide.md#about-curl).

## Rules 

A [rule](../overview/rules.md) on the Trusted Twin platform is used to manage visibility and access of a user to a resource or to define conditions triggering a [notification](../reference/notifications/index.md) event. Rules follow the [Rule Engine Syntax](https://zerosteiner.github.io/rule-engine/syntax.html).

Rules are used in [roles](../reference/roles/index.md), [Identities](../reference/identities/index.md), and [Ledger](../reference/ledgers/index.md) Entries. Rules in a role control the access of a user to Twins, Identities and Ledger Entries. They are held in the `"rules"` attribute of a role.    
In this guide, we are going to focus on the `"twin"` rule in a role.

### Visibility

The visibility of data on the Trusted Twin platform can be managed from two perspectives:
1. Resource perspective: the visibility of an Identity or a Ledger Entry.
2. User perspective: Twin, Identity and Ledger Entry.

#### Resource perspective

The resource perspective is designed to manage visibility in relation to users belonging to other Trusted Twin accounts. The resource visibility rules are used each time a user belonging to a different account tries to access a resource owned by another account.     
     
Please note that the resource visibility rules do not apply to users accessing resources owned by their account. All resources owned by the account are visible to all of its users.

#### User perspective

User perspective was designed to manage visibility of resources on the user level. Visibility rules defined in the user role control the access of the user to Twins (Twin rule), Identities (Identity rule) or Entries in a Ledger (Entry rule). Rules applied from the user perspective apply to all resources, including resources owned by the user’s own account.

Visibility rules are defined for each role you create (see [create_user_role](../reference/roles/create-user-role.md)). They can be modified when updating a role (see [update_user_role](../reference/roles/update-user-role.md)).

## Scenarios

We are going to go through various scenarios where a different visibility rules is required for a Ledger Entry:

<TableWrapper>
| Scenario         | Rule                                             | Description                                                                                                                                                                     |
|------------------|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Scenario 1](./working-with-twin-rules#scenario-1) | `"twin": null`                            | User with the role can get all Twins within the account they belong to.                                                                                                         |
| [Scenario 2](./working-with-twin-rules#scenario-2) | `"twin": "twin == 'b5d81fb3-9c66-4a19-8433-e763eec3f573'"`                           | User with the role can access only the Twin with the given Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) within the account hey belong to.            |
| [Scenario 3](./working-with-twin-rules#scenario-3) | `"visibility": "account in [{account},{account}]"`          | User with the role can only get a Twin with "department" value in the description field of the Twin equal to "department" value in the description field of the user.    |
| [Scenario 4](./working-with-twin-rules#scenario-4) | `"visibility": "account in [{account},{account}]"` | User with the role can only create a Twin with "department" value in the description field of the Twin equal to "department" value in the description field of the user. |
</TableWrapper>

## Preparation 

### Account 

For the purpose of this guide, we create a Trusted Twin account (account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `012623f3-1a18-487d-8dd2-9c26863a02f0`). For more information about how to create an account, please see the [Account creation](../account-and-access/account-creation.md) section.

### Role

On this account (account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `012623f3-1a18-487d-8dd2-9c26863a02f0`), we create a role (role [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7`) with permissions to perform the `"get_twin"`, `"get_twin_ledger_entry"`, and `"get_twin_identity"` actions.    
Please note that the `"twin"` rule in the `"rules"` section is currently set to its default value `null`. This means that there are no restrictions for the user with the role regarding the access to Twins. When it comes to Twins created in the account (account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `012623f3-1a18-487d-8dd2-9c26863a02f0`), the user with this role will be able to see all Twins owned by this account.


<ExtendedCodeGroup title="Role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "name": "read access twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": null,
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin",
            "get_twin_ledger_entry",
            "get_twin_identity"
        ]
    },
    "created_ts": 1675686462.256,
    "updated_ts": 1675686462.256
}
```
</CodeBlock>
</ExtendedCodeGroup>

### User

Next, we create a user with this role.

<ExtendedCodeGroup title="User" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "f7aa6f2d-a5f2-44a9-b24b-2cc5a039e552",
    "name": "User 1 twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "role": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "created_ts": 1675686534.675,
    "updated_ts": 1675686534.675
}
```
</CodeBlock>
</ExtendedCodeGroup>

We create as well a User Secret (API key) for this user.

### Twins

<ExtendedCodeGroup title="Twin A" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694358.707,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin A",
        "department": "finance"
    },
    "creation_certificate": {
        "uuid": "b5d81fb3-9c66-4a19-8433-e763eec3f573",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694358.707
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Twin B" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694622.629,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin B",
        "department": "sales"
    },
    "creation_certificate": {
        "uuid": "e2c0e7ff-e439-470a-8d6e-982137d25189",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694622.629
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

## Scenario 1 

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"twin": null` | User with the role can access all Twins within the account they belong to. |
</TableWrapper>

Let's check whether we can see the Twins with as the user with the role we created. 

### Role

<ExtendedCodeGroup title="Role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "name": "read access twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": null,
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin",
            "get_twin_ledger_entry",
            "get_twin_identity"
        ]
    },
    "created_ts": 1675686462.256,
    "updated_ts": 1675686462.256
}
```
</CodeBlock>
</ExtendedCodeGroup>


### Requests

In order to retrieve the Twins, we send requests to the [get_twin](../reference/twins/get-twin.md) endpoint to retrieve the respective Twins based on their Twin [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids).

<ExtendedCodeGroup title="Request to get Twin A" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/b5d81fb3-9c66-4a19-8433-e763eec3f573' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request to get Twin B" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/e2c0e7ff-e439-470a-8d6e-982137d25189' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>


### Responses

<div class="row">
<div class="column">

<ExtendedCodeGroup title="Response Twin A" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694358.707,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin A",
        "department": "finance"
    },
    "creation_certificate": {
        "uuid": "b5d81fb3-9c66-4a19-8433-e763eec3f573",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694358.707
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Response Twin B" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694622.629,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin B",
        "department": "sales"
    },
    "creation_certificate": {
        "uuid": "e2c0e7ff-e439-470a-8d6e-982137d25189",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694622.629
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 2 

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"twin": "twin == 'b5d81fb3-9c66-4a19-8433-e763eec3f573'"` | User with the role can access only the Twin with the given Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) within the account hey belong to. |
</TableWrapper>

### Role update

<ExtendedCodeGroup title="Updated role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "name": "read access twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": "twins == 'b5d81fb3-9c66-4a19-8433-e763eec3f573'",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin",
            "get_twin_ledger_entry",
            "get_twin_identity"
        ]
    },
    "created_ts": 1675686462.256,
    "updated_ts": 1675686952.11
}
```
</CodeBlock>
</ExtendedCodeGroup>


### Requests

In order to retrieve the Twins, we send a request to the [get_twin](../reference/twins/get-twin.md) endpoint.

<ExtendedCodeGroup title="Request to get Twin A" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/b5d81fb3-9c66-4a19-8433-e763eec3f573' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request to get Twin B" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/e2c0e7ff-e439-470a-8d6e-982137d25189' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

<div class="row">
<div class="column">

<ExtendedCodeGroup title="Response Twin A" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694358.707,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin A",
        "department": "finance"
    },
    "creation_certificate": {
        "uuid": "b5d81fb3-9c66-4a19-8433-e763eec3f573",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694358.707
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Response Twin B" copyable>
<CodeBlock title="json">

```json
{
    "subcode": 401000,
    "trace": "0fb1a296-aba7-493e-a99b-3b586dd0a3c4",
    "description": "Twin is not accessible.",
    "help": "https://trustedtwin.com/docs/introduction/errors.html#error-subcodes"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 3

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"twin": "TWIN.department == USER.department"` | User with the role can only get a Twin with "department" value in the description field of the Twin equal to "department" value in the description field of the user. |
</TableWrapper>

### Role and user

<ExtendedCodeGroup title="Updated role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "name": "read access twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": "TWIN.department == USER.department",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_identity",
            "get_twin_ledger_entry",
            "get_twin"
        ]
    },
    "created_ts": 1675686462.256,
    "updated_ts": 1675695033.345
}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Updated user" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "f7aa6f2d-a5f2-44a9-b24b-2cc5a039e552",
    "name": "User 1 twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "role": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "description": {
        "department": "sales"
    },
    "created_ts": 1675686534.675,
    "updated_ts": 1675695374.857
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Requests

In order to retrieve the Twins, we send requests to the [get_twin](../reference/twins/get-twin.md) endpoint.

<ExtendedCodeGroup title="Request to get Twin A" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/b5d81fb3-9c66-4a19-8433-e763eec3f573' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request to get Twin B" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/e2c0e7ff-e439-470a-8d6e-982137d25189' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

<div class="row">
<div class="column">

<ExtendedCodeGroup title="Response Twin A" copyable>
<CodeBlock title="json">

```json
{
    "subcode": 401000,
    "trace": "cba784fd-166d-417c-a7f4-d8ba3c2df3ac",
    "description": "Twin is not accessible.",
    "help": "https://trustedtwin.com/docs/introduction/errors.html#error-subcodes"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Response Twin B" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675694622.629,
    "description": {
        "purpose": "Trusted Twin rule guide - Twin rules",
        "name": "Twin B",
        "department": "sales"
    },
    "creation_certificate": {
        "uuid": "e2c0e7ff-e439-470a-8d6e-982137d25189",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675694622.629
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 4

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"twin": "TWIN.department == USER.department"` | User with the role can only create a Twin with "department" value in the description field of the Twin equal to "department" value in the description field of the user.  |
</TableWrapper>

<ExtendedCodeGroup title="Updated role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "name": "create twins rule guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": "TWIN.department == USER.department",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_ledger_entry",
            "get_twin",
            "get_twin_identity",
            "create_twin"
        ]
    },
    "created_ts": 1675686462.256,
    "updated_ts": 1675695808.584
}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Updated user" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "f7aa6f2d-a5f2-44a9-b24b-2cc5a039e552",
    "name": "User 1 twins rules guide",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "role": "6b7ee989-e02d-4cbc-9a2d-ab405bb1b9b7",
    "description": {
        "department": "sales"
    },
    "created_ts": 1675686534.675,
    "updated_ts": 1675695374.857
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Requests

In order to retrieve the Twins, we send a request to the [create_twin](../reference/twins/create-twin.md) endpoint.

<ExtendedCodeGroup title="Request to create a Twin (no description field)" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>


<ExtendedCodeGroup title="Request to create a Twin (department - finance in description field)" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{ "description": {
    "department": "finance"
}}'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request to create a Twin (department - sales in description field)" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{ "description": {
    "department": "finance"
}}'
```
</CodeBlock>
</ExtendedCodeGroup>


### Response

The user can only create a Twin if a description field contains `"department": "sales"`. The user cannot create a Twin if the `"description"` field of the Twin is empty or if `"department"` has a different value than `"sales"`.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="Response to create a Twin (no description field)" copyable>
<CodeBlock title="cURL">

```json
{
    "subcode": 400897,
    "trace": "b3c340a5-b45b-473f-8e35-77581b014208",
    "description": "You cannot create a Twin not permitted by the Twin rule defined in your users Role.",
    "help": "https://trustedtwin.com/docs/introduction/errors.html#error-subcodes"
}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request to create a Twin (department - finance in description field)" copyable>
<CodeBlock title="cURL">

```json
{
    "subcode": 400897,
    "trace": "b3c340a5-b45b-473f-8e35-77581b014208",
    "description": "You cannot create a Twin not permitted by the Twin rule defined in your users Role.",
    "help": "https://trustedtwin.com/docs/introduction/errors.html#error-subcodes"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request to create a Twin (department - sales in description field)" copyable>
<CodeBlock title="cURL">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1675696528.994,
    "description": {
        "department": "sales"
    },
    "creation_certificate": {
        "uuid": "ef78f693-45e0-41fa-91a2-52f02a886c52",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1675696528.994
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>







