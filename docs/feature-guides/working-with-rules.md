---
tags:
  - rules
  - access rules
---

# Rules (Entry visibility)

## About this guide

This guide describes how to use [rules](../overview/rules.md) to set visibility of operational data shared between partners belonging to different organizations who work together on a joint business process.

The guide contains the following sections:

<TableWrapper>
| Section                     | Contents                                                                                                                                              |
|:----------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Rules](../feature-guides/working-with-rules.md#rules)             | Introduction to [rules](../overview/rules.md) on the Trusted Twin platform and how they work from resources' and from users' perspectives.        |
| [Preparation](../feature-guides/working-with-rules.md#preparation) | In this section we create accounts, roles, users, Twins, and Ledgers. We will use these objects to illustrate how access rules work. |
| [Scenarios](../feature-guides/working-with-rules.md#scenarios)     | List of scenarios we illustrate the use of rules on visibility of Entries of a Ledger.                                                                | 
| [Resources](../feature-guides/working-with-rules.md#resources)     | Resources related to access rules.                                                                                                            |
</TableWrapper>

In this guide we use cURL to provide examples. For more information about cURL, please see our [cURL quickstart guide](../quickstart-guides/curl-quickstart-guide.md#about-curl).

## Rules

### Digital Twin, data sharing and visibility

On the Trusted Twin platform you can build data-sharing solutions by using the [digital twin concept](../introduction/digital-twin-concept.md). A digital twin ties together data coming from partners from different organizations working together on a joint business process. Hence, a digital twin is a shared object.     
     
Within a digital twin, a [Twin](../reference/twins/index.md) is the main object as it ties together all the data in a digital twin. It has the following characteristics:
- A Twin object is visible to all accounts on the Trusted Twin platform as long as the user knows the unique identifier of the Twin (Twin's [UUID](../overview/trusted-twin-api.md#trusted-twin-ids)) and if a `"twin"` rule in a [role](../reference/roles/index.md) doesn't prevent the user from seeing the Twin.
- A Twin contains a description field. This field is public - it is visible to all accounts on the platform if they know the unique identifier of the Twin (Twin's [UUID](../overview/trusted-twin-api.md#trusted-twin-ids)) and if a `"twin"` rule in a [role](../reference/roles/index.md) doesn't prevent the calling user from seeing the Twin.
- A Twin is initially owned by the account that created the Twin. The owner of a Twin can change the description field of the Twin. The ownership of a Twin can be transferred.      
    
Each account on the Trusted Twin platform can attach data to a Twin. These data can have the form of a Ledger (JSON document containing Entries), Identities, or a Docs.      
      
As there are many stakeholders from different organizations involved in a joint business process, they will require different permissions (or no permission at all) to view data attached to a shared object by different users. This is where we use [rules](../overview/rules.md) to set up visibility of the data in a flexible and secure manner. 

### Visibility

The visibility of data on the Trusted Twin platform can be managed from two perspectives:
1. Resource perspective: the visibility of an Identity and Ledger Entry.
2. User perspective: Twin, Identity and Ledger Entry.

In both cases, visibility is defined as a [rule](../overview/rules.md) that follows the [Rule Engine Syntax](https://zerosteiner.github.io/rule-engine/syntax.html).
     
In what regards [Docs](../reference/docs/index.md), a Doc can be downloaded from the Trusted Twin platform by anyone who knows the unique link to download the Doc.

#### Resource perspective

The resource perspective is designed to manage visibility in relation to users belonging to other Trusted Twin accounts. The resource visibility rules are used each time a user belonging to a different account tries to access a resource owned by another account.     
     
Please note that the resource visibility rules do not apply to users accessing resources owned by their account. All resources owned by the account are visible to all of its users.

Resource visibility rules are defined each time you create an Identity (see [create_twin_identity](../reference/identities/create-twin-identity.md)) or Entry in the Ledger (see [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md)).

#### User perspective

User perspective was designed to manage visibility of resources on the user level. Visibility rules defined in the user role controls if the given user can access a requested Twin (Twin rule) or a requested Entry in the Ledger (Entry rule). Unlike rules applied from a resource perspective (see section above), rules applied from the user perspective apply to all resources, including resources owned by the user’s account.

User visibility rules are defined for each user role you create (see [create_user_role](../reference/roles/create-user-role.md)).

## Preparation

### Account

For the purpose of this guide, we created a Trusted Twin account (account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `012623f3-1a18-487d-8dd2-9c26863a02f0`). For more information about how to create an account, please go to the [Account creation](../account-and-access/account-creation.md) section.  

### Role
     
On this account (account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `012623f3-1a18-487d-8dd2-9c26863a02f0`), we created a role (role [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `955d30d5-b97d-4040-8585-f2fb157032df`) with permissions to perform the `"get_twin"`, `"get_twin_ledger_entry"`, and `"get_twin_identity"` actions.

<ExtendedCodeGroup title="Role" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "955d30d5-b97d-4040-8585-f2fb157032df",
    "name": "read access limited objects",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "rules": {
        "twin": null,
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_ledger_entry",
            "get_twin_identity",
            "get_twin"
        ]
    },
    "created_ts": 1660296338.373,
    "updated_ts": 1660296338.373
}
```
</CodeBlock>
</ExtendedCodeGroup>

<br/>

### Users

Next, we created a user with this role - user 1 with the user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `f51b2a52-80ed-4170-8598-2929fc628c87`).

<ExtendedCodeGroup title="User 1" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "f51b2a52-80ed-4170-8598-2929fc628c87",
    "name": "User 1",
    "account": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "role": "955d30d5-b97d-4040-8585-f2fb157032df",
    "created_ts": 1660296412.058,
    "updated_ts": 1660296412.058
}
```
</CodeBlock>
</ExtendedCodeGroup>

We created a User Secret for user 1 (`ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q`).

<br/>

### Twin

We created a Twin with the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `34bbd862-6fe7-4f9b-b90b-d5aba9a68046`.

<ExtendedCodeGroup title="Twin" copyable>
<CodeBlock title="json">

```json
{
    "owner": "012623f3-1a18-487d-8dd2-9c26863a02f0",
    "status": "alive",
    "updated_ts": 1660649617.111,
    "description": {
        "purpose": "TT Rule guide"
    },
    "creation_certificate": {
        "uuid": "34bbd862-6fe7-4f9b-b90b-d5aba9a68046",
        "creator": "012623f3-1a18-487d-8dd2-9c26863a02f0",
        "created_ts": 1660297112.604
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Ledger

The Ledger has one Entry where "visibility" is set to null. This means that the Entry is visible to all users belonging to the account where the Entry was created.

<ExtendedCodeGroup title="Ledger" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

<br/>

### Account 2

In order to illustrate the visibility of Entries for users of other accounts, we created another account (`"account 2"`) with account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `7321ed94-28df-4eec-ad54-4de7c7f3b416`.    

### Role and user account 2
     
On this account, we created a role. The role is allowed to perform the `"get_twin"`, `"get_twin_ledger_entry"`, and `"get_twin_identity"` actions and a user with this role (`"user 2"`) with user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `8a616133-29ca-492a-9497-585c2f174018`.

<ExtendedCodeGroup title="User 2" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "8a616133-29ca-492a-9497-585c2f174018",
    "name": "User 2",
    "account": "7321ed94-28df-4eec-ad54-4de7c7f3b416",
    "role": "e79f4b68-f87d-4aad-9bd3-0db526e4d386",
    "created_ts": 1660650474.922,
    "updated_ts": 1660650474.922
}
```
</CodeBlock>
</ExtendedCodeGroup>


We created a User Secret for user 2 (`YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h`).

## Scenarios

We are going to go through various scenarios where a different visibility rules is required for a Ledger Entry:

<TableWrapper>
| Scenario         | Rule                                             | Description                                                                 |
|------------------|--------------------------------------------------|-----------------------------------------------------------------------------|
| [Scenario 1](./working-with-rules#scenario-1) | `"visibility": null`                         | Entry is visible only to users belonging to account that created the Entry. |
| [Scenario 2](./working-with-rules#scenario-2) | `"visibility": "true"`                           | Entry is visible to all accounts.                                           |
| [Scenario 3](./working-with-rules#scenario-3) | `"visibility": "account == '{account}'"`           | Entry is visible to account with the UUID specified and to the account that created the Entry.                     |
| [Scenario 4](./working-with-rules#scenario-4) | `"visibility": "account in [{account},{account}]"` | Entry is visible to the accounts specified on the list and to the account that created the Entry.                                                                            |
| [Scenario 5](./working-with-rules#scenario-5) | `"visibility": "user == '{user}'"`                 | Entry is visible to user with the UUID specified and to the account that created the Entry.                                                                             |
| [Scenario 6](./working-with-rules#scenario-6) | `"visibility": "account in LEDGER.entry_name"`   |   Entry is visible to user on a list containing account UUIDs stored in an Entry.                                                                          |
| [Scenario 7](./working-with-rules#scenario-7) | `"LEDGER[USER.city].status == 'opened'"`         | Entry is visible to user with specified description key equal to Entry name and Entry value stores mapping with specified value.                                                                            |
</TableWrapper>

## Scenario 1

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": null`   | Entry is visible only to users belonging to account that created the Entry. |
</TableWrapper>

### Entry 

In this scenario, we are going to show what users of different accounts can when the `"visibility"` parameter in a Ledger Entry is set to `null`. Please note that if you don't include the `"visibility"` parameter in the request body of an Entry when adding the Entry to a Ledger, the `"visibility"` value will be by default set to `null`. 

<ExtendedCodeGroup title="Entry 1" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's check what each of the users can see when retrieving the Entries of the Ledger.

In order to retrieve Entries of a Ledger, we need to send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint. As user 1 belongs to the account where we created the Entry, we send a request to retrieve Entries from our `personal` Ledger. User 2 does not belong to the account where we created the Entry. Therefore, we need to add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries in the request.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

The response returns the details of `"entry1"` for user 1. It is because when `"visibility"` is set to `null` for an Entry, the Entry is visible for all users belonging to the account where the Entry was created.      
     
The response for user 2 does not return `"entry1"`. The reason for this is that user 2 belongs to a different account than account with the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"012623f3-1a18-487d-8dd2-9c26863a02f0"` and an Entry where `"visibility"` is set to `null` is only visible to users from the account where an Entry was created.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {}
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 2

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": "true"` | Entry is visible to all accounts. |
</TableWrapper>

### Entry

In this scenario, we are going to show what users belonging to different accounts can see when an Entry has the `"visibility"` parameter set to `"true"`. When `"visibility"` is set to `"true"`, the Entry should be visible to all accounts, as long as they know what Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) and account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) they need to include in the request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint.

<ExtendedCodeGroup title="Entry 2" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

Both user 1 and user 2 can see `"entry2"`. This is because the `"visibility"` of the Entry is set to `"true"`. It makes it visible to any account.      
     
Please note that user 2 cannot see the `"visibility"` attribute as this attribute in an Entry is only visible to users belonging to the account where the Entry was created.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 3

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": "account == '{account}'"` | Entry is visible to account with the UUID specified and to the account that created the Entry. |
</TableWrapper>

### Entry

In this scenario, we are going to show how to set up `"visibility"` so that the Entry is visible to users of a foreign account of our choice. We can set up a [rule](../overview/rules.md) with the `account` variable (for a list of available variables please see [Rule variables](../overview/rules.md#rule-variables)) and the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the foreign account (in our case it is `"7321ed94-28df-4eec-ad54-4de7c7f3b416"` as it is the account that user 2 belongs to).

<ExtendedCodeGroup title="Entry 3" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

Both users can see `"entry3"`:
- User 1 can see `"entry3"` because the Entry was created in the account they belong to. 
- User 2 can see `"entry3"` because the visibility rule is set up to make the Entry visible for account with the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"7321ed94-28df-4eec-ad54-4de7c7f3b416'"`. It is the account that user 2 belongs to. Please note that user 2 cannot see the `"visibility"` attribute of `"entry3"` as this attribute in an Entry is only visible to users belonging to the account where the Entry was created.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 4

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": "account in [{account},{account}]"` | Entry is visible to the accounts specified on the list and to the account that created the Entry. |
</TableWrapper>

### Entry

In this scenario, we want the Entry to be visible to users of a various foreign accounts of our choice. We can set up a [rule](../overview/rules.md) with the `account` variable (for a list of available variables please see [Rule variables](../overview/rules.md#rule-variables)) and a list with [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids) of the foreign accounts (in our case it is `"7321ed94-28df-4eec-ad54-4de7c7f3b416"` as it is the account that user 2 belongs to, and an account with the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"7321ed94-28df-4eec-ad54-4de7c7f3b415"`).

<ExtendedCodeGroup title="Entry 4" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

As we can see, both users can see `"entry4"`:
- User 1 can see `"entry4"` because the Entry was created in the account they belong to, and 
- User 2 can see `"entry4"` because the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"7321ed94-28df-4eec-ad54-4de7c7f3b416'"` of the account they belong to is included in the list of accounts in our visibility [Rule](../overview/rules.md). Please note that user 2 cannot see the `"visibility"` attribute of can see `"entry4"` as this attribute in an Entry is only visible to users belonging to the account where the Entry was created.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 5

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": "user == '{user}'"` | Entry is visible to user with the UUID specified and to the account that created the Entry.  |
</TableWrapper>

### Entry

In this scenario, we want the Entry to be visible to a user of our choice who belongs to a foreign account (in our case it will be user 2 wih the user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"8a616133-29ca-492a-9497-585c2f174018"`).     
      
We can set up a [rule](../overview/rules.md) with the `user` variable (for a list of available variables please see [Rule variables](../overview/rules.md#rule-variables)) and the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the user of the foreign account (in our case it is `"78a616133-29ca-492a-9497-585c2f174018"` ).

<ExtendedCodeGroup title="Entry 5" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234,
            "visibility": "user == '8a616133-29ca-492a-9497-585c2f174018'"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>


Both Users can see `"entry5"`:
- User 1 can see `"entry5"` because the Entry was created in the account they belong to. 
- User 2 can see `"entry5"` because the visibility rule is set up to make the Entry visible for user with the user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) `"78a616133-29ca-492a-9497-585c2f174018"`. It is the user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of user 2. Please note that user 2 cannot see the `"visibility"` attribute of can see `"entry5"` as this attribute in an Entry is only visible to users belonging to the account where the Entry was created.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234,
            "visibility": "user == '8a616133-29ca-492a-9497-585c2f174018'"
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Scenario 6

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"visibility": "account in LEDGER.entry_name"` | Entry is visible to user on a list containing account UUIDs stored in an Entry. |
</TableWrapper>

### Entry

In this scenario, we want the Entry to be visible to a user who is on a list containing account [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids) stored in the Entry with the name "contactors":     

<ExtendedCodeGroup title="Entry `contractors`" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "contractors": {
            "entry_created_ts": 1661346996.171,
            "entry_updated_ts": 1661346996.171,
            "value_changed_ts": 1661346996.171,
            "value": [
                "7321ed94-28df-4eec-ad54-4de7c7f3b416",
                "7321ed94-28df-4eec-ad54-4de7c7f3b415"
            ],
            "visibility": null
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

We can set up a [rule](../overview/rules.md) with the `account` variable (for a list of available variables please see [Rule variables](../overview/rules.md#rule-variables)) where the Entry will be only visible to foreign Accounts if the requesting user's account is stored in "contractors" Entry (`account in LEDGER.contractors`):

<ExtendedCodeGroup title="Entry 6" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345,
            "visibility": "account in LEDGER.contractors"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

Both users can see `"entry6"`:
- User 1 can see `"entry6"` because the Entry was created in the account they belong to. 
- User 2 can see `"entry6"` because the visibility rule is set up to make the Entry visible for users belonging to the accounts listed in the Entry `"contractors"`. Please note that user 2 cannot see the `"visibility"` attribute of can see `"entry6"` as this attribute in an Entry is only visible to users belonging to the account where the Entry was created. 

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        },
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345,
            "visibility": "account in LEDGER.contractors"
        },
        "contractors": {
            "entry_created_ts": 1661346996.171,
            "entry_updated_ts": 1661346996.171,
            "value_changed_ts": 1661346996.171,
            "value": [
                "7321ed94-28df-4eec-ad54-4de7c7f3b416",
                "7321ed94-28df-4eec-ad54-4de7c7f3b415"
            ],
            "visibility": null
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234,
            "visibility": "user == '8a616133-29ca-492a-9497-585c2f174018'"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        },
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>


## Scenario 7

<TableWrapper>
| Rule | Description |
|------|-------------|
| `"LEDGER[USER.city].status == 'opened'"` | Entry is visible to user with specified description key equal to Entry name and Entry value stores mapping with specified value.  |
</TableWrapper>

### Entry

In this scenario, we want the Entry to be visible to a user who has a city in their description. The city is equal to a name of an Entry in the Ledger. As well, we want the Entry to be visible only if the Entry value stores a mapping containing `"status"` field where the value is equal to `"opened"`.

Let's start by updating our user ([update_user](../reference/users/update-user.md)) and adding a description field to user 2 containing `"city": "gdansk"`:

<ExtendedCodeGroup title="User 2 (updated)" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "8a616133-29ca-492a-9497-585c2f174018",
    "name": "User 2",
    "account": "7321ed94-28df-4eec-ad54-4de7c7f3b416",
    "role": "e79f4b68-f87d-4aad-9bd3-0db526e4d386",
    "description": {
        "city": "gdansk"
    },
    "created_ts": 1660650474.922,
    "updated_ts": 1661353614.403
}
```
</CodeBlock>
</ExtendedCodeGroup>

Next, let's create an Entry `"gdansk"` with status `"opened"`:

<ExtendedCodeGroup title="Entry 'gdansk' status 'opened'" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "gdansk": {
            "entry_created_ts": 1661355094.037,
            "entry_updated_ts": 1661355094.037,
            "value_changed_ts": 1661355094.037,
            "value": {
                "status": "opened"
            },
            "visibility": "LEDGER[USER.city].status == 'opened'"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

### Request

Let's send a request to the [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) endpoint as user 1 and user 2:
- For user 1, we retrieve the Entries from their `personal` Ledger as the Entries were created in the account that user 1 belongs to.
- For user 2, we add the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account where we created the Entries (`012623f3-1a18-487d-8dd2-9c26863a02f0`) in the request as user 2 does not belong to the account where we created the Entries.

<ExtendedCodeGroup title="User 1 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/personal' \
--header 'Authorization: ig/M0rK3yIjWKJ2bYjeQEgPgkGZ/Z89WrvMubfX9niBHhH4GSrx/3JgX9Xs+sM+Q'
```
</CodeBlock>
</ExtendedCodeGroup>


<ExtendedCodeGroup title="User 2 request" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/34bbd862-6fe7-4f9b-b90b-d5aba9a68046/ledgers/012623f3-1a18-487d-8dd2-9c26863a02f0' \
--header 'Authorization: YyqUuOt3IPPlRbqW5Ctp3YgNo+/RWQGhaUYSI2qB3jlRyQe9gSqsYw/QUohG3C1h'
```
</CodeBlock>
</ExtendedCodeGroup>

### Response

Both users can see the Entry `"gdansk"`:
- User 1 can see the Entry `"gdansk"` because the Entry was created in the account they belong to. 
- User 2 can see the Entry `"gdansk"` because they have a description with a city (`"city": "gdansk"`), there is an Entry in the Ledger with the name `"gdansk"` and the `"status"` of the Entry is `"opened"` (`"status": "opened"`).

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        },
        "contractors": {
            "entry_created_ts": 1661346996.171,
            "entry_updated_ts": 1661346996.171,
            "value_changed_ts": 1661346996.171,
            "value": [
                "7321ed94-28df-4eec-ad54-4de7c7f3b416",
                "7321ed94-28df-4eec-ad54-4de7c7f3b415"
            ],
            "visibility": null
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        },
        "gdansk": {
            "entry_created_ts": 1661355094.037,
            "entry_updated_ts": 1661355094.037,
            "value_changed_ts": 1661355094.037,
            "value": {
                "status": "opened"
            },
            "visibility": "LEDGER[USER.city].status == 'opened'"
        },
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345,
            "visibility": "account in LEDGER.contractors"
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234,
            "visibility": "user == '8a616133-29ca-492a-9497-585c2f174018'"
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789
        },
        "gdansk": {
            "entry_created_ts": 1661355094.037,
            "entry_updated_ts": 1661355094.037,
            "value_changed_ts": 1661355094.037,
            "value": {
                "status": "opened"
            }
        },
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Entry update

Let's update the Entry `"gdansk"` so that the status is `"closed"`:

<ExtendedCodeGroup title="Entry 'gdansk' status 'closed'" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "gdansk": {
            "entry_created_ts": 1661355094.037,
            "entry_updated_ts": 1661436568.334,
            "value_changed_ts": 1661436568.334,
            "value": "closed",
            "visibility": "LEDGER[USER.city].status == 'opened'"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

### Response

Only user 1 can see the Entry `"gdansk"`:
- User 1 can see the Entry `"gdansk"` because the Entry was created in the account they belong to. 
- User 2 cannot see the Entry `"gdansk"`. Although they have a description with a city (`"city": "gdansk"`) and there is an Entry in the Ledger with the name `"gdansk"`, because the `"status"` of the Entry is `"closed"` (`"status": "closed"`) resolves to `False`.

<div class="row">
<div class="column">

<ExtendedCodeGroup title="User 1 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789,
            "visibility": "account in ['7321ed94-28df-4eec-ad54-4de7c7f3b416','7321ed94-28df-4eec-ad54-4de7c7f3b415']"
        },
        "contractors": {
            "entry_created_ts": 1661346996.171,
            "entry_updated_ts": 1661346996.171,
            "value_changed_ts": 1661346996.171,
            "value": [
                "7321ed94-28df-4eec-ad54-4de7c7f3b416",
                "7321ed94-28df-4eec-ad54-4de7c7f3b415"
            ],
            "visibility": null
        },
        "entry1": {
            "entry_created_ts": 1660909406.061,
            "entry_updated_ts": 1660909406.061,
            "value_changed_ts": 1660909406.061,
            "value": 123,
            "visibility": null
        },
        "gdansk": {
            "entry_created_ts": 1661355094.037,
            "entry_updated_ts": 1661436568.334,
            "value_changed_ts": 1661436568.334,
            "value": "closed",
            "visibility": "LEDGER[USER.city].status == 'opened'"
        },
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345,
            "visibility": "account in LEDGER.contractors"
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789,
            "visibility": "account == '7321ed94-28df-4eec-ad54-4de7c7f3b416'"
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234,
            "visibility": "user == '8a616133-29ca-492a-9497-585c2f174018'"
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456,
            "visibility": "true"
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="User 2 response" copyable>
<CodeBlock title="json">

```json
{
    "entries": {
        "entry4": {
            "entry_created_ts": 1660911746.356,
            "entry_updated_ts": 1660911746.356,
            "value_changed_ts": 1660911746.356,
            "value": 789
        },
        "entry6": {
            "entry_created_ts": 1661347329.582,
            "entry_updated_ts": 1661347329.582,
            "value_changed_ts": 1661347329.582,
            "value": 12345
        },
        "entry3": {
            "entry_created_ts": 1660911543.531,
            "entry_updated_ts": 1660911543.531,
            "value_changed_ts": 1660911543.531,
            "value": 789
        },
        "entry5": {
            "entry_created_ts": 1660912031.774,
            "entry_updated_ts": 1660912031.774,
            "value_changed_ts": 1660912031.774,
            "value": 234
        },
        "entry2": {
            "entry_created_ts": 1660911090.826,
            "entry_updated_ts": 1660911090.826,
            "value_changed_ts": 1660911090.826,
            "value": 456
        }
    }
}

```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Resources

The [rules](../overview/rules.md) section contains references to the [Rule syntax](../overview/rules.md#rule-syntax) and a comprehensive list of [Rule variables](../overview/rules.md#rule-variables) available on the Trusted Twin platform for each of the resources.     
     
To learn more about the objects used in this guide please go to the respective section:
- [Twin](../reference/twins/index.md)
- [Identity](../reference/identities/index.md)
- [Ledger](../reference/ledgers/index.md)
- [User](../reference/users/index.md)
- [Role](../reference/roles/index.md)
- [User Secret](../reference/secrets/index.md)


