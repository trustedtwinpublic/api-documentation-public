---
tags:
  - custom header
  - custom headers
  - X-TrustedTwin
  - guide
---

# Custom headers

## About this guide

Follow this guide to learn how to use the `X-TrustedTwin` custom header to enhance the [rule](../overview/rules.md) functionality through using variables passed by the user in the request.

The guide contains the following sections:

<TableWrapper>
| Section | Contents |
|:--------|:------|
| [`X-TrustedTwin` custom header](../feature-guides/working-with-custom-headers.md#x-trustedtwin-custom-header)        |Introduction to the `X-TrustedTwin` custom header. |
|[Introduction](../feature-guides/working-with-custom-headers.md#about-the-scenario) | Context and goals of the scenario. |
| [Preparation](../feature-guides/working-with-custom-headers.md#preparation) | In this section we create a role, a user, and a Twin. We will use these objects to illustrate how the `X-TrustedTwin` custom header works. | 
| [Scenarios](../feature-guides/working-with-custom-headers.md#scenarios) | Example scenarios using the `X-TrustedTwin` header. | 
</TableWrapper>

In this guide we use cURL to provide examples. For more information about cURL, please see our [cURL quickstart guide](../quickstart-guides/curl-quickstart-guide.md#about-curl).

## `X-TrustedTwin` custom header

The `X-TrustedTwin` header enhances the [rule](../overview/rules.md) functionality through using variables passed by the user in the request to the Trusted Twin API. To learn more about the `X-TrustedTwin` custom header, please consult the [Custom headers](../account-and-access/custom-headers.md#x-trustedtwin) section.

### About the scenarios

In our scenarios, we want a user only to be able to access a Twin if the Twin serial number in the description of a Twin is equal to the serial number passed in the `X-TrustedTwin` header in the request. This way there is no need to create multiple users (one per Twin) to allow for this level of access granularity.

### Preparation

First, let's create a role. The role should allow to access a given Twin only if the serial number passed in the `X-TrustedTwin` header (`DICT.serial`) is equal to the serial number of the Twin (`TWIN.serial`):

<ExtendedCodeGroup title="Role details" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "53f86dd3-5ede-4fc1-b1c9-af0a152a774d",
    "name": "Role 1",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "rules": {
        "twin": "TWIN.serial == DICT.serial",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_identity",
            "get_twin",
            "get_twin_identities",
            "get_twin_ledger_entry",
            "get_user_role",
            "get_user"
        ]
    },
    "created_ts": 1663159648.653,
    "updated_ts": 1663159648.653
}
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

Next, let's create a user with this role:

<ExtendedCodeGroup title="User details" copyable>
<CodeBlock title="json">

```json
{
    "uuid": "e0d93032-0614-48b8-a1a5-b9fb99a87ac2",
    "name": "User 1",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "role": "53f86dd3-5ede-4fc1-b1c9-af0a152a774d",
    "created_ts": 1663159890.314,
    "updated_ts": 1663159890.314
}
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

And let's create a Twin with the serial number 123 (`{"serial": "123"}`):

<ExtendedCodeGroup title="Twin with {'serial': '123'}" copyable>
<CodeBlock title="json">

```json
{
    "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "status": "alive",
    "updated_ts": 1663146075.642,
    "description": {
        "serial": "123"
    },
    "creation_certificate": {
        "uuid": "9ad5dab6-63ec-46e4-bb98-61c2a7ddd411",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1663146075.642
    }
}
```

</CodeBlock>
</ExtendedCodeGroup>

### Scenarios

Let's test what the user can see depending on the `X-TrustedTwin` header passed in the request to retrieve the Twin.

#### No `X-TrustedTwin` header in the request

Let's start with a request where we don't pass the `X-TrustedTwin` header:

<ExtendedCodeGroup title="Request without `X-TrustedTwin` header" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/9ad5dab6-63ec-46e4-bb98-61c2a7ddd411' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>
</ExtendedCodeGroup>

<br/>

The response returns a 403 status code and information that the Twin is not accessible:

<ExtendedCodeGroup title="Response" copyable>
<CodeBlock title="json">

```json
{
    "subcode": 401000,
    "trace": "a24ef17a-d7a7-4dfb-84a9-b79d6f74f56c",
    "description": "Twin is not accessible.",
    "help": "https://trustedtwin.com/docs/overview/errors.html#error-subcodes"
}
```
</CodeBlock>
</ExtendedCodeGroup>

<br/>

#### Request with `X-TrustedTwin` header `{"serial": "123"}`

Next, let's pass the encoded `{"serial": "123"}` dictionary in the `X-TrustedTwin` header (`eyJzZXJpYWwiOiAiMTIzIn0=`):

<ExtendedCodeGroup title="Request with `X-TrustedTwin: eyJzZXJpYWwiOiAiMTIzIn0=` header" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/9ad5dab6-63ec-46e4-bb98-61c2a7ddd411' \
--header 'X-TrustedTwin: eyJzZXJpYWwiOiAiMTIzIn0=' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

The response returns the details of the Twin:

<ExtendedCodeGroup title="Response" copyable>
<CodeBlock title="json">

```json
{
    "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "status": "alive",
    "updated_ts": 1663146075.642,
    "description": {
        "serial": "123"
    },
    "creation_certificate": {
        "uuid": "9ad5dab6-63ec-46e4-bb98-61c2a7ddd411",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1663146075.642
    }
}
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

#### Request with `X-TrustedTwin` header `{"serial": "345"}`

Next, let's try to pass the encoded `{"serial": "345"}` dictionary in the `X-TrustedTwin` header (`eyJzZXJpYWwiOiAiMzQ1In0=`):

<ExtendedCodeGroup title="Request with `X-TrustedTwin: eyJzZXJpYWwiOiAiMzQ1In0=` header" copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/9ad5dab6-63ec-46e4-bb98-61c2a7ddd411' \
--header 'X-TrustedTwin: eyJzZXJpYWwiOiAiMzQ1In0=' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

The response returns a 403 status code with information that the Twin is not accessible to the user:

<ExtendedCodeGroup title="Response" copyable>
<CodeBlock title="json">

```json
{
    "subcode": 401000,
    "trace": "a00ec673-6b9e-4103-ade4-e00b2c575659",
    "description": "Twin is not accessible.",
    "help": "https://trustedtwin.com/docs/overview/errors.html#error-subcodes"
}
```

</CodeBlock>
</ExtendedCodeGroup>

## Resources

Please consult the [Custom headers](../account-and-access/custom-headers.md) section for more information on the `X-TrustedTwin` custom header.

