---
tags:
  - digital twin
  - data sharing
  - data collaboration
  - shared object
  - intro
  - introduction
---

# Digital twin concept

## Introduction

A digital twin is a virtual representation of a real or an abstract object (e.g., an item, a device, a system, a process, an organization, a service, or a person). It exists independently among cooperating partners in a form of shared knowledge. It does not belong solely to a single partner or organization. Therefore, a digital twin cannot be stored in a single partner's or organization's infrastructure. It must exist in the cloud.

The Trusted Twin platform is a developer platform that allows you to create and share digital twins (shared objects). It acts as an exchange layer in the technology stack, so that knowledge storing and sharing is decentralized, flexible, and secure.

## Digital twin

A digital twin is dynamically created by all [users](../reference/users/index.md) involved in the process:
- Each user can attach their data (in form of [Identities](../reference/identities/index.md), [Ledgers](../reference/ledgers/index.md), and [Docs](../reference/docs/index.md)) to the digital twin and retain control over these data.
- Each user has access to data of other users attached to the digital twin to an extent granted by users who own the data.
- Each user can create any number of [Identities](../reference/identities/index.md) and decide about their visibility. Identities can be resolved to Twin [UUIDs](../overview/trusted-twin-api.md#trusted-twin-ids).

On the Trusted Twin platform, a single digital twin consists of:
- A [Twin](../reference/twins/index.md) (main object of the Trusted Twin platform, many per account). It ties together knowledge coming from different accounts through [Ledgers](../reference/ledgers/index.md), [Identities](../reference/identities/index.md), and [Docs](../reference/docs/index.md).
- [Ledgers](../reference/ledgers/index.md) (one per Twin per account). It is a JSON document storing information about the state of the Twin. Ledgers are dictionaries that store Entries (key-value pairs).
- [Identities](../reference/identities/index.md) (many per Twin per account). Identities are user-defined unique identifiers of Twins. Identities can be private or public.
- [Docs](../reference/docs/index.md) (many per Twin per account). Each Doc is as static file of any type (text, video, audio etc.) and size.

### Advanced digital twin services

The Trusted Twin offers platform offers advanced services:

- The [History](../reference/history/index.md) service lets you store historical values of [Ledger](../reference/ledgers/index.md) Entries.
- The database services let you aggregate and view digital twins in form of a relational SQL database:
  - [Timeseries](../reference/timeseries/index.md) tables (many per account to store digital twin history).
  - [Indexes](../reference/indexes/index.md) tables (many per account to store digital twin states).
- You can automatically invoke external services via publish/notify service (e.g., webhooks) by using the [Notifications](../reference/notifications/index.md) service.
- You can use the [Stickers (workflow tags) service](../reference/stickers/index.md) to facilitate workflow processes.

Please see the [Advanced services](./advanced-services.md) section for more information.

## Twin 

A [Twin](../reference/twins/index.md) is the main object of the Trusted Twin platform as it aggregates knowledge coming from different accounts. It ties together knowledge from different users and accounts through Identities, Ledgers, and Docs attached by different users.

A Twin is created by a single user - the creator and initial owner of the account. The ownership of a Twin can be transferred.

## Identity

Each Twin on the Trusted Twin platform has a unique system ID (Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids)). It is generated upon creation of a Twin. This unique system ID is used to access a given Twin via REST API.   
   
However, you might need other user-defined IDs relevant for your business to identify a Twin. Such IDs could be registration numbers, RFID tags, or a product serial numbers. You can use [Identities](../reference/identities/index.md) to identify Twins with such IDs. 
An Identity is created by a user in the context of an account and a Twin, and it must be unique within this context. This means that the same Identities can be attached to multiple Twins, and that they can be used to group Twins. 

Among many operations that you can perform on Identities, the [resolve_twin_identity](../reference/identities/resolve-twin-identity.md) operation is of great importance as it provides a list of Twins to which a given Identity is attached.

## Ledger

A [Ledger](../reference/ledgers/index.md) is a JSON document. It stores information about the state of a Twin. It is a dictionary that stores Entries (key-value pairs). There is exactly one Ledger for an account-Twin pair. A Ledger is independent and separated from other Ledgers owned by different accounts.

Each Entry stores a single value of JSON type (a number, a string, a list, an object). The value can be provided directly by a call to the respective API endpoint, or it can reference a value of a different Ledger. Such `"ref"` (reference) type values are guaranteed to reflect changes of the value to which we provide a reference.

The visibility of a Ledger can be private or public. It depends on the visibility of Entries in the Ledger:
- If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are visible only to users of the account that owns the Ledger. If all Entries of a Ledger are private, the Ledger is private.
- If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger. They are also visible to users of other accounts if the visibility [rule](../overview/rules.md#rules) for a given Entry evaluates to `True`. A visibility rule can be set independently for each Entry. If any of the Entries of a Ledger is public, the Ledger is public.  

If you need to store historic values of Entries, you can use the `"history"` attribute. You can retrieve the history of Ledger Entries at any time through the [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md) endpoint. To learn more, see the [History service](../reference/history/index.md) section.

If the [Timeseries](../reference/timeseries/index.md) service is enabled for your account, an Entry can also have a `"timeseries"` attribute. The `"timeseries"` attribute allows you to create structured and multidimensional views of digital twin history. The views are updated automatically each time an Entry is changed for a measurement.

An Entry can also have a `"publish"` attribute. It allows you to set up [notifications](../reference/notifications/index.md) to automatically invoke external services each time a Ledger Entry is changed.

## Doc

A [Doc](../reference/docs/index.md) stores static content of any type (text, audio, video files etc.). Docs are attached to Twins. A single Doc can be attached to multiple Twins.




