---
tags:
  - advanced services
---

# Advanced services

The Trusted Twin platform offers a series of advanced services. 

<TableWrapper>
| Advanced service                                          | Description                                                  |
|:----------------------------------------------------------|:-------------------------------------------------------------|
| [History](../introduction/advanced-services.md#history)                               | The History service stores historical values of Ledger Entries.                   |
| [Databases](../introduction/advanced-services.md#databases)                                       | The Databases service lets you create structured and multidimensional views in for of user-defined tables - [Timeseries](../introduction/advanced-services.md#timeseries) and [Indexes](../introduction/advanced-services.md#indexes).  | 
| [Timeseries](../introduction/advanced-services.md#timeseries)                                 | The Timeseries database service stores structured and multidimensional views of Twin history. | 
| [Indexes](../introduction/advanced-services.md#indexes)                                       | The Indexes database service stores structured and multidimensional views of Twin states.  | 
| [Notifications (publish/notify)](../introduction/advanced-services.md#notifications)           | The Notifications service allows to automatically invoke external services via publish/notify service (e.g., webhooks).                                                  | 
| [Stickers (workflow tags)](../introduction/advanced-services.md#stickers)  | The Stickers (workflow tags) service facilitates workflow processes.                                                            |
</TableWrapper>

## History

The [History](../reference/history/index.md) service lets you store historical values of Entries of a [Ledger](../reference/ledgers/index.md) with a timestamp denoting when the value was changed. 

The historical values, as well as the current value of the Entry, can be viewed at any time by sending a request to the [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md) endpoint. The response returns a list of Entries with the history of Entry value changes in form of key-value pairs:

<ExtendedCodeGroup title="Example History response" copyable>

<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "1651074230.191": 20.9,
            "1651074132.101": 20.9,
            "1651074123.54": 21,
            "1651074114.147": 19.2,
            "1651074109.225": 19.1,
            "1651073498.69": 15
        },
        "krakow": {
            "1651074230.191": 21,
            "1651073498.69": 21
        },
        "wroclaw": {
            "1651073715.03": 21,
            "1651073498.69": 21
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

## Databases

The Databases service on the Trusted Twin platform allows you to store structured and multidimensional views of Twin history in form of user-defined tables ([Timeseries](../introduction/advanced-services.md#timeseries) service) as well as to store structured and multidimensional views of Twin states in form of user-defined tables ([Indexes](../introduction/advanced-services.md#indexes) service).

You can have one or multiple databases for a single Trusted Twin account.


## Timeseries

A [Timeseries](../reference/timeseries/index.md) database on the Trusted Twin platform stores structured and multidimensional views of Twin history in form of user-defined tables. A Timeseries database consists of one or more tables.

Below you can find an example of a Timeseries table. It contains a timestamp column (added automatically by the system), one dimension ("city") and three measurements ("temperature", "ozone", "relative_humidity"):

A Timeseries table is automatically updated each time an Entry with a `"timeseries"` attribute for the given Timeseries table is being changed.<br/>

The Timeseries database service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.


<TableWrapper>
| _timestamp                 | city    | temperature | ozone | relative_humidity |
|:---------------------------|:--------|:------------|:------|:------------------|
| 21 Apr 2022 2022, 15:00:00 | Gdańsk  | 12          | 29    | 55                |
| 21 Apr 2022 2022, 15:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 16:00:00 | Gdańsk  | 13          | 30    | 56                |
| 21 Apr 2022 2022, 16:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 17:00:00 | Gdańsk  | 12          | 29    | 54                |
| 21 Apr 2022 2022, 17:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 18:00:00 | Gdańsk  | 10          | 30    | 54                |
| 21 Apr 2022 2022, 18:00:00 | Wrocław | 12          | 29    | 55                |
</TableWrapper>

## Indexes

[Indexes](../reference/indexes/index.md)* allow you to efficiently access Ledger records and are especially helpful if you manage a large number of Twins within your account. The Indexes database on the Trusted Twin platform stores structured and multidimensional views of Twin states in form of user-defined tables. An Indexes database consists of one or more tables. An Index can occur only once in a given Twin.   

Below you can find an example of an Indexes table. It contains a Twin column (stating the [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin where the index occurs), a timestamp column stating when the index was last changed (added automatically by the system), and three properties (`"gdansk_temperature"`, `"gdansk_ozone"`, `"gdansk_relative_humidity"`).<br/>

The Indexes service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.

<TableWrapper>
| _twin                                | _timestamp            | gdansk_temperature | gdansk_ozone | gdansk_relative_humidity |
|:-------------------------------------|:----------------------|:-------------------|:-------------|:-------------------------|
| ca12b80e-045a-4c93-9e9b-2fc407fc5f2f | 18 May 2022, 11:36:39 | 21                 | 29           | 55                       |
| b27289ba-da39-4232-a9d7-7bef299d548c | 18 May 2022, 12:00:00 | 20                 | 29           | 56                       |
</TableWrapper>

## Notifications

[Notifications](../reference/notifications/index.md) allow to automatically invoke external services via publish/notify service (e.g., webhooks). Notifications can be triggered by the following events:
- creation of a [Ledger Entry](../reference/ledgers/index.md) of a given name,
- change of value of a Ledger Entry,
- deletion of a Ledger Entry,
- creation of a [Sticker](../reference/stickers/index.md) (workflow tag),
- removal of a Sticker (workflow tag),
- expiration of a Sticker (workflow tag).


<ExtendedCodeGroup title="Example notification" copyable>
<CodeBlock title="json">

```json
{
    "type": "Notification",
    "message": "c919d1b3-6d95-4883-9c7f-6879f575e045",
    "twin": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    "event": {
        "entry_1": {
            "new_value": 45,
            "old_value": 43,
            "changed_ts": 1656599237.497
        }
    },
    "sent_ts": 1656599238.407,
    "subscription": {
        "topic": "value-increase",
        "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "validity_ts": 1656667772.152,
        "unsubscribe_url": "https://rest.trustedtwin.com/notifications/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=Nzk1YTY3MjQtNTg4Ny00NWVjLTlhNjktYjg4ZmU3OWI4MjU5X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdGVzdF9zdWJfNl90ZXN0X19fYWI4ZmZhMTkxOWE0MjU5ZDcyYmViYzc5YjRlMjQ4ZjU=",
        "refresh_url": "https://rest.trustedtwin.com/notifications/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=Nzk1YTY3MjQtNTg4Ny00NWVjLTlhNjktYjg4ZmU3OWI4MjU5X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdGVzdF9zdWJfNl90ZXN0X19fYWI4ZmZhMTkxOWE0MjU5ZDcyYmViYzc5YjRlMjQ4ZjU="
    }
}
```

</CodeBlock>
</ExtendedCodeGroup>

## Stickers

[Stickers](../reference/stickers/index.md) (workflow tags) are a functionality designed to facilitate workflow processes. They support passing ownership of workflow items (e.g., tasks to be done) between multiple users.

<ExtendedCodeGroup title="Example stickers" copyable>
<CodeBlock title="json">

```json
{
    "stickers": {
        "red": {
            "note": "This is an urgent request to replace parts of the device.",
            "recipients": [
                "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
            ],
            "validity_ts": 1665382457,
            "created_ts": 1664973830.463
        },
        "yellow": {
            "note": "Please examine the device.",
            "recipients": [
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts": 1667632457,
            "created_ts": 1664973830.463
        }
    }
}
```

</CodeBlock>
</ExtendedCodeGroup>


