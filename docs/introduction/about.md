# Welcome!

Welcome to the Trusted Twin developer documentation.     
     
Explore the Trusted Twin API the way it works best for you - whether you prefer to begin with a [Postman quickstart guide](../quickstart-guides/postman-quickstart-guide.md) or jump straight into an [API Reference section with code examples](../reference/twins/index.md) in your preferred programming language, we've got you covered! 

## Introduction

<div class="row">
<div class="column">

Learn about the digital twin concept and digital twin advanced services available on the platform. 

</div>
<div class="column">


[Digital twin concept](./digital-twin-concept.md)<br/>[Advanced services](./advanced-services.md)                                                      


</div>
</div>

## API Changelog

<div class="row">
<div class="column">

The Trusted Twin API changelog lists changes to the Trusted Twin API.

</div>
<div class="column">

[Versions](../api-changelog/api-changelog.md)                                                    


</div>
</div>

## Overview

<div class="row">
<div class="column">

Overview of the Trusted Twin API.

</div>
<div class="column">

[Trusted Twin API](../overview/trusted-twin-api.md)<br/>[API reference index](../overview/api-reference-index.md)<br/>[Rules](../overview/rules.md)<br/>[Templates](../overview/templates.md)<br/>[Status codes](../overview/status-codes.md)<br/>[Log messages](../overview/log-messages.md)  

</div>
</div>



## Account & access

<div class="row">
<div class="column">

In this section you will learn:<br/>- how to create a Trusted Twin account,<br/>- how to authenticate on the Trusted Twin platform,<br/>- how to use optional custom HTTP headers in requests,<br/>- how to access the Trusted Twin [Indexes](../reference/indexes/index.md) and [Timeseries](../reference/timeseries/index.md) databases.

</div>
<div class="column">

[Account creation](../account-and-access/account-creation.md)<br/>[Users and roles](../account-and-access/users-and-roles.md)<br/>[Authentication (API key)](../account-and-access/authentication.md)<br/>[Custom headers](../account-and-access/custom-headers.md)<br/>[Database services access](../account-and-access/database-services-access.md) 

</div>
</div>

## Accounting

<div class="row">
<div class="column">

This section describes the accounting functionalities of the Trusted Twin platform. 

</div>
<div class="column">

[User Activity Log](../accounting/user-activity-log.md)<br/>[Resource Access Log](../accounting/resource-access-log.md)   

</div>
</div>

## Reference

<div class="row">
<div class="column">

Consult the Trusted Twin API reference containing resource descriptions, endpoints, methods, parameters and sample requests in various programming languages and sample responses. 

</div>
<div class="column">

[Account](../reference/account/index.md)<br/>[Batches](../reference/batches/index.md)<br/>[Cache](../reference/cache/index.md)<br/>[Databases](../reference/databases/index.md)<br/>[Docs](../reference/docs/index.md)<br/>[History](../reference/history/index.md)<br/>[Identities](../reference/identities/index.md)<br/>[Indexes](../reference/indexes/index.md)<br/>[Ledgers](../reference/ledgers/index.md)<br/>[Log](../reference/log/index.md)<br/>[Notifications](../reference/notifications/index.md)<br/>[Roles](../reference/roles/index.md)<br/>[Stickers](../reference/stickers/index.md)<br/>[Timeseries](../reference/timeseries/index.md)<br/>[Trace](../reference/trace/index.md)<br/>[Twins](../reference/twins/index.md)<br/>[Usage](../reference/usage/index.md)<br/>[Users](../reference/users/index.md)<br/>[User Secrets (API keys)](../reference/secrets/index.md)<br/>[User Tokens](../reference/token/index.md)<br/>[Who am I](../reference/whoami/index.md)

</div>
</div>

## Feature guides

<div class="row">
<div class="column">

Learn more about selected Trusted Twin features with our guides. 

</div>
<div class="column">
 
[Custom headers](../feature-guides/working-with-custom-headers.md)<br/>[Rules (Entry visibility)](../feature-guides/working-with-rules.md)<br/>[Rules (Twin rule)](../feature-guides/working-with-twin-rules.md)<br/>                                    

</div>
</div>

## Customer Data Hub

<div class="row">
<div class="column">

Best practices related to the Customer Data Hub.

</div>
<div class="column">

[Identities](../customer-data-hub/identities.md)

</div>
</div>

## Libraries

<div class="row">
<div class="column">

Check out our SDKs in Python and JavaScript (with a sample JavaScript client application).

</div>
<div class="column">


[Python](../libraries/library-python.md)<br/>[JavaScript](../libraries/library-js.md) 

</div>
</div>

## Tools

<div class="row">
<div class="column">

Explore Python tools developed for the Trusted Twin platform.

</div>
<div class="column">

[Log monitor](../tools/log_monitor.md)<br/>[Ping](../tools/ping.md)<br/>[Usage monitor](../tools/usage_monitor.md) 

</div>
</div>

## Quickstart guides

<div class="row">
<div class="column">

Our quickstart guides let you quickly get started on the Trusted Twin platform and play with the Trusted Twin API.

</div>
<div class="column">

[cURL quickstart guide](../quickstart-guides/curl-quickstart-guide.md)<br/>[Postman quickstart guide](../quickstart-guides/postman-quickstart-guide.md)<br/> 

</div>
</div>

## Tutorials

<div class="row">
<div class="column">

Explore our functionalities in depth on examples that will inspire you how to implement the Trusted Twin features for your business.

</div>
<div class="column">

[Create a digital twin of a meter](../tutorials/python-meter-tutorial.md)<br/> [Stickers (workflow tags) and notifications](../tutorials/stickers-notifications-tutorial.md) 

</div>
</div>



If you have any questions, please email <hello@trustedtwin.com>. Thank you!
