# Webhook subscribe

<div class="row">
<div class="column">

This endpoint creates a webhook subscription for a given topic.

</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*                                                          |
|:-------|:-----|:--------------------------------------------------------------------|
| POST    | /notifications/webhooks | [webhook_subscribe](./subscribe-to-a-webhook.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform the "webhook_subscribe" operation, the "webhook_subscribe" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                       | Type                     | In   | Description                                                                                                                                                                      |
|:--------------------------------|:-------------------------|:-----|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **callback_url**<br/>*required* | string                   | body | Target URL to which messages are to be delivered. It can have maximum 512 characters.                                                                                                                             |
| **topic**<br/>*required*        | string                   | body | Name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`.                             |
|**client_secret** <br/> *optional** | string | body |  Client secret provided for the purpose of calculating a notification hash to be used for hash verification. It must be a URL-safe-Base64 compatible 16 character long string. |
| **expires** <br/>*optional***    | integer, `DEFAULT=604800` | body | Time in seconds when the notification subscription expires counted from the time of the creation of the subscription. The maximum and default value is 604800 seconds (7 days). | 
</TableWrapper>

::: footnote *
The `"client_secret"` parameter is optional and does not need to be included in the request body when refreshing subscriptions. If it is not included in the request body, the hash is not calculated and a `server_secret` is not be returned. 
:::

::: footnote **
The `"expires"` parameter is optional and does not need to be included in the request body when creating subscriptions. If it is not included in the request body, its default value (`DEFAULT=604800`) is used. 
:::

<ExtendedCodeGroup title="Example request body" switchable copyable>
<CodeBlock title="json">
```json
{
        "callback_url": "https://examplecallbackurl.com/",
        "topic": "value-increase",
        "client_secret": "1234abcd1234ABCD",
        "expires": 86400
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/notifications/webhooks' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
        "callback_url": "https://examplecallbackurl.com/",
        "topic": "value-increase",
        "client_secret": "1234abcd1234ABCD",
        "expires": 86400
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

subscribe_body = {
        "callback_url": "https://examplecallbackurl.com/",
        "topic": "value-increase",
        "client_secret": "1234abcd1234ABCD",
        "expires": 86400
}

status, response = TT_SERVICE.webhook_subscribe(body=subscribe_body)
```

</CodeBlock>

<CodeBlock title="Node.js">

```js
import { WebhooksApi, Configuration } from "@trustedtwin/js-client";

var webhooksApi = new WebhooksApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const response = await webhooksApi.webhookSubscribe({
  webhookSubscribe: {
    callbackUrl: "https://examplecallbackurl.com/",
    topic: "value-increase",
    client_secret: "1234abcd1234ABCD",
    expires: 86400
  },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-----------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **callback_url** | string     | Target URL to which messages are to be delivered. It can have maximum 512 characters.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 
| **subscription** | dictionary | <details> <ExtendedSummary markdown="span"> Subscription details. </ExtendedSummary> <TableWrapper>  <table>  <thead> <tr> <th>Attribute</th> <th>Type</th> <th> Description </th> </tr> </thead> <tbody> <tr> <td>**topic**</td> <td>string</td> <td> Name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. </td> </tr> <tr> <td>**account**</td> <td>string</td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). </td> </tr> <tr> <td>**validity_ts**</td> <td>timestamp</td> <td> Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr><tr> <td>**server_secret**</td> <td>string</td> <td> URL-safe-Base64 compatible 16-character long string to be used in the hash verification process. </td> </tr> </tbody> </table> </TableWrapper> </details> |
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "callback_url": "https://examplecallbackurl.com/",
    "subscription": {
        "topic": "value-increase",
        "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "validity_ts": 1656755614.921,
        "server_secret": "ZS617eLXl8_bRpOV"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Confirmation message

The confirmation message is sent to the provided callback URL.

<div class="row">
<div class="column">

<TableWrapper>
| Attribute            | Type                                                | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:---------------------|:----------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **type**             | string                                              | Message type. For messages generating a `"confirmation_url"` value should be `"Confirmation"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 
| **message**          | string                                              | Message [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | 
| **sent_ts**          | timestamp                                           | Time at which the message was sent. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 
| **confirmation_url** | string                                              | URL to which a GET request needs to be sent (see [confirm_a_webhook_subscription](./confirm-a-webhook-subscription) in order to confirm the subscription.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **subscription**     | dictionary                                          | <details> <ExtendedSummary markdown="span"> Subscription details. </ExtendedSummary> <TableWrapper>  <table>  <thead> <tr> <th>Attribute</th> <th>Type</th> <th> Description </th> </tr> </thead> <tbody> <tr> <td>**topic**</td> <td>string</td> <td> Name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. </td> </tr> <tr> <td>**account**</td> <td>string</td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). </td> </tr> <tr> <td>**validity_ts**</td> <td>timestamp</td> <td> Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody> </table> </TableWrapper> </details> |
| **hash** | string | Message hash. Please see the [Hash verification](./hash-verification.md) section for more details. | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example message" copyable>
<CodeBlock title="json">
```json
{
    "type": "Confirmation",
    "message": "bf552416-d85e-4591-a3b2-bc467e630e16",
    "sent_ts": 1656669215.435,
    "confirmation_url": "https://rest.trustedtwin.com/notifications/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=MjMzNjQxMmYzN2ZiNjg3ZjVkNTFlNmUyNDI1ZGFjYmJhOGJkYjQzMTNmNWNjM2YwZTBkOGRjYTAwMTU2YjM5Yzc4MGVlMDA3MDcwZTNlOGEyN2NjNmFiNWYwMDk5NThkODA4ZjdhZWNiYjgyYWNiZDI3M2ZhZmE3MGE2ZTM2MmIyNjkzYjBmYWE4NzllZDdiZGJjYTE5MTkzNmIwNmYzMzQ2YjBlZGZkYjAyMTViYjNiYzNiYTgxNDE5YzMyZmUzYjNkZDA1NjE2MTY0ODhkZmE1YWFlZTE2ZjM3ZGYyNDc4MjEwYzU3MmVmN2JkZjdiMDJhMWUyNTNiY2VmNTlkNTljYzBiYjMyNGZkYmViYTBkMmRkOWY2MjUyODQ0MWFhMzQzNDg4ZmUwY2IxZTNjMjRkZGVkNDcyNWM5ZDhjMzRfX19kNjc3OTc2MC1lZmZhLTRkMmItYTk1NS0xZmI0NDI4YWYxMTdfX192YWx1ZV9pbmNyZWFzZV9fXzgwNTkwNzVmZWZiYjQ5YTgwOTZkOGNiNGY2NWM1NTYx",
    "subscription": {
        "topic": "value-increase",
        "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "validity_ts": 1656755614.921
    },
    "hash": "DUU2t7Ta+qv634kC0f18i9EuAczcRD1Lmwn764D32uw="
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
