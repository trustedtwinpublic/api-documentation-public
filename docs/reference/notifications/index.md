---
tags:
  - pub/sub
  - pub-sub
  - pubsub
  - publish-subscribe
  - publish subscribe
  - publishsubscribe
  - notarization
---

# Notifications

<div class="row">
<div class="column">

## About

Notifications on the Trusted Twin platform allow to automatically invoke external services via a publish/notify service (e.g., webhooks). 

Notifications can be set in the following objects:

- in [Ledgers](../ledgers/index.md) at the Entry level. They are set in the `"publish"` property of an Entry.
- in [Stickers](../stickers/index.md) at the Sticker level. They are set in the `"publish"` property of a Sticker.

The following events can trigger a notification:

<TableWrapper>
| Event triggering a notification       | Description                                 | 
|:-------------------------|:--------------------------------------------|
| `"ledger.entry.added"`   | Adding a new Entry to a Ledger.             | 
| `"ledger.entry.updated"` | Change to the value of a Ledger Entry.      |
| `"ledger.entry.deleted"`   | Removing an Entry from a Ledger.             |
| `"sticker.put"` | Putting a given Sticker on a given Twin.    |
| `"sticker.removed"` | Removing a given Sticker from a given Twin. |
| `"sticker.expired"` | A given Sticker expiring.                   |
</TableWrapper>

## Available operations

The following operations are available for the Notifications service:

<TableWrapper>
| Operation                                                           | Description                                                                           |
|:--------------------------------------------------------------------|:--------------------------------------------------------------------------------------|
| [webhook_subscribe](./webhook-subscribe.md)                    | Creates a webhook subscription.                                                       |
| [webhook_confirm_subscription](./webhook-confirm-subscription.md) | Confirms a webhook subscription.                                                      |
| [webhook_refresh_subscription](./webhook-refresh-subscription.md) | Refreshes the webhook subscription through updating the validity of the subscription. | 
| [webhook_unsubscribe](./webhook-unsubscribe.md)              | Removes a webhook subscription.                                                       |
</TableWrapper>

## Notification flow

Please see the [Notification flow](./notification-flow.md) section.

## Retry policy

The delivery of notification messages is retried five times within a 40 seconds period. There is no immediate retry attempt. The first retry attempt occurs at 3 seconds after the previous notification message delivery attempt.

## Subscription attributes

A subscription has the following attributes:

<TableWrapper>
| Attribute        | Description | 
|:-----------------|:-----------| 
| **callback_url** | Target URL where messages are to be delivered. |
| **subscription** | Subscription details:<br/>-`"topic"`: Name of the topic to which a subscription is created. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-_]{0,127}$`.<br/>- 'account': Account UUID.<br/>-`validity_ts`: Time at which the subscription expires. |
| **server_secret** | Random integer generated on the base of the `"client_secret"` provided in the request. It allows to generate a hash that the user can use for [hash verification](./hash-verification.md). If the `"client_secret"` parameter is not provided in the request, a `"server_secret"` is not r in the response. |
| **validity_ts** | Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). The `"validity_ts"` is defined by the `"expires"` parameter provided in the request. If no `"expires"` parameter is provided when creating a subscription, the `"validity_ts"` is defined as 604800 seconds (7 days) from the time when the subscription was created. The maximum `"validity_ts"` of a subscription is 604800 seconds (7 days). The `"validity_ts"` subscription can be updated through the [refresh_webhook_subscription](../notifications/refresh-a-webhook-subscription.md) endpoint. |
</TableWrapper>

  
## Confirmation message attributes

A confirmation message sent to the client server URL has the following attributes:

<TableWrapper>
| Attribute            | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|:---------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **type**             | Message type. The value for a confirmation message is `"Confirmation"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **message**          | Unique ID of the message in [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) format.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **sent_ts**          | Timestamp denoting when the message was sent. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **confirmation_url** | URL to be used in a GET request ([confirm_webhook_subscription](./confirm-a-webhook-subscription.md)) to confirm the subscription.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **subscription**     | Subscription details:<br/> - `"topic"`: Name of the topic to which a subscription is created.<br/>- `"account"`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).<br/> - `"server_secret"`: Random integer generated on the base of the `"client_secret"` provided in the request. It allows to generate a hash which the user can use for [hash verification](./hash-verification.md). If the `"client_secret"` parameter is not provided in the request, a `"server_secret"` is not returned in the response. <br/> - `"validity_ts"`: Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). The `"validity_ts"` is defined by the `"expires"` parameter provided in the request. If no `"expires"` parameter is provided when creating a subscription, the `"validity_ts"` is defined as 604800 seconds (7 days) from the time when the subscription was created. The maximum `"validity_ts"` of a subscription is 604800 seconds (7 days). The `"validity_ts"` subscription can be updated through the [refresh_webhook_subscription](../notifications/refresh-a-webhook-subscription.md) endpoint. |
| **hash**             | Confirmation message hash. Used for [message verification](./hash-verification.md).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | 
</TableWrapper>


## Notification message attributes

A notification message sent to the client server URL has the following attributes:

<TableWrapper>
| Attribute        | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|:-----------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **type**         | Message type. The value for notification messages is `"Notification"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **trigger**      | Trigger of the notification event. The value can be `"ledger.entry.added"`, `"ledger.entry.deleted"` `"ledger.entry.updated"`, `"sticker.put"`, `"sticker.removed"` or `"sticker.expired"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| **message**      | Unique ID of the message in [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) format.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **twin**         | [Twin](../../reference/twins/index.md) [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **event**        | Details of the event which triggered the subscription. For a list of notification events and their attributes, please go to [Notification event attributes](./index.md#notification-event-attributes).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **sent_ts**      | Timestamp denoting when the message was sent. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 
| **subscription** | Subscription details:<br/>- `"topic"`: Name of the topic to which a subscription is created.<br/> - `"account"`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account where the notification was triggered by an event.<br/> -`"server_secret"`: random integer generated on the base of the `"client_secret"` provided in the request. It allows to generate a hash which the user can use for [hash verification](./hash-verification.md). If the `"client_secret"` parameter is not provided in the request, a `"server_secret"` is not returned in the response. <br/>- `"validity_ts"`: Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). The `"validity_ts"` is defined by the `"expires"` parameter provided in the request. If no `"expires"` parameter is provided when creating a subscription, the `"validity_ts"` is defined as 604800 seconds (7 days) from the time when the subscription was created. The maximum `"validity_ts"` of a subscription is 604800 seconds (7 days). The `"validity_ts"` subscription can be updated through the [refresh_webhook_subscription](../notifications/refresh-a-webhook-subscription.md) endpoint.<br/>- `"unsubscribe_url"`: URL to be used in a DELETE request ([webhook_unsubscribe](./unsubscribe-from-a-webhook.md)) to remove the subscription.<br/>- `"refresh_url"`: URL to be used in a PATCH request ([webhook_refresh_subscription](./refresh-a-webhook-subscription.md)) to refresh the subscription by updating the `"validity_ts"` of the subscription. | 
| **hash**         | Confirmation message hash. Used for [message verification](./hash-verification.md).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 
</TableWrapper>

### Notification event attributes (Ledger Entries)

#### `"ledger.entry.added"` 

<TableWrapper>
| Attribute | Description                          |
|:----------|:-------------------------------------|
| **new_value**          | New value of the Ledger Entry. |
| **old_value** | Previous value of the Ledger Entry. As the Entry was just created, the value is `null`. |
| **changed_ts** | Timestamp denoting when the Ledger Entry value was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | 
</TableWrapper>

#### `"ledger.entry.deleted"` 

<TableWrapper>
| Attribute | Description                          |
|:----------|:-------------------------------------|
| **new_value**          | New value of the Ledger Entry. As the Entry was deleted, the value is `null`. |
| **old_value** | Last value of the Ledger Entry.  |
| **changed_ts** | Timestamp denoting when the Ledger Entry value was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | 
</TableWrapper>

#### `"ledger.entry.updated"` 

<TableWrapper>
| Attribute      | Description                                                                                                                                                                                       |
|:---------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **new_value**  | New value of the Ledger Entry.                                                                                                                                                                    | 
| **old_value**  | Previous value of the Ledger Entry.                                                                                                                                                               | 
| **changed_ts** | Timestamp denoting when the Ledger Entry value was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | 
</TableWrapper>

### Notification event attributes (Stickers)
 

#### `"sticker.put"` 

<TableWrapper>
| Attribute       | Description                                                                                                                                                                                                                                                           |
|:----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **color**       | User-defined category of the Sticker.                                                                                                                                                                                                                                 |
| **note**     | Note for the recipients of the Sticker.                                                                                                                                                                                                                            |
| **validity_ts** | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                   |
| **created_ts**  | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                               |
| **put_by**      | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUID](../../overview/trusted-twin-api.md#) and user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user who put the Sticker on the Twin. | 
</TableWrapper>

#### `"sticker.removed"` 

<TableWrapper>
| Attribute | Description | 
|:-------|:-------|
| **color** | User-defined category of the Sticker. |
| **note** | Note for the recipients of the Sticker. |
| **validity_ts** | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **created_ts** | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **removed_by** | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user who removed the Sticker from the Twin. |
</TableWrapper>

#### `"sticker.expired"` 

A notification message is triggered up to 60 second from the time a Sticker has expired.

<TableWrapper>
| Attribute       | Description | 
|:----------------|:-----|
| **color**       | User-defined category of the Sticker. |
| **note**     | Note for the recipients of the Sticker. |
| **validity_ts** | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **created_ts**  | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | 
</TableWrapper>


</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*                                                          |
|:-------|:-----|:--------------------------------------------------------------------|
| POST    | /notifications/webhooks | [webhook_subscribe](./webhook-subscribe.md)                   |
| GET    | /notifications/webhooks/{account}?token={token} | [webhook_confirm_subscription](./webhook-confirm-subscription.md) |
| PATCH | /notifications/webhooks/{account}?token={token} | [webhook_refresh_subscription](./webhook-refresh-subscription.md) |
| DELETE | /notifications/webhooks/{account}?token={token} | [webhook_unsubscribe](./webhook-unsubscribe.md)              |
| PATCH    | /notifications/webhooks/access/accounts/{account} | [update_notifications_account_access](./update-notifications-account-access.md)|
| GET    | /notifications/webhooks/access | [get_notifications_access](./get-notifications-access.md)|
</EndpointsTable>

::: footnote *
In order for a user to perform the "webhook_subscribe", "update_notifications_account_access", and "get_notifications_access" operation, the "webhook_subscribe", "update_notifications_account_access", and "get_notifications_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). The "webhook_confirm_subscription", "webhook_refresh_subscription", and "webhook_unsubscribe" operations do not require authorization with a User Secret (API key).
:::


</div>
</div>
