# Update notifications account access

<div class="row">
<div class="column">

This endpoint grants or removes access to a given topic for a given foreign account.

</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*                                                          |
|:-------|:-----|:--------------------------------------------------------------------|
| PATCH    | /notifications/webhooks/access/accounts/{account} | [update_notifications_account_access](./update_notifications_account_access.md)|
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_notifications_account_access" operation, the "update_notifications_account_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type                                                | In   | Description                                                                                                                                                            |
|:----------------------------------|:----------------------------------------------------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **publish**<br/>*required**       | list of strings                                          | body | List of topics to which the given account should have access.                                                                                                                                     |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/notifications/webhooks/access/accounts/b48ef310-63af-4145-ae8b-859421671616' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "publish":[
        "topic-1",
        "topic-2"
    ]
}
'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

update_access_body = {
    "topics": ["topic-1", "topic-2"]
}

status, response = TT_SERVICE.update_notifications_account_access("b48ef310-63af-4145-ae8b-859421671616", body=update_access_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var webhooksApi = new WebhooksApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const roles = await webhooksApi.updateNotificationsAccountAccountAccess({
    account: "b48ef310-63af-4145-ae8b-859421671616c",
    updateNotificationsAccountAccess: {
        topics: ["topic-1", "topic-2"],
    },
});
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>


## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **topics**       | list of strings    |     List of subscription topics to which the given account has access.  |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "topics": ["topic-1", "topic-2"]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
