# Overview

## Entry notification

<div class="row">
<div class="column">

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-----------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **type**         | string     | Message type. For notification messages value should be `"Notification"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 
| **trigger** | string | Event triggering then Notification. In case of Ledger Entries it can be `"ledger.entry.added"', `"ledger.entry.updated"`, or `"ledger.entry"deleted"`. |
| **message**      | string     | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the notification message.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | 
| **twin**         | string     | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **event**        | dictionary | <details> <ExtendedSummary markdown="span"> List of Entries where an Entry update triggered the notification with details of the Entry update in form of key-value pairs. </ExtendedSummary> <TableWrapper><table> <thead> <tr> <th>Key</th>  <th>Value</th> </tr> </thead> <tr> <td>**new_value**</td>  <td> Value of the Entry after the update. </td> </tr> <tr> <td>**old_value**</td>  <td> Value of the Entry before the update. </td> </tr> <tr> <td>**changed_ts**</td>  <td> Time at which the value of the Entry was changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr>  </table></TableWrapper> </details>                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **sent_ts**      | timestamp  | Time at which the message was sent. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **subscription** | dictionary | <details> <ExtendedSummary markdown="span"> Subscription details. </ExtendedSummary> <TableWrapper>  <table>  <thead> <tr> <th>Attribute</th> <th>Type</th> <th> Description </th> </tr> </thead> <tbody> <tr> <td>**topic**</td> <td>string</td> <td> Name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. </td> </tr> <tr> <td>**account**</td> <td>string</td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). </td> </tr> <tr> <td>**validity_ts**</td> <td>timestamp</td> <td> Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody> </table> </TableWrapper> </details> |
| **hash** | string | Message hash. Please see the [Hash verification](./hash-verification.md) section for more details. | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example Entry notification message" copyable>
<CodeBlock title="json">
```json
{
    "type": "Notification",
    "trigger": "ledger.entry.updated",
    "message": "6f6be94d-c8bb-4a56-93d5-094cc6bc1c19",
    "twin": "6e309312-f54c-42e0-b95b-8f02d51577ed",
    "event": {
        "gdansk1": {
            "new_value": 42,
            "old_value": 15,
            "changed_ts": 1657016462.928
        }
    },
    "sent_ts": 1657016463.293,
    "subscription": {
        "topic": "value-increase",
        "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "validity_ts": 1657102675.737,
        "unsubscribe_url": "https://rest.trustedtwin.com/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=ZGU4YTYyNDktZGM0OC00M2NhLWFmZjMtZjQ5Y2Y1MmFjMzY0X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdmFsdWVfaW5jcmVhc2VfX19iZTY4YzQ2YjAzMGRkNTJhZmY5YmY0MGY1MjY1NjU4Mg==",
        "refresh_url": "https://rest.trustedtwin.com/notifications/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=ZGU4YTYyNDktZGM0OC00M2NhLWFmZjMtZjQ5Y2Y1MmFjMzY0X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdmFsdWVfaW5jcmVhc2VfX19iZTY4YzQ2YjAzMGRkNTJhZmY5YmY0MGY1MjY1NjU4Mg=="
    },
    "hash": "DUU2t7Ta+qv634kC0f18i9EuAczcRD1Lmwn764D32uw="
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Sticker notification

<div class="row">
<div class="column">

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-----------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **type**         | string     | Message type. For notification messages value should be `"Notification"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 
| **trigger** | string | Event triggering then Notification. In case of Stickers it can be `"sticker.put"', `"sticker.removed"', or `"sticker.expired"`. |
| **message**      | string     | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the notification message.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | 
| **twin**         | string     | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **event**        | dictionary | <details> <ExtendedSummary markdown="span"> Sticker details. </ExtendedSummary> <TableWrapper><table> <thead> <tr> <th>Key</th>  <th>Value</th> </tr> </thead> <tr> <td>**color**</td>  <td> Sticker color. </td> </tr> <tr> <td>**note**</td>  <td> Sticker note. </td> </tr> <tr> <td>**validity_ts**</td>  <td> Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr><tr> <td>**created_ts**</td>  <td> Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> <tr> <td>**put_by**</td>  <td> Dictionary containing the account, role and user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user who put the Sticker on the Twin. </td> </tr> </table></TableWrapper> </details>                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **sent_ts**      | timestamp  | Time at which the message was sent. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **subscription** | dictionary | <details> <ExtendedSummary markdown="span"> Subscription details. </ExtendedSummary> <TableWrapper>  <table>  <thead> <tr> <th>Attribute</th> <th>Type</th> <th> Description </th> </tr> </thead> <tbody> <tr> <td>**topic**</td> <td>string</td> <td> Name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. </td> </tr> <tr> <td>**account**</td> <td>string</td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account that subscribed to the topic. </td> </tr> <tr> <td>**validity_ts**</td> <td>timestamp</td> <td> Time at which the subscription expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody> </table> </TableWrapper> </details> |
| **hash** | string | Message hash. Please see the [Hash verification](./hash-verification.md) section for more details. | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example Sticker notification message" copyable>
<CodeBlock title="json">
```json
{
    "type": "Notification",
    "trigger": "sticker.put"
    "message": "6f6be94d-c8bb-4a56-93d5-094cc6bc1c19",
    "twin": "6e309312-f54c-42e0-b95b-8f02d51577ed",
    "event": {
        "color": "red",
        "note": "Please replace the device",
        "validity_ts": 1732191142.575,
        "created_ts": 1657016463.293,
        "put_by": {
            "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "role": "64120563-e835-461c-bf90-0d4c4b2d30ff",
            "user": "197c8027-5c41-4586-9e84-6fe06948a309"
        }
    },
    "sent_ts": 1657016463.293,
    "subscription": {
        "topic": "new-sticker",
        "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "validity_ts": 1657102675.737,
        "unsubscribe_url": "https://rest.trustedtwin.com/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=ZGU4YTYyNDktZGM0OC00M2NhLWFmZjMtZjQ5Y2Y1MmFjMzY0X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdmFsdWVfaW5jcmVhc2VfX19iZTY4YzQ2YjAzMGRkNTJhZmY5YmY0MGY1MjY1NjU4Mg==",
        "refresh_url": "https://rest.trustedtwin.com/notifications/webhooks/9891264d-4a77-4fa2-ae7f-84c9af14ae3b?token=ZGU4YTYyNDktZGM0OC00M2NhLWFmZjMtZjQ5Y2Y1MmFjMzY0X19fZDY3Nzk3NjAtZWZmYS00ZDJiLWE5NTUtMWZiNDQyOGFmMTE3X19fdmFsdWVfaW5jcmVhc2VfX19iZTY4YzQ2YjAzMGRkNTJhZmY5YmY0MGY1MjY1NjU4Mg=="
    },
    "hash": "DUU2t7Ta+qv634kC0f18i9EuAczcRD1Lmwn764D32uw="
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
