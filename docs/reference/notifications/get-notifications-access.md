# Get notifications access

<div class="row">
<div class="column">

This endpoint retrieves foreign accounts with access to topics in the account of the calling user. Each of the accounts in the response is returned with a list of topics to which a given account has access.

</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*                                                          |
|:-------|:-----|:--------------------------------------------------------------------|
| GET    | /notifications/webhooks/access | [get_notifications_access](./get-notifications-access.md)|
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_notifications_access" operation, the "get_notifications_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/notifications/webhooks/access' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_notifications_access()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var webhooksApi = new WebhooksApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const roles = await wbhooksApi.getNotificationsAccess();
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>


## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **accounts**       | dictionary    |  Key-value pairs:<br/> - key: Account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) of a foreign account that has access to subscription topics in your account.<br/> - value: Dictionary where the key is "topics" and the value is a list of subscription topics to which the given account has access. |

</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "accounts": {
        "f6589917-568b-439c-b6fd-22a042983209": {
            "topics": ["topic-1", "topic-2"]
        },
        "8c9f7283-b313-49bd-a925-8e51b1e5a1b5": {
            "topics": ["topic-1", "topic-a"]
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
