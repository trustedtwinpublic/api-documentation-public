---
tags:
  - notarization
---

# Hash verification

We use [blake2b](https://www.blake2.net/) to encode hashes of confirmation and notification messages. 

## Hash verification in Python

In order to verify a message by comparing hashes:

1. Construct a `_hash` object by calling the `"blake2b"` function. 

<ExtendedCodeGroup title="Construct _hash object" switchable copyable>
<CodeBlock title="Python">

```python
_hash = blake2b(digest_size=32, key=hash_seed)
```
</CodeBlock>
</ExtendedCodeGroup>

2. Pass the `"digest_size"` and `"key"` arguments to the `"blake2b"` function:
- `"digest_size"`: size of the resulting hash in bytes. It needs to be set to 32 bytes (`digest_size=32`).
- `"key"`: for keyed hashing. It is a concatenation of UTF-8 encoded `"client_secret"` (provided in the [webhook_subscribe](./webhook-subscribe.md) request) and UTF-8 encoded `"server_secret"` (returned in the response to the [webhook_subscribe](./webhook-subscribe.md) if the `"client_secret"` was provided in the request). In our example it is called `"hash_seed"`.

<ExtendedCodeGroup title="Construct hash_seed" switchable copyable>
<CodeBlock title="Python">

```python
client_secret = '1234567890abcdef'
server_secret = 'EbTWqhGPZU66deAS'

hash_seed = server_secret.encode('utf-8') + client_secret.encode('utf-8')
```
</CodeBlock>
</ExtendedCodeGroup>

3. UTF-8 encode message keys. The keys to be encoded depend on the message type. The [confirmation message](./index#confirmation-message-attributes) keys are as follows: 

<ExtendedCodeGroup title="Confirmation message keys" switchable copyable>
<CodeBlock title="Python">

```python
keys_list = ["type", "message", "sent_ts", "confirmation_url", "subscription"]
```
</CodeBlock>
</ExtendedCodeGroup>

The [notification message](./index#notification-message-attributes) keys are as follows:

<ExtendedCodeGroup title="Notification message keys" switchable copyable>
<CodeBlock title="Python">

```python
keys_list = ["type", "trigger", "message", "twins", "event", "sent_ts", "subscription"]
```
</CodeBlock>
</ExtendedCodeGroup>

For example, in case of a confirmation message the keys to be UTF-8 encoded are `"type"`, `"message"`, `"sent_ts"`, `"confirmation_url"` and `"subscription"`:

<ExtendedCodeGroup title="Confirmation message keys encoding" switchable copyable>
<CodeBlock title="Python">

```python
_hash.update(json.dumps(message['type']).encode("utf-8"))
_hash.update(json.dumps(message['message']).encode("utf-8"))
_hash.update(json.dumps(message['sent_ts']).encode("utf-8"))
_hash.update(json.dumps(message['confirmation_url']).encode("utf-8"))
_hash.update(json.dumps(message['subscription']).encode("utf-8"))
```
</CodeBlock>
</ExtendedCodeGroup>

4. Construct a `calculated_hash` object by returning the digest of the data fed to the `_hash` object by using the `digest()` method. Next, UTF-8 decode the digest of the data.

<ExtendedCodeGroup title="Construct calculated_hash" switchable copyable>
<CodeBlock title="Python">

```python
calculated_hash = b64encode(_hash.digest()).decode('utf-8')
```

</CodeBlock>
</ExtendedCodeGroup>

5. Compare the `"calculate_hash"` with hash passed in the message in the `"hash"` field.

<ExtendedCodeGroup title="Compare hashes" switchable copyable>
<CodeBlock title="Python">

```python
assert calculated_hash == message['hash']
```
</CodeBlock>
</ExtendedCodeGroup>

## Example of hash verification in Python

<ExtendedCodeGroup title="Hash verification" switchable copyable>
<CodeBlock title="Python">

```python
from hashlib import blake2b
import json
from base64 import b64encode

message = {
    "type": "Confirmation",
    "message": "cdebc2fe-6f96-491a-b6de-4038a3d51a10",
    "sent_ts": 1669302098.752,
    "confirmation_url": "https://rest.trustedtwin.com/notifications/webhooks/e4cb5f14-55bf-4840-a292-4126d8a67024?token=MjMzNjQxMmYzN2ZiNjg3ZjVkNTFlNmUyNDI1ZGFjYmE2ZjAwMTZlMzM2MjI4MDU3Nzc4N2E5YzQ5YmJjYmFlZWM1MWYyY2RlYzIyMjNkMDUzYmQ0MGQyMmRlNTAxNjMzZTk1MDc4YjUyMmM0ZjAxZjVkMmMzOTZmZTk2NjQyMDA4ZDgxMmRlYWJlN2NkNGE4MDFkNzAxZTVjZWM0ZmQwMzE1ZjNjNGQyMGEwMzcyZmQ0NWIyMmY0ODBhOTE5YzA1M2Q5OGUxZWM0YzQ0YmQ1NWZjYmIxOGFkZjE4NDczNWI1ZWU5Y2ViMDA3YWIxMjM1NWMzNzljNGVkZmNlMmYyOGMwMGEyOTQ2MWJkOGE4NDA2OTAwM2JjZGNjMWFkYzU3Mzk0Yjk1OGI1YmZhOTZkMDk2M2JhNTM4ZWM2NDJiMGRfX19lNGNiNWYxNC01NWJmLTQ4NDAtYTI5Mi00MTI2ZDhhNjcwMjRfX192YWx1ZV9pbmNyZWFzZTJfX185NTFkYjQ1MTY4Y2JmYzYwMTJjN2NmNWNkYmJlMWI3NQ==",
    "subscription": {
        "topic": "value-increase2",
        "account": "e4cb5f14-55bf-4840-a292-4126d8a67024",
        "validity_ts": 1669906898.432
    },
    "hash": "DSl2tiBa+qv634kC0fDexddEuAczcRD1LmwnU8K32uw="
}

keys_list = ["type", "message", "sent_ts", "confirmation_url", "subscription"] # confirmation 
keys_list = ["type", "trigger", "message", "twins", "event", "sent_ts", "subscription"] # notification

current_hash = message['hash']

client_secret = '1234567890abcdef'
server_secret = 'EbTWqhGPZU66deAS'


hash_seed = server_secret.encode('utf-8') + client_secret.encode('utf-8')
_hash = blake2b(digest_size=32, key=hash_seed)
_hash.update(json.dumps(message['type']).encode("utf-8"))
_hash.update(json.dumps(message['message']).encode("utf-8"))
_hash.update(json.dumps(message['sent_ts']).encode("utf-8"))
_hash.update(json.dumps(message['confirmation_url']).encode("utf-8"))
_hash.update(json.dumps(message['subscription']).encode("utf-8"))

calculated_hash = b64encode(_hash.digest()).decode('utf-8')
assert calculated_hash == message['hash']
```
</CodeBlock>
</ExtendedCodeGroup>
