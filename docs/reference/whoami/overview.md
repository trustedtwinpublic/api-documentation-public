# Overview

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **account**       | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user.                                                                                                              |
| **user**       | string    | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user. |
| **role** | string | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "account":"9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user":"3d0f1348-612a-4804-b632-24c4b871e76e",
    "role":"cba1a586-b5b9-46f5-a99b-76f70404508f"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
