
# Who am I 

<div class="row">
<div class="column">

This endpoint retrieves the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids), user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids), and role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /whoami | who_am_i   |
</EndpointsTable>

::: footnote *
In order for a user to perform the "who_am_i" operation, the "who_am_i" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/whoami' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.who_am_i()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { UsersApi, Configuration } from "@trustedtwin/js-client";

var usersApi = new UsersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const response = await usersApi.whoAmI();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **account**       | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user.                                                                                                              |
| **user**       | string    | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user. |
| **role** | string | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "account":"9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user":"3d0f1348-612a-4804-b632-24c4b871e76e",
    "role":"cba1a586-b5b9-46f5-a99b-76f70404508f"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.



