

# Who am I

<div class="row">
<div class="column">

This endpoint lets you retrieve the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids), user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids), and role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) associated with the User Secret (API key) of the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /whoami | [who_am_i](./who-am-i.md)   |
</EndpointsTable>

::: footnote *
In order for a user to perform the "who_am_i" operation, the "who_am_i" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
