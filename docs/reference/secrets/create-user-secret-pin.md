---
tags:
  - pin
---

# Create User Secret PIN

<div class="row">
<div class="column">

This endpoint creates a User Secret PIN for a given user. A User Secret PIN is 6 characters long string of numbers and digits. The User Secret PIN is required to generate a User Secret.

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
A User Secret PIN is valid for 10 minutes, and it can be used only once to generate a User Secret. After a User Secret has been generated, the User Secret PIN is automatically deleted from the system. 
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                  | Operation*          |
|:-------|:----------------------|:--------------------|
| POST   | /users/{user}/secrets | create_user_secret_pin |
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_user_secret_pin" operation, the "create_user_secret_pin" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
::: 

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type                      | In   | Description                                                                                                                                                                                                                                               |
|:----------------------------------|:--------------------------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|**{user}**<br/>*required*       | string                    | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user for whom a User Secret PIN is generated.                                                                                                                            |
 | **validity_ts**<br/>*optional* * | timestamp, `DEFAULT=null` | body | Time at which the User Secret expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Secret does not expire.                 |
</TableWrapper>

::: footnote *
The `"validity_ts"` parameter is optional and does not need to be included in the request body when creating a User Secret PIN. If it is not included in the request body, its default value (`null`) is used.
:::

In our example, user A (user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"fcf998ae-80e0-4eac-b7e8-378db626aefe"`), who already has access to the Trusted Twin platform, generates a User Secret PIN for user B (user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"333b4bf7-43b6-4df1-927a-9ddbd7336144"`). As user A wants the User Secret to expire on the 16th of March 2022 at 17:00:00 GMT, they set the `"validity_ts"` to `1647450000.0`.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "validity_ts": 1647450000.0
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/users/333b4bf7-43b6-4df1-927a-9ddbd7336144/secrets' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "validity_ts": 1647450000.0
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_secret_pin_body = {
    "validity_ts": 1647450000.0
}

status, response = TT_SERVICE.create_user_secret_pin("333b4bf7-43b6-4df1-927a-9ddbd7336144", body=user_secret_pin_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { SecretsApi, Configuration } from "@trustedtwin/js-client";

var secretsApi = new SecretsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const createUserSecretPINResponse = await secretsApi.createUserSecretPIN({
    user: "333b4bf7-43b6-4df1-927a-9ddbd7336144",
    createUserSecretPIN: {validityTs: 1647450000.0}
});
```

</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns the User Secret PIN `"P700SC"` generated by user A for user B (user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"333b4bf7-43b6-4df1-927a-9ddbd7336144"`). User B will use this User Secret PIN to generate a User Secret through the [create_user_secret](./create-user-secret.md) endpoint.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "pin": "P700SC"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
