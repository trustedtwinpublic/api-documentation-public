# Get User Secret

<div class="row">
<div class="column">

This endpoint retrieves the details of the User Secret for a given user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                   | Operation*    |
|:-------|:-----------------------|:--------------|
| GET    | /users/{user}/secrets | get_user_secret |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_user_secret" operation, the "get_user_secret" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type   | In   | Description                                                                                                                   |
|:--------------------------|:-------|:-----|-------------------------------------------------------------------------------------------------------------------------------|
| **{user}**<br/>*required* | string | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the User for whom the User Secret was created. |
</TableWrapper>

In our example, we retrieve the details of the User Secret created for the user with the user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"333b4bf7-43b6-4df1-927a-9ddbd7336144"`.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/users/333b4bf7-43b6-4df1-927a-9ddbd7336144/secrets' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_user_secret("333b4bf7-43b6-4df1-927a-9ddbd7336144")
```

</CodeBlock>

<CodeBlock title="Node.js">

```js
import { SecretsApi, Configuration } from "@trustedtwin/js-client";

var secretsApi = new SecretsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userSecret = await secretsApi.getUserSecret({
    user: "333b4bf7-43b6-4df1-927a-9ddbd7336144",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                                                                                            |
|:----------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **fingerprint** | string    | 4 last characters of the User Secret.                                                                                                                                                                                                                  |
| **account**     | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                        |
| **user**        | string    | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user for whom the User Secret was created.                                                                                                                          |
| **validity_ts** | timestamp | Time at which the User Secret expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  If the value is `null`, the User Secret does not expire.              |
| **created_ts**  | timestamp | Time at which the User Secret was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                           |
| **updated_ts**  | timestamp | Time at which the User Secret was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                      |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "fingerprint": "hdS2",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user": "333b4bf7-43b6-4df1-927a-9ddbd7336144",
    "validity_ts": 1647450000.0,
    "created_ts": 1646227994.603,
    "updated_ts": 1646227994.603
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
