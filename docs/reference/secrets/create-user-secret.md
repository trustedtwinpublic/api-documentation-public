---
tags:
  - api key
---

# Create User Secret

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
You must have a PIN to generate a User Secret. You can create the User Secret PIN using the [create_user_secret_pin](./create-user-secret-pin.md) endpoint. 
:::

This endpoint creates a User Secret for a given account. A User Secret is a 64 characters long unique string of numbers and letters. A User Secret is associated with a specific user - the user for whom the User Secret is created.   

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
The User Secret is displayed **only once**. It is not stored in the system, and it **cannot be retrieved**. Keep your User Secret stored in a secure place. If you lose your User Secret, you will need to start the User Secret generation process from the beginning.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                     | Operation*       |
|:-------|:-------------------------|:-----------------|
| POST   | /secrets/{account}/{pin} | create_user_secret |
</EndpointsTable>

::: footnote *
The operation "create_user_secret" is available to all users. It does not require any authentication.
::: 

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                    | Type   | In   | Description                                                                                                                                                    |
|:-----------------------------|:-------|:-----|:---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{account}**<br/>*required* | string | path | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                |
| **{pin}**<br/>*required*    | string | path | User Secret PIN. You can create the User Secret PIN using the [create_user_secret_pin](./create-user-secret-pin.md) endpoint.                 |
</TableWrapper>

In our example, user B (user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"333b4bf7-43b6-4df1-927a-9ddbd7336144"`) sends a request to generate a User Secret. They include the account UUID and the User Secret PIN in the path of the request. 

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/secrets/9891264d-4a77-4fa2-ae7f-84c9af14ae3b/P700SC' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService()

status, response = TT_SERVICE.create_user_secret("9891264d-4a77-4fa2-ae7f-84c9af14ae3b", "P700SC")
```

</CodeBlock>

<CodeBlock title="Node.js">

```js
import { SecretsApi, Configuration } from "@trustedtwin/js-client";

var secretsApi = new SecretsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userSecretResponse = await secretsApi.createUserSecret({
    account: "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    pin: "P700SC",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                                                                                                                                                                                                  |
|:----------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **secret**      | string    | User Secret. A 64 characters long unique string of numbers and letters associated with the user for whom the User Secret is created.                                                                                                                                                                                                                         |
| **account**     | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                              |
| **user**        | string    | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user for whom the User Secret was created.                                                                                                                                                                                                                                |
| **validity_ts** | timestamp | Time at which the User Secret expires. Defined at the time of creation of the User Secret PIN. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Secret does not expire.                                                            |
| **created_ts**  | timestamp | Time at which the User Secret was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                  |
| **updated_ts**  | timestamp | Time at which the User Secret was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                             |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "secret": "90oUh1v+qOxRi0vuBPBeOJj0TCyK7rKLQI5COKf5JoGBlN7Q8DZIPEtdE1i8hdS2",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user": "333b4bf7-43b6-4df1-927a-9ddbd7336144",
    "validity_ts": 1647450000.0,
    "created_ts": 1646227994.603,
    "updated_ts": 1646227994.603
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
