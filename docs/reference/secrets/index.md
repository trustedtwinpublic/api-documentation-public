---
tags:
  - user secret
  - api key
---

# User authentication (API key)

<div class="row">
<div class="column">

This section describes the user authentication scheme on the Trusted Twin platform and the endpoints related to the process of user authentication.

## User authentication

On the Trusted Twin platform, user authentication is based on User Secrets (API keys). A User Secret is 64 characters long unique string of numbers and letters associated with the user for whom the User Secret is generated. There is exactly one User Secret for each user on the Trusted Twin platform.    
    
A User Secret has a `"validity_ts"` attribute that holds a [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) with the time at which the User Secret expires. If the value of the `"validity_ts"` attribute is `null`, the User Secret does not expire.     

The User Secret should be passed in the HTTP header of each request. It is the API key on the Trusted Twin platform as it is required to access the Trusted Twin API methods.    

The authentication scheme on the Trusted Twin platform consists of two steps: 

1. Generation of the User Secret PIN through the [create_user_secret_pin](./create-user-secret-pin.md) endpoint. 
2. Generation of the User Secret through the [create_user_secret](./create-user-secret.md) endpoint.

The User Secret PIN is an additional safety lever used on the Trusted Twin platform. It was introduced as there can be more than one user involved in the process of generating a User Secret. 

### Example scenario 

- User A already has access to the Trusted Twin platform. They want to grant access to the platform to user B who has not used the Trusted Twin platform before.
- User A sends a request to generate a User Secret PIN for user B. User A includes the user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of user B in the path of the request as the User Secret is generated for a given user. 
- After the User Secret PIN has been generated, user A passes the User Secret PIN to user B. 
- User B sends a request to generate a User Secret and adds the User Secret PIN in the path of the request. User B includes as well the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request. The inclusion of the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request is another safety measure for creating User Secrets.
- The User Secret is generated and displayed to user B. As the User Secret is displayed only once, user B has to take note of the User Secret, keep it safe, and not lose it as the User Secret is not stored in the Trusted Twin system.

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
The User Secret is displayed **only once**. It is not stored in the system, and it **cannot be retrieved**. Keep your User Secret stored in a secure place. If you lose your User Secret, the User Secret generation process will need to be started from the beginning.
:::

After the User Secret has been generated, you can:
- Retrieve the fingerprint of the User Secret by using the [get_user_secret](./get-user-secret.md) endpoint. Please note that the User Secret itself cannot be retrieved - only the last 4 characters of the User Secret will be retrieved. They are held in the `"fingerprint"` attribute. 
- Change the validity (`"validity_ts"`) of the User Secret by using the [update_user_secret](./update-user-secret.md) endpoint.
- Delete the User Secret by using the [delete_user_secret](./delete-user-secret.md) endpoint.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                     | Operation*                                                                          |
|:-------|:-------------------------|:------------------------------------------------------------------------------------|
| POST   | /users/{user}/secrets    | [create_user_secret_pin](./create-user-secret-pin.md) |
| POST   | /secrets/{account}/{pin} | [create_user_secret](./create-user-secret.md)        |
| GET    | /users/{user}/secrets   | [get_user_secret](./get-user-secret.md)              |
| PATCH  | /users/{user}/secrets   | [update_user_secret](./update-user-secret.md)        |
| DELETE | /users/{user}/secrets   | [delete_user_secret](./delete-user-secret.md)        |
</EndpointsTable>

:::footnote *
In order for a user to perform operations related to the process of user authentication, the respective permissions "create_user_secret_pin", "get_user_secret", "update_user_secret", and "delete_user_secret", must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).<br/>
The operation "create_user_secret" is available to all users. It does not require any authentication.
:::

</div>
</div>
