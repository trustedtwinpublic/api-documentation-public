# Overview

<div class="row">
<div class="column">

In our example, you can see a User Secret generated for the User with the user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"333b4bf7-43b6-4df1-927a-9ddbd7336144"`:
- The User Secret is `"90oUh1v+qOxRi0vuBPBeOJj0TCyK7rKLQI5COKf5JoGBlN7Q8DZIPEtdE1i8hdS2"`.
- It was generated in the context of the account with the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"9891264d-4a77-4fa2-ae7f-84c9af14ae3b"`.
- As `"validity_ts"` is `null`, the User Secret does not expire.

</div>
<div class="column">

<ExtendedCodeGroup title="User Secret example" copyable>
<CodeBlock title="json">
```json
{
    "secret": "90oUh1v+qOxRi0vuBPBeOJj0TCyK7rKLQI5COKf5JoGBlN7Q8DZIPEtdE1i8hdS2",
    "account": "d6779760-effa-4nobrd2b-a955-1fb4428af117",
    "user": "333b4bf7-43b6-4df1-927a-9ddbd7336144",
    "validity_ts": null,
    "created_ts": 1646227994.603,
    "updated_ts": 1646227994.603
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                                                                                                                                                    |
|:----------------|:----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **secret**      | string    | User Secret. A 64 characters long unique string of numbers and letters associated with the user for whom the User Secret is created.                                                                                                                                                                           |
| **account**     | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                |
| **user**        | string    | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) for the user for whom the User Secret was created.                                                                                                                                                                                 |
| **validity_ts** | timestamp | Time at which the User Secret expires. Defined at the time of creation of the User Secret PIN. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Secret does not expire.              |
| **created_ts**  | timestamp | Time at which the User Secret was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                     |
| **updated_ts**  | timestamp | Time at which the User Secret was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                |
</TableWrapper>
