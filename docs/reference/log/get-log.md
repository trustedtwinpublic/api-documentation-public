# Get log

<div class="row">
<div class="column">

This endpoint retrieves logs. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*         |
|:-------|:-----|:-------------------|
| GET    | /log | get_log |
</EndpointsTable>


::: footnote *
In order for a user to perform the "get_log" operation, the "get_log" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                   | Type   | In           | Description                                                                     |
|:----------------------------|:-------|:-------------|:--------------------------------------------------------------------------------|
| **{fragment}**<br/> *optional* * | string | query string | ID that identifies the log. | 
</TableWrapper>

::: footnote *
This parameter is optional. If it is not included in the request, you are not checking whether the log returned in the response is subsequent to the previous log you retrieved.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/log' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_log()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LogApi, Configuration } from "@trustedtwin/js-client";

var logApi = new LogApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const log = await logApi.getLog();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute    | Type            | Description                                                                      |
|:-------------|:----------------|:---------------------------------------------------------------------------------|
| **fragment** | string          | ID that identifies the log. |
| **messages** | list of strings | Log message(s). You can find a list of log messages with links to relevant resources in the [Log messages](../../overview/log-messages.md) section.                                                                    | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
   "fragment":"2552bca1-d9bb-4be3-a169-50a2bd1fe530",
   "messages":[
      "Invalid Ledger configuration. Attempt to use a disabled Timeseries service: twin=[1b48e107-a9ed-4767-ae7a-a0a813c88ecd]."
   ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>




