# Overview

<div class="row">
<div class="column">

A log consists of a `"fragment"` (it identifies the log) and a `"message"` (it contains the log message). 

In our example, the log refers to the `"fragment"` with the ID `"2552bca1-d9bb-4be3-a169-50a2bd1fe530"`. The `"message"` is `"Invalid Ledger configuration. Attempt to use a disabled Timeseries service: twin=[1b48e107-a9ed-4767-ae7a-a0a813c88ecd]."`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example log" copyable>
<CodeBlock  title="json">
```json
{
   "fragment":"2552bca1-d9bb-4be3-a169-50a2bd1fe530",
   "messages":[
      "Invalid Ledger configuration. Attempt to use a disabled Timeseries service: twin=[1b48e107-a9ed-4767-ae7a-a0a813c88ecd]."
   ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute    | Type            | Description                                                                      |
|:-------------|:----------------|:---------------------------------------------------------------------------------|
| **fragment** | string          | ID that identifies the log. |
| **messages** | list of strings | Log message(s). You can find a list of log messages with links to relevant resources in the [Log messages](../../overview/log-messages.md) section.                                                                   | 
</TableWrapper>
