---
tags:
  - log endpoints
meta:
  - name: description
    content: 
---

# Log

<div class="row">
<div class="column">

## About

You can retrieve logs ad hoc by sending a request to the [get_log](./get-log.md) endpoint. A request to the [get_log](./get-log.md) endpoint needs to be made at least every 10 seconds to ensure continuity of logs.

To gain direct access to the log content as it is recorded, you can use the [Log monitor](../../tools/log_monitor.md) program. 

## Log fragment

If you are generating multiple logs one after another, you may need to confirm that these logs are subsequent and that no log fragments are missing in the sequence because they have been retrieved by another user. This is why the [get_log](./get-log.md) request returns a fragment [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the response. You can add the fragment [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) as a query string parameter in the request to confirm that the log fragments you are retrieving are subsequent and no log fragments have been retrieved by another user in the meantime. Please note that the first fragment you retrieve will return `"null"` instead of a fragment [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the response.

## Log messages

You can find a list of log messages with links to relevant resources in the [Log messages](../../overview/log-messages.md) section.

</div>
<div class="column">

<EndpointsTable>
| Method | Path | Operation*         |
|:-------|:-----|:-------------------|
| GET    | /log | [get_log](./get-log.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_log" operation, the "get_log" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
