---
tags:
  - usage endpoints
  - usage bucket
meta:
  - name: description
    content: 
---

# Usage

<div class="row">
<div class="column">

## About

On the Trusted Twin platform, you can retrieve usage data related to your account or of a given user belonging to that account.

## Usage buckets

Usage data is stored in buckets. A bucket is created at the beginning of an hour and stores one hour's worth of usage data. After the hour finishes, a new bucket is created and the bucket with the data from the previous hour is moved to the accounting system thus making the usage data no longer available through the [get_account_usage](./get-account-usage.md) and [get_user_usage](get-user-usage.md) endpoints.     
     
If you send a request to the [get_account_usage](./get-account-usage.md) or [get_user_usage](get-user-usage.md) at the beginning of an hour, you might receive two buckets - one containing usage data from the previous and another one containing usage data from the current hour.

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| GET   | /usage | [get_account_usage](./get-account-usage.md) |
| GET | /usage/{usage} | [get_user_usage](./get-user-usage.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_account_usage" and "get_user_usage" operations, the "get_account_usage" and "get_user_usage" permissions must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
