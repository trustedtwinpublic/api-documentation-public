# Get user usage

<div class="row">
<div class="column">

This endpoint retrieves the usage data of a given user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| GET   | /usage/{user} | get_user_usage |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_user_usage" operation, the "get_user_usage" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type   | In   | Description                                                                  |
|:--------------------------|:-------|:-----|:-----------------------------------------------------------------------------|
| **{user}**<br/>*required* | string | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) whose usage data are to be retrieved. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="curl">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/usage/3d0f1348-612a-4804-b632-24c4b871e76e' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_user_usage("3d0f1348-612a-4804-b632-24c4b871e76e")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { UsageApi, Configuration } from "@trustedtwin/js-client";

var usageApi = new UsageApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const response = await usageApi.getUserUsage({ user: "userUUID" });
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

## Usage items

<TableWrapper>
| Item                                | Category | Description                                                                                                  |
|:------------------------------------|:---------|:-------------------------------------------------------------------------------------------------------------|
| **db_read_cost**                    | database | DB read operations abstract cost.                                                                           |
| **db_write_cost**                   | database | DB write operations abstract cost.                                                                          |
| **hist_db_read_cost**               | database | Number of rows read from History database.                                                                   |
| **hist_db_write_cost**              | database | Number of rows modified in History database.                                                                 |
| **ts_db_read_cost**                 | database | Number of rows read from Timeseries database.                                                                |
| **ts_db_write_cost**                | database | Number of rows modified in Timeseries database.                                                              | 
| **ix_db_read_cost**                 | database | Number of rows read from Indexes database.                                                                   |
| **ix_db_write_cost**                | database | Number of rows modified in Indexes database.                                                                 |
| **stickers_db_read_cost**           | database | number of rows read from the Stickers database.                                                              |
| **stickers_db_write_cost**          | database | Number of rows modified in the Stickers database.                                                            |
| **adv_db_read_cost**                | database | Number of rows read from Advanced Database Services database.                                                |
| **adv_db_write_cost**               | database | Number of rows modified in the Advanced Database Services database.                                          |
| **batch_db_read_cost**              | database | Number of rows read from Batches database.                                                                   |
| **batch_db_write_cost**             | database | Number of rows modified in the Batches database.                                                             |
| **rest_count**                      | requests | Number of REST requests processed.                                                                           | 
| **request_count**                   | requests | Number of task requests processed.                                                                           |
| **task_publish_topic_count**        | requests | Number of publishes to subscription topics.                                                                  |
| **task_{name}_count***              | tasks    | Number of specified task requests processed.                                                                 |
| **doc_size_delta**                  | storage  | Doc storage space used delta (based on the actual file sizes).                                               |
| **access_optimized_doc_size_delta** | storage  | Doc storage space used delta for access_optimized storage.                                                   |
| **cost_optimized_doc_size_delta**   | storage  | Doc storage space used delta for cost_optimized storage.                                                     |
| **database_size_delta**             | storage  | Database volume size delta in bytes (used for advanced database services).                                   |
| **ledger_size_delta**               | storage  | Ledgers size delta in bytes.                                                                                 |
| **history_db_size_delta**           | storage  | History database size delta in bytes.                                                                        |
| **twins_created**                   | objects  | Number of created Twins.                                                                                     |
| **twins_terminated**                | objects  | Number of terminated Twins.                                                                                  |
| **identities_added**                | objects  | Number of Identities added.                                                                                  |
| **identities_deleted**              | objects  | Number of Identities deleted.                                                                                |
| **ledgers_added**                   | objects  | Number of Ledgers added.                                                                                     | 
| **ledgers_deleted**                 | objects  | Number of deleted Ledgers.                                                                                   | 
| **task_queue_count**                | system   | Number of queue requests processed.                                                                          |
| **doc_queue_count**                 | system   | Doc processing service messages send counter.                                                                |
| **database_queue_count**            | system   | Number of handled advanced database messages.                                                                | 
| **result_io_count**                 | system   | Task result cache I/O operations count.                                                                      |
| **log_io_count**                    | system   | Log cache I/O operations count.                                                                              |
| **dead_service_count**              | system   | Number of updates to Ledger entries with Timeseries configuration, when service was not enabled for account. |
| **l1_hit_count**                    | cache    | DB L1 cache hit counter.                                                                                     |
| **l2_hit_count**                    | cache    | DB L2 cache hit counter.                                                                                     |
| **l2_miss_count**                   | cache    | DB L2 cache miss counter.                                                                                    |
| **ts_conn_pool_hit_count**          | cache    | Number of times when Timeseries database connection was already available in the pool.                       |
| **ts_conn_pool_miss_count**         | cache    | Number of times when Timeseries database connection was not available in the pool.                           |
| **ts_conn_pool_broken_count**       | cache    | Number of times when connections to Timeseries database failed verification.                                 |
| **ts_conn_pool_new_count**          | cache    | Number of successful new connections to Timeseries database.                                                 |
| **ts_conn_pool_fail_count**         | cache    | Number of unsuccessful connections tries to Timeseries database.                                             |
| **ix_conn_pool_hit_count**          | cache    | Number of times when Indexes database connection was already available in the pool.                          |
| **ix_conn_pool_miss_count**         | cache    | Number of times when Indexes database connection was not available in the pool.                              |
| **ix_conn_pool_broken_count**       | cache    | Number of times when connections to Indexes database failed verification.                                    |
| **ix_conn_pool_new_count**          | cache    | Number of successful new connections to Indexes database.                                                    |
| **ix_conn_pool_fail_count**         | cache    | Number of unsuccessful connections tries to Indexes database.                                                |
| **db_io_time**                      | time     | ODB operations I/O time.                                                                                     |
| **l2_io_time**                      | time     | DB L2 cache I/O time.                                                                                        |
| **result_io_time**                  | time     | Task result cache I/O operations time.                                                                       |
| **log_io_time**                     | time     | Log cache I/O operations time.                                                                               |
| **lambda_time**                     | time     | Time spent in the request handler lambda.                                                                    |
| **doc_outbound_traffic_delta**      | transfer | Size of downloaded files in bytes.                                                                           |
| **database_outbound_traffic_delta** | transfer | Database outbound traffic in bytes.                                                                          |
| **rest_outbound_traffic_delta**     | transfer | REST API requests outbound traffic in bytes.                                                                 |
</TableWrapper>

::: footnote *
Please see [list of tasks](./get-user-usage.md#tasks) for description of a given task.
:::


## Tasks


<TableWrapper>
| Task                                          | Category                                   | Description                                                                      |
|:----------------------------------------------|:-------------------------------------------|:---------------------------------------------------------------------------------|
| **task_get_account_count**                    | [Account](../account/index.md)             | [get_account](../account/get-account.md)                                         |
| **task_get_account_count**                    | [Account](../account/index.md)             | [update_account](../account/update-account.md)                                   |
| **task_create_batch_count**                   | [Batches](../batches/index.md)             | [create_batch](../batches/create-batch.md)                                       |
| **task_get_batch_count**                      | [Batches](../batches/index.md)             | [get_batch](../batches/get-batch.md)                                             |
| **task_get_batch_count**                      | [Batches](../batches/index.md)             | [get_batches](../batches/get-batches.md)                                         |
| **task_get_batch_count**                      | [Batches](../batches/index.md)             | [update_batch](../batches/update-batch.md)                                       |
| **task_get_batch_count**                      | [Batches](../batches/index.md)             | [delete_batch](../batches/delete-batch.md)                                       |
| **task_create_upload_url_count**              | [Cache](../cache/index.md)                 | [create_upload_url](../cache/create-upload-url.md)                               |
| **task_invalidate_upload_url_count**          | [Cache](../cache/index.md)                 | [invalidate_upload_url](../cache/invalidate-upload-url.md)                       |
| **task_get_database_count**                   | [Databases](../databases/index.md)         | [get_database](../databases/get-database.md)                                     |
| **task_get_databases_count**                  | [Databases](../databases/index.md)         | [get_databases](../databases/get-databases.md)                                   |
| **task_update_database_count**                | [Databases](../databases/index.md)         | [update_database](../databases/update-database.md)                               |
| **task_get_database_access_count**            | [Databases](../databases/index.md)         | [get_database_access](../databases/get-database-access.md)                       |
| **task_update_database_user_access_count**    | [Databases](../databases/index.md)         | [update_database_user_access](../databases/update-database-user-access.md)       |
| **task_enable_service_count**                 | [Databases](../databases/index.md)         | Enables database service for an account.                                           |
| **task_update_service_count**                 | [Databases](../databases/index.md)         | Updates database service for the account.                                          |
| **task_disable_service_count**                | [Databases](../databases/index.md)     | Disables database service for the account.                                         |
| **tasK_add_services_database_count**          | [Databases](../databases/index.md)     | Adds services database to an account.                                              |
| **task_remove_services_database_count**       | [Databases](../databases/index.md)        | Removes services database from an account.                                         |
| **task_update_services_database_count**       | [Databases](../databases/index.md)    | Updates services database for an account.                                          |
| **task_attach_twin_doc_count**                | [Docs](../docs/index.md)                    | [attach_twin_doc](../docs/attach-twin-doc.md)                                    |
| **task_get_twin_doc_count**                   | [Docs](../docs/index.md)                    | [get_twin_doc](../docs/get-twin-doc.md)                                          |
| **task_get_twin_docs_count**                  | [Docs](../docs/index.md)                    | [get_twin_docs](../docs/get-twin-docs.md)                                        |
| **task_update_twin_doc_count**                | [Docs](../docs/index.md)                    | [update_twin_doc](../docs/update-twin-doc.md)                                    |
| **task_update_twin_doc_status_count**         | [Docs](../docs/index.md)                    | Updates the status of [Doc](../docs/index.md).                                   |
| **task_delete_twin_doc_count**                | [Docs](../docs/index.md)                    | [delete_twin_doc](../docs/delete-twin-doc.md)                                    |
| **task_download_doc_count**                   | [Docs](../docs/index.md)                    | [download_twin_doc](../docs/get-twin-doc.md)                                     |
| **task_get_twin_ledger_entry_history_count**  | [History](../history/index.md)             | [get_twin_ledger_entry_history](../history/get-twin-ledger-entry-history.md)     |
| **task_history_append_count**                 | [History](../history/index.md)             | Appends a [History](../history/index.md) record.                                 |
| **task_history_delete_count**                 | [History](../history/index.md)             | Deletes a [History](../history/index.md) record.                                 |
| **task_create_twin_identity_count**           | [Identities](../identities/index.md)         | [create_twin_identity](../identities/create-twin-identity.md)                    | 
| **task_get_twin_identity_count**              | [Identities](../identities/index.md)          | [get_twin_identity](../identities/get-twin-identity.md)                          | 
| **task_get_twin_identities_count**            | [Identities](../identities/index.md)           | [get_twin_identities](../identities/get-twin-identities.md)                      |
| **task_update_twin_identity_count**           | [Identities](../identities/index.md)          | [update_twin_identity](../identities/update-twin-identity.md)                    | 
| **task_delete_twin_identity_count**           | [Identities](../identities/index.md)           | [delete_twin_identity](../identities/delete-twin-identity.md)                    |
| **task_resolve_twin_identity_count**          | [Identities](../identities/index.md)          | [resolve_twin_identity.md](../identities/resolve-twin-identity.md)               |
| **task_create_indexes_table_count**           | [Indexes](../indexes/index.md)             | [create_indexes_table](../indexes/create-indexes-table.md)                       | 
| **task_get_indexes_table_count**              | [Indexes](../indexes/index.md)             | [get_indexes_table](../indexes/get-indexes-table.md)                             |
| **task_get_indexes_tables_count**             | [Indexes](../indexes/index.md)             | [get_indexes_tables](../indexes/get-indexes-tables.md)                           |
| **task_update_indexes_table_count**           | [Indexes](../indexes/index.md)             | [update_timeseries_table](../indexes/update-indexes-table.md)                    | 
| **task_truncate_indexes_table_count**         | [Indexes](../indexes/index.md)             | [truncate_indexes_table](../indexes/truncate-indexes-table.md)                   |
| **task_delete_indexes_table_count**           | [Indexes](../indexes/index.md)             | [delete_indexes_table](../indexes/delete-indexes-table.md)                       |
| **task_add_twin_ledger_entry_count**          | [Ledgers](../ledgers/index.md)              | [add_twin_ledger_entry](../ledgers/add-twin-ledger-entry.md)                     | 
| **task_get_twin_ledger_entry_count**          | [Ledgers](../ledgers/index.md)               | [get_twin_ledger_entry](../ledgers/get-twin-ledger-entry.md)                     |
| **task_update_twin_ledger_entry_value_count** | [Ledgers](../ledgers/index.md)                | [update_twin_ledger_entry_value](../ledgers/update-twin-ledger-entry-value.md)      |
| **task_update_twin_ledger_entry_count**       | [Ledgers](../ledgers/index.md)               | [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md)               |
| **task_delete_twin_ledger_entry_count**       | [Ledgers](../ledgers/index.md)               | [delete_twin_ledger_entry](../ledgers/delete-twin-ledger-entry.md)               |
| **task_add_ledger_entry_ref_count**           | [Ledgers](../ledgers/index.md)               | Adds a [Ledger](../ledgers/index.md) Entry reference.                            |
| **task_update_ledger_entry_ref_count**        | [Ledgers](../ledgers/index.md)               | Updates a [Ledger](../ledgers/index.md) Entry reference.                         |
| **task_delete_ledger_entry_ref_count**        | [Ledgers](../ledgers/index.md)               | Deletes a [Ledger](../ledgers/index.md) Entry reference.                         |
| **task_get_log_count**                        | [Log](../log/index.md)                     | [get_log](../log/get-log.md)                                                     |
| **task_webhook_subscribe_count**              | [Notifications](../notifications/index.md) | [webhook_subscribe](../notifications/webhook-subscribe.md)                       |
| **task_create_user_role_count**               | [Roles](../roles/index.md)                  | [create_user_role](../roles/create-user-role.md)                                 |
| **task_get_user_role_count**                  | [Roles](../roles/index.md)                   | [get_user_role](../roles/get-user-role.md)                                       |
| **task_get_user_roles_count**                 | [Roles](../roles/index.md)                    | [get_user_roles](../roles/get-user-roles.md)                                     |
| **task_update_user_role_count**               | [Roles](../roles/index.md)                   | [update_user_role](../roles/update-user-role.md)                                 |
| **task_delete_user_role_count**               | [Roles](../roles/index.md)                    | [delete_user_role](../roles/delete-user-role.md)                                 |
| **task_put_sticker_count**                    | [Stickers](../stickers/index.md)           | [put_sticker](../stickers/put-sticker.md)                                        |
| **task_get_sticker_count**                    | [Stickers](../stickers/index.md)           | [get_sticker](../stickers/get-sticker.md)                                        |
| **task_get_stickers_count**                   | [Stickers](../stickers/index.md)           | [get_stickers](../stickers/get-stickers.md)                                      |
| **task_list_stickers_count**                  | [Stickers](../stickers/index.md)           | [list_stickers](../stickers/list-stickers.md)                                    |
| **task_remove_sticker_count**                 | [Stickers](../stickers/index.md)           | [remove_sticker](../stickers/remove-sticker.md)                                  |   
| **task_create_timeseries_table_count**        | [Timeseries](../timeseries/index.md)       | [create_timeseries_table](../timeseries/create-timeseries-table.md)              |
| **task_get_timeseries_table_count**           | [Timeseries](../timeseries/index.md)       | [get_timeseries_table](../timeseries/get-timeseries-table.md)                    | 
| **task_get_timeseries_tables_count**          | [Timeseries](../timeseries/index.md)       | [get_timeseries_tables](../timeseries/get-timeseries-tables.md)                  |
| **task_update_timeseries_table_count**        | [Timeseries](../timeseries/index.md)       | [update_timeseries_table](../timeseries/update-timeseries-table.md)              |
| **task_truncate_timeseries_table_count**      | [Timeseries](../timeseries/index.md)       | [truncate_timeseries_table](../timeseries/truncate-timeseries-table.md)          |
| **task_delete_timeseries_table_count**        | [Timeseries](../timeseries/index.md)       | [delete_timeseries_table](../timeseries/delete-timeseries-table.md)              |
| **task_trace_count**                          | [Trace](../trace/index.md)                 | [trace](../trace/trace.md)                                                       |
| **task_trace_system_count**                   | [Trace](../trace/index.md)                 | Task triggered by a [trace](../trace/trace.md) request.                          |
| **task_create_twin_count**                    | [Twins](../twins/index.md)                  | [create_twin](../twins/create-twin.md)                                           | 
| **task_get_twin_count**                       | [Twins](../twins/index.md)                  | [get_twin](../twins/get-twin.md)                                                 |
| **task_scan_twins_count**                     | [Twins](../twins/index.md)                  | [scan_twins](../twins/scan-twins.md)                                             | 
| **task_update_twin_count**                    | [Twins](../twins/index.md)                   | [update_twin](../twins/update-twin.md)                                           | 
| **task_delete_twin_count**                    | [Twins](../twins/index.md)                   | [delete_twin](../twins/terminate-twin.md)                                        | 
| **task_get_account_usage_count**              | [Usage](../usage/index.md)                 | [get_account_usage](../usage/get-account-usage.md)                               |
| **task_get_user_usage_count**                 | [Usage](../usage/index.md)                 | [get_user_usage](../usage/get-user-usage.md)                                     |
| **task_create_user_count**                    | [Users](../users/index.md)                  | [create_user](../users/create-user.md)                                           |
| **task_get_user_count**                       | [Users](../users/index.md)                   | [get_user](../users/get-user.md)                                                 |
| **task_get_users_count**                      | [Users](../users/index.md)                   | [get_user](../users/get-users.md)                                                |
| **task_update_user_count**                    | [Users](../users/index.md)                   | [update_user](../users/update-user.md)                                           |
| **task_delete_user_count**                    | [Users](../users/index.md)                  | [delete_user](../users/delete-user.md)                                           |
| **task_create_user_secret_pin_count**         | [User Secrets](../secrets/index.md)         | [create_user_secret_pin](../secrets/create-user-secret-pin.md)                   |
| **task_get_user_secret_count**                | [User Secrets](../secrets/index.md)         | [get_user_secret](../secrets/get-user-secret.md)                                 |
| **task_update_user_secret_count**             | [User Secrets](../secrets/index.md)         | [update_user_secret](../secrets/update-user-secret.md)                           |
| **task_delete_user_secret_count**             | [User Secrets](../secrets/index.md)         | [delete_a_user_secret](../secrets/delete-user-secret.md)                         |
| **task_create_user_token_count**              | [User Token](../token/index.md)            | [create_user_token](../token/create-user-token.md)                               |
| **task_refresh_user_token_count**             | [User Token](../token/index.md)            | [refresh_user_token](../token/refresh-user-token.md)                             |
| **task_who_am_i_count**                       | [Who am I](../whoami/index.md)             | [who am I](../whoami/who-am-i.md)                                                |
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "usage": {
        "2022-07-02-10": {
            "rds_read_cost": 0.0,
            "rds_write_cost": 0.0,
            "l1_hit_count": 0.0,
            "l2_hit_count": 13.0,
            "l2_miss_count": 0.0,
            "rds_io_time": 0.0,
            "l2_io_time": 0.06241416931152343,
            "lambda_time": 3.0083863735198975,
            "rest_count": 3.0,
            "task_get_account_usage_count": 1.0,
            "result_io_time": 1.1284253597259521,
            "result_io_count": 685.0,
            "rest_outbound_traffic_delta": 2213.0,
            "request_count": 2.0,
            "db_read_cost": 0.0,
            "db_write_cost": 0.0,
            "db_io_time": 0.0,
            "task_add_twin_ledger_entry_count": 1.0,
            "log_io_time": 0.0,
            "log_io_count": 0.0
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
