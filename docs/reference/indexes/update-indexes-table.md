# Update Indexes table

<div class="row">
<div class="column">

This endpoint updates a given Indexes table.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                         | Operation*                                                                     |
|:-------|:-----------------------------|:-------------------------------------------------------------------------------|
| PATCH   | /account/services/indexes/{index} | update_indexes_table  |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_indexes_table" operation, the "update_indexes_table" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

::: tip UPDATING PROPERTIES
<Icon name="info-circle" :size="1.75" />
You can add a new property or update the name of an exiting property. Please note that the entire `"properties"` dictionary must be included in the request body when adding new properties or updating existing properties. The new `"properties"` dictionary content will replace the old `"properties"` dictionary content.<br/>
**Adding a new property**<br/>
When adding a new property, add a new property after the existing one(s) and a data type for this property to the `"types"` list. You must as well add a new template for the property.
**Updating an exiting property**<br/>
When updating a property name, add a list containing the old and the new property name (e.g. `["gdansk_ozone", "gdansk_ozone_new"]`).It is not possible to update the property type.<br/>

:::

<TableWrapper>
| Parameter                 | Type   | In   | Description                                                 |
|:--------------------------|:-------|:-----|:------------------------------------------------------------|
| **index**<br/>*required* | string | path | Name of the Indexes table to be updated. |
| **rule**<br/>*optional* * | string | body | [Rule](../../overview/rules.md) used to select Twins that should be present in the given Indexes table. This rule is evaluated when the Ledger is changed. This rule is not evaluated when a user or a Twin are changed.                                                                                        |
| **properties**<br/>*optional* ** | dictionary | body | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of property column names and a list of data types of the property columns. You can add a new property or update the name of an exiting property. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Names of the property columns. Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the property columns.  </td> </tr> </table></TableWrapper> </details>| 
|**templates**<br/>*optional* *** | dictionary | body | Dictionary containing [templates](../../overview/templates.md) for `"properties"` in form of key-value pairs:<br/> - key: property name, <br/>- value: template.<br/> The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).
</TableWrapper>

::: footnote *
This parameter is optional. If it is not included in the request, no changes will be made to the Indexes table.
:::

::: footnote **
If the `"properties"` dictionary with attributes is provided in the request body, the `"properties"` dictionary from the request body will replace the current `"properties"` dictionary.<br/>      
If no attributes are provided in the request body of `"properties"` (`"properties": {}`), or if `"properties"` with the value `null` is provided in the request body, the response will return an error, because `"properties"` columns cannot be removed.<br/>
If the request body does not contain `"properties"`, no changes will be made to the `"properties"` field.
:::

::: footnote ***
If the `"templates"` dictionary with attributes is provided in the request body, the `"templates"` dictionary from the request body will replace the current `"templates"` dictionary.<br/>      
If no attributes are provided in the request body of `"templates"` (`"templates": {}`), or if `"templates"` with the value `null` is provided in the request body, the response will return an error, because `"templates"` columns cannot be removed.<br/>
If the request body does not contain `"templates"`, no changes will be made to the `"templates"` field.<br/>
The `"templates"` dictionary must be provided if you are adding a new property.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
 { "rule": "LEDGER.gdansk_temperature > 5" }
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/indexes/indexes_1' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw ' { "rule": "LEDGER.gdansk_temperature > 5" }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

rule_update_body = {
    "rule": "LEDGER.gdansk_temperature > 5"
}

status, response = TT_SERVICE.update_indexes_table("indexes_1", body=rule_update_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IndexesApi, Configuration } from "@trustedtwin/js-client";

var indexesApi = new IndexesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const updatedIndexesTable = await indexesApi.updateIndexesTable({
    index: "indexes_1",
    indexTableUpdate: {
        rule: "LEDGER.gdansk_temperature > 5"
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:---------------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **stats**      | dictionary | <details> <ExtendedSummary markdown="span">Indexes table stats.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**table_size**</td>  <td> integer </td>  <td>Size, in bytes, of actual data stored in the database table backing the Index. </td>  </tr>  <tr>  <td> **index_size** </td>  <td> integer </td>  <td> Size, in bytes, of indices created on the database table backing the index. </td>  </tr>  <tr>  <td> **toast_size** </td>  <td> integer </td>  <td> Size, in bytes, of the TOAST ([The Oversized-Attribute Storage Technique](https://www.postgresql.org/docs/current/storage-toast.html)) data for the database table backing the index. </td>  </tr> <tr>  <td> **total_size** </td>  <td> integer </td>  <td> Total size, in bytes, of the database relation (sum of actual, indices and toast data) backing the index.</td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **properties** | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of property names and a list of data types of the properties. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Names of the properties. Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the properties.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **rule**       | string     | [Rule](../../overview/rules.md) used to select Twins that should be present in the given Indexes table.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |  
| **templates**  | dictionary | Dictionary containing [templates](../../overview/templates.md) for `"properties"` in form of key-value pairs:<br/>- key: property name, <br/>- value: template.<br/> The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 
| **database** | string| Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "stats": {
        "table_size": 0,
        "index_size": 32768,
        "toast_size": 0,
        "total_size": 32768
    },
    "rule": "LEDGER.gdansk_temperature > 5",
    "properties": {
        "names": [
            "gdansk_temperature",
            "gdansk_ozone",
            "gdansk_relative_humidity"
        ],
        "types": [
            "real",
            "real",
            "real"
        ]
    },
    "templates": {
        "properties": {
            "gdansk_ozone": "{LEDGER.gdansk_ozone}",
            "gdansk_temperature": "{LEDGER.gdansk_temperature}",
            "gdansk_relative_humidity": "{LEDGER.gdansk_relative_humidity}"
        }
    },
    "database": "24520f1d-20ef-4a84-94a6-92a853da0680"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
