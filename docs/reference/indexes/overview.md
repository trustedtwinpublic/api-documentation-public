# Overview

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:---------------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **stats**      | dictionary | <details> <ExtendedSummary markdown="span">Indexes table stats.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**table_size**</td>  <td> integer </td>  <td>Size, in bytes, of actual data stored in the database table backing the Index. </td>  </tr>  <tr>  <td> **index_size** </td>  <td> integer </td>  <td> Size, in bytes, of indices created on the database table backing the index. </td>  </tr>  <tr>  <td> **toast_size** </td>  <td> integer </td>  <td> Size, in bytes, of the TOAST ([The Oversized-Attribute Storage Technique](https://www.postgresql.org/docs/current/storage-toast.html)) data for the database table backing the index. </td>  </tr> <tr>  <td> **total_size** </td>  <td> integer </td>  <td> Total size, in bytes, of the database relation (sum of actual, indices and toast data) backing the index.</td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **properties** | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of property names and a list of data types of the properties. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name/names of the property/properties. Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the properties.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **rule**       | string     | [Rule](../../overview/rules.md) used to select Twins that should be present in the given Indexes table. This rule is evaluated when the Ledger is changed. It is not evaluated when a user or a Twin change.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |  
| **templates**  | dictionary | Dictionary containing [templates](../../overview/templates.md) for `"properties"` in form of key-value pairs:<br/>- key: property name, <br/>- value: template.<br/> The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Indexes table example" copyable>
<CodeBlock title="json">
```json
{
    "stats": {
        "table_size": 0,
        "index_size": 32768,
        "toast_size": 0,
        "total_size": 32768
    },
    "rule": "LEDGER.gdansk_temperature > 10",
    "properties": {
        "names": [
            "gdansk_temperature",
            "gdansk_ozone",
            "gdansk_relative_humidity"
        ],
        "types": [
            "real",
            "real",
            "real"
        ]
    },
    "templates": {
        "properties": {
            "gdansk_temperature": "{LEDGER.gdansk_temperature}",
            "gdansk_ozone": "{LEDGER.gdansk_ozone}",
            "gdansk_relative_humidity": "{LEDGER.gdansk_relative_humidity}"
        }
    },
    "database": "24520f1d-20ef-4a84-94a6-92a853da0680"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
