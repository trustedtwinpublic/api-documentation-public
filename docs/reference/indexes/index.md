---
tags:
  - indexes
  - indexes endpoints
  - indexes attributes
  - indices
  - tsdb
---

# Indexes

<div class="row">
<div class="column">

## About

Indexes allow you to efficiently access Ledger records. They are especially helpful if you manage a large number of Twins within your account. 

The Indexes database on the Trusted Twin platform stores structured and multidimensional views of digital twin states in form of user-defined tables created through the [create_indexes_table](../indexes/create-indexes-table.md) endpoint.   

The Indexes service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.

## Indexes database access

::: tip INDEXES DATABASE ACCESS
<Icon name="info-circle" :size="1.75" />
To learn more about accessing the Indexes database, please go to the [Database services access](../../account-and-access/database-services-access.md) section.
:::

## Indexes database structure

An Indexes database consists of one or more Indexes tables. An Index can occur only once in a given Twin.

## Indexes table

The name of an Indexes table must be unique in the context of an account, and it must match the [regular expression](https://regexr.com/) `^(_*[a-z0-9]+)([_-][0-9a-z]+)*$`.

Below you can find an example of an Indexes table. It contains a Twin column (stating the [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin where the index occurs), a timestamp column stating when the index was last changed (added automatically by the system), and three properties ("gdansk_temperature", "gdansk_ozone", "gdansk_relative_humidity"):

<TableWrapper>
| _twin                                | _timestamp            | gdansk_temperature | gdansk_ozone | gdansk_relative_humidity |
|:-------------------------------------|:----------------------|:-------------------|:-------------|:-------------------------|
| ca12b80e-045a-4c93-9e9b-2fc407fc5f2f | 18 May 2022, 11:36:39 | 21                 | 29           | 55                       |
| b27289ba-da39-4232-a9d7-7bef299d548c | 18 May 2022, 12:00:00 | 20                 | 29           | 56                       |
</TableWrapper>

::: tip TEMPLATES
<Icon name="info-circle" :size="1.75" />
The `"properties"` attribute uses [templates](../../overview/templates.md). Templates on the Trusted Twin platform follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). Please see  [Available variables](../../overview/templates.md#available-variables) for variables available for templates.
:::



</div>
<div class="column">

<EndpointsTable>
| Method | Path                                           | Operation                                                                         |
|:-------|:-----------------------------------------------|:----------------------------------------------------------------------------------|
| PATCH  | /account/services/indexes                   | [update_indexes_access](./update-indexes-access.md)     |
| POST   | /account/services/indexes                   | [create_indexes_table](./create-indexes-table.md)      |
| GET    | /account/services/indexes/{index}      | [get_indexes_table](./get-indexes-table.md)           |
| GET    | /account/services/indexes                   | [get_indexes_tables](./get-indexes-tables.md)          |
| PATCH  | /account/services/indexes/{index}      | [update_indexes_table](./update-indexes-table.md)     |
| DELETE | /account/services/indexes/{index}/data | [truncate_indexes_table](./truncate-indexes-table.md) |
| DELETE | /account/services/indexes/{index}      | [delete_indexes_table](./delete-indexes-table.md)     |
</EndpointsTable>

::: footnote *
In order for a user to perform operations on Indexes tables, the respective permissions "create_indexes_table", "get_indexes_tables", "update_indexes_access", "get_indexes_table", "update_indexes_table", "delete_indexes_table", and "truncate_indexes_table" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). To learn more about accessing the Indexes database, please go to [Database services access](../../account-and-access/database-services-access.md).
:::

</div>
</div>
