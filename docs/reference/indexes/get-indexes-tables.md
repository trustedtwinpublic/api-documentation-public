# Get Indexes tables

<div class="row">
<div class="column">

This endpoint retrieves a lists of existing Indexes tables in an account.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                         | Operation*                                                                     |
|:-------|:-----------------------------|:-------------------------------------------------------------------------------|
| GET   | /account/services/indexes | get_indexes_tables  |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_indexes_tables" operation, the "get_indexes_tables" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request 

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/account/services/indexes' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_indexes_tables()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IndexesApi, Configuration } from "@trustedtwin/js-client";

var indexesApi = new IndexesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const indexesTables = await indexesApi.getIndexesTables();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns the `"stats"` attribute. It holds the `"status"` and the `"database_size"` attributes, a list of users with their respective access permissions to the Indexes database held in the `"users"` attribute, and the names of the Indexes tables held in the `"indexes"` attribute.

<TableWrapper>
| Attribute   | Type            | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|:------------|:----------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **indexes** | list of strings | List of names of Indexes tables in the Indexes database.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "indexes": [
        "table_3",
        "index_table_1",
        "indexes_1"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
