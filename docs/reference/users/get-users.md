# Get users

<div class="row">
<div class="column">

This endpoint retrieves users belonging to the account of the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /users | get_users    |
</EndpointsTable>

::: footnote *
In order for a User to perform the "get_users" operation, the "get_users" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
footnote
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/users' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_users()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { UsersApi, Configuration } from "@trustedtwin/js-client";

var usersApi = new UsersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userDefinition = await usersApi.getUsers();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type       | Description                                                                                                                                                                                                                                                                                                             |
|:----------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **name**        | string     | Name of the user.                                                                                                                                                                                                                                            | 
| **role**        | string     | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the user.              |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "users":{
        "863c38bd-976b-43fd-bb0a-b8724fda1786":{
            "name":"User 1",
            "role":"6e6efeda-e4f9-49d0-b943-6d9775f36f6b"
        },
        "32206901-7550-4cf4-83fd-2401ac92dd49":{
            "name":"User 2",
            "role":"18686803-afe4-41d4-88e3-bbfdcdaf07a6"
        },
        "d3e3983c-4897-4327-990e-88f8156ad8bc":{
            "name":"User 3",
            "role":"df5141c3-9c96-419c-b919-9a940025bf71"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.


