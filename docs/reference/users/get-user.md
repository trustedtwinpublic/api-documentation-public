# Get user

<div class="row">
<div class="column">

This endpoint retrieves the details of the requested user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /users/{user} | get_user    |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_user" operation, the "get_user" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
footnote
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type   | In   | Description                                                                  |
|:--------------------------|:-------|:-----|:-----------------------------------------------------------------------------|
| **{user}**<br/>*required* | string | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). |
</TableWrapper>

In our example, we retrieve the user with the user [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"3d0f1348-612a-4804-b632-24c4b871e76e"`:

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/users/3d0f1348-612a-4804-b632-24c4b871e76e' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_user("3d0f1348-612a-4804-b632-24c4b871e76e")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { UsersApi, Configuration } from "@trustedtwin/js-client";

var usersApi = new UsersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userDefinition = await usersApi.getUser({
    user: "3d0f1348-612a-4804-b632-24c4b871e76e",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type       | Description                                                                                                                                                                                                                                                                                                             |
|:----------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**        | string     | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                            | 
| **name**        | string     | Name of the user. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                               |
| **account**     | string     | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                         |
| **role**        | string     | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                            |
| **description** | dictionary | Attributes of the user in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. |
| **created_ts**  | timestamp  | Time at which the user was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                     |
| **updated_ts**  | timestamp  | Time at which the user was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                |
| **activity**<br/>*optional* **         | dictionary                     | <details> <ExtendedSummary markdown="span"> [User Activity Log](../../overview/user-activity-log.md) attribute. It holds the name(s) of the Timeseries table(s) where the User Activity Log is to be stored, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the User Activity Log entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details>  |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
   "uuid":"3d0f1348-612a-4804-b632-24c4b871e76e",
   "name":"Oliver Adams",
   "account":"9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
   "role":"cba1a586-b5b9-46f5-a99b-76f70404508f",
   "description":{
      "company":"Best Shoes",
      "position":"accounting",
      "in_house_payroll":true
   },
   "activity":{
      "user_activity_log":{
      }
   },
   "created_ts":1646308575.845,
   "updated_ts": 1646308575.845
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
