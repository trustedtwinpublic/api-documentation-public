# Update user

<div class="row">
<div class="column">

This endpoint updates the name, the role of the user, and the description of the user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| PATCH  | /users/{user} | update_user |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_user" operation, the "update_user" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type       | In   | Description                                                                                                                                                                                                                                                                                                             |
|:----------------------------------|:-----------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{user}**<br/>*required*         | string     | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                            |
| **name**<br/>*optional* *         | string     | body | Name of the user. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                               |
| **role**<br/>*optional* *         | string     | body | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                            |
| **description**<br/>*optional* ** | dictionary | body | Attributes of the user in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. |
| **activity**<br/>*optional* ***         | dictionary                     | body | <details> <ExtendedSummary markdown="span"> [User Activity Log](../../overview/user-activity-log.md) attribute. It holds the name(s) of the Timeseries table(s) where the User Activity Log is to be stored, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the User Activity Log entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                             |
</TableWrapper>

::: footnote *
Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters. Please note that if `"description"` is provided in the request body, the entire description will be updated.
:::

::: footnote **
If the `"description"` field with attributes is provided in the request body, the `"description"` field from the request body will replace the current `"description"` field. <br/>
If no attributes are provided in the request body of `"description"` (`"description": {}`), all attributes from the current `"description"` will be removed. The response will return an empty dictionary (`"description": {}`). <br/>   
If `"description"` with the value `null` is provided in the request body, all attributes from the current `"description"` will be removed. The response will not return the `"description"` field. <br/>
If the request body does not contain `"description"`, no changes will be made to the `"description"` field.
:::

::: footnote ***
If the `"activity"` field with attributes is provided in the request body, the `"activity"` field from the request body will replace the current `"activity"` field. <br/>
If no attributes are provided in the request body of `"activity"` (`"activity": {}`), all attributes from the current `"description"` will be removed. The response will return an empty dictionary (`"activity": {}`). <br/>   
If `"activity"` with the value `null` is provided in the request body, the [User Activity Log](../../accounting/user-activity-log.md) for the given user will be disabled. The response will not return the `"activity"` field. <br/>
If the request body does not contain `"activity"`, no changes will be made to the `"activity"` field.
:::


In our example, we change the values of two attributes in the `"description"` field:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "description": {
    "position": "sales",
    "in_house_payroll": false
  }   
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/users/3d0f1348-612a-4804-b632-24c4b871e76e' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
      "position": "sales",
      "in_house_payroll": false
    }
  }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_update_body = {
      "description": {
            "position": "sales",
            "in_house_payroll": False
      }
}

status, response = TT_SERVICE.update_user("3d0f1348-612a-4804-b632-24c4b871e76e", body=user_update_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { UsersApi, Configuration } from "@trustedtwin/js-client";

var usersApi = new UsersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userDefinition = await usersApi.updateUser({
    user: "3d0f1348-612a-4804-b632-24c4b871e76e",
    patchUser: {
        description: {
            position: "sales",
            inHousePayroll: false,
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type       | Description                                                                                                                                                                                                                                                                                                              |
|:----------------|:-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**        | string     | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                          | 
| **name**        | string     | Name of the user. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                                |
| **account**     | string     | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                          |
| **role**        | string     | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                             |
| **description** | dictionary | Attributes of the user in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. |
| **created_ts**  | timestamp  | Time at which the user was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                     |
| **updated_ts**  | timestamp  | Time at which the user was las updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                 |
| **activity**<br/>*optional* **         | dictionary                     | <details> <ExtendedSummary markdown="span"> [User Activity Log](../../overview/user-activity-log.md) attribute. It holds the name(s) of the Timeseries table(s) where the User Activity Log is to be stored, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the User Activity Log entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
   "uuid":"3d0f1348-612a-4804-b632-24c4b871e76e",
   "name":"Oliver Adams",
   "account":"9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
   "role":"cba1a586-b5b9-46f5-a99b-76f70404508f",
   "description":{
      "company":"Best Shoes",
      "position":"accounting",
      "in_house_payroll":true
   },
   "activity":{
      "user_activity_log":{
      }
   },
   "created_ts":1646308575.845,
   "updated_ts": 1646309196.741
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
