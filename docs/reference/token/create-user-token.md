# Create User Token

<div class="row">
<div class="column">

This endpoint generates a User Token with JWT functionality. 

The User Token can be generated on the base of a User Secret (API key) or a User Token if the user is using a User Token to authenticate on the Trusted Twin platform and the User Token allows for creating new User Tokens (`"options":["create"]`).

</div>
<div class="column">


<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /token | create_user_token |
</EndpointsTable>

::: footnote *
In order for a user to be able to create User Tokens using the User Secret authorization, the permission "create_user_token" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). <br/>
For a user to be able to create a User Tokens using the User Token authorization, the "create" permission must be included in the `"options"` list of the User Token.
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type                                                | In   | Description                                                                                                                                                            |
|:----------------------------------|:----------------------------------------------------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **secret_dict**<br/>*optional**   | dictionary                                          | body | User-defined key-value pair:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value.                                                                                                                                            |
| **options**<br/>*optional***      | list of strings, value is `"create"` or `"refresh"` | body |  Defines whether a new User Token can be created or refreshed by a user authorizing with the User Token.                                                                                                                                                                      | 
| **validity_ts**<br/>*optional**** | timestamp, `DEFAULT=null`                           | body | Time at which the User Token expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If not provided, the User Token does not expire. |
</TableWrapper>

::: footnote *
The `"secret_dict"` parameter is optional and does not need to be included in the request body when creating User Tokens.
:::

::: footnote **
The `"options"` parameter is optional and does not need to be included in the request body when creating User Tokens. If it is not included in the request body, the caller who is using the User Token will not be able to create or refresh the User Token. 
:::

::: footnote ***
The `"validity_ts"` parameter is optional and does not need to be included in the request body when creating User Tokens. If it is not included in the request body, its default value (`null`) is used and the User Token does not expire. 
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "secret_dict":{
        "serial":"b0f531c3-914b-4408-9895-b61f59c06b66"
    },
    "options":[
        "create",
        "refresh"
    ],
    "validity_ts":1667040148
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/token' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "secret_dict":{
        "serial":"b0f531c3-914b-4408-9895-b61f59c06b66"
    },
    "options":[
        "create",
        "refresh"
    ],
    "validity_ts":1667040148
}
'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_token_body = {
    "secret_dict": {
        "serial": "b0f531c3-914b-4408-9895-b61f59c06b66"
    },
    "options": [
        "create",
        "refresh"
    ],
    "validity_ts": 1667040148
}

status, response = TT_SERVICE.create_user_token(body=user_token_body)
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description |
|:----------------|:----------|:------------|
| **token**       | string    | User Token.        |
| **validity_ts** | timestamp | Time at which the User Token expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Token does not expire.                               | 
| **options**      | list of strings, value is `"create"` or `"refresh"` | Defines whether a new User Token can be created or refreshed with the generated token. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "token":"kxjCrGbjTC9rLHAMkSWpozreJfdRtsiv/asbwD5eclbwiGHBer8rtvbtq39q",
    "validity_ts":1667040148,
    "options":[
        "create",
        "refresh"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>


</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
