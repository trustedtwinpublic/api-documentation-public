---
tags:
  - user token
---

# User Token

<div class="row">
<div class="column">

## About

A User Token is a token with JWT functionality. It is generated on the base of the [User Secret (API key)](../secrets/index.md) of the calling user. Multiple User Tokens can be generated for a single User Secret (API key).

The purpose of the User Token is to support direct communication between a web application and the Trusted Twin API. While a User Secret (API key) is intended for use by backend applications that require access to the Trusted Twin API, a User Token is intended for use by frontend applications. A User Token allows for direct access to the Trusted Twin API bypassing backend applications. 

## User Token attributes

<TableWrapper>
| Attribute       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:----------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **token**       | User Token. It is an alphanumeric string.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **validity_ts** | The `"validity_ts"` attribute defines when the User Token expires. If the `"validity_ts"` is not provided in the request body when creating or refreshing a User Token, the default value (`"validity_ts"`: `null`) is used and the User Token does not expire.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| **options**     | When creating a User Token, the user creating the User Token defines in the `"options"` whether the User Token can be used to create a new User Token (`"options": ["create"]`) and refresh the User Token (`"options":["refresh"]`), or both create a new User Token and refresh the existing one (`"options": ["create", "refresh"]`).                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **secret_dict** | A User Token can optionally contain a `"secret_dict"`. It defines variables that can be used in the access rules (see [Rules](../../overview/rules.md)). The variables provided in the User Token are equivalent to variables provided in the `X-TrustedTwin` custom header (see [Custom headers](../../account-access-and-logs/custom-headers.md)). The variables provided in the User Token have priority over variables provided in the `X-TrustedTwin` header when using the User Token (i.e. they replace the variables). The user using the User Token is not able to see the `"secret_dic"` of their User Token. If a user refreshes their User Token or creates a new User Token with their User Token, the refreshed User Token or the new User Token will inherit the `"secret_dict"` with its value. |
</TableWrapper>


## User Token permissions

A User Token inherits the permissions of the creating user (i.e., the permissions from current settings of the user role associated with the User Secret (API key) of that user). Therefore, we recommend creating a dedicated user and User Secret (API key) for generating User Tokens with limited permissions. </br>

## User Token personalization (`"secret_dict"`)

A User Token can be personalized by using a `“secret_dict”` to limit User Token access to only selected resources (e.g., Twins, Entries, Identities). The `“secret_dict”` is a dictionary containing variables that will be passed to authorization [rules](../../overview/rules.md) and [templates](../../overview/templates.md). It is not visible to the user creating the User Token. If both the User Token and the `"X-TrustedTwin"` [custom header](../../account-and-access/custom-headers.md) are provided, the `“secret_dict”` passed in the User Token takes precedence over variables of the same name passed in the `“X-TrustedTwin”` custom header. <br/>

## Creating and refreshing User Tokens with a User Token

A User Token can be used to create new User Tokens or update the `"validity_ts"` of the User Token (refresh the User Token).  A “refreshed” User Token should be treated as a copy of this User Token with updated validity (i.e, the `“secret_dict”` is inherited from the User Token that was used to refresh it). Refreshing an existing User Token does not automatically invalidate the User Token that was used to refresh it. <br/>

## Deactivating User Tokens

A User Token is invalidated when the `"validity_ts"` of that User Token is reached. As well, because User Tokens are always subordinate to the User Secret (API key) used to generate it, when a User Secret (API key) is invalidated, all User Tokens that were generated on the base of this User Secret (API key) are automatically invalidated. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /token | [create_user_token](./create-user-token.md) |
| POST | /token/refresh | [refresh_user_token](./refresh-user-token.md) | 
</EndpointsTable>

::: footnote *
In order for a user to be able to create User Tokens using the User Secret (API key) authorization, the permission "create_user_token" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). <br/>
For a user to be able to create and refresh User Tokens using the User Token authorization, the respective "create" or "refresh" permissions must be included in the `"options"` list of the User Token.
:::

</div>
</div>
