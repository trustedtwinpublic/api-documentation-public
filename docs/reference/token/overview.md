# Overview

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description |
|:----------------|:----------|:------------|
| **token**       | string    | User Token.        |
| **validity_ts** | timestamp | Time at which the User Token expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Token does not expire.                               | 
| **options**      | list of strings, value is `"create"` or `"refresh"` | Defines whether a new User Token can be created or refreshed with the generated token. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example User Token" copyable>
<CodeBlock title="json">

```json
{
    "token":"kxjCrGbjTC9rLHAMkSWpozreJfdRtsiv/asbwD5eclbwiGHBer8rtvbtq39q",
    "validity_ts":1667040148,
    "options":[
        "create",
        "refresh"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>



