# Refresh User Token

<div class="row">
<div class="column">

This endpoint refreshes a User Token. Refreshing a User Token updates the `"validity_ts"` of the User Token. A “refreshed” User Token should be treated as a copy of this User Token with updated validity (i.e, the `“secret_dict”` is inherited from the User Token that was used to refresh it). Refreshing an existing User Token does not automatically invalidate the User Token that was used to refresh it.

</div>
<div class="column">


<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /token/refresh | refresh_user_token |
</EndpointsTable>

::: footnote *
For a user to be able to refresh User Tokens using the User Token authorization, the "refresh" permission must be included in the `"options"` list of the User Token.
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                       | Type      | In   | Description                                                                                                                                                                                                             |
|:--------------------------------|:----------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **validity_ts**<br/>*optional** | timestamp | body | Time at which the User Token expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If not provided, the User Token does not expire. |
</TableWrapper>

::: footnote *
The `"validity_ts"` parameter is optional and does not need to be included in the request body when refreshing User Tokens. If it is not included in the request body, its default value (`null`) is used and the refreshed User Token does not expire. 

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "validity_ts":1672400548
}
```

</CodeBlock>
</ExtendedCodeGroup>
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/token/refresh' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "validity_ts":1672400548
}
'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_token_update_body = {
    "validity_ts": 1667040148
}

status, response = TT_SERVICE.refresh_user_token(body=user_token_update_body)
```
</CodeBlock>
</ExtendedCodeGroup>


</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description |
|:----------------|:----------|:------------|
| **token**       | string    | User Token. |
| **validity_ts** | timestamp | Time at which the User Token expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the User Token does not expire.                               |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "token":"kxjCrGbjTC9rLHAMkSWpozreJfdRtsiv/asbwocbireqGjddwevf87654",
    "validity_ts":1672400548
  
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
