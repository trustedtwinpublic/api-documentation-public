---
tags:
  - history
  - history endpoints
  - history attributes
meta:
  - name: description
    content: 
---

# History


<div class="row">
<div class="column">


## About

The History service lets you store historical values of [Ledger](../ledgers/index.md) Entries with a timestamp denoting when the value was changed. 

Every time a value of an Entry with the History service enabled is changed (i.e., via request to the [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md) and [update_twin_ledger_entry_value](../ledgers/update-twin-ledger-entry-value.md) endpoint), a new item is added to the history. The historical records, as well as the current value of the Entry, can be viewed at any time by sending a request to the [get_twin_ledger_entry_history](../history/get-twin-ledger-entry-history.md) endpoint. 


</div>
<div class="column">


<EndpointsTable>
| Method | Path                                   | Operation*                                                      |
|:-------|:---------------------------------------|:----------------------------------------------------------------|
| GET    | /twins/{twin}/ledgers/{ledger}/history** | [get_twin_ledger_entry_history](./get-twin-ledger-entry-history.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_ledger_entry_history" operation, the "get_twin_ledger_entry_history" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::


</div>
</div>


## Enabling History service

The History service is enabled at the Entry level through adding the `"history"` parameter to a [Ledger](../ledgers/index.md) Entry with a value matching the following [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. The value will denote a time period `([1-9][0-9]{0,2}[DWMY])` (historical records will be stored for the given time period subject to the 1000 records limit) or infinity `(INF)` (the most recent 1000 records will be stored).

 The `"history"` attribute can be added when creating an Entry (see [add_twin_ledger_entry](../ledgers/add-twin-ledger-entry.md)) or when updating an Entry (see [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md)). 

## History configuration

History is configured at the Entry level.
- If an Entry does not have a `"history"` attribute, the History service is not enabled for the Entry.
- If the `"history"` attribute is set to a time period (e.g., days, weeks, months), the historical records of the Entry are stored for the given time period subject to the maximum records limit (1000).
- If the `"history"` attribute is set to `INF`, the most recent 1000 records are stored.    

## Disabling History service 

You can disable the History service for an Entry by sending a request to the [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md) endpoint with the `"history"` parameter value set to `null`.

## Configuration examples

### No `"history"` attribute

The Entry below has no `"history"` attribute. The History service is not enabled for the Entry.

<ExtendedCodeGroup title="No 'history' attribute" copyable>
<CodeBlock title="json">

```json
{
    "entries":{
        "gdynia":{
            "value":15
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### `"history"` set to `"2W"` (2 weeks)

The Entry below has the `"history"` attribute set to `"2W"` (2 weeks). The History service will store 2 weeks of historical values of the Entry. If the number of stored records exceeds 1000, the most recent 1000 records will be stored.

<ExtendedCodeGroup title="'history' attribute set to a time period" copyable>
<CodeBlock title="json">

```json
{
    "entries":{
        "warszawa":{
            "value":17,
            "history":"2W"
        }
    }
}
```

</CodeBlock>
</ExtendedCodeGroup>

### `"history"` set to `INF`

The Entry below has the `"history"` attribute set to `INF`. The History service will store Entry value changes for an infinite (`INF`) period of time, unless the number of records exceeds 1000. If the number of records exceeds 1000, the most recent 1000 records will be stored.

<ExtendedCodeGroup title="'history' attribute set to null" copyable>

<CodeBlock title="json">

```json
{
    "entries":{
        "lublin":{
            "value":16,
            "history": "INF"
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

