# Get Twin Ledger Entry History

<div class="row">
<div class="column">

This endpoint retrieves the historical values of Ledger Entries with corresponding [Unix epoch timestamps](https://en.wikipedia.org/wiki/Unix_time). The timestamps denote when the Entry values from the given Ledger were updated.    

The limit of stored historical values of an Entry is 1000. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent Entry values will be stored. 

The soft quota of a stored item is 512 bytes. The value of the `"history"` attribute must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                   | Operation*                                                      |
|:-------|:---------------------------------------|:----------------------------------------------------------------|
| GET    | /twins/{twin}/ledgers/{ledger}/history** | get_twin_ledger_entry_history |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_ledger_entry_history" operation, the "get_twin_ledger_entry_history" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                      | Type                                                    | In           | Description                                                                                                                                                                       |
|:-------------------------------|:--------------------------------------------------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*      | string                                                  | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                      |
| **{ledger}**<br/>*required* *  | string                                                  | path         | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                    |
| **ge**<br/>*optional* **       | timestamp                                               | query string | Returns Entries where update time is greater than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                |
| **le**<br/>*optional* **      | timestamp                                               | query string | Returns Entries where update time is lower than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                  |
| **limit**<br/>*optional* **    | integer, `DEFAULT=100`                                    | query string | Limit on the number of Entry values to be returned in descending order. The default and maximum value is 100.                                                                                                           |
| **entries**<br/>*optional* *** | string | query string | Keys of the Entries to be returned.                                                                                                                               |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::

::: footnote **
The `ge`, `le`, `limit`, and `entries` parameters are optional and do not need to be included in the request. If they are not included in the request, their default values are used. If the number of the records to be returned exceeds the `limit` provided in the request:
- if the `ge` parameter is provided, the records to be returned are counted starting from the timestamp provided.
- if the `le` parameter is provided, the records to be returned are counted from the timestamp provided.
- if both `ge` and `le` parameters are provided, the records to be returned are counted from the timestamp provided in the `le` parameter (i.e., the most recent records are returned).
:::

::: footnote ***
If no Entry keys are specified, all Entries will be returned in the response filtered by the `"ge"`, `"le"`, and/or `"limit"` parameters (if these parameters have been included in the query string).
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal/history' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_twin_ledger_entry_history("0c0832ea-36f4-4459-aefd-8422f550e4b1")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const LedgerEntriesHistory = await twinsApi.getTwinLedgerEntryHistory({
    twin: "0c0832ea-36f4-4459-aefd-8422f550e4b1",
    ledger: "personal",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns a list of Entries with the history of Entry value changes in form of key-value pairs.

<TableWrapper>
| Attribute   | Type       | Description                                                                                    |
|:------------|:-----------|:-----------------------------------------------------------------------------------------------|
| **entries** | dictionary | Key-value pairs:<br/> - key: Time when the Entry value was changed <br/> - value: Entry value. |a
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>

<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "1651074230.191": 20.9,
            "1651074132.101": 20.9,
            "1651074123.54": 21,
            "1651074114.147": 19.2,
            "1651074109.225": 19.1,
            "1651073498.69": 15
        },
        "krakow": {
            "1651074230.191": 21,
            "1651073498.69": 21
        },
        "wroclaw": {
            "1651073715.03": 21,
            "1651073498.69": 21
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
