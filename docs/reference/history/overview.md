# Overview

<div class="row">
<div class="column">

The response to the [get_twin_ledger_entry_history](./get-twin-ledger-entry-history.md) returns a list of Entries with the history of Entry value changes in form of key-value pairs.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>

<CodeBlock title="json">

```json
{
    "entries": {
        "warszawa": {
            "1651074230.191": 20.9,
            "1651074132.101": 20.9,
            "1651074123.54": 21,
            "1651074114.147": 19.2,
            "1651074109.225": 19.1,
            "1651073498.69": 15
        },
        "lublin": {
            "1651074230.191": 21,
            "1651073498.69": 21
        },
        "gdynia": {
            "error": "History service not enabled for the Entry”."
        },
      "sopot": {
            "error": "History service not enabled for the Entry”."
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>


