# Overview

<div class="row">
<div class="column">


<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                               |
|:---------------|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **batch**   | string    | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                              |
| **status**     | string, value can be `"new"`, `"copying"`, `"copied"`, `"hash_mismatch"`, `"validating"`, `"validation_failed"`, `"processing"`, `"processed"`, `"writing_results"`, `"cancel"`, `"cancelling"`, `"cancelled"`, `"deleting"`, `"completed"`   | Status of the batch. For more details, see the [Batch statuses](./index.md#batch-statuses) section.|
| **created_ts** | timestamp | Time at which the batch was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
| **updated_ts** | timestamp     | Time at which the batch was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time) |
| **hash** * | string | [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2). |
</TableWrapper>

::: footnote *
The `"hash"` attribute is returned only if it was provided in the request to the endpoint.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "batch": "b1f75411-41c1-4dff-95e2-bff4d2660a01",
    "status": "new",
    "created_ts": 1693494617.199,
    "updated_ts": 1693494617.199
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
