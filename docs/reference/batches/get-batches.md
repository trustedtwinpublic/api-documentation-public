---
tags:
  - batch operations
  - bulk operations
  - get batches
---

# Get batches

<div class="row">
<div class="column">

This endpoint retrieves batches and their corresponding statuses of the batch operations of the account of the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                        | Operation*    |
|:-------|:----------------------------|:--------------|
| GET   | /batches        | get_batches |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_batches" operation, the "get_batches" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). 
:::

</div>
</div>

## Request


<div class="row">
<div class="column">

<TableWrapper>
| Parameter                             | Type                    | In           | Description                                                                                                |
|:--------------------------------------|:------------------------|:-------------|:-----------------------------------------------------------------------------------------------------------|
| **cursor**<br/>*optional*           | string                  | query string         | Position of the cursor denoting the start of the list of batches to be returned. |
| **ge**<br/>*optional* **       | timestamp                                               | query string | Returns batches where creation time is greater than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                |
| **le**<br/>*optional* **      | timestamp                                               | query string | Returns batches where creation time is lower than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                  |
| **limit**<br/>*optional* **    | integer, `DEFAULT=1000`                                    | query string | Limit on the number of batches to be returned. The default and maximum value is 1000.                                                                                                           |

</TableWrapper>

::: footnote *
The `"cursor"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, its default value is used.
::: 

::: footnote **
The `ge`, `le`, and `limit` parameters are optional and do not need to be included in the request. If they are not included in the request, their default values are used. If the number of the records to be returned exceeds the `limit` provided in the request:
- if the `ge` parameter is provided, the records to be returned are counted starting from the timestamp provided.
- if the `le` parameter is provided, the records to be returned are counted from the timestamp provided.
- if both `ge` and `le` parameters are provided, the records to be returned are counted from the timestamp provided in the `le` parameter (i.e., the most recent records are returned).
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/batches' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_batches()
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:----------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **batches** | dictionary | Key-value pairs:<br/>- key: Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). <br/>- value: Status of the batch. |

</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "batches": [
        {
            "batch": "c1a19b16-c5e6-4909-a787-cb9e4ac45c25",
            "status": "completed"
        }
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

