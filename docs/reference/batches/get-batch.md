---
tags:
  - batch operations
  - bulk operations
---

# Get batch

<div class="row">
<div class="column">

This endpoint cancels a given batch.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                        | Operation*    |
|:-------|:----------------------------|:--------------|
| GET   | /batches/{batch}        | get_batch |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_batch" operation, the "get_batch" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). 
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                        | Type                       | In   | Description                                                                                                                                                                                                                                                                                                               |
|:---------------------------------|:---------------------------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| **{batch}**<br/>*required* | string | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). |
| **download**<br/>*optional* * | boolean, `DEFAULT=False` | query string | Denotes whether to return a download URL or not. The download URL is a temporary URL allowing to download the results of the individual operations of the given batch.                 |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/batches/b1f75411-41c1-4dff-95e2-bff4d2660a01' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_batch("b1f75411-41c1-4dff-95e2-bff4d2660a01")
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">


<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                               |
|:---------------|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **batch**   | string    | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                              |
| **created_ts** | timestamp | Time at which the batch was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
| **status**     | string, value can be `"new"`, `"copying"`, `"copied"`, `"hash_mismatch"`, `"validating"`, `"validation_failed"`, `"processing"`, `"processed"`, `"writing_results"`, `"cancel"`, `"cancelling"`, `"cancelled"`, `"deleting"`, `"completed"`   | Status of the batch. For more details, see the [Batch statuses](./index.md#batch-statuses) section.
| **updated_ts** | timestamp     | Time at which the batch was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **hash** * | string | [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2). |
| **download**        **     | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Holds the temporary download URL and the validity of the download URL. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**validity_ts**</td>  <td> timestamp </td>  <td> Time at which the download URL expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **url** </td>  <td> string </td>  <td> URL allowing to download the results of the given batch operation. </td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                 | 
</TableWrapper>

::: footnote *
The `"hash"` attribute is returned only if it was provided in the request to the [create_batch](./create-batch.md) endpoint.
:::

::: footnote *
The `"download"` attribute is returned only if the parameter `"download"` was provided in the query string of the request.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "batch": "b1f75411-41c1-4dff-95e2-bff4d2660a01",
    "status": "copied",
    "created_ts": 1693494617.199,
    "updated_ts": 1693494617.199
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
