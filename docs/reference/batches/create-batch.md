---
tags:
  - batch operations
  - bulk operations
  - create batch
---

# Create batch

<div class="row">
<div class="column">

This endpoint creates a batch for a given operation. Please note that the permission of the operation requested to be performed in batch must be included in the statement of the role of the calling user.      
For more information about the format of files that can be uploaded in batch, please visit the [batch file](./index.md#batch-file) section.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                        | Operation*    |
|:-------|:----------------------------|:--------------|
| POST   | /batches        | create_batch |
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_batch" operation, the "create_batch" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). 
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                        | Type                       | In   | Description                                                                                                                                                                                                                                                                                                               |
|:---------------------------------|:---------------------------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| **handler**<br/>*required*        | string                                                                                  | body | URL handler generated through the [create_upload_url](../reference/cache/create-upload-url.md) endpoint.    |
| **operation**<br/>*required* | string | body | Name of the operation to be performed in batch. For a comprehensive list of operations, please see the [List of endpoints](../overview/api-reference-index.md) section.
| **hash**<br/>*optional* *         | string                                                                                  | body | [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2). It must match the [regular expression](https://regexr.com/) '^[a-f0-9]{64}$'. |
| **batch_dict**<br/>*optional* *         | dictionary                                                                                  | body | User-defined key-value pair:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON-compliant value. |
</TableWrapper>

::: footnote *
If the `"hash"` parameter is provided in the request, the method will check whether the hash provided by the user is equivalent to the hash generated for the batch. If they don't match, a copy of the batch will not be generated.     
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "handler": "3af21942-0309-44c9-b139-a371e18d395d-1693498126",
    "operation": "create_batch",
    "hash": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    "batch_dict": {
        "user": "team_1"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/batches' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "operation": "create_twin",
    "handler": "b1f75411-41c1-4dff-95e2-bff4d2660a01"
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

batch_body = {
        "operation": "create_twin",
        "handler": "b1f75411-41c1-4dff-95e2-bff4d2660a01"
}

status, response = TT_SERVICE.create_batch(body=batch_body)
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">


<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                               |
|:---------------|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **batch**   | string    | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                              |
| **status**     | string, value can be `"new"`, `"copying"`, `"copied"`, `"hash_mismatch"`, `"validating"`, `"validation_failed"`, `"processing"`, `"processed"`, `"writing_results"`, `"cancel"`, `"cancelling"`, `"cancelled"`, `"deleting"`, `"completed"`   | Status of the batch. For more details, see the [Batch statuses](./index.md#batch-statuses) section.                                                                                                                                                     | 
| **created_ts** | timestamp | Time at which the batch was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
| **updated_ts** | timestamp     | Time at which the batch was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time) |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "batch": "b1f75411-41c1-4dff-95e2-bff4d2660a01",
    "status": "new",
    "created_ts": 1693494617.199,
    "updated_ts": 1693494617.199
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

