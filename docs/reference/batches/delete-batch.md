---
tags:
  - batch operations
  - bulk operations
---

# Delete batch

<div class="row">
<div class="column">

This endpoint deletes a given batch.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                        | Operation*    |
|:-------|:----------------------------|:--------------|
| DELETE   | /batches/{batch}        | delete_batch |
</EndpointsTable>

::: footnote *
In order for a user to perform the "delete_batch" operation, the "delete_batch" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). 
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                        | Type                       | In   | Description                                                                                                                                                                                                                                                                                                               |
|:---------------------------------|:---------------------------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| **{batch_id}**<br/>*required* | string | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/batches/b1f75411-41c1-4dff-95e2-bff4d2660a01' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.delete_batch("b1f75411-41c1-4dff-95e2-bff4d2660a01")
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">


<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                               |
|:---------------|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **batch**   | string    | Batch [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                              |
| **status**     | string, value can be `"new"`, `"copying"`, `"copied"`, `"hash_mismatch"`, `"validating"`, `"validation_failed"`, `"processing"`, `"processed"`, `"writing_results"`, `"cancel"`, `"cancelling"`, `"cancelled"`, `"deleting"`, `"completed"`   | Status of the batch. For more details, see the [Batch statuses](./index.md#batch-statuses) section.
| **created_ts** | timestamp | Time at which the batch was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
| **updated_ts** | timestamp     | Time at which the batch was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **hash** * | string | [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2). |
</TableWrapper>

::: footnote *
The `"hash"` attribute is returned only if it was provided in the request to the [create_batch](./create-batch.md) endpoint.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "batch": "b1f75411-41c1-4dff-95e2-bff4d2660a01",
    "status": "deleting",
    "created_ts": 1693494617.199,
    "updated_ts": 1693494617.199
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
