---
tags:
  - batch
  - batch endpoints
  - bach attributes
meta:
  - name: description
    content: 
---

# Batch

<div class="row">
<div class="column">

## About

On the Trusted Twin platform, you can perform operations in batch. A batch can contain one operation type at a time. A maximum of 100,000 single operations can be processed in one batch. 

## Batch file

A batch to be processed is uploaded in a file. One line of the file corresponds to one operation. Empty payloads should be marked as `{}`. Empty lines will result in a validation error.

### Example batch files 

Below you can find view the contents of an example batch file for the [create_twin](../twins/create-twin.md) operation. Each line of the file creates a new Twin. There will be three Twins created:
1. Twin with description `{"description": {"name": "first twin"}}`
2. Twin without description
3. Twin with description `{"description": {"name": "third twin"}}`

<TableWrapper>
| example_file_create_twin.txt                                                                                   |
|:---------------------------------------------------------------------------------------------------------------|
| {"body": {"description": {"name": "first twin"}}}<br/>{}<br/>{"body": {"description": {"name": "third twin"}}} |
</TableWrapper> 

Below you can view the contents of an example batch file for the [add_twin_ledger_entry](../../reference/ledgers/add-twin-ledger-entry.md) operation. Each line of the file creates a new Entry in the Ledger of a given Twin. There will be two Entries created:
1. Entry named `"gdansk"` in the Ledger of the Twin with the Twin UUID `"7700f158-4627-4fe6-8178-4946f296e5d2"`,
2. Entry named `"sopot"` in the Ledger of the Twin with the Twin UUID `"b1855303-8e36-4756-8a3f-cd893258896f"`.

<TableWrapper>
| example_file_add_twin_ledger_entry.txt                                                                                                 |
|:---------------------------------------------------------------------------------------------------------------------------------------|
| {"resources": {"twin": "7700f158-4627-4fe6-8178-4946f296e5d2", "ledger": "personal"}, "body": {"entries": {"gdansk": {"value": 123}}}}<br/>{"resources": {"twin": "b1855303-8e36-4756-8a3f-cd893258896f", "ledger": "personal"}, "body": {"entries": {"sopot": {"value": 456}}}}  |
</TableWrapper>

## Uploading a batch file
    
A batch file needs to be uploaded to a temporary URL. The process consists of two steps:
1. Generate a temporary URL through the [create_upload_url](../cache/create-upload-url.md) endpoint. 
2. Upload the file through the temporary upload URL (PUT request).

## Cancelling a batch file upload

To cancel the upload process of a batch file, make a request to the [update_batch](./update-batch.md) endpoint.

## Batch statuses

<TableWrapper>
| Status            | Description                                                                                                                                                                                                   |
|:------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **new**               | A new batch job is started. The job automatically moves into the `"copying"` state.                                                                                                                           |
| **copying**           | Batch tasks have been validated and the batch file is being copied.                                                                                                                                           |
| **copied**            | A copy of the batch file has been successfully generated.                                                                                                                                                     |
| **hash_mismatch**     | The "hash" provided in the request does not match the hash of the batch file. A copy of the natch file could not be generated. Once a job reaches this state, it can only transition to the `"deleting"` state. Batch deletion can be either performed through the delete_batch endpoint or it executes automatically for batches older than 30 days. |
| **validating**        | Batch tasks are being validated.                                                                                                                                                                              |
| **validation_failed** | Batch tasks could not be validated. Once a job reaches this state, it can only transition to the `"deleting"` state. Batch deletion can be either performed through the delete_batch endpoint or it executes automatically for batches older than 30 days. |
| **processing**        | Batch tasks are being processed.                                                                                                                                                                              |
| **processed**         | All batch tasks have been validated, have failed validation, and/or have been cancelled. The results file still needs to be written.                                                                          |
| **writing results**   | Writing results to the results file.                                                                                                                                                                          |
| **cancel**            | Status provided in request body of the [update_batch](./update-batch.md) endpoint denoting that the batch should be cancelled. Batch job is waiting for cancelling process to start, tasks are no longer taken for processing. |
| **cancelling**        | The batch job and its tasks are being cancelled.  |
| **cancelled**         | The batch job has successfully been cancelled. Results file can be downloaded for tasks completed before the batch job was cancelled. Once a job reaches this state, it can only transition to the `"deleting"` state. Batch deletion can be either performed through the delete_batch endpoint or it executes automatically for batches older than 30 days.                                                                       |
| **deleting**          | The batch tasks are being deleted. Request and results files are being removed. After deletion of the batch, tasks and results are no longer present in the system.                                           |
| **completed**         | The batch job has been completed. The `"completed"` state is a terminal state. Once a job reaches this state, it can only transition to the `"deleting"` state. Batch deletion can be either performed through the [delete_batch](./delete-batch.md) endpoint, or it executes automatically for batches older than 30 days. |
</TableWrapper>

</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*                                       |
|:-------|:------------------------------|:-------------------------------------------------|
| POST   | /batches            | [create_batch](./create-batch.md)                   |
| GET    | /batches/{batch} | [get_batch](./get-batches.md)                         |
| GET    | /batches           | [get_batches](./get-batches.md)                         |
| PATCH  | /batches/{batch} | [update_batch](./update-batch.md)                   |
| DELETE | /batches/{batch} | [delete_batch](./delete-batch.md)                   |
</EndpointsTable>


::: footnote *
In order for a user to perform batch operations, the respective permissions "create_upload_url", "create_batch", "get_batch", "get_batches", "update_batch", and "delete_batch" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

