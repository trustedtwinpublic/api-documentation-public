# Overview

<div class="row">
<div class="column">

In our example, the Doc with the name `"Certification/certification_2022_v1.txt"` is stored in the `"Certification"` folder (`"Certification/"`):
- The `"storage_class"` of the Doc is `"access_optimized"`. This storage class is for frequently accessed data.
- The Doc was last updated on the 28th of March 2022 at 12:27:49.687 p.m. GMT (`"updated_ts": 1648470469.687"`).
- The `"status"` of the Doc is `"ok"`. The Doc was copied, the copy of the Doc was successfully attached to the Twin, and it is stored in the Twin's directory.
- The `"description"` holds attributes of the Doc in form of key-value pairs.

</div>
<div class="column">

<ExtendedCodeGroup title="Example Doc" copyable>
<CodeBlock title="json">
```json
"Certification/certification_2022_v2.txt": {
    "creation_certificate": {
        "created_ts": 1648470469.687,
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "hash": "d8782455adf5f103c8e4cfc7fb145698a57f7d8f831cf791261eac551d69a79e", 
        "size": 59736
    },
    "storage_class": "access_optimized",
    "updated_ts": 1648470469.687,
    "status": "ok",
    "description": {
        "title": "certification_2022_v1",
        "author": "Ana Ramos", 
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute                | Type                                                                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-------------------------|:--------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **creation_certificate** | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon sending a request to copy the Doc ([attach_twin_doc](./attach-a-doc.md) endpoint). It cannot be modified after it has been generated. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**created_ts**</td>  <td> timestamp </td>  <td>Time at which the Doc was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Account creating the Doc.</td>  </tr>  <tr>  <td> **hash** * </td>  <td> string </td>  <td> [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2).  </td>  </tr> <tr>  <td> **size** </td>  <td> integer </td>  <td> Size of the Doc in bytes. </td>  </tr>  </tbody>  </table></TableWrapper> </details> |                                                                                                                                                                                                                                                                                        
| **storage_class**        | string, value can be `"access_optimized"` or `"cost_optimized"`           | Storage class of the Doc:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to the Twin, the `"storage_class"` cannot be changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | 
| **status**               | string, value can be `"copying"`, `"ok"`, `"error"`, or `"hash_mismatch"` | Status of the Doc:<br/> - `"copying"`: The Doc is in the process of being copied.<br/> - `"ok"`: The copy of the Doc has been successfully generated. It is attached to the Twin and stored in the Twin's directory.<br/> - `"hash_mismatch"`: The `"hash"` provided in the request does not match the hash of the Doc. A copy of the Doc could not be generated.<br/> - `"error"`: An error occurred and a copy of the Doc could not be generated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **updated_ts**           | timestamp                                                                 | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 
| **description**          | dictionary                                                                | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 
</TableWrapper>

::: footnote *
The `"hash"` attribute is only displayed in the `"creation_certificate"` if it was provided in the request to the [attach_twin_doc](./attach-twin-doc.md) endpoint.
:::
