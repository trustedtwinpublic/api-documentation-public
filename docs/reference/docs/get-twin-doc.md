---
tags:
  - download doc
  - download file
---

# Get Twin Doc

<div class="row">
<div class="column">

This endpoint retrieves the details od the given Doc. You can include the optional query string parameter `"download"` in the request to generate a temporary download URL to download the Doc in `.zip` format.

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
The download URL is temporary and expires after 60 minutes. After the URL has expired, you will need to generate a new download URL.
After the download URL has been generated, you need to send a GET request to that URL to download the given Doc.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                                                     | Operation* |
|:-------|:-------------------------------------------------------------------------|:-----------|
| GET    | /twins/{twin}/docs/{doc_name}                                            | get_twin_doc |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_doc" operation, the "get_twin_doc" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                     | Type                     | In           | Description                                                                                                                                                                                      |
|:------------------------------|:-------------------------|:-------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*     | string                   | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                     |
| **doc_name**<br/>*required* | string                   | path         | Name of the Doc. It must match the [regular expression](https://regexr.com/) `^([a-zA-Z0-9_-]{1,32}\/){0,5}([a-zA-Z0-9_-]{1,32})(.[a-zA-Z0-9]{1,8})?$`. It must be unique in the context of the Twin. |
| **download**<br/>*optional* * | boolean, `DEFAULT=False` | query string | Denotes whether to return a download URL or not. The download URL is a temporary URL allowing to download a copy of the Doc.                 |
| **validity_ts**<br/>optional ** | timestamp | query string | Time at which the download URL expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If not provided, the download URL expires 60 minutes after it has been generated.  |
</TableWrapper>

::: footnote *
The `"download"` parameter is optional and does not need to be included in the request. If it is not included, its default value is used. 
:::

::: footnote **
The `"validity_ts"` parameter is optional and does not need to be included in the request. If it is not included in the request, but the `download` parameter is set to `True`, the download URL expires 60 minutes after it has been generated. 
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/docs/Certifications%2Fcertification_2022_v1.txt?download=true' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_twin_doc("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", "Certifications/certification_2022_v1.txt", params={"download": True})
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const getDoc = await docsApi.getTwinDoc({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    docName: "Certifications/certification_2022_v1.txt",
    download: true,
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute                | Type                                                                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-------------------------|:--------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **creation_certificate** | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon sending a request to copy the Doc ([attach_twin_doc](./attach-a-doc.md) endpoint). It cannot be modified after it has been generated. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**created_ts**</td>  <td> timestamp </td>  <td>Time at which the Doc was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Doc.</td>  </tr>  <tr>  <td> **hash** * </td>  <td> string </td>  <td> [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2).  </td>  </tr> <tr>  <td> **size** </td>  <td> integer </td>  <td> Size of the Doc in bytes. </td>  </tr>  </tbody>  </table></TableWrapper> </details> |                                                                                                                                                                                                                                                                                        
| **storage_class**        | string, value can be `"access_optimized"` or `"cost_optimized"`           | Storage class of the Doc:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to a Twin, the `"storage_class"` cannot be changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | 
| **status**               | string, value can be `"copying"`, `"ok"`, `"error"`, or `"hash_mismatch"` | Status of the Doc:<br/> - `"copying"`: The Doc is in the process of being copied.<br/> - `"ok"`: The copy of the Doc has been successfully generated. It is attached to the Twin and stored in the Twin's directory.<br/> - `"hash_mismatch"`: The `"hash"` provided in the request does not match the hash of the Doc. A copy of the Doc could not be generated.<br/> - `"error"`: An error occurred and a copy of the Doc could not be generated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **updated_ts**           | timestamp                                                                 | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 
| **description**          | dictionary                                                                | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 
| **download**             | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Holds the temporary download URL and the validity of the download URL. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**validity_ts**</td>  <td> timestamp </td>  <td> Time at which the download URL expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **url** </td>  <td> string </td>  <td> Download URL for the Doc. </td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                 | 
</TableWrapper>

::: footnote *
The `"hash"` attribute is only displayed in the `"creation_certificate"` if it was provided in the request to attach the Doc to the given Twin [attach_twin_doc](./attach-twin-doc.md) endpoint.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "creation_certificate": {
        "created_ts": 1648801647.877,
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "size": 59736
    },
    "storage_class": "access_optimized",
    "updated_ts": 1648801647.877,
    "description": {
        "title": "instructions_2022_v1",
        "author": "Ana Ramos"
    },
    "status": "ok",
    "download": {
        "url": "https://rest.trustedtwin.com/6fda5fe7e14c/Certifications%2Fcertification_2022_v1.txt&Expires=1648805895",
        "validity_ts": 1648805895.728
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## GET request

<div class="row">
<div class="column">

In order to access the Doc, you need to download it by sending a GET request to the download URL.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/6fda5fe7e14c/Certifications%2Fcertification_2022_v1.txt&Expires=1648805895' \
--form '=@"/path/to/file"'
```
</CodeBlock>

<CodeBlock title="Python">

```python
import requests

file_to_upload_path = "@/C:/Users/User_A/Documents"

response = requests.get("https://rest.trustedtwin.com/6fda5fe7e14c/Certifications%2Fcertification_2022_v1.txt&Expires=1648805895", data=open(file_to_upload_path, 'rb'))
print(response.text)
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
