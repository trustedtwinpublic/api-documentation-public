---
tags:
  - doc
  - doc endpoints
  - doc attributes
  - file
  - files
meta:
  - name: description
    content: 
---

# Doc

<div class="row">
<div class="column">

## About

A Doc on the Trusted Twin platform stores static content such as text, audio, video files. A Doc is attached to a Twin. 

## Attaching a Doc to a Twin
    
A Doc is attached to a Twin. The process of attaching a Doc to a Twin consists of two steps:
1. Generation of a temporary upload URL through the [create_upload_url](../cache/create-upload-url.md) endpoint. You will use the upload URL to upload the Doc to temporary storage.
2. Attaching of a copy of the Doc to the given Twin through the [attach_twin_doc](/attach-twin-doc.md) endpoint. 

## Doc attributes

<TableWrapper>
| Attribute | Description | 
|:------|:------|
| **creation_certificate** | A creation certificate is generated automatically by the system when a request is sent to create a copy of a Doc ([attach_twin_doc](./attach-a-doc.md)). It cannot be modified after it has been generated. It holds the [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) denoting the creation time of the creation certificate in the `"created_ts"` attribute, the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account requesting to attach the Doc to the Twin in the `"creator"` attribute, the [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2) of the Doc held in the `"hash"` attribute (if the `"hash"` attribute was provided in the body of the request when attaching the Doc to the Twin), and the size of the Doc in bytes held in the `"size"` attribute.|
| **storage_class** | Holds the storage class of the Doc. It can have one of the following values:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to a Twin, the `"storage_class"` cannot be changed.|
| **updated_ts** | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **status** | Status of the Doc. <br/> - `"copying"`: The Doc is in the process of being copied. <br/> - `"ok"`: A copy of the Doc was successfully generated. It is attached to the Twin and stored in the Twin's directory.<br/> - `"hash_mismatch"`: The hash provided in the request does not match the hash of the Doc. A copy of the Doc could not be generated.<br/> - `"error"`: An error occurred and a copy of the Doc could not be generated. |
| **description** |  User-defined attributes of the Twin in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value.<br/> For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. | 
</TableWrapper>


</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*                                       |
|:-------|:------------------------------|:-------------------------------------------------|
| POST   | /twins/{twin}/docs            | [attach_twin_doc](./attach-twin-doc.md)                   |
| GET    | /twins/{twin}/docs/{doc_name} | [get_twin_doc](./get-twin-doc.md)                         |
| GET    | /twins/{twin}/docs            | [get_twin_docs](./get-twin-docs.md)                         |
| PATCH  | /twins/{twin}/docs/{doc_name} | [update_twin_doc](./update-twin-doc.md)                   |
| DELETE | /twins/{twin}/docs/{doc_name} | [delete_twin_doc](./delete-twin-doc.md)                   |
| DELETE | /twins/{twin}/docs            | [delete_twin_docs](./delete-twin-docs.md)                   |
</EndpointsTable>


::: footnote *
In order for a user to perform operations on Docs, the respective permissions "create_upload_url", "invalidate_upload_url", "attach_twin_doc", "get_twin_docs", "delete_twin_docs", "get_twin_doc", "update_twin_doc", and "delete_twin_doc" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
