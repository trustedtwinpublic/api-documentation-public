---
tags:
  - notarization
---

# Attach Twin Doc

<div class="row">
<div class="column">

This endpoint creates a copy of a Doc previously uploaded to temporary storage through an upload URL (see [create_upload_url](../cache/create-upload-url.md) endpoint). The copy of the Doc is attached to the Twin and stored in the Twin's directory under the given `"doc_name"`.

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
In order to create a copy of a Doc and attach it to a Twin, the Doc needs to be already uploaded to temporary storage through an upload URL. The upload URL is returned in the response to the [create_upload_url](../cache/create-upload-url.md) endpoint.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                        | Operation*    |
|:-------|:----------------------------|:--------------|
| POST   | /twins/{twin}/docs          | attach_twin_doc |
</EndpointsTable>

::: footnote *
In order for a user to perform the "attach_twin_doc" operation, the "attach_twin_doc" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type                                                                                    | In   | Description                                                                                                                                                                                                                                                                                                                                                                                   |
|:----------------------------------|:----------------------------------------------------------------------------------------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*         | string                                                                                  | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                  |
| **doc_name**<br/>*required*       | string                                                                                  | body | Name of the Doc. It must match the [regular expression](https://regexr.com/) `^([a-zA-Z0-9_-]{1,32}\/){0,5}([a-zA-Z0-9_-]{1,32})(.[a-zA-Z0-9]{1,8})?$`. It must be unique in the context of the Twin.                                                                                                                                                                                             | 
| **hash**<br/>*optional* *         | string                                                                                  | body | [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2).                                                                                                                                                                                                                                                                                                                                          |                 
| **handler**<br/>*required*        | string                                                                                  | body | URL handler generated through the [create_upload_url](../reference/cache/create-an-upload-url.md) endpoint.                                                                                                                                                                                                                                                                                                          |
| **storage_class**<br/>*required*  | string, value is `"access_optimized"` or `"cost_optimized"`, `DEFAULT=access_optimized` | body | Storage class of the Doc:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to the Twin, the `"storage_class"` cannot be changed.                                                                                | 
| **description**<br/>*optional* ** | dictionary                                                                              | body | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/> If no fields have been added, the `"description"` attribute will not be displayed. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. | 
</TableWrapper>

::: footnote *
If the `"hash"` parameter is provided in the request, the method will check whether the hash provided by the user is equivalent to the hash generated for the Doc. If they don't match, a copy of the Doc will not be generated. Please note that the `"status"` of the Doc will have the value `"copying"` in the response to attaching the Doc to the Twin ([attach_twin_doc](./attach-twin-doc.md) endpoint). The `"hash_mismatch"` status will be returned when retrieving the Doc ([get_twin_doc](./get-twin-doc.md) endpoint).     
:::

::: footnote **
If no attributes are provided in the request body of `"description"` (`"description": {}`), the response will return an empty dictionary (`"description": {}`). <br/>    
If `"description"` with the value `null` is provided in the request body, the response will not return the `"description"` field. <br/>     
If the request body does not contain `"description"`, the response will not return the `"description"` field.
:::
   
In our example, we attach three Docs to the Twin. Each of the Docs will contain a copy of the file that we uploaded through the upload URL to temporary storage.

<ExtendedCodeGroup title="Example request body" copyable>

<CodeBlock title="json">

```json
{
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "access_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            }
        },
        "Certifications/certification_2022_v1_copy1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "cost_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 1
            }
        },
        "Certifications/certification_2022_v1_copy2.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 2
            }
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/docs' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
        "docs": {
            "Certifications/certification_2022_v1.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "storage_class": "access_optimized",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos"
                }
            },
            "Certifications/certification_2022_v1_copy1.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "storage_class": "cost_optimized",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos",
                    "copy": 1
                }
            },
            "Certifications/certification_2022_v1_copy2.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos",
                    "copy": 2
                }
            }
        }
    }'    
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

docs_body = {
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "access_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            },
        },
        "Certifications/certification_2022_v1_copy1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "cost_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 1
            },
        },
        "Certifications/certification_2022_v1_copy2.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 2
            }
        }
    }
}

status, response = TT_SERVICE.attach_twin_doc("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=docs_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const attachTwinDocumentResponse = await docsApi.attachTwinDoc({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    attachTwinDocument: {
        name: "Certifications/certification_2022_v1.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        storageClass: "access_optimized",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
        },
        name: "Certifications/certification_2022_v1_copy1.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        storageClass: "cost_optimized",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
            copy: 1, 
        },
        name: "Certifications/certification_2022_v1_copy2.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
            copy: 2,
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute                | Type                                                                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-------------------------|:--------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **creation_certificate** | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon sending a request to copy the Doc ([attach_twin_doc](./attach-a-doc.md) endpoint). It cannot be modified after it has been generated. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**created_ts**</td>  <td> timestamp </td>  <td>Time at which the Doc was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Doc.</td>  </tr>  <tr>  <td> **hash** * </td>  <td> string </td>  <td> [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2).  </td>  </tr> <tr>  <td> **size** </td>  <td> integer </td>  <td> Size of the Doc in bytes. </td>  </tr>  </tbody>  </table></TableWrapper> </details> |                                                                                                                                                                                                                                                                                        
| **storage_class**        | string, value can be `"access_optimized"` or `"cost_optimized"`           | Storage class of the Doc:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to the Twin, the `"storage_class"` cannot be changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | 
| **status**               | string, value can be `"copying"`, `"ok"`, `"error"`, or `"hash_mismatch"` | Status of the Doc:<br/> - `"copying"`: The Doc is in the process of being copied.<br/> - `"ok"`: The copy of the Doc has been successfully generated. It is attached to the Twin and stored in the Twin's directory.<br/> - `"hash_mismatch"`: The `"hash"` provided in the request does not match the hash of the Doc. A copy of the Doc could not be generated.<br/> - `"error"`: An error occurred and a copy of the Doc could not be generated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **updated_ts**           | timestamp                                                                 | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 
| **description**          | dictionary                                                                | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 
</TableWrapper>

::: footnote *
The `"hash"` attribute is only displayed in the `"creation_certificate"` if it was provided in the request to the [attach_twin_doc](./attach-twin-doc.md) endpoint.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "creation_certificate": {
                "created_ts": 1649167449.703,
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "size": 23
            },
            "storage_class": "access_optimized",
            "updated_ts": 1649167449.703,
            "status": "copying",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            }
        },
        "Certifications/certification_2022_v1_copy1.txt": {
            "creation_certificate": {
                "created_ts": 1649167449.703,
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "size": 23
            },
            "storage_class": "cost_optimized",
            "updated_ts": 1649167449.703,
            "status": "copying",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 1
            }
        },
        "Certifications/certification_2022_v1_copy2.txt": {
            "creation_certificate": {
                "created_ts": 1649167449.703,
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "size": 23
            },
            "storage_class": "access_optimized",
            "updated_ts": 1649167449.703,
            "status": "copying",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 2
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Response ('Doc already exists')

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="alert-circle" :size="1.75" />
If you try to create a Doc that already exists, you will receive the error message `"error": 'Doc already exists.'"` for this Doc in the response. However, the response itself will return the `201 Created` status code. Please see the below example for more details.
:::

In our example, we want to attach two Docs to the Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `3f359a50-65e2-4252-ba96-6fda5fe7e14c`:    
- The Doc with the name `"Certifications/certification_2022_v1.txt"`. A Doc with the name `"Certifications/certification_2022_v1.txt"` is already attached to the Twin.
- The Doc with the name `"Certifications/certification_2022_v1_copy3.txt"`. There is no Doc with this name attached to the Twin yet. 

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "access_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            }
        },
        "Certifications/certification_2022_v1_copy3.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "cost_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 3
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/3f359a50-65e2-4252-ba96-6fda5fe7e14c/docs' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
        "docs": {
            "Certifications/certification_2022_v1.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "storage_class": "access_optimized",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos"
                }
            },
            "Certifications/certification_2022_v1_copy3.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "storage_class": "cost_optimized",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos",
                    "copy": 3
                }
            }
        }
    }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

docs_body = {
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "access_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            },
        },
        "Certifications/certification_2022_v1_copy3.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "cost_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 3
            }
        }
    }
}

status, response = TT_SERVICE.attach_twin_doc("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=docs_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const attachTwinDocumentResponse = await docsApi.attachTwinDoc({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    attachTwinDocument: {
        name: "Certifications/certification_2022_v1.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        storageClass: "access_optimized",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
        },
        name: "Certifications/certification_2022_v1_copy3.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        storageClass: "cost_optimized",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
            copy: 3, 
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

The response returns the message `"error": "Doc already exists."` for the Doc `"Certifications/certification_2022_v1.txt"`, because a Doc with the name `"Certifications/certification_2022_v1.txt"` is already attached to the Twin. The status code of the response is `201 Created`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v1.txt": {
            "error": "Doc already exists."
        },
        "Certifications/certification_2022_v1_copy3.txt": {
            "creation_certificate": {
                "created_ts": 1648801834.23,
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "size": 59736
            },
            "storage_class": "cost_optimized",
            "updated_ts": 1648801834.23,
            "status": "copying",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos",
                "copy": 3
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Response ('No Doc uploaded through the upload URL')

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="alert-circle" :size="1.75" />
If you have not uploaded a Doc through the upload URL, but try to attach a Doc, you will receive the error message `"error": "No Doc uploaded through the upload URL - check handler []."` for this Doc in the response. However, the response itself will return the `201 Created` status code (see example below).
:::

In our example, we want to attach a Doc with the name `"Certifications/certification_2022_v3.txt"` to the Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `3f359a50-65e2-4252-ba96-6fda5fe7e14c`. We have previously sent a PUT request to the upload URL. However, we have not attached a file to that request.

<ExtendedCodeGroup title="Example request body" copyable>

<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v3.txt": {
            "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
            "storage_class": "access_optimized",
            "description": {
                "title": "instructions_2022_v1",
                "author": "Ana Ramos"
            }
        }
    }
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/3f359a50-65e2-4252-ba96-6fda5fe7e14c/docs' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
        "docs": {
            "Certifications/certification_2022_v3.txt": {
                "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
                "storage_class": "access_optimized",
                "description": {
                    "title": "instructions_2022_v1",
                    "author": "Ana Ramos"
                }
            }
        }
    }'
```
</CodeBlock>


<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

docs_body = {
    "Certifications/certification_2022_v1.txt": {
        "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        "storage_class": "access_optimized",
        "description": {
            "title": "instructions_2022_v1",
            "author": "Ana Ramos"
        }
    }
}

status, response = TT_SERVICE.attach_twin_doc("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", docs=docs_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const attachTwinDocumentResponse = await docsApi.attachTwinDoc({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    attachTwinDocument: {
        name: "Certifications/certification_2022_v1.txt",
        handler: "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
        storageClass: "access_optimized",
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

The response is the error message `"error": "No Doc uploaded through the upload URL - check handler []."`

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v3.txt": {
            "error": "No Doc uploaded through the upload URL - check handler [a7fbcd7c-50a7-4df5-a6e5-416559ae24ae]."
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

<TableWrapper>
| Status code | Message | Comment                                                                                                                                                                                                                                                                                                                                                                                                    |
|:------------|:--------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 201         | Created | If there is already a Doc with the given name attached to the Twin, you will receive the error: `"error": "Doc already exists."` for this Doc in the response.<br/>If you have not uploaded a Doc through the upload URL, but try to attach a Doc, you will receive the error message `"error": "No Doc uploaded through the upload URL - check handler []."` for this Doc in the response.|
</TableWrapper>
