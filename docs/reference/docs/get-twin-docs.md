# Get Twin Docs

<div class="row">
<div class="column">

This endpoint retrieves the Docs attached to the given Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*  |
|:-------|:------------------------------|:------------|
| GET    | /twins/{twin}/docs/           | get_twin_docs|
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_docs" operation, the "get_twin_docs" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type                                              | In           | Description                                                                              |
|:--------------------------|:--------------------------------------------------|:-------------|:-----------------------------------------------------------------------------------------|
| **{twin}**<br/>*required* | string                                            | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).             |
| **view**<br/>*optional* * | string, value is `tree` or `list`, `DEFAULT=list` | query string | Allows choosing between a tree and a list view.                                         |
| **mask**<br/>*optional* * | string, `DEFAULT=none`                            | query string | Allows specifying files to be returned using [regular expressions](https://regexr.com/). |
</TableWrapper>

::: footnote *
The `"view"` and `"mask"` parameters are optional and do not need to be included in the request. If they are not included in the request, their default values are used. 
:::

In our example, we retrieve Docs attached to the Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `6cbb62f2-a837-44dd-b02b-4567d5888a4e`.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/3f359a50-65e2-4252-ba96-6fda5fe7e14c/docs' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_twin_docs("f63ce1df-4643-49b2-9d34-38f4b35b9c7a")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const getDocs = await docsApi.getTwinDocs({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns a list of Docs attached to the Twin with the values of the `"created_ts"`, `"updated_ts"`, and `"size"` attributes. 

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                      |
|:----------------|:----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **created_ts**  | timestamp | Time at which the Doc was attached to the Twin. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **updated_ts**  | timestamp | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).         | 
| **size**        | integer   | Size of the Doc in bytes.                                                                                                                                                        | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "docs": {
        "Certifications/certification_2022_v1.txt": [
            1648801647.877,
            1648801647.877,
            59736
        ],
        "Certifications/certification_2022_v1_copy1.txt": [
            1648801647.877,
            1648801647.877,
            59736
        ],
        "Certifications/certification_2022_v1_copy2.txt": [
            1648801647.877,
            1648802574.79,
            59736
        ]
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
