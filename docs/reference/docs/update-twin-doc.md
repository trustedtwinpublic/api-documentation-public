# Update Twin Doc

<div class="row">
<div class="column">

This endpoint updates the `"description"` of the given Doc attached to the given Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*    |
|:-------|:------------------------------|:--------------|
| PATCH  | /twins/{twin}/docs/{doc_name} | update_twin_doc |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_twin_doc" operation, the "update_twin_doc" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                        | Type       | In   | Description                                                                                                                                                                                                                                                                                                                                                                                   |
|:---------------------------------|:-----------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{handler}**<br/>*required*        | string     | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                  |
| **doc_name**<br/>*required*    | string     | path | Name of the Doc. It must match the [regular expression](https://regexr.com/) `^([a-zA-Z0-9_-]{1,32}\/){0,5}([a-zA-Z0-9_-]{1,32})(.[a-zA-Z0-9]{1,8})?$`. It must be unique in the context of the Twin.                                                                                                                                                                                              |
| **description**<br/>*optional* * | dictionary | body | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/> If no fields have been added, the `"description"` attribute will not be displayed. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section. |
</TableWrapper>

::: footnote *
If the `"description"` field with attributes is provided in the request body, the `"description"` field from the request body will replace the current `"description"` field.<br/>       
If no attributes are provided in the request body of `"description"` (`"description": {}`), all attributes from the current `"description"` will be removed. The response will return an empty dictionary (`"description": {}`).<br/>        
If `"description"` with the value `null` is provided in the request body, all attributes from the current `"description"` will be removed. The response will not return the `"description"` field.<br/>    
If the request body does not contain `"description"`, no changes will be made to the `"description"` field.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "description": {
        "title": "instructions_2022_v1",
        "author": "Ana Ramos",
        "copy": 2,
        "region": "EMEA"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/3f359a50-65e2-4252-ba96-6fda5fe7e14c/docs/certification_2022_v1_copy2.txt' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
        "title": "instructions_2022_v1",
        "author": "Ana Ramos",
        "copy": 2,
        "region": "EMEA" }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

new_description_body = {
    "description": {
        "title": "instructions_2022_v1",
        "author": "Ana Ramos",
        "copy": 2,
        "region": "EMEA" }
}

status, response = TT_SERVICE.update_twin_doc("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", "Certifications/certification_2022_v1.txt", body=new_description_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const updatedDoc = await twinsApi.updateTwinDoc({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    docName: "Certifications/certification_2022_v1.txt",
    updateTwinDoc: {
        description: {
            title: "instructions_2022_v1",
            author: "Ana Ramos",
            copy: 2,
            region: "EMEA",
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns the details of the updated Doc.

<TableWrapper>
| Attribute                | Type                                                                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:-------------------------|:--------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **creation_certificate** | dictionary                                                                    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon sending a request to copy the Doc ([attach_twin_doc](./attach-a-doc.md) endpoint). It cannot be modified after it has been generated. </ExtendedSummary> <table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**created_ts**</td>  <td> timestamp </td>  <td>Time at which the Doc was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Account creating the Doc.</td>  </tr>  <tr>  <td> **hash** * </td>  <td> string </td>  <td> [SHA-256 hash](https://en.wikipedia.org/wiki/SHA-2).  </td>  </tr> <tr>  <td> **size** </td>  <td> integer </td>  <td> Size of the Doc in bytes. </td>  </tr>  </tbody>  </table> </details> |                                                                                                                                                                                                                                                                                        
| **storage_class**        | string, value can be `"access_optimized"` or `"cost_optimized"`           | Storage class of the Doc:<br/> - `"access_optimized"`: Storage class for frequently accessed data.<br/> - `"cost_optimized"`: Storage class for automatic cost savings for less frequently accessed data. <br/>Once the Doc has been copied and attached to a Twin, the `"storage_class"` cannot be changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | 
| **status**               | string, value can be `"copying"`, `"ok"`, `"error"`, or `"hash_mismatch"` | Status of the Doc:<br/> - `"copying"`: The Doc is in the process of being copied.<br/> - `"ok"`: The copy of the Doc has been successfully generated. It is attached to the Twin and stored in the Twin's directory.<br/> - `"hash_mismatch"`: The `"hash"` provided in the request does not match the hash of the Doc. A copy of the Doc could not be generated.<br/> - `"error"`: An error occurred and a copy of the Doc could not be generated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **updated_ts**           | timestamp                                                                 | Time at which the Doc was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 
| **description**          | dictionary                                                                | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
</TableWrapper>

::: footnote *
The `"hash"` attribute is only displayed in the `"creation_certificate"` if it was provided in the request to attach the [attach_twin_doc](./attach-twin-doc.md) endpoint.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "creation_certificate": {
        "created_ts": 1648801647.877,
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "size": 59736
    },
    "storage_class": "access_optimized",
    "updated_ts": 1648802574.79,
    "status": "ok",
    "description": {
        "title": "instructions_2022_v1",
        "author": "Ana Ramos",
        "copy": 2,
        "region": "EMEA"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
