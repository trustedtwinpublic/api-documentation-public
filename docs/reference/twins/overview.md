# Overview

<div class="row">
<div class="column">

## Alive Twin

In our example we see the response for a Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"f63ce1df-4643-49b2-9d34-38f4b35b9c7a"`. The Twin was created (`"creator"`) and is owned (`"owner"`) by the account with the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"9891264d-4a77-4fa2-ae7f-84c9af14ae3b"`. The `"status"` of the Twin is `"alive"` - it means that the `"description"` can be updated by the owner of the Twin.

</div>
<div class="column">

<ExtendedCodeGroup title="Twin example (alive Twin)" copyable>
<CodeBlock title="json">
```json
{
    "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "status": "alive",
    "updated_ts": 1646305579.969,
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": true,
        "company": "Shoes",
        "certifications": [
            "xptI2021",
            "fht76j",
            "IPS1449"
        ]
    },
    "creation_certificate": {
        "uuid": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1646305579.969
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

## Terminated Twin 

In our example we see the response for a Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"f63ce1df-4643-49b2-9d34-38f4b35b9c7a"`. It was created (`"creator"`), is owned (`"owner"`), and was terminated (`"issuer"`) by the account with the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"9891264d-4a77-4fa2-ae7f-84c9af14ae3b"`. The status of the Twin is `"terminated"` - it means that the `"description"` of the Twin cannot be updated. Ledger Entries, Identities, and Docs can be attached to alive and terminated Twins by all users involved in the process.

</div>
<div class="column">

<ExtendedCodeGroup title="Twin example (terminated Twin)" copyable>
<CodeBlock title="json">
```json
{
    "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "status": "terminated",
    "updated_ts": 1646305708.843,
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": true,
        "company": "Shoes",
        "certifications": [
            "xptI2021",
            "fht76j",
            "IPS1449"
        ]
    },
    "creation_certificate": {
        "uuid": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1646305579.969
    },
    "termination_certificate": {
        "issuer": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "terminated_ts": 1646305708.843
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute                   | Type                                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|:----------------------------|:---------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **owner**                   | string                                       | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account which is the current owner of the Twin. The ownership of a Twin can be transferred.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **status**                  | string, value is `"alive"` or `"terminated"` | The status of a Twin can be `"alive"` or `"terminated"`. In case of alive Twins, the `"description"` can be updated by the owner of the Twin. In case of terminated Twins, the `"description"` cannot be updated. Ledger Entries, Identities, and Docs can be attached to alive and terminated Twins by all users involved in the process.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **updated_ts**              | timestamp                                    | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **description**             | dictionary                                   | Attributes of the Twin defined by the owner in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value.<br/>Only the owner of the Twin can update the description of the Twin. <br/>For more details consult the [description](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **creation_certificate**    | dictionary                                       | <details> <ExtendedSummary markdown="span">Certificate generated automatically by the system upon creation of the Twin. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**uuid**</td>  <td> string </td>  <td>Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). It is generated automatically when the Twin is created and stored in the `"uuid"` field of the creation certificate. </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Twin.</td>  </tr>  <tr>  <td> **created_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  </td>  </tr>  </tbody>  </table></TableWrapper></details> |
| **termination_certificate** | dictionary                                       | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system when the Twin is terminated. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**issuer**</td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account terminating the Twin. </td>  </tr>   <tr>  <td> **terminated_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was terminated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                     |
</TableWrapper>
