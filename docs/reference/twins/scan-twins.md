# Scan Twins


<div class="row">
<div class="column">

This endpoint retrieves a list of Twin UUIDs of Twins owned by the account of the calling user after performing a Twin scan (if the `details` parameter is set to `False`) or details of the listed Twins (if the `details` parameter is set to `True`): 
- If the `details` parameter is set to `False` (default value), [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) of all Twins owned by the account of the calling user are returned, regardless whether they are visible to the calling user. 
- If the `details` parameter is set to `True`, only Twins visible to the calling user are returned. Please see the [request `details=True`](./scan-twins.md#request-detailstrue) and [response `details=True`](./scan-twins.md#response-detailstrue) sections for more information.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /twins | scan_twins    |
</EndpointsTable>

::: footnote *
In order for a user to perform the "scan_twins" operation, the "scan_twins" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                             | Type                    | In           | Description                                                                                                |
|:--------------------------------------|:------------------------|:-------------|:-----------------------------------------------------------------------------------------------------------|
| **cursor**<br/>*optional*           | string                  | query string         | Position of the cursor denoting where the scan of Twins should be started. |
| **count**<br/> *optional* * | integer | query string, `DEFAULT=100` | Number of Twins to be scanned. The minimum number of Twins to be scanned is 1, the maximum is 1000.                               |
| **match**<br/> *optional* ** | string | query string | [Rule](../../overview/rules.md) to be applied to the Twin scan.                                     |
| **details**<br/> *optional* *** | boolean, `DEFAULT=False` | query string | Denotes whether the details of each Twin should be returned.                                     |
</TableWrapper>

::: footnote *
The `"cursor"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, its default value is used, i.e. the scan is starting from the beginning of the list.
::: 

::: footnote **
The `"count"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, its default value is used. 
::: 

::: footnote **
The `"match"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, no [Rule](../../overview/rules.md) is used when scanning the Twins. 
::: 

::: footnote ***
The `"details"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, a list of Twin UUIDs is returned. 
::: 



</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins?cursor=%20\
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.scan_twins()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const twin = await twinsApi.scanTwins();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **cursor**       | string    | Position of the cursor denoting where the scan of Twins has finished. If no more Twins are to be scanned, the returned value is `null`. |
| **uuid**       | string    | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                              |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "cursor": "MDdjYjBlMjItYWYxYS00YTMzLTgxNDYtMmU1ZjljMTE4MjU2",
    "twins": [
        "f98bd395-c41d-4a80-8cdd-86cd275a5f3d",
        "aa445449-138e-4a20-87df-d78edbb845b3",
        "b482b1b7-481f-49a3-8f61-50f19d823941",
        "162d4f02-6edb-404f-bc20-5137ba4556f4",
        "fd9e374d-0349-4006-b6e8-220cb115ed84",
        "56e979d7-f361-4460-a412-1e46a22b8730",
        "6e2ccbe2-8e3a-4705-adc3-8dc38eb9109e",
        "ee1cb9fc-f725-43b0-8858-441315834da0",
        "67f5a84a-e332-4525-8341-0ea6bf6d54c9",
        "2c3f02e4-9e52-4759-9158-99278ccfa085"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Request (`details=True`)

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                             | Type                    | In           | Description                                                                                                |
|:--------------------------------------|:------------------------|:-------------|:-----------------------------------------------------------------------------------------------------------|
| **cursor**<br/>*optional* *           | string, `DEFAULT= `                  | query string         | Position of the cursor denoting where the scan of Twins should be started. |
| **count**<br/> *optional* ** | integer, `DEFAULT=100` | query string  | Number of Twins to be scanned. The minimum number of Twins to be scanned is 1, the maximum is 1000.                               |
| **match**<br/> *optional* *** | string | query string | [Rule](../../overview/rules.md) to be applied to the Twin scan.                                     |
| **details**<br/> *optional* **** | boolean, `DEFAULT=False` | query string | Denotes whether the details of each Twin should be returned.                                     |
</TableWrapper>

::: footnote **
The `"cursor"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, a new scan is started. 
::: 

::: footnote **
The `"count"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, its default value is used. 
::: 

::: footnote **
The `"match"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, no [Rule](../../overview/rules.md) is used when scanning Twins. 
::: 

::: footnote ***
The `"details"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, a list of Twin UUIDs is returned. 
::: 



</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins?details=True' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.scan_twins(params={"details": True})
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const twin = await twinsApi.scanTwins();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response (`details=True`)

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **cursor**       | string    | Position of the cursor denoting where the scan of Twins has finished. If no more Twins are to be scanned, the returned value is `null`. |
| **owner**                   | string                                       | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account which is the current owner of the Twin. The ownership of a Twin can be transferred.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **status**                  | string, value is `"alive"` or `"terminated"` | The status of a Twin can be `"alive"` or `"terminated"`. In case of alive Twins, the `"description"` can be updated by the owner of the Twin. In case of terminated Twins, the description field cannot be updated. Ledger Entries, Identities, and Docs can be attached to alive and terminated Twins by all users involved in the process.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts**              | timestamp                                    | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **description**             | dictionary                                   | Attributes of the Twin defined by the owner in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>Only the owner of the Twin can update the description of the Twin.<br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **creation_certificate**    | dictionary                                       | <details> <ExtendedSummary markdown="span">Certificate generated automatically by the system upon creation of a Twin. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**uuid**</td>  <td> string </td>  <td>Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). It is generated automatically when a Twin is created and stored in the `"uuid"` field of the creation certificate. </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Twin. </td>  </tr>  <tr>  <td> **created_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  </td>  </tr>  </tbody>  </table></TableWrapper></details> |
| **termination_certificate** | dictionary                                       | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system when a Twin is terminated. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**issuer**</td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account terminating the Twin. </td>  </tr>   <tr>  <td> **terminated_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was terminated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  </td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                        |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "cursor": "MDBiNDlhZjAtZWQ2YS00MGJlLTg3NzQtNTMzMTI5ODBkYmI0",
    "twins": [
        {
            "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "status": "alive",
            "updated_ts": 1688797476.023,
            "description": {
                "serial_number": 123
            },
            "creation_certificate": {
                "uuid": "f98bd395-c41d-4a80-8cdd-86cd275a5f3d",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1688797476.023
            }
        },
        {
            "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "status": "terminated",
            "updated_ts": 1688934276.843,
            "creation_certificate": {
                "uuid": "aa445449-138e-4a20-87df-d78edbb845b3",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1688916276.69
            },
          "termination_certificate": {
                "issuer": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "terminated_ts": 1688934276.843
          } 
        }
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>



## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
