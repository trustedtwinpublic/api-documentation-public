# Update Twin

<div class="row">
<div class="column">

This endpoint updates the description of the Twin. Only the owner of a Twin can update the description of the Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| PATCH  | /twins/{twin} | update_twin |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_twin" operation, the "update_twin" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type       | In   | Description                                                                                                                                                                                                                                                                                                                                                                                    |
|:----------------------------------|:-----------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*           | string                  | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin to be retrieved. |
| **description**<br/>*optional*  * | dictionary | body | Attributes of the Twin in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>Only the owner of the Twin can update the `"description"` of the Twin.<br/>For more details consult the [description](../../overview/trusted-twin-api.md#description-field) section. |
</TableWrapper>

::: footnote *
If the `"description"` field with attributes is provided in the request body, the `"description"` field from the request body will replace the current `"description"` field.<br/>      
If no attributes are provided in the request body of `"description"` (`"description": {}`), all attributes from the current `"description"` will be removed. The response will return an empty dictionary (`"description": {}`).<br/>    
If `"description"` with the value `null` is provided in the request body, all attributes from the current `"description"` will be removed. The response will not return the `"description"` field.<br/>
If the request body does not contain `"description"`, no changes will be made to the `"description"` field.
:::

In our example, we want to change the value of the `"qa_passed"` attribute from `true` to `false`. The attribute is nested in the `"description"` field of the Twin:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": false,
        "company": "Shoes",
        "certifications": ["xptI2021", "fht76j", "IPS1449"]
        }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": false,
        "company": "Shoes",
        "certifications": ["xptI2021", "fht76j", "IPS1449"]
        }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

description_body = {
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": False,
        "company": "Shoes",
        "certifications": ["xptI2021", "fht76j", "IPS1449"]
    }
}

status, response = TT_SERVICE.update_twin("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=description_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const TwinAlive = await twinsApi.updateTwin({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    description: {
        description: {
            "size": 41,
            "colour": "black",
            "type": "electric hazard",
            "model": "Xp6",
            "production_year": 2022,
            "qa_passed": false,
            "company": "Shoes",
            "certifications": ["xptI2021", "fht76j", "IPS1449"]
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute                | Type                                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:-------------------------|:---------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **owner**                | string                                       | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account that is the current owner of the Twin. The ownership of a Twin can be transferred.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **status**               | string, value is `"alive"` or `"terminated"` | The status of a Twin can be `"alive"` or `"terminated"`. In case of alive Twins, the `"description"` can be updated by the owner of the Twin. In case of terminated Twins, the `"description"` cannot be updated. Ledger Entries, Identities, and Docs can be attached to alive and terminated Twins by all users involved in the process.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **updated_ts**           | timestamp                                    | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **description**          | dictionary                                   | Attributes of the Twin in form of key-value pairs:<br/> -key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> -value: JSON compliant value. <br/>Only the owner of the Twin can update the `"description"` of the Twin.<br/>For more details consult the [description](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **creation_certificate** | dictionary                                       | <details> <ExtendedSummary markdown="span">Certificate generated automatically by the system upon creation of the Twin. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**uuid**</td>  <td> string </td>  <td>Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). It is generated automatically when the Twin is created and stored in the `"uuid"` field of the creation certificate. </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Twin.</td>  </tr>  <tr>  <td> **created_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td>  </tr>  </tbody>  </table></TableWrapper></details> |
|
</TableWrapper>

</div>
<div class="column">


<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "status": "alive",
    "updated_ts": 1646305579.969,
    "description": {
        "size": 41,
        "colour": "black",
        "type": "electric hazard",
        "model": "Xp6",
        "production_year": 2022,
        "qa_passed": false,
        "company": "Shoes",
        "certifications": [
            "xptI2021",
            "fht76j",
            "IPS1449"
        ]
    },
    "creation_certificate": {
        "uuid": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1646305579.969
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
