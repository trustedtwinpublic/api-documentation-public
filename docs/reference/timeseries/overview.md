# Overview

<div class="row">
<div class="column">

In our example, the Timeseries table with the name `environmental_data` has the following attributes:
- `"stats"`: Stats regarding the Timeseries table: `"table_size"`, `"index_size"`, `"toast_size"`, and `"total_size"`. All values of stats are provided in bytes. 
- `"dimensions"`: The dimension name is `"city"` and the data type of this dimension `"varchar"`.
- `"measurements"`: The measurement column names are `"temperature"`, `"ozone"`, and `"relative_humidity"`, and the data types of the dimensions are `"real"`.
- `"defaults"`: Dictionary containing a [template](../../overview/templates.md) with default dimension value for the measurement `"temperature"`. It is set to `Gdańsk`.
- `"retention"`: The retention is set to 3 weeks (`"retention": "3M"`).
- `"chunk"`: The chunk length is set to 1 week (`"chunk": "1W"`).
- `"database"`: [Database](../databases/index.md) [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).

Our request creates a Timeseries table with the following columns:

<TableWrapper>
| _timestamp | city | temperature | ozone | relative_humidity |
|:-----------|:-----|:------------|:------|:------------------|
| | | | | |
</TableWrapper>

If the Timeseries table were to be filled with updated values of Entries of a [Ledger](../ledgers/index.md), an example of how they would be displayed would be:

<TableWrapper>
| _timestamp                 | city    | temperature | ozone | relative_humidity |
|:---------------------------|:--------|:------------|:------|:------------------|
| 21 Apr 2022, 15:00:00 | Gdańsk  | 12          | 29    | 55                |
| 21 Apr 2022, 15:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022, 16:00:00 | Gdańsk  | 13          | 30    | 56                |
| 21 Apr 2022, 16:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022, 17:00:00 | Gdańsk  | 15          | 31    | 54                |
| 21 Apr 2022, 17:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022, 18:00:00 | Gdańsk  | 10          | 30    | 54                |
| 21 Apr 2022, 18:00:00 | Wrocław | 12          | 29    | 55                |
</TableWrapper>

::: tip TIP
<Icon name="info-circle" :size="1.75" />
See [add_twin_ledger_entry](../ledgers/add-twin-ledger-entry.md) for information on how to add a Ledger Entry and [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md) and [update_twin_ledger_entry_value](../ledgers/update-twin-ledger-entry-value.md) on how to update the value of a Ledger Entry.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Timeseries table example" copyable>
<CodeBlock title="json">
```json
{
    "stats": {
        "table_size": 0,
        "index_size": 8192,
        "toast_size": 0,
        "total_size": 16384
    },
    "dimensions": {
        "names": [
            "city"
        ],
        "types": [
            "varchar"
        ]
    },
    "measurements": {
        "names": [
            "temperature",
            "ozone",
            "relative_humidity"
        ],
        "types": [
            "real",
            "real",
            "real"
        ]
    },
    "defaults": {
        "measurement": "temperature",
        "dimensions": {
            "city": "Gdańsk"
        }
    },
    "retention": "3M",
    "chunk": "1W",
    "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:-----------------|:-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **stats**        | dictionary | <details> <ExtendedSummary markdown="span">Timeseries table stats.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**table_size**</td>  <td> integer </td>  <td>Size of the Timeseries table in bytes. </td>  </tr>  <tr>  <td> **index_size** </td>  <td> integer </td>  <td> Size of all indexes attached to the Timeseries table in bytes. </td>  </tr>  <tr>  <td> **toast_size** </td>  <td> integer </td>  <td> TOAST size in bytes.  </td>  </tr> <tr>  <td> **total_size** </td>  <td> integer </td>  <td> Total size in bytes.</td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **dimensions**   | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of dimension names and a list of data types of the corresponding dimensions. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the dimension(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding dimensions.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |  
| **measurements** | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of measurement column names and a list of data types of the corresponding measurement columns.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the measurement column(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding measurement columns.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 
| **defaults**     | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing [templates](../../overview/templates.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry. The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string  </td>  <td>  Template that determines the name of the measurement column to store the Entry value. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Template that determines the value for each of the dimensions. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </td> </tr> <tr> <td>**timestamp**</td>  <td> string </td>  <td> [Template](../../overview/templates.md) that determines the value of the timestamp.  </td> </tr> </table></TableWrapper> </details> | 
| **retention**    | string     | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **chunk**        | string     | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **database** | string| Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). | 
</TableWrapper>
