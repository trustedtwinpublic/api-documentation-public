# Get Timeseries tables

<div class="row">
<div class="column">

This endpoint retrieves a lists of existing Timeseries tables. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path                         | Operation*                                                              |
|:-------|:-----------------------------|:------------------------------------------------------------------------|
| GET    | /account/services/timeseries | get_timeseries_tables |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_timeseries_tables" operation, the "get_timeseries_table" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request 

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/account/services/timeseries' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_timeseries_tables()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TimeseriesApi, Configuration } from "@trustedtwin/js-client";

var timeseriesApi = new TimeseriesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const timeseriesTables = await timeseriesApi.getTimeseriesTables();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns the names of Timeseries database tables in an account held in the `"timeseries"` attribute.

<TableWrapper>
| Attribute      | Type            | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|:---------------|:----------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **timeseries** | list of strings | List of names of Timeseries tables in the Timeseries database.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "timeseries": [
        "water_pollution",
        "environmental_data",
        "air_quality_index"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
