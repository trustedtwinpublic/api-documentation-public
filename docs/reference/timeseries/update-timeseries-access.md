# Update Timeseries access

::: warning ENDPOINT DEPRECATION
<Icon name="alert-circle" :size="1.75" />
This endpoint is no longer be supported after release 3.12.00 (7th of February 2024, 10 p.m. CET). Starting with version 3.12.00, you can update access to databases through the [update_database_user_access](../databases/update-database-user-access.md) endpoint.
:::

