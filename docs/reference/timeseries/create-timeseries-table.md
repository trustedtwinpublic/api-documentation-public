# Create Timeseries table

<div class="row">
<div class="column">

This endpoint creates one or more Timeseries tables.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                         | Operation*                                                                     |
|:-------|:-----------------------------|:-------------------------------------------------------------------------------|
| POST   | /account/services/timeseries | create_timeseries_table  |
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_timeseries_table" operation, the "create_timeseries_table" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request 

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                       | Type                     | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:--------------------------------|:-------------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **dimensions**<br/>*required*   | dictionary               | body | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of dimension names and a list of data types of the corresponding dimensions. </ExtendedSummary><TableWrapper><table><thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the dimension(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding dimensions.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |  
| **measurements**<br/>*required* | dictionary               | body | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of measurement column names and a list of data types of the corresponding measurement columns.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the measurement column(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding measurement columns.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 
| **defaults**<br/>*optional* *   | dictionary, DEFAULT=null | body | <details> <ExtendedSummary markdown="span"> Dictionary containing [templates](../../overview/templates.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry. The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string  </td>  <td>  Template that determines the name of the measurement column to store the Entry value. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Template that determines the value for each of the dimensions. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </td> </tr> <tr> <td>**timestamp**</td>  <td> string </td>  <td> [Template](../../overview/templates.md) that determines the value of the timestamp.  </td> </tr> </table></TableWrapper> </details> | 
| **retention**<br/>*optional* *  | string, DEFAULT=null     | body | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **chunk**<br/>*optional* *      | string, DEFAULT=null     | body | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **database**<br/>*optional* * | string, DEFAULT="default" | body | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the database where the Timeseries table should be created. If not provided, the Timeseries table will be created in the [default database](../../reference/databases/index.md#default-database). | 
</TableWrapper>

::: footnote *
The `"defaults"`, `"retention"`, `"chunk"`, and `"database"` parameters are optional and do not need to be included in the request body when creating Timeseries tables. If they are not included in the request body, their default values are used. 
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "timeseries": {
        "environmental_data": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["temperature", "ozone", "relative_humidity"],
                "types": ["real", "real", "real"]
            },
            "defaults": {
                "measurement": "temperature",
                "dimensions": {
                    "city": "Gdańsk"
                }
            },
            "retention": "3M",
            "chunk": "1W",
            "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
        },
        "air_quality_index": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["air_quality_index"],
                "types": ["int"]
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

::: tip TIP
<Icon name="info-circle" :size="1.75" />
The Trusted Twin platform provides you with the flexibility to use the double asterisk `**` to unpack a dictionary passed as a value of the `"value"` attribute of a Ledger Entry.
:::

<ExtendedCodeGroup title="Example request body with **" copyable>
<CodeBlock title="json">
```json
{
   "timeseries":{
      "environmental_data":{
         "dimensions":{
            "names":[
               "city"
            ],
            "types":[
               "varchar"
            ]
         },
         "measurements":{
            "names":[
               "temperature",
               "ozone",
               "relative_humidity"
            ],
            "types":[
               "real",
               "real",
               "real"
            ]
         },
         "defaults":{
            "measurement":"**",
            "dimensions":{
               "city":"Gdańsk"
            }
         },
         "retention":"3M",
         "chunk":"1W",
         "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
      }
   }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/account/services/timeseries' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "timeseries": {
        "environmental_data": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["temperature", "ozone", "relative_humidity"],
                "types": ["real", "real", "real"]
            },
            "defaults": {
                "measurement": "temperature",
                "dimensions": {
                    "city": "Gdańsk"
                }
            },
            "retention": "3M",
            "chunk": "1W",
            "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
        },
       "air_quality_index": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["air_quality_index"],
                "types": ["int"]
            }
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

timeseries_body = {
    "timeseries": {
        "environmental_data": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["temperature", "ozone", "relative_humidity"],
                "types": ["real", "real", "real"],
            },
            "defaults": {
                "measurement": "temperature",
                "dimensions": {
                    "city": "Gdańsk"
                }
            },
            "retention": "3M",
            "chunk": "1W",
            "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
        },
        "air_quality_index": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["air_quality_index"],
                "types": ["int"]
            },
         }
    } 
}

status, response = TT_SERVICE.create_timeseries_table(body=timeseries_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TimeseriesApi, Configuration } from "@trustedtwin/js-client";

var timeseriesApi = new TimeseriesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const CreateTimeseriesTableResponse =
    await timeseriesApi.createTimeseriesTable({
        timeseriesTables: {
            timeseries: {
                environmental_data: {
                    dimensions: {
                        names: ["city"],
                        types: ["varchar"],
                    },
                    measurements: {
                        names: ["temperature", "ozone", "relative_humidity"],
                        types: ["real", "real", "real"],
                    },
                    defaults: {
                        measurement: "temperature",
                        dimensions: {
                            city: "Gdańsk",
                        },
                    },
                    retention: "3M",
                    chunk: "1W",
                    database: "22d057e1-15ce-4403-ba28-dba18bf208cb"
                },
                air_quality_index: {
                    dimensions: {
                        names: ["city"],
                        types: ["varchar"],
                    },
                    measurements: {
                        names: ["air_quality_index"],
                        types: ["int"],
                    },
                },
            },
        },
    });
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response 

<div class="row">
<div class="column">

The response returns a list of Timeseries tables with their attributes.

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:-----------------|:-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **dimensions**   | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of dimension names and a list of data types of the corresponding dimensions. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the dimension(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> s value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding dimensions.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |  
| **measurements** | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of measurement column names and a list of data types of the corresponding measurement columns.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the measurement column(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding measurement columns.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 
| **defaults**     | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing [templates](../../overview/templates.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry. The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string  </td>  <td>  Template that determines the name of the measurement column to store the Entry value. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Template that determines the value for each of the dimensions. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </td> </tr><tr> <td>**timestamp**</td>  <td> string </td>  <td> [Template](../../overview/templates.md) that determines the value of the timestamp.  </td> </tr> </table></TableWrapper> </details> | 
| **retention**    | string     | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **chunk**        | string     | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **database** | string| Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "timeseries": {
        "environmental_data": {
            "dimensions": {
                "names": [
                    "city"
                ],
                "types": [
                    "varchar"
                ]
            },
            "measurements": {
                "names": [
                    "temperature",
                    "ozone",
                    "relative_humidity"
                ],
                "types": [
                    "real",
                    "real",
                    "real"
                ]
            },
            "defaults": {
                "measurement": "temperature",
                "dimensions": {
                    "city": "Gdańsk"
                }
            },
            "retention": "3M",
            "chunk": "1W",
            "database": "22d057e1-15ce-4403-ba28-dba18bf208cb"
        },
        "air_quality_index": {
            "dimensions": {
                "names": [
                    "city"
                ],
                "types": [
                    "varchar"
                ]
            },
            "measurements": {
                "names": [
                    "air_quality_index"
                ],
                "types": [
                    "int"
                ]
            },
            "defaults": null,
            "retention": null,
            "chunk": null,
            "database": "48d3b876-149c-418b-ad12-d9ef5f63de29"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Response ('Table already exists')

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="alert-circle" :size="1.75" />
If you try to create a Timeseries table that already exists, you will receive the message `"error": "Table already exists."` for this table in the response. However, the response itself will return the `201 Created` status code (see example below).
:::

In our example, we send a request to create two Timeseries tables:
- `"water_pollution"`: A Timeseries table with the name `"water_pollution"` does not exists in the account yet.
- `"air_quality_index"`: A Timeseries table with the name `"air_quality_index"` already exists in the account.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "timeseries": {
        "water_pollution": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["water_pollution"],
                "types": ["real"]
            }
        },
        "air_quality_index": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["air_quality_index"],
                "types": ["int"]
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="cURL" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/account/services/timeseries' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "timeseries": {
        "water_pollution": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["water_pollution"],
                "types": ["real"]
            }
        },
        "air_quality_index": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["air_quality_index"],
                "types": ["int"]
            }
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

timeseries_body = {
    "timeseries": {
        "water_pollution": {
            "dimensions": {
                "names": ["city"],
                "types": ["varchar"]
            },
            "measurements": {
                "names": ["water_pollution"],
                "types": ["real"]
            },
            "air_quality_index": {
                "dimensions": {
                    "names": ["city"],
                    "types": ["varchar"]
                },
                "measurements": {
                    "names": ["air_quality_index"],
                    "types": ["real"]
                }
            }
        }
    }
}

status, response = TT_SERVICE.create_timeseries_table(body=timeseries_body)
```

</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TimeseriesApi, Configuration } from "@trustedtwin/js-client";

var timeseriesApi = new TimeseriesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const CreateTimeseriesTableResponse =
    await timeseriesApi.createTimeseriesTable({
        timeseriesTables: {
            timeseries: {
                water_pollution: {
                    dimensions: {
                        names: ["city"],
                        types: ["varchar"],
                    },
                    measurements: {
                        names: ["water_pollution"],
                        types: ["real"],
                    },
                air_quality_index: {
                    dimensions: {
                        names: ["city"],
                        types: ["varchar"],
                    },
                    measurements: {
                        names: ["air_quality_index"],
                        types: ["int"],
                    },
                },
            },
        },
    });
```

</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

The response returns the details of the Timeseries table `"water_polution"` and the message `"error": "Table already exists."` for the Timeseries table `"air_quality_index"`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
   "timeseries":{
      "water_pollution":{
         "dimensions":{
            "names":[
               "city"
            ],
            "types":[
               "varchar"
            ]
         },
         "measurements":{
            "names":[
               "water_pollution"
            ],
            "types":[
               "real"
            ]
         },
         "defaults":null,
         "retention":null,
         "chunk":null,
         "database": "48d3b876-149c-418b-ad12-d9ef5f63de29"
      },
      "air_quality_index":{
         "error":"Table already exists."
      }
   }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

<TableWrapper>
| Status code | Message | Comment                                                                                                                                                            |
|:------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 201         | Created | If a Timeseries table that you are trying to create already exists, you will receive the error: `"error": "Table already exists."` for this table in the response. |
</TableWrapper>
