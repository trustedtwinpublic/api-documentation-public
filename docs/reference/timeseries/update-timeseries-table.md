# Update Timeseries table

<div class="row">
<div class="column">

This endpoint updates a given Timeseries table.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                      | Operation*                                                                    |
|:-------|:------------------------------------------|:------------------------------------------------------------------------------|
| PATCH  | /account/services/timeseries/{timeseries} | update_timeseries_table |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_timeseries_table" operation, the "update_timeseries_table" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                | In         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|:-----------------------------------|:--------------------|:-----------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **timeseries**<br/>*required*      | string              | path       | Name of the Timeseries table. It must be unique in the context of an account and must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **defaults**<br/>*optional* *      | dictionary          | body       | <details> <ExtendedSummary markdown="span"> Dictionary containing [templates](../../overview/templates.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry. The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string  </td> <td> body </td> <td>Template that determines the name of the measurement column to store the Entry value. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).</td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body  </td> <td> Template that determines the value for each of the dimensions. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </td> </tr> <tr> <td>**timestamp**</td>  <td> string </td>  <td> [Template](../../overview/templates.md) that determines the value of the timestamp.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| **retention**<br/>*optional* *     | string              | body       | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **chunk**<br/>*optional* *         | string              | body       | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | 
| **measurements**<br/>*optional* ** | dictionary          | body       | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of measurement column names and a list of data types of the corresponding measurement columns. You can add a new measurement or update the name of an exiting measurement. Please note that the entire **measurements** dictionary must be included in the request body when adding new measurements or updating the name of an existing measurement as the new **measurements** content will replace the old content. When adding a new measurement, add a new measurement after the existing one(s) and a data type for this measurement to the `"types"` list. When updating a measurement, add a list containing the old and the new measurement (e.g. ["ozone", "ozone_new"]).    </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the measurement column(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding measurement columns.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | 
| **dimensions**                     | dictionary<br/>*optional* *** | body | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of dimension names and a list of data types of the corresponding dimensions. You can update the name of an exiting dimension. Please note that the entire **dimensions** dictionary must be included in the request body when updating the name of an existing dimension as the new **dimensions** content will replace the old content. </ExtendedSummary><TableWrapper><table><thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the dimension(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding dimensions.  </td> </tr> </table></TableWrapper> </details> | 
</TableWrapper>

::: footnote *
Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters.
:::

::: footnote **
If the `"measurements"` dictionary with attributes is provided in the request body, the `"measurements"` dictionary from the request body will replace the current `"measurements"` dictionary.<br/>      
If no attributes are provided in the request body of `"measurements"` (`"measurements": {}`), or if `"measurements"` with the value `null` is provided in the request body, the response will return an error, because `"measurements"` columns cannot be removed.<br/>
If the request body does not contain `"measurements"`, no changes will be made to the `"measurements"` field.
:::

::: footnote **
If the `"dimensions"` dictionary with attributes is provided in the request body, the `"dimensions"` dictionary from the request body will replace the current `"dimensions"` dictionary.<br/>      
If no attributes are provided in the request body of `"dimensions"` (`"dimensions": {}`), or if `"dimensions"` with the value `null` is provided in the request body, the response will return an error, because `"dimensions"` columns cannot be removed.<br/>
If the request body does not contain `"dimensions"`, no changes will be made to the `"dimensions"` field.
:::

In our example, we send a request to update the `"defaults"` so that the dimension value of the `"dimension"` `"city"` defaults to `"Gdańsk"` if not provided, we set the `"retention"` to 3 months (`"retention":"3M"`) and the `"chunk"` to 1 week (`"chunk":"1W"`).

<ExtendedCodeGroup title="Example request body" copyable>

<CodeBlock title="json">
```json
{
   "defaults":{
      "measurement":"air_quality_index",
      "dimensions":{
         "city":"Gdańsk"
      }
   },
   "retention":"3M",
   "chunk":"1W"
}
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">

```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/timeseries/air_quality_index' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw ' {
             "defaults": {
                "measurement": "air_quality_index",
                "dimensions": {
                    "city": "Gdańsk"
                }
            },
            "retention": "3M",
            "chunk": "1W"   
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

update_timeseries_body = {
    "defaults": {
        "measurement": "air_quality_index",
        "dimensions": {
            "city": "Gdańsk"
        }
    },
    "retention": "3M",
    "chunk": "1W"
}

status, response = TT_SERVICE.update_timeseries_table("air_quality_index", body=update_timeseries_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TimeseriesApi, Configuration } from "@trustedtwin/js-client";

var timeseriesApi = new TimeseriesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const timeseriesTable = await timeseriesApi.updateTimeseriesTable({
    timeseries: "air_quality_index",
    timeseriesTableUpdate: {
        defaults: {
            measurement: "air_quality_index",
            dimensions: {
                city: "Gdańsk",
            },
        },
        retention: "3M",
        chunk: "1W",
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute        | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:-----------------|:-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **stats**        | dictionary | <details> <ExtendedSummary markdown="span">Timeseries table stats.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**table_size**</td>  <td> integer </td>  <td>Size of the Timeseries table in bytes. </td>  </tr>  <tr>  <td> **index_size** </td>  <td> integer </td>  <td> Size of all indexes attached to the Timeseries table in bytes. </td>  </tr>  <tr>  <td> **toast_size** </td>  <td> integer </td>  <td> TOAST size in bytes.  </td>  </tr> <tr>  <td> **total_size** </td>  <td> integer </td>  <td> Total size in bytes.</td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **dimensions**   | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of dimension names and a list of data types of the corresponding dimensions. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the dimension(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding dimensions.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |  
| **measurements** | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing a list of measurement column names and a list of data types of the corresponding measurement columns.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**names**</td>  <td> list of strings  </td>  <td> Name(s) of the measurement column(s). Each of the names must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. </td> </tr> <tr> <td>**types**</td>  <td> list of strings, value is `"bigint"`, `"bool"`, `"boolean"`, `"character varying"`, `"date"`, `"double precision"`, `"geography"`, `"geometry"`, `"int"`, `"integer"`, `"interval"`, `"json"`, `"jsonb"`, `"numeric"`, `"real"`, `"smallint"`, `"text"`, `"time"`, `"timestamp"`, `"timestamptz"`, `"varchar"`, or `"uuid"`. </td>  <td> Data types of the corresponding measurement columns.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 
| **defaults**     | dictionary | <details> <ExtendedSummary markdown="span"> Dictionary containing [templates](../../overview/templates.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry. The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string  </td>  <td>  Template that determines the name of the measurement column to store the Entry value. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Template that determines the value for each of the dimensions. Provided default template is used if no default template is defined in the `"timeseries"` property of a Ledger's Entry. The template follows the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).  </td> </tr> <tr> <td>**timestamp**</td>  <td> string </td>  <td> [Template](../../overview/templates.md) that determines the value of the timestamp.  </td> </tr> </table></TableWrapper> </details> | 
| **retention**    | string     | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **chunk**        | string     | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **database** | string| Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "stats": {
        "table_size": 0,
        "index_size": 8192,
        "toast_size": 0,
        "total_size": 16384
    },
    "dimensions": {
        "names": [
            "city"
        ],
        "types": [
            "varchar"
        ]
    },
    "measurements": {
        "names": [
            "air_quality_index"
        ],
        "types": [
            "int"
        ]
    },
    "defaults": {
        "measurement": "air_quality_index",
        "dimensions": {
            "city": "Gdańsk"
        }
    },
    "retention": "3M",
    "chunk": "1W",
    "database": "48d3b876-149c-418b-ad12-d9ef5f63de29"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
