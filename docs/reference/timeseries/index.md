---
tags:
  - timeseries
  - time series
---

# Timeseries

<div class="row">
<div class="column">

## About

The Timeseries database on the Trusted Twin platform is a database optimized for ingestion, storing, and retrieval of large volumes of time-series data.       
     
The database is based on the TimescaleDB [Apache 2.0 License](https://github.com/timescale/timescaledb/blob/main/LICENSE-APACHE) version. Please see the [Feature comparison](https://docs.timescale.com/about/latest/timescaledb-editions/) section for TimescaleDB features available in each TimescaleDB version. Should you require a different TimescaleDB version, please email <hello@trustedtwin.com>.

The Timeseries database stores values of [Ledger](../ledgers/index.md) Entries with timestamps denoting when the values were changed. Entries can be added through the [add_twin_ledger_entry](../ledgers/add-twin-ledger-entry.md) endpoint and their values changed through the [update_twin_ledger_entry](../ledgers/update-twin-ledger-entry.md) or [update_twin_ledger_entry_value](../ledgers/update-twin-ledger-entry-value.md) endpoint. For more information about Ledger Entries, please consult the [Ledger](../ledgers/index.md) section.

The Timeseries service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.

## Database services access

::: tip DATABASE SERVICES ACCESS
<Icon name="info-circle" :size="1.75" />
To learn how to access the Timeseries database, please go to the [Database services access](../../account-and-access/database-services-access.md) section.
:::

## Timeseries database structure

A Timeseries database consists of one or more tables. The name of a Timeseries table must be unique in the context of an account and must match the [regular expression](https://regexr.com/) `^(_*[a-z0-9]+)([_-][0-9a-z]+)*$`.   

## Timeseries table attributes

A Timeseries table has the following attributes:

<TableWrapper>
| Attribute | Description |
|:------|:------|
| **dimensions** | Dictionary containing a list of dimension names held in the `"names"` attribute and a list of data types of the corresponding dimensions held in the `"types"` attribute. |
| **measurements** | Dictionary containing a list of measurement column names held in the `"names"` attribute and a list of data types of the corresponding measurement columns held in the `"types"` attribute. |
| **defaults** | Dictionary containing [templates](../../overview/template.md) for `"measurement"` (to determine the name of the measurement column to store the Entry value) and `"dimensions"` (to determine the value for each of the dimensions). Provided default templates are used if no default templates are defined in the `"timeseries"` property of a Ledger's Entry.  |
| **retention** | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. |
| **chunk** | Chunk length. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. | 
| **retention** | Time for which the data is to be stored. It must match the [regular expression](https://regexr.com/) <code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. |
| **database** | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). | 
</TableWrapper>

::: tip TEMPLATES
<Icon name="info-circle" :size="1.75" />
The `"defaults"` attribute uses [templates](../../overview/templates.md). Templates on the Trusted Twin platform follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). Please see [Available variables](../../overview/templates.md#available-variables) for a list of variables available for templates.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                           | Operation*                                                                        |
|:-------|:-----------------------------------------------|:----------------------------------------------------------------------------------|
| PATCH  | /account/services/timeseries                   | [update_timeseries_access](./update-timeseries-access.md)     |
| POST   | /account/services/timeseries                   | [create_timeseries_table](./create-timeseries-table.md)     |
| GET    | /account/services/timeseries/{timeseries}      | [get_timeseries_table](./get-timeseries-table.md)           |
| GET    | /account/services/timeseries                   | [get_timeseries_tables](./get-timeseries-tables.md)           |
| PATCH  | /account/services/timeseries/{timeseries}      | [update_timeseries_table](./update-timeseries-table.md)     |
| DELETE | /account/services/timeseries/{timeseries}/data | [truncate_timeseries_table](./truncate-timeseries-table.md) |
| DELETE | /account/services/timeseries/{timeseries}      | [delete_timeseries_table](./delete-timeseries-table.md)     |
</EndpointsTable>

::: footnote *
In order for a user to perform operations on the Timeseries database, the respective permissions "create_timeseries_table", "get_timeseries_tables", "update_timeseries_access", "get_timeseries_table", "update_timeseries_table", "delete_timeseries_table", and "truncate_timeseries_table" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>w
</div>
