---
tags:
  - ledger endpoints
  - visibility
meta:
  - name: description
    content: 
---

# Ledger

<div class="row">
<div class="column">

A Ledger is an object that stores information about the state of the Twin in form of Entries. Each account has its own Ledger. It is independent and separated from other Ledgers. 

## Ledger structure

A Ledger contains Entries that store information about the state of the Twin. Entries are user-defined key-value pairs: 
- key: alphanumeric string unique within the Ledger. It must match the [regular expression](https://regexr.com/) `(_*[a-z0-9]+)([_-][0-9a-z]+)`.
- value: valid JSON data type.

## Ledger visibility

The visibility of a Ledger depends on the visibilities of Entries in the Ledger.

<TableWrapper>
| Ledger visibility  | Description |
|:-------------------|:------|
| **Private Ledger** | If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of a Ledger are private, the Ledger is private. |
| **Public Ledger** | If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public. |
</TableWrapper>

## Entry types

An Entry can be of one of the following types:

<TableWrapper>
| Entry type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Example                                                                       |
|:--------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------|
| **Value**     | A Value type Entry stores a value provided directly by the account. The value change event originates from the account’s requests to the API. This Entry type is most commonly used to store account’s data.                                                                                                                                                                                                                                                                                              | [Value type Entry example](./overview.md#value-type-entry-attributes)         | 
| **Reference** | A Reference type Entry stores a reference to a value from a different Ledger belonging to the user’s own account or another account’s Ledger associated with the same or a different Twin. The value change event originates from the change of the referenced value. The value of such an Entry is updated automatically every time the referenced value changes. Reference type Entries are used to build relationships between shared objects (i.e., graphs of Twins) which are updated automatically. | [Reference type Entry example](./overview.md#reference-type-entry-attributes) |
| **Include**   | An Include type Entry stores a reference to a value from a different Ledger of the same account. The value of such an Entry is loaded each time a Ledger is being accessed but is not updated automatically upon the source value change. This Entry type was introduced to support global variables used in multiple Ledgers.                                                                                                                                                                            | [Include type Entry example](./overview.md#include-type-entry-attributes)     |
</TableWrapper>


## Entry structure

Depending on the Entry type, an Entry will have either a `"value"`, a `"ref"`, or an `"include"` attribute.

<TableWrapper>
| Attribute | Type                 | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:----|:---------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **value** | valid JSON data type | Value of the Entry.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 
| **ref** | dictionary               | <details> <ExtendedSummary markdown="span"> Reference property. It allows to create an Entry based on the `"value"` field of an Entry in a different Ledger, especially a Ledger of a different account. The value that the reference is pointing to must be visible to the account creating the reference.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**source**</td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)), `{ledger}` (Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td> Source path to the value that we want the Entry to reference. </td> </tr> <tr> <td>**status**</td>  <td> enum, value is `"created"`, `"ok"`, `"not_found"`, `"loop_detected"` or `"too_many_hops"`  </td>  <td> Status of the reference. It can have one of the following values:<br/>- `"created"`: The Entry was created.<br/>- `"ok"`: The Entry value is consistent with the value that the reference is pointing to.<br/>- `"not_found"`: The value could not be found. <br/>- `"loop_detected"`: The Entry is not accessible to the account because of a circular reference.<br/>- `"too_many_hops"`: There are too many transfers between references (the maximum number of hops allowed is 32). </td> </tr> </table></TableWrapper> </details> |
| **include** | dictionary             | <details> <ExtendedSummary markdown="span"> Include property. It allows to create an Entry that fetches upon request the `"value"` field of an Entry in a different Ledger in the same account.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**source**<br/>*required** </td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td>body</td> <td> Source path to the value that we want the Entry to include. </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 
</TableWrapper>

Depending on whether an Entry is of `"value"`, `"ref"`, or `"include"` type, it can have the following attributes:

<TableWrapper>
| Attribute            | **value** | **ref** | **include** | Service                                                 |  
|:---------------------|:----------|:--------|:------------|:--------------------------------------------------------|
| **entry_created_ts** | &check;   | &check; | &check;     |                                                         |
| **entry_updated_ts** | &check;   | &check; | &check;     |                                                         |
| **value_changed_ts** | &check;   | &check; | &check;     |                                                         |
| **value**            | &check;   | &check; | &check;     |                                                         |
| **visibility**       | &check;   | &check; | &check;             |                                                         |
| **history**          | &check;   | &check; |             | [History](../../reference/history/index.md)             |
| **timeseries**       | &check;   | &check; |             | [Timeseries](../../reference/timeseries/index.md)       |
| **publish**          | &check;   | &check; |             | [Notifications](../../reference/notifications/index.md) |
</TableWrapper>

<TableWrapper>
| Attribute | Description |
|:------|:------|
| **value** |  Value of the Entry. This field cannot be changed by the user if the `"ref"` (reference) or `"include"` attribute is used in the Entry. |
| **visibility** | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. If the `"visibility"` of an Entry is not `null`, the Entry is public.  |
| **entry_created_ts** | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  |
| **entry_updated_ts** |  Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
| **value_changed_ts** | Time at which the value of the Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **history** | The [History](../history/index.md) service can be enabled for an Entry through adding the `"history"` attribute. The `'history"` defines the time for which the history of changes of the Entry's value is to be stored. Please note that the limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored. The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.<br/>If there is no `"history"` attribute, the [History](../../reference/history/index.md) service is not enabled.<br/> If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values are stored for the given time period subject to the maximum number of history records limit (1000 per Entry). |
| **timeseries** | If the [Timeseries](../timeseries/index.md) service is enabled on your account, and the Entry value is stored in a Timeseries table, the Entry can also have a `"timeseries"` attribute. The `"timeseries"` attribute allows you to define in which Timeseries table Entry values are stored and in which `"measurement"` column they are stored. You can as well define the `"dimension"` for the Entry and the [template](../../overview/templates.md) for the value of the dimension.<br/>The `"timeseries"` attribute holds:<br/>- The name of the [Timeseries](../timeseries/index.md) table where the Entry values are to be stored.<br/>- The name of the measurement column of the given Timeseries table. <br/>- (optional) The name of the dimension from the given Timeseries table a [template](../../overview/templates.md) for the value of the dimension.<br/>The Timeseries table, name of the `"measurement"` column and, optionally, `"dimension"` are defined through the [add_twin_ledger_entry](./add-twin-ledger-entry.md) endpoint. The Entry values are updated through the [update_twin_ledger_entry](./update-twin-ledger-entry.md) endpoint. The historical values of Entries with the corresponding [Unix timestamps](https://en.wikipedia.org/wiki/Unix_time) denoting when the values were changed can be retrieved through the [get_twin_ledger_entry_history](../history/get-twin-ledger-entry-history.md) endpoint.| 
| **publish** | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/>- key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).<br/>If no `"publish"` attribute is returned in the response, no notifications are set for the Entry.             |
</TableWrapper>


An example of a Timeseries table with updated Entry values:

<TableWrapper>
| _timestamp                 | city    | temperature | ozone | relative_humidity |
|:---------------------------|:--------|:------------|:------|:------------------|
| 21 Apr 2022 2022, 15:00:00 | Gdańsk  | 12          | 29    | 55                |
| 21 Apr 2022 2022, 15:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 16:00:00 | Gdańsk  | 13          | 30    | 56                |
| 21 Apr 2022 2022, 16:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 17:00:00 | Gdańsk  | 12          | 29    | 54                |
| 21 Apr 2022 2022, 17:00:00 | Wrocław | 12          | 29    | 55                |
| 21 Apr 2022 2022, 18:00:00 | Gdańsk  | 10          | 30    | 54                |
| 21 Apr 2022 2022, 18:00:00 | Wrocław | 12          | 29    | 55                |
</TableWrapper>



</div>
<div class="column">

<EndpointsTable>
| Method | Path                                   | Operation*                                                      |
|:-------|:---------------------------------------|:----------------------------------------------------------------|
| POST   | /twins/{twin}/ledgers/{ledger}**         | [add_twin_ledger_entry](./add-twin-ledger-entry.md)              |
| GET    | /twins/{twin}/ledgers/{ledger}/value*** | [get_twin_ledger_entry_value](./get-twin-ledger-entry-value.md) |
| GET    | /twins/{twin}/ledgers/{ledger}****         | [get_twin_ledger_entry](./get-twin-ledger-entry.md)              |
| PATCH   | /twins/{twin}/ledgers/{ledger}/value** | [update_twin_ledger_entry_value](./update-twin-ledger-entry-value.md) |
| PATCH  | /twins/{twin}/ledgers/{ledger}**        | [update_twin_ledger_entry](./update-twin-ledger-entry.md)        |
| DELETE | /twins/{twin}/ledgers/{ledger}**        | [delete_twin_ledger_entry](./delete-twin-ledger-entry.md)        |
</EndpointsTable>

::: footnote *
In order for a user to perform all operations on Ledgers, the respective permissions "add_twin_ledger_entry", "get_twin_ledger_entry_value", "get_twin_ledger_entry", "update_twin_ledger_entry_value", "update_twin_ledger_entry", and "delete_twin_ledger_entry" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

::: footnote ***
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::

::: footnote ****
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). For requests on Ledgers where you are the creator of the Twin, you can use `creator` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/creator`).
:::

</div>
</div>
