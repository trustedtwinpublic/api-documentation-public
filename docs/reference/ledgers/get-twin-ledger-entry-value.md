# Get Twin Ledger Entry value

<div class="row">
<div class="column">

This endpoint allows you to retrieve Ledger Entry values. You can retrieve values from Entries:
- from your personal Ledger,
- from the Ledger of the owner of a specific Twin,
- based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md).

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*         |
|:-------|:-------------------------------|:-------------------|
| GET    | /twins/{twin}/ledgers/{ledger}/value | get_twin_ledger_entry_value |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_ledger_entry_value" operation, the "get_twin_ledger_entry_value" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                   | Type   | In   | Description                                                                                                                   |
|:----------------------------|:-------|:-----|:------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*   | string | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                  |
| **ledger**<br/>*required** | string | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                |
| **entries**<br/>*optional*** | string  | query string | Key of the Ledger Entry to be returned. |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::

::: footnote **
The `entries` parameter is optional. If it is not included in the request, all Entries visible to the caller are returned.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal/value' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_twin_ledger_entry_value("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userLedger = await ledgerApi.getTwinLedgerEntryValue({
    twin: "0c0832ea-36f4-4459-aefd-8422f550e4b1",
    ledger: "personal",
});
```
</CodeBlock>


</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

#### Entry attributes

<TableWrapper>
| Attribute            | Type       | Description  |
|:---------------------|:-----------|:--------------|
| **entry_created_ts** | timestamp  | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **entry_updated_ts** | timestamp  |  Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).    |
| **value_changed_ts** | timestamp  | Time at which the value of an Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **value**            | string     | Value of the Entry. |   
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "poznan": {
            "value": 23,
            "entry_created_ts": 213423423424.123,
            "entry_updated_ts": 534543545345.123,
            "value_changed_ts": 204543545345.123
        },
        "warszawa": {
            "value": 22, 
            "entry_created_ts": 213423423424.123,
            "entry_updated_ts": 534543545345.123,
            "value_changed_ts": 204543545345.123
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
