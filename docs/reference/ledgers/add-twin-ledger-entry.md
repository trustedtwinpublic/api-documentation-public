# Add Twin Ledger Entry

<div class="row">
<div class="column">

This endpoint adds one or multiple Entries to a Ledger. You can add Entries:
- to your personal Ledger.
- based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md).

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*         |
|:-------|:-------------------------------|:-------------------|
| POST   | /twins/{twin}/ledgers/{ledger}** | add_twin_ledger_entry |
</EndpointsTable>

::: footnote *
In order for a user to perform the "add_twin_ledger_entry" operation, the "add_twin_ledger_entry" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

</div>
</div>

## Request

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
You can create Entries of value, reference, and include type. Please note that you cannot update the Entry type the once it has been created.
:::

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                   | Type       | In   | Description                                                                                                                                                                                            |
|:----------------------------|:-----------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*   | string     | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                           |
| **ledger**<br/>*required** | string     | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                      |
| **entries**<br/>*required*  | dictionary | body | Key-value pairs:<br/> - key: alphanumeric string unique within the Ledger. It must match the [regular expression](https://regexr.com/) `(_*[a-z0-9]+)([_-][0-9a-z]+)`.<br/> - value: Valid JSON data type. |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::


#### Value type Entry parameters

<TableWrapper>
| Parameter                         | Type                   | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|:----------------------------------|:-----------------------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **value**<br/>*required**         | valid JSON data type   | body | User-defined value of the Entry. This field cannot be changed by a user for reference and include type Entries.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **visibility**<br/>*optional***   | string, `DEFAULT=null` | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                                                                                                                                                                       |
| **history**<br/>*optional****     | string                 | body | Time for which the history of changes of the Entry's value is to be stored.The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. The limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored. |
| **timeseries**<br/>*optional***** | dictionary             | body | <details> <ExtendedSummary markdown="span">  Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td> <td> body </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored.  </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pair:<br/> - key: name of the dimension under which the Entry value is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                    |
| **publish**<br/>*optional******   | dictionary             | body | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/> - key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of topics in form of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |                    
</TableWrapper>

::: footnote *
The `"value"` parameter is required for value type Entries. For other Entry types, please see [reference type Entry parameters](./add-twin-ledger-entry.md#reference-type-entry-parameters) and [include type Entry parameters](./add-twin-ledger-entry.md#include-type-entry-parameters).
:::

::: footnote **
The `"visibility"` parameter is optional and does not need to be included in the request body when adding Entries. If it is not included in the request body, its default value (`DEFAUL=null`) is used.
:::

::: footnote ***
The `"history"` parameter is optional.<br/>
If there is no `"history"` parameter included in the request when adding a Ledger Entry, the [History](../../reference/history/index.md) service will not be enabled for the Entry.<br/>
If there is no `"history"` parameter in the request, the [History](../../reference/history/index.md) service will not be enabled for the Entry.<br/>
If the value of the `"history"` parameter is set to `null`, the [History](../../reference/history/index.md) service will not be enabled for the Entry.<br/>
If the value of the `"history"` parameter is set to `"INF"`, the most recent 1000 historical Entry values will be stored for the Entry.<br/>
If the value of the `"history"` parameter is set to a time period (e.g., days, weeks, months), the historical Entry values will be stored for the given time period subject to the maximum number of history records limit (1000 per Entry).
:::

::: footnote ****
The `"timeseries"` parameter is optional. If there is no `"timeseries"` parameter included in the request, Entry value is not stored in the [Timeseries](../timeseries/index.md) database. In order to use the [Timeseries](../timeseries/index.md) service, it needs to be enabled for your account.
:::

::: footnote *****
The `"publish"` parameter is optional. If there is no `"publish"` parameter included in the request, no [Notifications](../../reference/notifications/index.md) are set for a given Entry.
:::

#### Reference type Entry parameters

<TableWrapper>
| Parameter                         | Type                   | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|:----------------------------------|:-----------------------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **ref**<br/>*required*           | dictionary             | body | <details> <ExtendedSummary markdown="span"> Reference. It allows to create an Entry based on the `"value"` field of an Entry in a different Ledger, especially a Ledger of a different account. The value that the reference is pointing to must be visible to the account creating the reference.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**source**<br/>*required** </td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)), `{ledger}` (Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td>body</td> <td> Source path to the value that we want the Entry to reference. </td> </tr> </table></TableWrapper>  </details>                                                                   |
| **visibility**<br/>*optional***   | string, `DEFAULT=null` | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                                                                                                                                                                         |
| **history**<br/>*optional****     | string                 | body | Time for which the history of changes of the Entry's value is to be stored.The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. The limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored.  |
| **timeseries**<br/>*optional***** | dictionary             | body | <details> <ExtendedSummary markdown="span">  Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td> <td> body </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored.  </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pair:<br/> - key: name of the dimension under which the Entry value is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details>                                                                                                                                                      |
| **publish**<br/>*optional******   | dictionary             | body | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/>- key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md). The `"topic"` value must match the [regular expression](https://regexr.com/).<code>^[0-9A-Za-z\-_]{0,127}(?:@[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})?$</code>.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
</TableWrapper>


::: footnote *
The `"ref"` parameter is required for reference type Entries.
:::

::: footnote **
The `"visibility"` parameter is optional and does not need to be included in the request body when adding Entries. If it is not included in the request body, its default value (`DEFAUL=null`) is used.
:::

::: footnote ***
The `"history"` parameter is optional.<br/>
If there is no `"history"` parameter included in the request when adding a Ledger Entry, the [History](../../reference/history/index.md) service will not be enabled for the Entry.<br/>
If there is no `"history"` parameter in the request, the [History](../../reference/history/index.md) service will not be enabled for the Entry.<br/>
If the value of the `"history"` parameter is set to null, the [History](../../reference/history/index.md) service will not be enabled.<br/>
If the value of the `"history"` parameter is set to `"INF"`, the most recent 1000 historical Entry values will be stored for the Entry.<br/>
If the value of the `"history"` parameter is set to a time period (e.g., days, weeks, months), the historical Entry values will be stored for the given time period subject to the maximum number of history records limit (1000 per Entry).
:::

::: footnote ****
The `"timeseries"` parameter is optional. If there is no `"timeseries"` parameter included in the request, Entry value is not stored in the [Timeseries](../timeseries/index.md) database. In order to use the [Timeseries](../timeseries/index.md) service, it needs to be enabled for your account.
:::

::: footnote *****
The `"publish"` parameter is optional. If there is no `"publish"` parameter included in the request, no [Notifications](../../reference/notifications/index.md) are set for a given Entry.
:::

#### Include type Entry parameters

<TableWrapper>
| Parameter                    | Type   | In   | Description                                                 |
|:-----------------------------|:-------|:-----|:------------------------------------------------------------|
| **include**<br/> *required** | dictionary | body | <details> <ExtendedSummary markdown="span"> Include property. It allows to create an Entry that fetches upon request the `"value"` field of an Entry in a different Ledger in the same account.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**source**<br/>*required** </td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td>body</td> <td> Source path to the value that we want the Entry to include. </td> </tr> </table></TableWrapper> </details>  |
| **visibility***<br/> *optional** | string, `DEFAUL=null` | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public. |
</TableWrapper>


::: footnote *
The `"include"` parameter is required for include type Entries.
:::

::: footnote **
The `"visibility"` parameter is optional. If it is not included in the request, its default value (`DEFAUL=null`) is used.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "value": 15,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "wroclaw": {
            "ref": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw"
            },
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "sopot": {
            "include": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/sopot_1"
            },
        "visibility": "USER.profession == 'analyst'",
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST '/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal ' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "entries": {
        "gdansk": {
            "value": 15,
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                  "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "wroclaw": {
            "ref": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw"
            },
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "sopot": {
            "include": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/sopot_1"
            },
            "visibility": "USER.profession == '\''analyst'\''"
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

entries_body = {
    "entries": {
        "gdansk": {
            "value": 15,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "wroclaw": {
            "ref": {
                "source": "twins://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw"
            },
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "sopot": {
            "include": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/sopot_1"
            },
            "visibility": "USER.profession == 'analyst'"
        }
    }   
}

status, response = TT_SERVICE.add_twin_ledger_entry("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal", body=entries_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userLedger = await ledgerApi.addTwinLedgerEntry({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    ledger: "personal",
    postLedgerEntries: {
        entries: {
            gdansk: {
                value: 15,
                visibility: "USER.profession == 'analyst'",
                history: "2W",
                timeseries: {
                    environmental_data: {
                        measurement: "temperature",
                        dimensions: {
                            city: "{entry_name}",
                        },
                    },
                },
                publish: {
                    "entry_new_value>entry_old_value": [
                        "value-increase"
                    ],
                },
            },
            wroclaw: {
                ref: {
                    source: "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw",
                },
                visibility: "USER.profession == 'analyst'",
                history: "2W",
                timeseries: {
                    environmental_data: {
                        measurement: "temperature",
                        dimensions: {
                            city: "{entry_name}",
                        },
                    },
                },
                publish: {
                    "entry_new_value>entry_old_value": [
                        "value-increase"
                    ],
                },
            },
            sopot: {
                include: {
                    source: "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/sopot_1",
                },
                visibility: "USER.profession == 'analyst'"
            },
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

#### Entry attributes

<TableWrapper>
| Attribute            | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:---------------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **entry_created_ts** | timestamp  | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **entry_updated_ts** | timestamp  | Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **value_changed_ts** | timestamp  | Time at which the value of an Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **value**            | string     | User-defined value of the Entry. This value of this field cannot be changed by users for Reference and Include type Entries.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **visibility**       | string     | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **ref***             | dictionary | <details> <ExtendedSummary markdown="span"> Reference property. It allows to create an Entry based on the `"value"` field of an Entry in a different Ledger, especially a Ledger of a different account. The value that the reference is pointing to must be visible to the account creating the reference.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**source**</td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)), `{ledger}` (Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td> Source path to the value that we want the Entry to reference. </td> </tr> <tr> <td>**status**</td>  <td> enum, value is `"created"`, `"ok"`, `"not_found"`, `"loop_detected"` or `"too_many_hops"`  </td>  <td> Status of the reference. It can have one of the following values:<br/>- `"created"`: The Entry was created.<br/>- `"ok"`: The Entry value is consistent with the value that the reference is pointing to.<br/>- `"not_found"`: The value could not be found. <br/>- `"loop_detected"`: The Entry is not accessible to the account because of a circular reference.<br/>- `"too_many_hops"`: There are too many transfers between references (the maximum number of hops allowed is 32). </td> </tr> </table></TableWrapper> </details> |
| **include****        | dictionary     | <details> <ExtendedSummary markdown="span"> Include property. It allows to create an Entry that fetches upon request the `"value"` field of an Entry in a different Ledger in the same account.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**source**<br/>*required** </td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td>body</td> <td> Source path to the value that we want the Entry to include. </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **history**          | string     | Time for which the history of changes of the Entry's value is to be stored. Please note that the limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored. The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.<br/>If no `"history"` attribute is returned in the response, the [History](../../reference/history/index.md) service is not enabled.<br/> If `"history"` is set to `"INF"`, the most recent 1000 historical Entry values are stored for the Entry.<br/>If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values are stored for the given time period subject to the maximum number of history records limit (1000 per Entry).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| **timeseries*****    | dictionary | <details> <ExtendedSummary markdown="span"> Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. If no `"timeseries"` attribute is returned in the response, the Entry value is not stored in a Timeseries table. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored. </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Key-value pair:<br/> - key: name of the dimension under which the Entry value is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension. </td> </tr></table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **publish**          | dictionary | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/>- key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).<br/>If no `"publish"` attribute is returned in the response, no notifications are set for the Entry.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
</TableWrapper>


::: footnote *
The `"ref"` attribute is only returned for reference type Entries.
:::

::: footnote **
The `"include"` attribute is only returned for include type Entries.
:::

::: footnote ***
The [Timeseries](../timeseries/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::



</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651073498.69,
            "value_changed_ts": 1651073498.69,
            "value": 15,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "wroclaw": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651073498.69,
            "value_changed_ts": 1651073498.69,
            "ref": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw",
                "status": "created"
            },
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            }
        },
        "sopot": {
            "include": {
                "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/sopot_1"
            },
            "visibility": "USER.profession == 'analyst'"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Response ('Entry already exists')

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
If you try to add an Entry that already exists, you will receive the message `"error": "Entry already exists."` for this Entry in the response. However, the response itself will return the `201 Created` status code. Please see the below example for more details.
:::

In the example below, we create two Entries - the Entry `"krakow"` which does not exist in the Ledger yet and the Entry `"wroclaw"` which already exists in the Ledger.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "krakow": {
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                  "measurement": "temperature",
                    "dimensions": {
                        "{entry_name}"
                    }
                }
            }
        },
        "wroclaw": {
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "{entry_name}"
                    }
                }
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
The templates follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)). Therefore, you can use variables in the templates (see [Timeseries](../timeseries/index.md) overview section for a list of available variables). In our example we use the `entry_name` variable. 
As a result, the Entry values will be tagged with a dimension which is equal to the name of the Entry (in our example it is`"wroclaw"`).
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "wroclaw": {
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",z
            "timeseries": {
                "environmental_data": {
                  "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal ' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{ "entries": {
        "krakow": {
            "value": 21,
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                  "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        },
        "wroclaw": {
            "value": 21,
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

entries_body = {
    "entries": {
        "krakow": {
            "value": 21,
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        },
        "wroclaw": {
            "value": 21,
            "visibility": "USER.profession == '\''analyst'\''",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        }
    }
}

status, response = TT_SERVICE.add_twin_ledger_entry("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal", body=entries_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userLedger = await ledgerApi.addTwinLedgerEntry({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    ledger: "personal",
    postLedgerEntries: {
        entries: {
            krakow: {
                value: 21,
                visibility: "USER.profession == 'analyst'",
                history: "2W",
                timeseries: {
                    environmental_data: {
                        measurement: "temperature",
                        dimensions: {
                            city: "{entry_name}",
                        },
                    },
                },
            },
            wroclaw: {
                ref: {
                    source: "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw",
                },
                visibility: "USER.profession == 'analyst'",
                history: "2W",
                timeseries: {
                    environmental_data: {
                        measurement: "temperature",
                        dimensions: {
                            city: "{entry_name}",
                        },
                    },
                },
            },
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

The response returns the details of the created Entry `"krakow"` and the message `"error": "Entry already exists"` for the Entry `"wroclaw"` which already exists in the Ledger. The status code of the response is `201 Created`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "krakow": {
            "entry_created_ts": 1651073715.03,
            "entry_updated_ts": 1651073715.03,
            "value_changed_ts": 1651073715.03,
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        },
        "wroclaw": {
            "error": "Entry already exists."
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

<TableWrapper>
| Status code | Message | Comment                                                                                                                                                  |
|:------------|:--------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| 201         | Created | If an Entry that you are trying to create already exists, you will receive the error: `"error": "Entry already exists."` for this Entry in the response. |
</TableWrapper>
