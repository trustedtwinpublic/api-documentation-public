# Update Twin Ledger Entry

<div class="row">
<div class="column">

This endpoint updates one or multiple Ledger Entries. You can update Ledger Entries:
- in your personal Ledger,
- based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md).    
    
Any optional parameters not provided in the request will remain unchanged.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*            |
|:-------|:-------------------------------|:----------------------|
| PATCH  | /twins/{twin}/ledgers/{ledger}** | update_twin_ledger_entry |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_twin_ledger_entry" operation, the "update_twin_ledger_entry" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

</div>
</div>

## Request

### Value type Entry

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                           | Type                 | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:------------------------------------|:---------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*             | string               | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **ledger**<br/>*required**          | string               | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **value**<br/>*optional***          | valid JSON data type | body | User-defined value of the Entry.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | 
| **visibility**<br/>*optional***     | string               | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                    |
| **history**<br/>*optional***        | string               | body | Time for which the history of changes of the Entry's value is to be stored.The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. The limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored.                                                                                                                                                                                                                                                                                                                                                                                                  |
| **timeseries**<br/>*optional*** *** | dictionary           | body | <details> <ExtendedSummary markdown="span">  Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td> <td> body </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored.  </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pair:<br/> - Key: name of the dimension under which the Entry value is to be stored. <br/> - Value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> |
| **publish**<br/>*optional***        | dictionary           | body | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/> - Key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- Value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).                                                                                                                                                                                                                                                                                                                                                                                                                          |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

::: footnote **
- Any optional parameters not provided in the request will be left unchanged. 
- Any optional parameters provided in the request will replace current parameters.
- Operations replacing any current `"visibility"`, `"timeseries"`, or `"publish"` parameters with values equal to current parameter values will result in an update to the `"entry_updated_ts"` timestamp. 
- Operations replacing the value of the current `"value"` parameter with the same value will result in an update to the `"value_changed_ts"` timestamp.
:::

::: footnote ***
If there is no `"history"` parameter in the request, no changed will be made to the History service.<br/>
If the `"history"` parameter value is replaced with value equal to current parameter value, the update will result in an update to the `"entry_updated_ts"` timestamp.<br/>
If the value of the `"history"` parameter is set to `null`, the [History](../../reference/history/index.md) service will be disabled for the given Entry.<br/>
If `"history"` is set to `"INF"`, the most recent 1000 historical Entry values will be stored for the Entry.<br/>
If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values will be stored for the given time period subject to the maximum number of history records limit (1000 per Entry).
:::

### Reference type Entry

<TableWrapper>
| Parameter                           | Type                 | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:------------------------------------|:---------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*             | string               | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **ledger**<br/>*required**          | string               | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **visibility**<br/>*optional***     | string               | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                    |
| **history**<br/>*optional***        | string               | body | Time for which the history of changes of the Entry's value is to be stored.The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>. The limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored.                                                                                                                                                                                                                                                                                                                                                                                                        |
| **timeseries**<br/>*optional*** *** | dictionary           | body | <details> <ExtendedSummary markdown="span">Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td> <td> body </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored.  </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pair:<br/> - Key: name of the dimension under which the Entry value is to be stored. <br/> - Value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> |
| **publish**<br/>*optional***        | dictionary           | body | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/> - Key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- Value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).                                                                                                                                                                                                                                                                                                                                                                                                                          |
</TableWrapper>

::: footnote *
The `"ledger"` parameter is required in the path of the request for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). In such a case, the Twin [rule](../../overview/rules.md) needs to allow you to perform operations on the given [Twin](../twins/index.md).
:::

::: footnote **
- Any optional parameters not provided in the request will be left unchanged. 
- Any optional parameters provided in the request will replace current parameters.
- Operations replacing any current `"visibility"`, `"timeseries"`, or `"publish"` parameters with values equal to current parameter values will result in an update to the `"entry_updated_ts"` timestamp.
:::

::: footnote ***
If there is no `"history"` parameter in the request, no changes will be made to the History service.<br/>
If the `"history"` parameter value is replaced with value equal to current parameter values will result in an update to the `"entry_updated_ts"` timestamp.<br/>
If the value of the `"history"` parameter is set to `null`, the [History](../../reference/history/index.md) service will be disabled for the given Entry.<br/>
If `"history"` is set to `"INF"`, the most recent 1000 historical Entry values will be stored for the Entry.<br/>
If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values will be stored for the given time period subject to the maximum number of history records limit (1000 per Entry).
:::

### Include type Entry

<TableWrapper>
| Parameter                           | Type                 | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|:------------------------------------|:---------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*             | string               | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **ledger**<br/>*required**          | string               | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **visibility**<br/>*optional***     | string               | body | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                    |
</TableWrapper>

::: footnote *
The `"ledger"` parameter is required in the path of the request for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). In such a case, the Twin [rule](../../overview/rules.md) needs to allow you to perform operations on the given [Twin](../twins/index.md).
:::

::: footnote **
- Any optional parameters not provided in the request will be left unchanged. 
- Any optional parameters provided in the request will replace current parameters.
- Operations replacing the current `"visibility"` parameter with values equal to current parameter values will result in an update to the `"entry_updated_ts"` timestamp.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "entries": {
    "gdansk": {
      "value": 20.9
    },
    "krakow": {
        "value": 21
    }
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "entries": {
    "gdansk": {
      "value": 20.9
    },
    "krakow": {
        "value": 21
    }
  }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

entries_update = {
    "entries": {
        "gdansk": {
            "value": 20.9
        },
        "krakow": {
            "value": 21
        }
    }
}

status, response = TT_SERVICE.update_twin_ledger_entry("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal", body=entries_update)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userLedger = await ledgerApi.updateTwinLedgerEntry({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    ledger: "personal",
    patchUserLedger: {
        entries: {
            gdansk: {
                value: 20.9,
            },
            krakow: {
                value: 21,
            },
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

#### Entry attributes

<TableWrapper>
| Attribute            | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:---------------------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **entry_created_ts** | timestamp  | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **entry_updated_ts** | timestamp  | Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **value_changed_ts** | timestamp  | Time at which the value of an Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **value**            | string     | User-defined value of the Entry. This field cannot be changed by a user for Reference and Include type Entries.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **visibility**       | string     | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **ref***             | dictionary | <details> <ExtendedSummary markdown="span"> Reference. It allows to create an Entry based on the `"value"` field of an Entry in a different Ledger, especially a Ledger of a different account. The value that the reference is pointing to must be visible to the account creating the reference.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**source**</td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)), `{ledger}` (Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ Name of the Entry in the Ledger* </td> <td> Source path to the value that we want the Entry to reference. </td> </tr> <tr> <td>**status**</td>  <td> enum, value is `"created"`, `"ok"`, `"not_found"`, `"loop_detected"` or `"too_many_hops"`  </td>  <td> Status of the reference. It can have one of the following values:<br/>- `"created"`: The Entry was created.<br/>- `"ok"`: The Entry value is consistent with the value that the reference is pointing to.<br/>- `"not_found"`: The value could not be found. <br/>- `"loop_detected"`: The Entry is not accessible to the account because of a circular reference.<br/>- `"too_many_hops"`: There are too many transfers between references (the maximum number of hops allowed is 32). </td> </tr> </table></TableWrapper> </details> |
| **include****        | string     | Entry is an external const-like value fetched upon request.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **history**          | string     | Time for which the history of changes of the Entry's value is to be stored. Please note that the limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored. The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.<br/>If no `"history"` attribute is returned in the response, the [History](../../reference/history/index.md) service is not enabled.<br/> If `"history"` is set to `"INF"`, the most recent 1000 historical Entry values are stored for the Entry.<br/>If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values are stored for the given time period subject to the maximum number of history records limit (1000 per Entry).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **timeseries*****    | dictionary | <details> <ExtendedSummary markdown="span"> Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. If no `"timeseries"` attribute is returned in the response, the Entry value is not stored in a Timeseries table. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored. </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Key-value pair:<br/> - key: name of the dimension under which the Entry value is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension. </td> </tr></table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **publish**          | dictionary | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/>- key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).<br/>If no `"publish"` attribute is returned in the response, no notifications are set for the Entry.             |
</TableWrapper>


::: footnote *
The `"ref"` attribute is only returned for reference type Entries.
:::

::: footnote **
The `"include"` attribute is only returned for include type Entries.
:::

::: footnote ***
The [Timeseries](../timeseries/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
Please note that once you create an Entry, the type of the Entry (whether it is a value, include or reference type Entry) cannot be changed. You would need to delete the Entry and create a new Entry of a different type.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651074230.191,
            "value_changed_ts": 1651074230.191,
            "value": 20.9,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            },
            "publish": {
                "entry_new_value>entry_old_value": [
                    "value-increase"
                ]
            },
        },
        "krakow": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651074230.191,
            "value_changed_ts": 1651074230.191,
            "value": 21,
            "visibility": "USER.profession == 'analyst'",
            "history": "2W",
            "timeseries": {
                "environmental_data": {
                    "measurement": "temperature",
                    "dimensions": {
                        "city": "{entry_name}"
                    }
                }
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
