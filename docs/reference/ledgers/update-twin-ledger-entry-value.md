# Update Twin Ledger Entry value

<div class="row">
<div class="column">

This endpoint updates the value of one or more Entries of a Ledger. You can update the value of an Entry in your personal Ledger.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*         |
|:-------|:-------------------------------|:-------------------|
| PATCH   | /twins/{twin}/ledgers/{ledger}/value** | [update_twin_ledger_entry_value](./update-twin-ledger-entry-value.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_twin_ledger_entry_value" operation, the "update_twin_ledger_entry_value" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                       | Type   | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:--------------------------------|:-------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*       | string | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **ledger**<br/>*required***    | string | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). It must be equal to the [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of your personal Ledger.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **value**<br/>*required*    | valid JSON data type | body | User-defined value of the Entry. This field can be changed by users only for value type Entries. This field cannot be changed by the user for include and reference type Entries.                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "entries": {
    "gdansk": {
      "value": 20.9
    },
    "krakow": {
        "value": 21
    }
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal/value' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "entries": {
    "gdansk": {
      "value": 20.9
    },
    "krakow": {
        "value": 21
    }
  }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

entries_update = {
    "entries": {
        "gdansk": {
            "value": 20.9
        },
        "krakow": {
            "value": 21
        }
    }
}

status, response = TT_SERVICE.update_twin_ledger_entry_value("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal", body=entries_update)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const userLedger = await ledgerApi.updateTwinLedgerEntryValue({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    ledger: "personal",
    patchUserLedger: {
        entries: {
            gdansk: {
                value: 20.9,
            },
            krakow: {
                value: 21,
            },
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>


## Response

<div class="row">
<div class="column">

#### Entry attributes

<TableWrapper>
| Attribute            | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:---------------------|:-----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **entry_created_ts** | timestamp  | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| **entry_updated_ts** | timestamp  |  Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **value_changed_ts** | timestamp  | Time at which the value of an Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **value**            | string     | User-defined value of the Entry. This field cannot be changed by the user if the `"ref"` (reference) field exists.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "entries": {
        "gdansk": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651074230.191,
            "value_changed_ts": 1651074230.191,
            "value": 20.9
        },
        "krakow": {
            "entry_created_ts": 1651073498.69,
            "entry_updated_ts": 1651074230.191,
            "value_changed_ts": 1651074230.191,
            "value": 21
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.

