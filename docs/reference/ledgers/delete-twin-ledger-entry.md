# Delete Twin Ledger Entry

<div class="row">
<div class="column">

This endpoint deletes one or multiple Ledger Entries in your personal Ledger. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*            |
|:-------|:-------------------------------|:----------------------|
| DELETE | /twins/{twin}/ledgers/{ledger}** | delete_twin_ledger_entry |
</EndpointsTable>

::: footnote *
In order for a user to perform the "delete_twin_ledger_entry" operation, the "delete_twin_ledger_entry" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
If you don't specify the Entries to be removed with the `entry` query string parameter, all Entries will be removed.
:::

<TableWrapper>
| Parameter                     | Type                                   | In           | Description                                                                                            |
|:------------------------------|:---------------------------------------|:-------------|:-------------------------------------------------------------------------------------------------------|
| **twin**<br/>*required*    | string                                 | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                           |
| **ledger***<br/>*required** | string     | path | Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                      |
| **entries** <br/>*optional** | string | query string | Keys of the Entries to be removed.                                                                     |
</TableWrapper>

::: footnote *
If you don't include the `"entries"` parameter in the query string of the request, all Entries of a given Ledger will be removed.
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../../overview/rules.md) allows you to perform operations on the given [Twin](../twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request (removes all Entries of a Ledger)" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.delete_twin_ledger_entry("0c0832ea-36f4-4459-aefd-8422f550e4b1")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { LedgerApi, Configuration } from "@trustedtwin/js-client";

var ledgerApi = new LedgerApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const entriesDeleted = await ledgerApi.deleteTwinLedgerEntry({
    twin: "67299ffd-45a6-448b-ad1a-0a43e0dd4b6b",
    ledger: "personal",
});
```
</CodeBlock>

</ExtendedCodeGroup>

The above request removes all Entries from the given Ledger.

<ExtendedCodeGroup title="Request (removes a given Entry of a Ledger)" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/twins/0c0832ea-36f4-4459-aefd-8422f550e4b1/ledgers/personal?entries=wroclaw' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.delete_twin_ledger_entry("0c0832ea-36f4-4459-aefd-8422f550e4b1", "personal", params={"entries": "wroclaw"})
```
</CodeBlock>

</ExtendedCodeGroup>

The above request removes specific Entries with keys of the Entries to be removed provided in the query string.

</div>
</div>

## Response

<div class="row">
<div class="column">

#### Entry attributes

<TableWrapper>
| Attribute            | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:---------------------|:-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **entry_created_ts** | timestamp  | Time at which the Entry was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **entry_updated_ts** | timestamp  | Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| **value_changed_ts** | timestamp  | Time at which the value of an Entry was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **value**            | string     | User-defined value of the Entry. This field cannot be changed by the user if the `"ref"` (reference) field exists.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **visibility**       | string     | Visibility of the Entry. If the `"visibility"` of an Entry is `null`, the Entry is private. Private Entries are only visible to users of the account that owns the Ledger. If all Entries of the Ledger are private, the Ledger is private. If the `"visibility"` of an Entry is not `null`, the Entry is public. Public Entries are visible to users of the account that owns the Ledger and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) evaluates to `True`. If any of the Entries of the Ledger is public, the Ledger is public.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **ref***             | dictionary | <details> <ExtendedSummary markdown="span"> Reference. It allows to create an Entry based on the `"value"` field of an Entry in a different Ledger, especially a Ledger of a different account. The value that the reference is pointing to must be visible to the account creating the reference.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**source**</td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)), `{ledger}` (Ledger [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td> Source path to the value that we want the Entry to reference. </td> </tr> <tr> <td>**status**</td>  <td> enum, value is `"created"`, `"ok"`, `"not_found"`, `"loop_detected"` or `"too_many_hops"`  </td>  <td> Status of the reference. It can have one of the following values:<br/>- `"created"`: The Entry was created.<br/>- `"ok"`: The Entry value is consistent with the value that the reference is pointing to.<br/>- `"not_found"`: The value could not be found. <br/>- `"loop_detected"`: The Entry is not accessible to the account because of a circular reference.<br/>- `"too_many_hops"`: There are too many transfers between references (the maximum number of hops allowed is 32). </td> </tr> </table></TableWrapper> </details>        |
| **include****        | dictionary    | <details> <ExtendedSummary markdown="span"> Include property. It allows to create an Entry that fetches upon request the `"value"` field of an Entry in a different Ledger in the same account.  </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**source**<br/>*required** </td>  <td> string, composed of `{twin}` (Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids))/ name of the Entry in the Ledger* </td> <td>body</td> <td> Source path to the value that we want the Entry to include. </td> </tr> </table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **history**          | string     | Time for which the history of changes of the Entry's value is to be stored. Please note that the limit of stored History records is 1000 per Entry. If there are more than 1000 Entry value changes within the specified time period, only 1000 most recent History records will be stored. The `"history"` value must match the [regular expression](https://regexr.com/).<code>^([1-9][0-9]{0,2}[DWMY])&vert;(INF)$</code>.<br/>If no `"history"` attribute is returned in the response, the [History](../../reference/history/index.md) service is not enabled.<br/> If `"history"` is set to `"INF"`, the most recent 1000 historical Entry values are stored for the Entry.<br/>If `"history"` is set to a time period (e.g., days, weeks, months), the historical Entry values are stored for the given time period subject to the maximum number of history records limit (1000 per Entry).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **timeseries*****    | dictionary | <details> <ExtendedSummary markdown="span"> Timeseries attribute. It holds the name of the Timeseries table, the `"measurement"` attribute, and (optionally) the `"dimensions"` attribute. If no `"timeseries"` attribute is returned in the response, the Entry value is not stored in a Timeseries table. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr> </thead> <tr> <td>**measurement**</td>  <td> string </td>  <td> Name of the measurement column in the given Timeseries table under which the Entry value is to be stored. </td> </tr> <tr> <td>**dimensions**</td>  <td> dictionary </td>  <td> Key-value pair:<br/> - key: name of the dimension under which the Entry value is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension. </td> </tr></table></TableWrapper> </details>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **publish**          | dictionary | Key-value pairs that hold the details about the [Notifications](../../reference/notifications/index.md) set up for the given Entry:<br/>- key: Notification [rule](../../overview/rules.md) that defines the conditions to be met for a notification to be sent.<br/>- value: List of [name templates](../../overview/templates.md) for the given [Notification](../../reference/notifications/index.md).<br/>If no `"publish"` attribute is returned in the response, no notifications are set for the Entry.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
</TableWrapper>


::: footnote *
The `"ref"` attribute is only returned for reference type Entries.
:::

::: footnote **
The `"include"` attribute is only returned for include type Entries.
:::

::: footnote ***
The [Timeseries](../timeseries/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
Please note that once you create an Entry where the `"value"` is picked up through a `"reference"` from another Entry, you cannot update the Entry so that it does not contain a `"reference"`. In such case, you would need to delete the Entry and create a new Entry without the `"reference"` field.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
 "wroclaw": {
    "entry_created_ts": 1651073498.69,
    "entry_updated_ts": 1651073498.69,
    "value_changed_ts": 1651073498.69,
    "ref": {
        "source": "twin://0c0832ea-36f4-4459-aefd-8422f550e4b1/ca12b80e-045a-4c93-9e9b-2fc407fc5f2f/wroclaw",
        "status": "ok"
    },
    "visibility": "USER.profession == 'analyst'",
    "value": 21,
    "history": "2W",
    "timeseries": {
        "environmental_data": {
            "measurement": "temperature",
            "dimensions": {
                "city": "{entry_name}"
            }
        }
    },
    "publish": {
        "entry_new_value>entry_old_value": [
            "value-increase"
                ]
            }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
