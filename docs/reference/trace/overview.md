# Overview

The trace task response provides the trace task request stages with accompanying timestamps in the terminal.

<div class="row">
<div class="column">

<TableWrapper>
| Column               | Type                    | Description                                                                               | 
|:---------------------|:------------------------|:------------------------------------------------------------------------------------------|
| **timestamp**        | string                  | Timestamp denoting the start time of the task.                                                 | 
| **message category** | string, value is `INFO` | The `INFO` category denotes that this is an informational message.                                             |
| **task type**        | string                  | Type of the task.                                                                         | 
| **request UUID**     | string                  | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the request. | 
| **message**          | string                  | Message stating the stage of execution of the trace task.                                                                                  |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Trace" copyable>
<CodeBlock title="cURL">

```
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request started on [2022-09-16T10:07:19.962417+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request system task created on [2022-09-16T10:07:19.978092+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request response returned on [2022-09-16T10:07:19.980839+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request finished on [2022-09-16T10:07:19.982431+00:00]. 
2022-09-16T10:07:20.001121+00:00 | INFO | trace_system | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Task started on [2022-09-16T10:07:20.000806+00:00]. 
2022-09-16T10:07:20.001121+00:00 | INFO | trace_system | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Task finished on [2022-09-16T10:07:20.003455+00:00]. 
```
 </CodeBlock>
</ExtendedCodeGroup>

</div>
</div>





