---
tags:
  - endpoints
meta:
  - name: description
    content: 
---

# Trace

<div class="row">
<div class="column">

## About

On the Trusted Twin platform, you can trigger trace requests to the Trusted Twin API. <br/>
<br/>
Trace is a task that propagates along system tasks and allows to monitor the execution times of the stages in the task execution pipeline. Trace tasks provide insights into the structure of the requests and their processing times. They are helpful when analysing platform use.

## Triggering trace requests

In order to trigger a trace request:

1. Launch the [Log monitor](../../tools/log_monitor.md) program.
2. Send a request to the [trace](./trace.md) endpoint.
3. View the response in the terminal. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /trace | [trace](./trace.md)      |
</EndpointsTable>

::: footnote *
In order for a user to perform trace operations, the "trace" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

