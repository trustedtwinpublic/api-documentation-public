# Trace

<div class="row">
<div class="column">

This endpoint generates a trace. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /trace | trace      |
</EndpointsTable>

::: footnote *
In order for a user to perform trace operations, the "trace" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/trace' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.trace()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TraceApi, Configuration } from "@trustedtwin/js-client";

var traceApi = new TraceApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const log = await traceApi.trace();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>


## Response

<div class="row">
<div class="column">

The response returns an empty dictionary.

</div>
<div class="column">

<ExtendedCodeGroup title="Response" copyable>
<CodeBlock title="cURL">

```json
{}
```

</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Response (terminal)

<div class="row">
<div class="column">

The trace task response in the terminal provides the trace task request stages with accompanying timestamps.

<TableWrapper>
| Column name               | Type                    | Description                                                                               | 
|:---------------------|:------------------------|:------------------------------------------------------------------------------------------|
| **timestamp**        | string                  | Timestamp denoting the start time of the task.                                                 | 
| **message category** | string, value is `INFO` | The `INFO` category denotes that this is an informational message.                                             |
| **task type**        | string                  | Type of the task.                                                                         | 
| **request UUID**     | string                  | [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the request. | 
| **message**          | string                  | Message stating the stage of the trace task execution.                                                                                  |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Trace" copyable>
<CodeBlock title="cURL">

```
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request started on [2022-09-16T10:07:19.962417+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request system task created on [2022-09-16T10:07:19.978092+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request response returned on [2022-09-16T10:07:19.980839+00:00]. 
2022-09-16T10:07:19.962758+00:00 | INFO | trace | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Request finished on [2022-09-16T10:07:19.982431+00:00]. 
2022-09-16T10:07:20.001121+00:00 | INFO | trace_system | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Task started on [2022-09-16T10:07:20.000806+00:00]. 
2022-09-16T10:07:20.001121+00:00 | INFO | trace_system | 615f76e7-3ef3-4a81-a9cf-3b4ff4b6d002 | Trace task execution. Task finished on [2022-09-16T10:07:20.003455+00:00]. 
```
 </CodeBlock>
</ExtendedCodeGroup>

</div>
</div>


