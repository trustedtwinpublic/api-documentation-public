# Create Twin Identity   

<div class="row">
<div class="column">

This endpoint creates one or more Identities and attaches them to the given Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                     | Operation*         |
|:-------|:-------------------------|:-------------------|
| POST   | /twins/{twin}/identities | create_twin_identity |
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_twin_identity" operation, the "create_twin_identity" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type       | In   | Type                                                                                                                                                                                                                                                            |
|:--------------------------|:-----------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required* | string     | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                               |
| identities                | dictionary | body | Dictionary containing Identities with their parameters:<br/>- key: Identity name. String matching the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`.<br/>- value: [Identity parameters](./create-twin-identity#identity-parameters). |
</TableWrapper>


### Identity parameters

<TableWrapper>
| Parameter                        | Type                      | In   | Description                                                                                                                                                                                                                                                                                                                                                                                           |
|:---------------------------------|:--------------------------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **validity_ts**<br/>*optional* * | timestamp, `DEFAULT=null` | body | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If not provided, the value is set to `null` and the Identity does not expire.                                                                                                                                                    |
| **visibility**<br/>*optional* *  | string, `DEFAULT=null`    | body | Visibility of the Identity. An Identity created in an account is visible to users belonging to that account. If the value of the `"visibility"` attribute is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity. If the value is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and as well to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`. |
</TableWrapper>

::: footnote *
The `"visibility"` and `"validity_ts"` parameters are optional and do not need to be included in the request body when creating Identities. If they are not included in the request body, their default values are used. 
:::

In our example we create two Identities. We attach them to the Twin with the Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"f63ce1df-4643-49b2-9d34-38f4b35b9c7a"`.

The first Identity is `"RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa"`:    
- It expires on the 8th of March 2023 at 10:23:14 a.m. GMT (`"validity_ts": 1678270994.000`).
- It is a public Identity. Public Identities are visible to users of the account that owns the Identity and also to users of other accounts if the visibility [rule](../../overview/rules.md) of the Identity evaluates to `True`. In our example, the Identity is visible to users whose `"profession"` is either `"accounting"` or `"sales"` (`"visibility": "USER.profession == 'accounting' or USER.profession == 'sales'"`). 

The second Identity is `"RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb"`:
- It does not expire (`"validity_ts": null`).
- The Identity is private (`"visibility": null`). Private Identities are only visible to users of the account that owns the Identity.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{ 
    "identities": {
        "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
          "validity_ts": 1678270994.000,
          "visibility": "USER.profession == 'accounting' or USER.profession == 'sales'"       
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": { 
          "validity_ts": null,
          "visibility": null
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": { 
           "validity_ts": null,
           "visibility": null 
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/identities' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{ 
    "identities": {
        "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
          "validity_ts": 1678270994.000,
          "visibility": "USER.profession == '\''accounting'\'' or USER.profession == '\''sales'\''"       
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": { 
           "validity_ts": null,
           "visibility": null 
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": { 
           "validity_ts": null,
           "visibility": null 
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

identities_body = {
    "identities": {
        "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
            "validity_ts": 1678270994.000,
            "visibility": "USER.profession == 'accounting' or USER.profession == 'sales'"
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
            "validity_ts": None,
            "visibility": None
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
            "validity_ts": None,
            "visibility": None
        }
    }    
}

status, response = TT_SERVICE.create_twin_identity("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=identities_body)
```
</CodeBlock>


<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const createdIdentities = await identitiesApi.createTwinIdentity({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    postTwinIdentities: {
        identities: {
            "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
                validityTs: 1678270994.0,
                visibility: "USER.profession == 'accounting' or USER.profession == 'sales'",
            },
            "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
                validityTs: null,
                visibility: null,
            },
            "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
                validityTs: null,
                visibility: null,
            },
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns a list of Identities with their attributes.

<TableWrapper>
| Attribute | Type | Description                                                                                                                                                                                                                                                     |
|:---------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **identities** | dictionary | Dictionary containing Identities with their attributes:<br/>- key: Identity name. String matching the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`.<br/>- value: [Identity attributes](./create-twin-identity#identity-attributes). |
</TableWrapper>

### Identity attributes

<TableWrapper>
| Attribute                | Type      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:-------------------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **visibility**           | string    | Visibility of the Identity. An Identity created in an account is visible to users belonging to that account. If the value of the `"visibility"` attribute is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity. If the value is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and as well to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **validity_ts**          | timestamp | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the Identity does not expire.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts**           | timestamp | Time at which the Identity was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **creation_certificate** | dictionary    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon creation of the Identity. The creation certificate cannot be modified after it has been generated. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**identity**</td>  <td> string </td>  <td> User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`. </td>  </tr> <tr>  <td> **creator** </td>  <td> string </td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Identity.  </td> </tr> <tr> <td> **created_ts** </td> <td> timestamp </td> <td> Time at which the Identity was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody>  </table></TableWrapper> </details> |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "identities": {
        "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
            "visibility": "USER.profession == 'accounting' or USER.profession == 'sales'",
            "validity_ts": 1678270994.0,
            "updated_ts": 1646302849.877,
            "creation_certificate": {
                "identity": "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1646302849.877
            }
        },
        "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
            "visibility": null,
            "validity_ts": null,
            "updated_ts": 1646302849.877,
            "creation_certificate": {
                "identity": "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1646302849.877
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>
 
</div>
</div>

## Response ('Identity already exists')

<div class="row">
<div class="column">

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
If you try to create an Identity that already exists, you will receive the message `"error": 'Identity already exists.'"` for this particular Identity in the response. However, the response itself will return the `201 Created` status code. Please see the below example for more details.
:::

In the example below, we create two Identities - the Identity `"RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa"` ( it already exists in the account) and the Identity `"RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd"` (it does not exist in the account yet). 

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
   "identities":{
      "RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa":{
         "validity_ts":1677834409.000,
         "visibility":null
      },
      "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd":{
         "validity_ts":1677834409.000,
         "visibility":null
      }
   }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/identities' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{ 
    "identities": {
      "RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa": {
        "validity_ts": 1677834409.000,
        "visibility": null
      },
      "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd": {
        "validity_ts": 1677834409.000,
        "visibility": null
      }
    }
  }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

identities_body = {
    "identities": {
        "RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa": {
            "validity_ts": 1677834409.000,
            "visibility": None
        },
        "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd": {
            "validity_ts": 1677834409.000,
            "visibility": None
        }
    }
}

status, response = TT_SERVICE.create_twin_identity("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=identities_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const createdIdentities = await identitiesApi.createTwinIdentity({
            twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
            postTwinIdentities: {
                identities: {
                    "iRFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa": {
                        validityTs: 1677834409.000,
                        visibility: null,
                    },
                    "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd": {
                        validityTs: 1677834409.000,
                        visibility: null,
                    },
                },
            });
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

<div class="row">
<div class="column">

The response returns the message `"error": "Identity already exists."` for the Identity `"RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa"` (the Identity that already exists in the account) and the details of the created Identity `"RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd"`. The status code of the response is `201 Created`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "identities": {
        "RFID#ae14bdc-0f6d-a00-4091-1a6d793aaaa": {
            "error": "Identity already exists."
        },
        "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd": {
            "visibility": null,
            "validity_ts": 1677834409.0,
            "updated_ts": 1647353785.28,
            "creation_certificate": {
                "identity": "RFID#ae14bdc-0f6d-a00-4091-1a6d793dddd",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1647353785.28
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.

<TableWrapper>
| Status code | Message | Comment                                                                                                                                                           |
|:------------|:--------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 201         | Created | If an Identity that you are trying to create already exists, you will receive the error: `"error": "Identity already exists."` for this Identity in the response. |
</TableWrapper>
