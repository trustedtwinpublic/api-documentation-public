# Resolve Twin Identity

<div class="row">
<div class="column">

This endpoint returns a list of Twin [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) (if the `details` parameter is set to `False`) or the details of Twins (if the `details` parameter is set to `True`):
- If the `details` parameter is set to `False` (default value) all Twin [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) to which a given Identity is attached (if the Identity is visible to the calling user) are returned, regardless of whether the Twin is visible to the calling user.
- If the `details` parameter is set to `True`, the details of Twins to which a given Identity is attached (if the Identity is visible to the calling user) are returned. Only Twins visible to the calling user are returned. Please see the sections [request `details=True`](./resolve-twin-identity#request-detailstrue) and [response `details=True`](./resolve-twin-identity#response-detailstrue) for more details.

When resolving an Identity, the user can provide additional context for the resolution process to narrow down their search.

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
The search might result costly and time-consuming if you do not narrow down the context of your search. We highly recommend that you keep the context of the search as narrow as possible or to cache the response. You can narrow down the context by using the query string parameter `context`.
:::


</div>
<div class="column">

<EndpointsTable>
| Method | Path                | Operation*          |
|:-------|:--------------------|:--------------------|
| GET    | /resolve/{identity} | resolve_twin_identity |
</EndpointsTable>

::: footnote *
In order for a user to perform the "resolve_twin_identity" operation, the "resolve_twin_identity" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                     | Type                                                                      | In           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|:------------------------------|:--------------------------------------------------------------------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{identity}**<br/>*required* | string                                                                    | path         | User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`. It is stored in the `"identity"` field of the creation certificate of the Identity.                                                                                                                                                                                                                                                                                               |
| **context**<br/>*optional* *  | string (value is `{account}`, `personal`, or `system`); or list (value is list of `{account}` values); `DEFAULT=personal` | query string | As it is possible to use the same Identity to group several Twins, you can provide additional context to narrow down the list of Identities to be included in the search:<br/>- `{account}`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Identities owned by the specified account and visible to the requesting user.<br/>- `personal`: Identities owned by the account of the requesting user.<br/>- `system`: Identities in the entire Trusted Twin system visible to the requesting user.<br/>-list of {account} values: List of account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids). Identities owned by the specified account and visible to the requesting user. |
| **details**<br/> *optional* ** | boolean, `DEFAULT=False` | query string | Denotes whether the details of each Twin should be returned.                                     |
</TableWrapper>

::: footnote *
The `"context"` parameter is optional and does not need to be included in the request body when resolving Identities. If it is not included in the request, its default value is used. 
:::

::: footnote **
The `"details"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, a list of Twin UUIDs is returned. 
::: 

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/resolve/RFID%23be144bdc-0f6d-4a00-4091-1a6d793cbbbb' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.resolve_twin_identity("RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const resolvedIdentities = await identitiesApi.resolveTwinIdentity({
    identity: "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns a list of Twin [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that the Identity is attached to. In our example, the Identity `"RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb"` is attached only to one Twin. The Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin is `"700e3a9c-ed26-4f8a-82ed-c0e4237fc600"`.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "twins": [
        "700e3a9c-ed26-4f8a-82ed-c0e4237fc600"
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Request (`details=True`)

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                     | Type                                                                      | In           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|:------------------------------|:--------------------------------------------------------------------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{identity}**<br/>*required* | string                                                                    | path         | User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`. It is stored in the `"identity"` field of the creation certificate of the Identity.                                                                                                                                                                                                                                                                                               |
| **context**<br/>*optional* *  | string, value is `{account}`, `personal`, or `system`; `DEFAULT=personal` | query string | As it is possible to use the same Identity to group several Twins, you can provide additional context to narrow down the list of Identities to be included in the search:<br/>- `{account}`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). Identities owned by the specified account and visible to the requesting user.<br/>- `personal`: Identities owned by the account of the requesting user.<br/>- `system`: Identities in the entire Trusted Twin system visible to the requesting user. |
| **details**<br/> *optional* ** | boolean, `DEFAULT=False` | query string | Denotes whether the details of each Twin should be returned.                                     |
</TableWrapper>

::: footnote *
The `"context"` parameter is optional and does not need to be included in the request body when resolving Identities. If it is not included in the request, its default value is used. 
:::

::: footnote **
The `"details"` parameter is optional and does not need to be included in the query string of the request. If it is not included in the request, a list of Twin UUIDs is returned. 
::: 

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/resolve/RFID%23be144bdc-0f6d-4a00-4091-1a6d793cbbbb?details=True' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.resolve_twin_identity("RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb", params={"details": True})
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const resolvedIdentities = await identitiesApi.resolveTwinIdentity({
    identity: "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response (`details=True`)

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **owner**                   | string                                       | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account which is the current owner of the Twin. The ownership of a Twin can be transferred.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **status**                  | string, value is `"alive"` or `"terminated"` | The status of a Twin can be `"alive"` or `"terminated"`. In case of alive Twins, the `"description"` can be updated by the owner of the Twin. In case of terminated Twins, the description field cannot be updated. Ledger Entries, Identities, and Docs can be attached to alive and terminated Twins by all users involved in the process.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts**              | timestamp                                    | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **description**             | dictionary                                   | Attributes of the Twin defined by the owner in form of key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: JSON compliant value. <br/>Only the owner of the Twin can update the description of the Twin.<br/>For more details consult the [description field](../../overview/trusted-twin-api.md#description-field) section.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **creation_certificate**    | dictionary                                       | <details> <ExtendedSummary markdown="span">Certificate generated automatically by the system upon creation of a Twin. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**uuid**</td>  <td> string </td>  <td>Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). It is generated automatically when a Twin is created and stored in the `"uuid"` field of the creation certificate. </td>  </tr>  <tr>  <td> **creator** </td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Twin. </td>  </tr>  <tr>  <td> **created_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  </td>  </tr>  </tbody>  </table></TableWrapper></details> |
| **termination_certificate** | dictionary                                       | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system when a Twin is terminated. It cannot be modified after it has been generated.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**issuer**</td>  <td> string </td>  <td>Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account terminating the Twin. </td>  </tr>   <tr>  <td> **terminated_ts** </td>  <td> timestamp </td>  <td>Time at which the Twin was terminated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  </td>  </tr>  </tbody>  </table></TableWrapper> </details>                                                                                                                                                                                                                                                        |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "twins": [
        {
            "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "status": "alive",
            "updated_ts": 1688797476.023,
            "description": {
                "serial_number": 123
            },
            "creation_certificate": {
                "uuid": "f98bd395-c41d-4a80-8cdd-86cd275a5f3d",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1688797476.023
            }
        },
        {
            "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "status": "terminated",
            "updated_ts": 1688934276.843,
            "creation_certificate": {
                "uuid": "aa445449-138e-4a20-87df-d78edbb845b3",
                "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "created_ts": 1688916276.69
            },
          "termination_certificate": {
                "issuer": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
                "terminated_ts": 1688934276.843
          } 
        }
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
