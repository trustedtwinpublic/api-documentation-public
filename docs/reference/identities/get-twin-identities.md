# Get Twin Identities

<div class="row">
<div class="column">

This endpoint retrieves a list of Identities of the given Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                     | Operation*        |
|:-------|:-------------------------|:------------------|
| GET    | /twins/{twin}/identities | get_twin_identities |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_twin_identities" operation, the "get_twin_identities" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                                                                                                                                                             | In           | Description                                                                                                                                                                                                                                                                                                                                            |
|:-----------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*          | string                                                                                                                                                           | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                           |
| **show_foreign**<br/>*optional* *  | boolean/string: if boolean `DEFAULT=False`, string value must be `{account}` (account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)) | query string | Shows public Identities owned by different accounts if the Identities are visible to the requesting user. Public Identities are visible to users of the account that owns the Identity and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`. | 
| **show_personal**<br/>*optional* * | boolean, `DEFAULT=True`                                                                                                                                          | query string | Shows Identities that are attached to the account of the requesting user.                                                                                                                                                                                                                                                                              |
| **show_expired**<br/>*optional* *  | boolean, `DEFAULT=False`                                                                                                                                         | query string | Additional filter for personal Identities: shows personal Identities where `validity_ts` <= `now` (current time).                                                                                                                                                                                                                                                                                          |
| **show_valid**<br/>*optional*  *   | boolean, `DEFAULT=True`                                                                                                                                          | query string | Additional filter for personal Identities: shows personal Identities where `validity_ts` > `now` (current time). |
| **show_public**<br/>*optional*  *  | boolean, `DEFAULT=True`                                                                                                                                          | query string | Additional filter for personal Identities: shows personal Identities that are public. Public Identities are visible to users of the account that owns the Identity and also visible to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`.                                                                      |                                                                                                                                                                                                                                                                                                                               |
| **show_private**<br/>*optional* *  | boolean, `DEFAULT=True`                                                                                                                                          | query string | Additional filter for personal Identities: shows personal Identities that are private. Private Identities are only visible to users of the account that owns the Identity.                                                                                                                                                                                                                       |

</TableWrapper>

::: footnote *
The `show_expired`, `show_valid`, `show_foreign`, `show_personal`, `show_public`, and `show_private` parameters are optional and do not need to be included in the request. If they are not included in the request, their default values are used.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/identities' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_twin_identities("f63ce1df-4643-49b2-9d34-38f4b35b9c7a")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const identities = await identitiesApi.getTwinIdentities({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

The response returns a list of [Identities](../../overview/trusted-twin-api.md#trusted-twin-ids) for the requested Twin grouped by account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids).

<TableWrapper>
| Attribute | Type | Description | 
|:----|:------|:-------|
| **accounts** | dictionary | Dictionary containing accounts with Identities:<br/>- key: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids)<br/>- value: [Identity](./get-twin-identities#identity). |
</TableWrapper>

### Identity

<TableWrapper>
| Attribute | Type | Description |
|:--------|:-------|:--------|
| **{identity}** | dictionary | Dictionary containing a given Identity with attributes:<br/>- key: Identity name. String matching the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`.<br/>- value: [Identity attributes](./get-twin-identities#identity-attributes). |
</TableWrapper>

### Identity attributes

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                                                                |
|:----------------|:----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **validity_ts** | timestamp | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the Identity does not expire. |
| **created_ts**  | timestamp | Time at which the Identity was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                   |
| **updated_ts**  | timestamp | Time at which the Identity was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                              | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "accounts": {
        "9891264d-4a77-4fa2-ae7f-84c9af14ae3b": {
            "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
                "validity_ts": null,
                "created_ts": 1646302849.877,
                "updated_ts": 1646302849.877
            },
            "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
                "validity_ts": 1677834409.0,
                "created_ts": 1646302849.877,
                "updated_ts": 1646302849.877
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
