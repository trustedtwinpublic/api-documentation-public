# Update Twin Identity

<div class="row">
<div class="column">

This endpoint updates the requested Identity of a given Twin. Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*         |
|:-------|:------------------------------------|:-------------------|
| PATCH  | /twins/{twin}/identities/{identity} | update_twin_identity |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_twin_identity" operation, the "update_twin_identity" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:-----------------------------------|:--------------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*        | string                    | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                 |
| **{identity}**<br/>*required*      | string                    | path | User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`. It is stored in the `"identity"` field of the creation certificate of the Identity.                                                                                                                                                                                                                          |
| **validity_ts**<br/>*optional* * | timestamp, `DEFAULT=null` | body | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the Identity does not expire.                                                                                                                                                                                                                                   |
| **visibility** <br/>*optional* *   | string, `DEFAULT=null`    | body | Visibility of the Identity. An Identity created in an account is visible to users belonging to that account. If the value of the `"visibility"` attribute is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity. If the value is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and as well to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`. |
</TableWrapper>

::: footnote *
Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters.
:::

In our example, we want to change the `"validity_ts"` of the Identity so that the Identity does not expire (`"validity_ts": null`). We also want to make it a private Identity (`"visibility": null`) so that it is only visible to users of the account that owns the Identity.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "validity_ts": null,
  "visibility": null
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/identities/RFID%23ae144bdc-0f6d-4a00-4091-1a6d793aaaa' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "validity_ts": null,
  "visibility": null
}
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

new_identity_body = {
    "validity_ts": None,
    "visibility": None
}

status, response = TT_SERVICE.update_twin_identity("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa", body=new_identity_body)

```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { IdentitiesApi, Configuration } from "@trustedtwin/js-client";

var identitiesApi = new IdentitiesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const updatedIdentityProperties = await identitiesApi.updateTwinIdentity({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    identity: "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa",
    patchIdentityProperties: {
        validityTs: null,
        visibility: null,
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute                | Type      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:-------------------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **visibility**           | string    | Visibility of the Identity. An Identity created in an account is visible to users belonging to that account. If the value of the `"visibility"` attribute is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity. If the value is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and as well to users of other accounts if the visibility [rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| **validity_ts**          | timestamp | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the Identity does not expire.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts**           | timestamp | Time at which the Identity was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **creation_certificate** | dictionary    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon creation of the Identity. The creation certificate cannot be modified after it has been generated. </ExtendedSummary> <table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**identity**</td>  <td> string </td>  <td> User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}$`. </td>  </tr> <tr>  <td> **creator** </td>  <td> string </td> <td> account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Identity.  </td> </tr> <tr> <td> **created_ts** </td> <td> timestamp </td> <td> Time at which the Identity was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody>  </table> </details> |                                       
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "visibility": null,
    "validity_ts": null,
    "updated_ts": 1646303123.409,
    "creation_certificate": {
        "identity": "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1646302849.877
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
