# Overview

## Private Identity

<div class="row">
<div class="column">

In our example, the Identity `"RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb"` is owned by the account with the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"9891264d-4a77-4fa2-ae7f-84c9af14ae3b"`:
- As `"visibility"` is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity.
- As `"validity_ts"` is `null`, the Identity does not expire.

</div>
<div class="column">

<ExtendedCodeGroup title="Private Identity example" copyable>
<CodeBlock title="json">
```json
"RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb": {
  "visibility": null,
  "validity_ts": null,
  "updated_ts": 1646302849.877,
  "creation_certificate": {
    "identity": "RFID#be144bdc-0f6d-4a00-4091-1a6d793cbbbb",
    "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "created_ts": 1646302849.877
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Public Identity

<div class="row">
<div class="column">

In our example, the Identity `"RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa"` is owned by the account with the account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"9891264d-4a77-4fa2-ae7f-84c9af14ae3b"`:   
- As `"visibility"` is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and also to users of other accounts if the visibility [rule](../../overview/rules.md) of the Identity evaluates to `True`. In our example, the Identity is visible to users whose `"profession"` is either `"accounting"` or `"sales"` (`"visibility": "USER.profession == 'accounting' or USER.profession == 'sales'"`). 
- As `"validity_ts"` is not `null`, the Identity is set to expire. The `"validity_ts"` attribute holds the [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) `1678270994.0`. It means that it expires on the 8th of March 2023 at 10:23:14 a.m. GMT.

</div>
<div class="column">

<ExtendedCodeGroup title="Public Identity example" copyable>
<CodeBlock title="json">
```json
"RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa": {
    "visibility": "USER.profession == 'accounting' or USER.profession == 'sales'",
    "validity_ts": 1678270994.0,
    "updated_ts": 1646302849.877,
    "creation_certificate": {
        "identity": "RFID#ae144bdc-0f6d-4a00-4091-1a6d793aaaa",
        "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
        "created_ts": 1646302849.877
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute                | Type      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:-------------------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **visibility**           | string    | Visibility of the Identity. An Identity created in an account is visible to users belonging to that account. If the value of the `"visibility"` attribute is `null`, the Identity is private. Private Identities are only visible to users of the account that owns the Identity. If the value is not `null`, the Identity is public. Public Identities are visible to users of the account that owns the Identity and as well to users of other accounts if the visibility [Rule](../../overview/rules.md) held in the `"visibility"` attribute evaluates to `True`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **validity_ts**          | timestamp | Time at which the Identity expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). If the value is `null`, the Identity does not expire.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts**           | timestamp | Time at which the Identity was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **creation_certificate** | dictionary    | <details> <ExtendedSummary markdown="span"> Certificate generated automatically by the system upon creation of the Identity. The creation certificate cannot be modified after it has been generated. </ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**identity**</td>  <td> string </td>  <td> User-defined ID for the Identity. It must match the [regular expression](https://regexr.com/) `^[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,64}$`. </td>  </tr> <tr>  <td> **creator** </td>  <td> string </td> <td> Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account creating the Identity.  </td> </tr> <tr> <td> **created_ts** </td> <td> timestamp </td> <td> Time at which the Identity was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). </td> </tr> </tbody>  </table></TableWrapper> </details> |
</TableWrapper>
