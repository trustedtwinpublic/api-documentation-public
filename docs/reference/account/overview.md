---
tags:
  - account
  - account details
meta:
    - name: Account overview
      content: This section presents an overview of the account object on the Trusted Twin platform.
---


# Overview

<div class="row">
<div class="column">
 
<TableWrapper>
| Attribute | Type | Description |
|:----------|:-----|:------------|
| **uuid** | string |  Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).   |
| **created_ts** | timestamp | Time at which the account was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **updated_ts** | timestamp | Time at which the account configuration was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **resource_access_log*** | dictionary |  <details> <ExtendedSummary markdown="span"> Dictionary defining the [Resource Access Log](../../account-and-access/resource-access-log.md) tables configured for the account. The key is a [Rule](../../overview/rules.md), the value is a dictionary with [Resource Access Log](../../account-and-access/resource-access-log.md) table names (keys) and their (optional) `"dimensions"` attributes. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the [Resource Access Log](../../account-and-access/resource-access-log.md) entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> |
</TableWrapper>

::: footnote *
The `"resource_access_log"` attribute can be added, updated and removed through the [update_account](./update-account.md) endpoint. If no `"resource_access_log"` attribute is present in the response, the [Resource Access Log](../../accounting/resource-access-log.md) functionality is not enabled for the account.
:::


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "ea31d883-8309-41f6-90c9-8ab2af20af34",
    "created_ts": "1700511077.00",
    "updated_ts": "1700856677.00",
    "resource_access_log": {
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_1": {},
            "resource_access_log_table_2": {}
        },
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
