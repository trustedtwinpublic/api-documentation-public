---
tags:
  - get account
  - account
  - account details
meta:
    - name: get account
      content: This endpoint retrieves the details of the account of the calling user.
---

# Get account

<div class="row">
<div class="column">

This endpoint retrieves the details of the account of the calling user.

</div>
<div class="column">


<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| GET   | /account | get_account |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_account" operation, the "get_account" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/account' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_account()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const twinAlive = await accountApi.getAccount({});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">
 
<TableWrapper>
| Attribute | Type | Description |
|:----------|:-----|:------------|
| **uuid** | string |  Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).   |
| **created_ts** | timestamp | Time at which the account was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **updated_ts** | timestamp | Time at which the account configuration was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **resource_access_log**        | dictionary                  | <details> <ExtendedSummary markdown="span"> Dictionary defining the [Resource Access Log](../../account-and-access/resource-access-log.md) tables configured for the account. The key is a [Rule](../../overview/rules.md), the value is a dictionary with [Resource Access Log](../../account-and-access/resource-access-log.md) table names (keys) and their (optional) `"dimensions"` attributes. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the [Resource Access Log](../../account-and-access/resource-access-log.md) entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> |
</TableWrapper>



In our example, the response returns the details of the account as well as the [Resource Activity Log](../../accounting/resource-access-log.md) details:

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "ea31d883-8309-41f6-90c9-8ab2af20af34",
    "created_ts": "1700511077.00",
    "updated_ts": "1700856677.00",
    "resource_access_log": {
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_1": {},
            "resource_access_log_table_2": {}
        },
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
