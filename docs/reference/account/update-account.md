---
tags:
  - update account
  - account
  - resource access log
meta:
    - name: update account
      content: How to add, update, or remove the Resource Access Log
---

# Update account

<div class="row">
<div class="column">

This endpoint adds, updates or removes the `"resource_activity_log"` property.

</div>
<div class="column">


<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| PATCH   | /account | update_account |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_account" operation, the "update_account" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                         | Type       | In   | Description                                                                                                                                                                                                                                                                                                                                                                                    |
|:----------------------------------|:-----------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **resource_access_log**<br/>*required*           | dictionary                  | body |  <details> <ExtendedSummary markdown="span"> Dictionary defining the [Resource Access Log](../../account-and-access/resource-access-log.md) tables configured for the account. The key is a [Rule](../../overview/rules.md), the value is a dictionary with [Resource Access Log](../../account-and-access/resource-access-log.md) table names (keys) and their (optional) `"dimensions"` attributes. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the [Resource Access Log](../../account-and-access/resource-access-log.md) entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details>|
</TableWrapper>

::: footnote *
If the `"resource_access_log"` field with attributes is provided in the request body, the `"resource_access_log"` field from the request body will replace the current `"resource_access_log"` field.<br/>      
If no attributes are provided in the request body of `"resource_access_log"` (`"resource_access_log": {}`), all attributes from the current `"resource_access_log"` will be removed. The response will return an empty dictionary (`"resource_access_log": {}`).<br/>    
If `"resource_access_log"` with the value `null` is provided in the request body, all attributes from the current `"resource_access_log"` will be removed. The response will not return the `"resource_access_log"` field.<br/>
If the request body does not contain `"resource_access_log"`, no changes will be made to the `"resource_access_log"` field.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "resource_access_log": {
        "account == '6727ff91-60f7-4526-b8f1-bb75de904e73'": {
            "resource_access_log_table_1": {}
        },
        "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
            "resource_access_log_table_2": {},
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "resource_access_log": {
        "account == '6727ff91-60f7-4526-b8f1-bb75de904e73'": {
            "resource_access_log_table_1": {}
        },
        "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
            "resource_access_log_table_2": {},
            "resource_access_log_table_3": {}
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

resource_access_log_body = {
    "resource_access_log": {
        "account == '6727ff91-60f7-4526-b8f1-bb75de904e73'": {
            "resource_access_log_table_1": {}
        },
        "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
            "resource_access_log_table_2": {},
            "resource_access_log_table_3": {}
        }
    }
}

status, response = TT_SERVICE.update_account(body=resource_access_log_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { TwinsApi, Configuration } from "@trustedtwin/js-client";

var twinsApi = new TwinsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const TwinAlive = await accountApi.updateAccount({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    description: {
        "resource_access_log": {
            "account == '6727ff91-60f7-4526-b8f1-bb75de904e73'": {
                "resource_access_log_table_1": {}
            },
            "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
                "resource_access_log_table_2": {},
                "resource_access_log_table_3": {}
            }
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">
 
<TableWrapper>
| Attribute | Type | Description |
|:----------|:-----|:------------|
| **uuid** | string |  Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).   |
| **created_ts** | timestamp | Time at which the account was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **updated_ts** | timestamp | Time at which the account configuration was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **resource_access_log**        | dictionary                  | <details> <ExtendedSummary markdown="span"> Dictionary defining the [Resource Access Log](../../account-and-access/resource-access-log.md) tables configured for the account. The key is a [Rule](../../overview/rules.md), the value is a dictionary with [Resource Access Log](../../account-and-access/resource-access-log.md) table names (keys) and their (optional) `"dimensions"` attributes. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tr> <td>**dimensions**</td>  <td> dictionary </td> <td> body </td>  <td> Key-value pairs:<br/> - key: name of the dimension under which the [Resource Access Log](../../account-and-access/resource-access-log.md) entry is to be stored. <br/> - value: [Template](../../overview/templates.md) for the value of the dimension.  </td> </tr> </table></TableWrapper> </details> |
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "ea31d883-8309-41f6-90c9-8ab2af20af34",
    "created_ts": "1700511077.00",
    "updated_ts": "1700856677.00",
    "resource_access_log": {
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_1": {},
            "resource_access_log_table_2": {}
        },
        "account == '4b7e036e-496d-409a-828e-594c7f7508eb': {
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.






