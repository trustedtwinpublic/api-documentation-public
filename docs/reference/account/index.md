---
tags:
  - account endpoints
meta:
    - name: Account endpoints
      content: How to retrieve the details of your account and update your account
---

# Account

<div class="row">
<div class="column">

## About 

The endpoints in this section allow you to get the details of your account of the calling user as well as to update your account.

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
Please contact <hello@trustedtwin.com> for more details about creating an account.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*                                       |
|:-------|:------------------------------|:-------------------------------------------------|
| GET   | /account            | [get_account](./get-account.md)                   |
| PATCH   | /account | [update_account](./update-account.md)                         |
</EndpointsTable>


::: footnote *
In order for a user to perform operations on the calling user's account, the respective permissions "get_account" and "update_account" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
