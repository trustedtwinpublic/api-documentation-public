# Cache

<div class="row">
<div class="column">

## About 

The endpoints in this section allow you to create and invalidate upload URLs. Upload URLs are used to upload [Docs](../docs/index.md) or batch files (see [Batches](../../reference/batches/index.md)) to temporary storage.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                          | Operation*                                       |
|:-------|:------------------------------|:-------------------------------------------------|
| POST   | /cache            | [create_upload_url](./create-upload_url.md)                   |
| DELETE   | /cache/{handler} | [invalidate_upload_url](./invalidate-upload-url.md)                         |
</EndpointsTable>


::: footnote *
In order for a user to perform operations related to upload URLs, the respective permissions "create_upload_url" and "invalidate_upload_url" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
