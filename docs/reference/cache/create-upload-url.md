---
tags:
  - upload doc
  - upload file
  - upload url
---

# Create upload URL

<div class="row">
<div class="column">

This endpoint creates a temporary upload URL. A temporary upload URL can be used to:
- upload a [Doc](../docs/index.md) to temporary storage. After the upload URL has been generated, you need to send a [PUT request](./create-upload-url#PUT-request) to that URL to upload the Doc to temporary storage.  
- upload a file with the details of a [batch](../batches/index.md) operation. After the upload URL has been generated, you need to send a [PUT request](./create-upload-url#PUT-request) to that URL to upload the file to temporary storage.  

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
The upload URL is temporary and expires after 60 minutes. After the upload URL has expired, you will need to generate a new upload URL.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path             | Operation*      |
|:-------|:-----------------|:----------------|
| POST   | /cache           | create_upload_url |
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_upload_url" operation, the "create_upload_url" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/cache' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.create_upload_url()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const fileAddResponse = await docsApi.createUploadURL();
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                                                                  |
|:----------------|:----------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **url**         | string    | Temporary upload URL used to upload the Doc to temporary storage through sending a PUT request to that URL.                                                                                                                  |
| **handler**     | string    | URL handler used in the [attach_twin_doc](./attach-a-doc.md) endpoint a Doc to a Twin and to store the Doc in the Twin's directory.                                                                                         | 
| **validity_ts** | timestamp | Time at which the upload URL expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). The upload URL expires 60 minutes after its creation. | 
</TableWrapper>

The response returns:
- A temporary upload URL (`"https://example_url.com/e544230b-cf65"`) held in the `"url"` attribute. The upload URL is used to upload the Doc to temporary storage. 
- A URL handler (`"a7fbcd7c-50a7-4df5-a6e5-416559ae24ae"`) held in the `"handler"` attribute. The handler is used in the [attach_twin_doc](../docs/attach-twin-doc.md) endpoint to attach a Doc to a Twin. 
- A [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) stating when the upload URL expires held in the `"validity_ts"` attribute. The upload URL expires 60 minutes after it has been generated.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "url": "https://rest.trustedtwin.com/uploads/e544230b-cf65&Expires=1648803695",
    "handler": "a7fbcd7c-50a7-4df5-a6e5-416559ae24ae",
    "validity_ts": 1648803695.701
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## PUT request

<div class="row">
<div class="column">

In order to upload the Doc to temporary storage, you need to send a PUT request to the upload URL.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PUT 'https://rest.trustedtwin.com/uploads/e544230b-cf65&Expires=1648803695' \
--header 'Content-Type: text/plain' \
--data-binary '@/C:/Users/User_A/Documents/example_file.txt'
```
</CodeBlock>

<CodeBlock title="Python">

```python
import requests

file_to_upload_path = "@/C:/Users/User_A/Documents/example_file.txt"

response = requests.put("https://rest.trustedtwin.com/uploads/e544230b-cf65&Expires=1648803695", data=open(file_to_upload_path, 'rb'))
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
