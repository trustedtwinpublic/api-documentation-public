# Invalidate upload URL

<div class="row">
<div class="column">

This endpoint invalidates the temporary upload URL. You will not be able to upload Docs or files for batch operation files via this URL after it has been invalidated.    

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
- If a Doc was uploaded through the upload URL, but was not attached to a Twin through the [attach_twin_doc](../docs/attach-twin-doc.md) endpoint, it will be deleted after the upload URL has been invalidated. 
- If a batch operation file was uploaded through the upload URL, but no request was submitted through the [create_batch](../../reference/batches/create-batch.md) endpoint, it will be deleted after the upload URL has been invalidated.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                                                     | Operation*          |
|:-------|:-------------------------------------------------------------------------|:--------------------|
| DELETE | /cache/{handler}                                                         | invalidate_upload_url |
</EndpointsTable>

::: footnote *
In order for a user to perform the "invalidate_upload_url" operation, the "invalidate_upload_url" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                  | Type   | In   | Description                                                                                                                                                                                                         |
|:---------------------------|:-------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{handler}**<br/>*required* | string | path | URL handler generated through the [create_upload_url](./create-an-upload-url.md) endpoint. It is used in the [attach_twin_doc](./attach-a-doc.md) endpoint to attach a Doc to a Twin and to store the Doc in the Twin's directory. |
</TableWrapper>

In our example, we invalidate the upload URL with the handler `128ba3ce-a0ea-47c7-a487-3a8531fc3312`.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/cache/128ba3ce-a0ea-47c7-a487-3a8531fc3312' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.invalidate_upload_url("128ba3ce-a0ea-47c7-a487-3a8531fc3312")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { DocsApi, Configuration } from "@trustedtwin/js-client";

var docsApi = new DocsApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const invalidateHandlerResponse = await docsApi.invalidateUploadURL({
    handler: "128ba3ce-a0ea-47c7-a487-3a8531fc3312",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute   | Type   | Description                                                                                                                                                                                                         |
|:------------|:-------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **handler** | string | URL handler generated through the [create_upload_url](./create-an-upload-url.md) endpoint. It is used in the [attach_twin_doc](./attach-a-doc.md) endpoint to attach a Doc to a Twin and to store the Doc in the Twin's directory. |
</TableWrapper>

The response returns the handler of the invalidated upload URL.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "handler": "128ba3ce-a0ea-47c7-a487-3a8531fc3312"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
