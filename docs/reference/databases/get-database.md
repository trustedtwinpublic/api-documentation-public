# Get database

<div class="row">
<div class="column">

This endpoint retrieves the details of the given database.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| GET    | account/services/databases/{database} | get_database |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_database" operation, the "get_database" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) or `"default"` for requests on default database. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_database("97a04302-9f36-4ad4-8530-800284b04ffe")

```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">


<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**          | string                                                            | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                           |
| **default**       | boolean                                                           | Denotes whether the given database is the default database for the account.                                                                                                                                                                                     |
| **status**        | string, value is `"running"`, `"pending"`, or `"service"`         | Status of the database:<br/> - `"running"`: The Timeseries database is running.<br/> - `"pending"`: The Timeseries database is in the process of initialising. <br/> - `"service"`: Unexpected behaviour occurred and the Timeseries database is not available. |
| **database_size** | integer                                                           | Size, in bytes, of the database.                                                                                                                                                                                                                                |
| **instance_type** | string, value is `"micro"`, `"medium"`, `"xlarge"`, or `"custom"` | Instance type used in the database.                                                                                                                                                                                                                             |
| **note**          | string                                                            | User-defined note. The maximum length of a note is 256 characters.                                                                                                                                                                                             |
| **created_ts**    | timestamp                                                         | Time at which the database was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                        |
| **updated_ts**    | timestamp                                                         | Time at which the database was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{   
    "default": true,
    "status": "running",
    "database_size": 19710755,
    "uuid": "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c",
    "instance_type": "medium",
    "note": "database_x", 
    "created_ts": 1706021380.00,
    "updated_ts": 1706021380.00
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>




