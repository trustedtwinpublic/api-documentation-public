# Overview

<div class="row">
<div class="column">


<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**          | string                                                            | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                           |
| **default**       | boolean                                                           | Denotes whether the given database is the default database for the account.                                                                                                                                                                                     |
| **status**        | string, value is `"running"`, `"pending"`, or `"service"`         | Status of the database:<br/> - `"running"`: The Timeseries database is running.<br/> - `"pending"`: The Timeseries database is in the process of initialising. <br/> - `"service"`: Unexpected behaviour occurred and the Timeseries database is not available. |
| **database_size** | integer                                                           | Size, in bytes, of the database.                                                                                                                                                                                                                                |
| **instance_type** | string, value is `"micro"`, `"medium"`, `"xlarge"`, or `"custom"` | Instance type used in the database.                                                                                                                                                                                                                             |
| **note**          | string                                                            | User-defined note. The maximum length of a note is 256 characters.                                                                                                                                                                                             |
| **created_ts**    | timestamp                                                         | Time at which the database was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                        |
| **updated_ts**    | timestamp                                                         | Time at which the database was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{   
    "default": true,
    "status": "running",
    "database_size": 19710755,
    "uuid": "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c",
    "instance_type": "medium",
    "note": "database_x", 
    "created_ts": 1706021380.00,
    "updated_ts": 1706021380.00
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
