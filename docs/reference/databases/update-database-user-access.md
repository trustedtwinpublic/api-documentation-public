# Update database user access

<div class="row">
<div class="column">

This endpoint adds, updates, or removes the access of a given user to a given database.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| PATCH    | account/services/databases/{database}/access/users/{user} | update_database_user_access |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_database_user_access" operation, the "update_database_user_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) or `"default"` for requests on default database. |
| **{user}**<br/>*required* | string | path | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). |
| **timeseries**<br/>*optional**    | list of strings, value is `"read"`, or empty list | body | Permissions of the user to Timeseries tables in the database.            |
| **indexes**<br/>*optional**         | list of strings, value is `"read"`, or empty list | body | Permissions of the user to Indexes tables in the database.            |
| **customer_data**<br/>*optional**   | list of strings, value is `"write"`, `"read"`, or empty list | body | Permissions of the user to the customer schema in the database. The customer schema allows users with the `"write"` permission to create their own tables in the database.            | 
</TableWrapper>

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "timeseries": ["read"],
    "indexes": [],
    "customer_data": ["read", "write"]

}
```

</CodeBlock>
</ExtendedCodeGroup>


</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe/users/10dbe3a4-e9d7-4125-8bdd-31ebb3838d5b' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "timeseries": ["read"],
  "indexes": [],
  "customer_data": ["read", "write"]
}
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

update_user_body = {
    "timeseries": ["read"],
    "indexes": [],
    "customer_data": ["read", "write"]
}

status, response = TT_SERVICE.update_database_user_access("97a04302-9f36-4ad4-8530-800284b04ffe", "10dbe3a4-e9d7-4125-8bdd-31ebb3838d5b", body=update_user_body)
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute | Type | Description                                                   | 
|:---------|:-----|:--------------------------------------------------------------|
| **timeseries** | list of strings, value is `"read"`, or empty list     | Permissions of the user to Timeseries tables in the database. |
| **indexes** | list of strings, value is `"read"`, or empty list     | Permissions of the user to Indexes tables in the database.    |
| **customer_data** |  list of strings, value is `"write"`, `"read"`, or empty list    | Permissions of the user to the customer schema in the database. The customer schema allows users with the `"write"` permission to create their own tables in the database. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "timeseries": ["read"],
    "indexes": [],
    "customer_data": ["read", "write"]

}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>


