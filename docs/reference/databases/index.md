---
tags:
  - databases
  - advanced database services
  - database endpoints
meta:
  - name: Database endpoints on the Trusted Twin platform
    content: This section describes the database endpoints on the Trusted Twin platform.
---

# Databases

<div class="row">
<div class="column">

## About

The advanced database services on the Trusted Twin platform allow you to store structured and multidimensional views of Twin history in form of user-defined tables ([Timeseries](../timeseries/index.md) service) as well as to store structured and multidimensional views of Twin states in form of user-defined tables ([Indexes](../indexes/index.md) service).

Please contact <hello@trustedtwin.com> for more details about enabling advanced database services for your account.

## Structure

You can have one or multiple databases for a single Trusted Twin account. 

A database stores [Timeseries](../timeseries/index.md) and [Indexes](../timeseries/index.md) tables. 

For all Timeseries tables in the context of an account, each Timeseries table name must be unique. The name must match the [regular expression](https://regexr.com/) `^(_*[a-z0-9]+)([_-][0-9a-z]+)*$`.

For all Indexes tables in the context of an account, each Indexes table name must be unique. The name must match the [regular expression](https://regexr.com/) `^(_*[a-z0-9]+)([_-][0-9a-z]+)*$`.

## Default database

If there is one database in a Trusted Twin account, this database is the default database for that account. If there is more than one database for a Trusted Twin account, the default database will be initially the database that was created first in the given account. The default database can be changed.

When performing operations on the default database, you can use `"default"` instead of the database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). For example:

<ExtendedCodeGroup title="Request (default database)" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/default' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_database("default")

```
</CodeBlock>

</ExtendedCodeGroup>

<ExtendedCodeGroup title="Request (non-default database)" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_database("97a04302-9f36-4ad4-8530-800284b04ffe")

```
</CodeBlock>

</ExtendedCodeGroup>

### Changing default database

You can change the default database in a Trusted Twin account through the [update_database](./update-database.md) endpoint. 

## Database attributes

<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**          | string                                                            | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                           |
| **default**       | boolean                                                           | Denotes whether the given database is the default database for the account.                                                                                                                                                                                     |
| **status**        | string, value is `"running"`, `"pending"`, or `"service"`         | Status of the database:<br/> - `"running"`: The Timeseries database is running.<br/> - `"pending"`: The Timeseries database is in the process of initialising. <br/> - `"service"`: Unexpected behaviour occurred and the Timeseries database is not available. |
| **database_size** | integer                                                           | Size, in bytes, of the database.                                                                                                                                                                                                                                |
| **instance_type** | string, value is `"micro"`, `"medium"`, `"xlarge"`, or `"custom"` | Instance type used in the database.                                                                                                                                                                                                                             |
| **note**          | string                                                            | User-defined note. The maximum length of a note is 256 characters.                                                                                                                                                                                             |
| **created_ts**    | timestamp                                                         | Time at which the database was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                        |
| **updated_ts**    | timestamp                                                         | Time at which the database was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                   |
</TableWrapper>

## Database access

For information about accessing database services on the Trusted Twin platform, please see the [Database services access](../../account-and-access/database-services-access.md) section.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*                                                        |
|:-------|:------------------------------------|:------------------------------------------------------------------|
| GET   | /account/services/databases           | [get_databases](./get-databases.md) |
| GET  | /account/services/databases/{database} | [get_database](./get-database.md)       |
| PATCH | /account/services/databases/{database} | [update_database](./update-database.md)      |
| GET  | /account/services/databases/{database}/access  | [get_database_access](./get-database-access.md) |
| PATCH | /account/services/databases/{database}/access/ips | [update_database_ip_access](./update-database-ip-access.md) |
| PATCH | /account/services/databases/{database}/access/users/{user} | [update_database_user_access](./update-database-user-access.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform operations on databases, the respective permissions "get_databases", "get_database", "update_database", "get_database_access", and "update_database_user_access" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>
