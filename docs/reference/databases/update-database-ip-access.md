# Update database IP access

<div class="row">
<div class="column">

This endpoint adds, updates, or removes the access of an IP address to a given database.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| PATCH    | account/services/databases/{database}/access/ips | update_database_ip_access |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_database_ip_access" operation, the "update_database_ip_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) or `"default"` for requests on default database. |
| **ip access rule**             | dictionary                                   | body|  Attribute in form of key-value pairs:<br/> - key: IP address.<br/> - value: User-defined name of the IP access rule. |                                                                                                                                                            
</TableWrapper>

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "192.158.1.38": "rule_a"
}
```

</CodeBlock>
</ExtendedCodeGroup>


</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe/access/ips' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "192.158.1.38": "rule_a"
}
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

update_ip_address = {
    "192.158.1.38": "rule_a"
}

status, response = TT_SERVICE.update_database_ip_access("97a04302-9f36-4ad4-8530-800284b04ffe", "10dbe3a4-e9d7-4125-8bdd-31ebb3838d5b", body=update_ip_address)
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) or `"default"` for requests on default database. |
| **ip access rule**             | dictionary                                   | body|  Attribute in form of key-value pairs:<br/> - key: IP address.<br/> - value: User-defined name of the IP access rule. |    
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "192.158.1.38/32": "rule_a"
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
