# Get database access

<div class="row">
<div class="column">

This endpoint retrieves users that have access to the given database and their respective access permissions.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| GET    | account/services/databases/{database}/access | get_database_access |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_database_access" operation, the "get_database_access" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) or `"default"` for requests on default database. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request GET 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe/access' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_database_access("97a04302-9f36-4ad4-8530-800284b04ffe")

```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<TableWrapper>
| Attribute | Type       | Description                                                                                                                                                                         |
|:----------|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **users** | dictionary | Dictionary containing users with their permissions:<br/>- key: User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).<br/>- value: [User permissions](./get-database-access.md#user-permissions) |
</TableWrapper>



### User permissions

<div class="row">
<div class="column">

<TableWrapper>
| Attribute | Type | Description                                                   | 
|:---------|:-----|:--------------------------------------------------------------|
| **timeseries** | list of strings, value is `"read"`, or empty list     | Permissions of the user to Timeseries tables in the database. |
| **indexes** | list of strings, value is `"read"`, or empty list     | Permissions of the user to Indexes tables in the database.    |
| **customer_data** |  list of strings, value is `"write"`, `"read"`, or empty list    | Permissions of the user to the customer schema in the database. The customer schema allows users with the `"write"` permission to create their own tables in the database. |
</TableWrapper>
 
</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{   
    "users": {
        "8000b758-3e91-4f43-b420-3a64f6475946": {
            "timeseries": ["read"],
            "indexes": [],
            "customer_data": ["read", "write"]
        },
        "9f36cd73-7c79-4051-87f4-973f9bd00a63": {
            "timeseries": ["read"],
            "indexes": ["read"],
            "customer_data": ["read", "write"]
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
