# Get databases

<div class="row">
<div class="column">

This endpoint retrieves databases belonging to the account of the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| GET    | account/services/databases | get_databases |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_databases" operation, the "get_databases" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
>--header 'Content-Type: text/plain'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_databases()
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **default**        | string | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the [default database](./index.md#default-database) of the account. 
| **databases**          | dictionary                                                           | Dictionary containing databases with their attributes:<br/>- key: Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).<br/>- value: [Database attributes](./get-databases#database-attributes)                                                                                                                                                                                            |
</TableWrapper>

### Database attributes

<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **status**        | string, value is `"running"`, `"pending"`, or `"service"`         | Status of the database:<br/> - `"running"`: The Timeseries database is running.<br/> - `"pending"`: The Timeseries database is in the process of initialising. <br/> - `"service"`: Unexpected behaviour occurred and the Timeseries database is not available. |
| **note**          | string                                                            | User-defined note. The maximum length of a note is 256 characters.                                                                                                                                                                                             |
| **created_ts**    | timestamp                                                         | Time at which the database was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                        |
| **updated_ts**    | timestamp                                                         | Time at which the database was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "default": "75f48545-2442-4491-81cd-7679f665fa27", 
    "databases": {
        "75f48545-2442-4491-81cd-7679f665fa27":{
            "status": "running",
            "created_ts": 1705570409,
            "updated_ts": 1705570409,
            "note": "database_x" 
        },
        "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c": {
            "status": "running",
            "created_ts": 1706521206,
            "updated_ts": 1706607606,
            "note": "database_y" 
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

