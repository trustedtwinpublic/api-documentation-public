# Update database

<div class="row">
<div class="column">

This endpoint updates the `"default"` and `"note"` properties of a given database.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                                | Operation*      |
|:-------|:------------------------------------|:----------------|
| PATCH    | account/services/databases/{database} | update_database |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_database" operation, the "update_database" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                          | Type                      | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:-----------------------------------|:--------------------------|:-----|:----------|
| **{database}**<br/>*required* | string                    | path | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids). |
| **default**<br/>*optional* * | boolean | body | Denotes whether the database is the default database for the account. There must be a single default database at any given time. Therefore, you need to change the value of the `"default"` property to `true` for the database given database. This will automatically change the value of the previous default database to `False`.  |
| **note** <br/>*optional* *   | string   | body | User-defined note. The maximum length of a note is 256 characters. |
</TableWrapper>

::: footnote *
Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "default": true,
  "note": "This database covers the EMEA region."
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/account/services/databases/97a04302-9f36-4ad4-8530-800284b04ffe' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "default": true,
  "note": "This database covers the EMEA region."
}
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

update_database_body = {
    "default": True,
    "note": "This database covers the EMEA region."
}

status, response = TT_SERVICE.update_database("97a04302-9f36-4ad4-8530-800284b04ffe", body=update_database_body)

```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**          | string                                                            | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                           |
| **default**       | boolean                                                           | Denotes whether the given database is the default database for the account.                                                                                                                                                                                     |
| **status**        | string, value is `"running"`, `"pending"`, or `"service"`         | Status of the database:<br/> - `"running"`: The Timeseries database is running.<br/> - `"pending"`: The Timeseries database is in the process of initialising. <br/> - `"service"`: Unexpected behaviour occurred and the Timeseries database is not available. |
| **database_size** | integer                                                           | Size, in bytes, of the database.                                                                                                                                                                                                                                |
| **instance_type** | string, value is `"micro"`, `"medium"`, `"xlarge"`, or `"custom"` | Instance type used in the database.                                                                                                                                                                                                                             |
| **note**          | string                                                            | User-defined note. The maximum length of a note is 256 characters.                                                                                                                                                                                             |
| **created_ts**    | timestamp                                                         | Time at which the database was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                        |
| **updated_ts**    | timestamp                                                         | Time at which the database was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{   
    "default": true,
    "status": "running",
    "database_size": 19710755,
    "uuid": "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c",
    "instance_type": "medium",
    "note": "This database covers the EMEA region.", 
    "created_ts": 1706021380.00,
    "updated_ts": 1706021380.00
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>
