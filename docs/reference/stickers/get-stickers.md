---
tags:
  - workflow tags
  - stickers
---

# Get Stickers

<div class="row">
<div class="column">

This endpoint retrieves Stickers of a given Twin that are visible to the calling user.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                    | Operation*   |
|:-------|:------------------------|:-------------|
| GET    | /twins/{twin}/stickers | get_stickers |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_stickers" operation, the "get_stickers" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                  | Type   | In   | Description                                                                                                                           |
|:---------------------------|:-------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/> *required* | string | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                        |
| **context** <br/> *optional** | string (value is `{account}`, `personal`, or `system`); or list (value is `account` list); `DEFAULT=system` | query string | You can narrow down the list of Stickers to be included in the search:<br/>- `{account}`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) where the Sticker were created and where the requesting user is on the `"recipients"` list.<br/>- `personal`: Stickers created by the account of the requesting user where they are on the `"recipients"` list. <br/>- `system`: Stickers in the entire Trusted Twin system where the requesting user is on the `"recipients"` list.<br/>- {account} list: List of account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) where the Sticker were created and where the requesting user is on the `"recipients"` list. |
</TableWrapper>

::: footnote *
If no `context` parameter is provided, the default value (`context=system`) is used and Stickers where the requesting user is on the `"recipients"` list are returned.
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/stickers' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_stickers("f63ce1df-4643-49b2-9d34-38f4b35b9c7a")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { StickersApi, Configuration } from "@trustedtwin/js-client";

var stickersApi= new StickersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const stickers = await stickersApi.getStickers({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a"
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>


## Response

<div class="row">
<div class="column">

The response returns a list of Stickers with their attributes grouped by account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).

<TableWrapper>
| Attribute       | Type            | Description                                                                                                                                                                                                                                                                                                                                                            |
|:----------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **note**     | string          | Note for the recipients of the Sticker. The maximum length of a note is 512 characters. |
| **validity_ts** | timestamp       | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                    |
| **created_ts**  | timestamp       | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                |
</TableWrapper>

</div>
<div class="column">

The response returns a list of Stickers for the requested Twin grouped by account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids).

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "accounts": {
        "9891264d-4a77-4fa2-ae7f-84c9af14ae3b": {
            "red": {
                "note": "This is an urgent request.",
                "validity_ts": 1667127289.0,
                "created_ts": 1664961051.357
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
