---
tags:
  - workflow tags
---

# Overview

## Example Stickers

<div class="row">
<div class="column">

In our example, we create two Stickers. The Stickers have different color (name) - "red" and "yellow" denoting the urgency of the requests. Please note that the "color" of a Sticker must be unique in the context of an account and that it must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`.<br/>

The Stickers have different `"validity_ts"` values - the "red" Sticker has its validity set to a few days after creation of the Sticker (`"created_ts"`), the "yellow" Sticker is valid one month from the creation of the Sticker(`"created_ts"`). <br/>

The Stickers have different UUIDs on the `"recipients"` lists - the "red" Sticker has the UUID of the account that the Sticker was created in (`9891264d-4a77-4fa2-ae7f-84c9af14ae3b`), the "yellow" sticker has a list with UUIDs of users belonging to a different account as we are requesting the examination of the device from a vendor.

</div>
<div class="column">

<ExtendedCodeGroup title="Stickers" copyable>
<CodeBlock title="json">

```json
{
    "stickers": {
        "red": {
            "note": "This is an urgent request to replace parts of the device.",
            "recipients": [
                "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
            ],
            "validity_ts": 1665382457,
            "created_ts": 1664973830.463,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        "yellow": {
            "note": "Please examine the device.",
            "recipients": [
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts": 1667632457,
            "created_ts": 1664973830.463,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

<TableWrapper>
| Attribute       | Type            | Description                                                                                                                                                                                                                                                                                                                                                            |
|:----------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **note**     | string          | Note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                          |
| **recipients**  | list of strings | List of user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), and account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that should be able to access and remove the sticker. The maximum number of items on the list is 100. |
| **validity_ts** | timestamp       | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                    |
| **created_ts**  | timestamp       | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                | 
| **publish** | dictionary | List of events that trigger a notification message for the Sticker with the subscription topics that the notifications are to be sent to. The key is the event. The value is the notification topics or a list of notifications topics. The event can be one of the following:<br/> - `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>  - `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/> - `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. A notification message is triggered up to 60 seconds from the time a Sticker has expired.<br/> The value is the subscription topic or a list of subscription topics. Each name of a subscription topic  must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. |
</TableWrapper>
