---
tags:
  - workflow tags
---

# Remove Sticker 

<div class="row">
<div class="column">

This endpoint removes a given Sticker from a given Twin. The Sticker removal operation is atomic.<br/>

::: tip STICKER RECIPIENTS LIST
<Icon name="info-circle" :size="1.75" />
Please note that in order to remove a Sticker a user needs to be included on the `"recipients"` list of the given Sticker. If a user created a Sticker and did not include themselves (i.e., the [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of their user, the [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of their role, or the [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account their user belong to) on the `"recipients"` list, they will not be able to remove the Sticker from the given Twin. They will receive the error message `"Sticker not found"` when trying to remove the Sticker.
:::



</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*     |
|:-------|:-------------------------------|:---------------|
| DELETE | /twins/{twin}/stickers/{color} | remove_sticker |
</EndpointsTable>

::: footnote *
In order for a user to perform the "remove_sticker" operation, the "remove_sticker" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md). 
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                    | Type                                                                      | In           | Description                                                                                                                                                                                                                                                                                                                                                                       |
|:-----------------------------|:--------------------------------------------------------------------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/>*required*    | string                                                                    | path         | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin from which the Sticker should be removed.                                                                                                                                                                                                                                                                      |
| **color**<br/> *required*    | string                                                                    | path         | User-defined category of the Sticker. Must be unique in the context of an account. Must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`.                                                                                                                                                                                                                                               |
| **context** <br/> *optional* | string, value is `{account}`, `personal`, or `system`; `DEFAULT=personal` | query string | Narrows down the list of Stickers to be removed:<br/>- `{account}`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account where the Stickers were created.<br/>- `personal`: Stickers for which the calling user is the recipient. <br/>- `system`: Stickers in the entire Trusted Twin system visible to the requesting user. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/stickers/red' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.remove_sticker("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", "red")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { StickersApi, Configuration } from "@trustedtwin/js-client";

var stickersApi= new StickersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const sticker = await stickersApi.removeSticker({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    color: "red"
});
```
</CodeBlock>

</ExtendedCodeGroup>


</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type            | Description                                                                                                                                                                                                                                                                                                                                                            |
|:----------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **account**     | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Sticker.                                                    |
| **note**     | string          | Note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                          |
| **validity_ts** | timestamp       | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                    |
| **created_ts**  | timestamp       | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                | 
| **publish** | dictionary | List of events that trigger a notification message for the Sticker with the subscription topics that the notifications are to be sent to. The key is the event. The value is the notification topics or a list of notifications topics. The event can be one of the following:<br/> - `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>  - `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/> - `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. A notification message is triggered up to 60 seconds from the time a Sticker has expired.<br/> The value is the name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "note": "This is an urgent request to replace parts of the device.",
    "validity_ts": 1665382457.0,
    "created_ts": 1664973830.463,
    "publish": {
      "on_put": "new-task-request",
      "on_remove": "task-completed",
      "on_expire": "task-expired"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
