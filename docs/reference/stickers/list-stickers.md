---
tags:
  - workflow tags
---

# List Stickers

<div class="row">
<div class="column">

This endpoint lists Stickers where the calling user is the recipient of the Sticker (i.e., the user, role or account UUID is on the `"recipients"` list of the Sticker).<br/>
By default, the endpoint lists only Stickers create in the account of the calling user. To widen the search, the user can add a query parameter to see all Stickers where they are on the recipients list regardless of the account where the Stickers were created (`context=system`) or Stickers which were created by an account with a given account UUID (`context={account}`).

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| GET   | /stickers | list_stickers |
</EndpointsTable>


::: footnote *
In order for a user to perform the "list_stickers" operation, the "list_stickers" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                      | Type                                                                      | In           | Description                                                                                                                                                                                                                                                                                                                                                                                      |
|:-------------------------------|:--------------------------------------------------------------------------|:-------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **color**<br/> *optional**     | string                                                                    | query string | Color of the sticker to be returned.                                                                                                                                                                                                                                                                                                                                           |
| **context** <br/> *optional*** | string, value is `{account}`, `personal`, or `system`; `DEFAULT=personal` | query string | You can narrow down the list of Stickers to be included in the search:<br/>- `{account}`: Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account where the Sticker was created where the requesting user is on the `"recipients"` list..<br/>- `personal`: Stickers created by the account of the requesting user where they are on the `"recipients"` list. <br/>- `system`: Stickers in the entire Trusted Twin system where the requesting user is on the `"recipients"` list.  |
| **ge** <br/> *optional****        | timestamp                                                                 | query string | Returns Stickers where creation timestamp is greater than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                                                                                                                                                                                                                              |
| **le** <br/> *optional****        | timestamp                                                                 | query string | Returns Stickers where creation timestamp is lower than or equal to the given [Unix](https://en.wikipedia.org/wiki/Unix_time) timestamp.                                                                                                                                                                                                                                                                |
| **limit**<br/> *optional****      | integer, `DEFAULT=100`                                                    | query string | Limit on the number of Stickers to be returned in descending order. The descending order is calculated taking into account the `"created_ts"`, `"account"`, `"twin"` and `"color"` values. The default and maximum value of this parameter is 100.                                                                                                                                                                                                                                                                                        |
| **offset** <br/> *optional*    | integer, `DEFAULT=0`                                                      | query string | Determines the offset from which the records are to be provided. The records are provided in descending order (i.e., starting from the most recent record).                                                                                                                                                                                                                                                                                                                        | 
</TableWrapper>

::: footnote *
If no `color` key is specified, all Stickers will be returned filtered by other parameters if such parameters are provided.
:::

::: footnote **
If no `context` parameter is provided, the default value (`context=personal`) is used and only Stickers created by the account of the requesting user where they are on the `"recipients"` list are returned.
:::

::: footnote ***
The `ge`, `le`, `limit`, and `offset` parameters are optional and do not need to be included in the request. If they are not included in the request, their default values are used. If the number of the records to be returned exceeds the `limit` provided in the request:
- if the `ge` parameter is provided, the records to be returned are counted starting from the timestamp provided.
- if the `le` parameter is provided, the records to be returned are counted from the timestamp provided.
- if both `ge` and `le` parameters are provided, the records to be returned are counted from the timestamp provided in the `le` parameter (i.e., the most recent records are returned).
:::

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/stickers' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.list_stickers()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { StickersApi, Configuration } from "@trustedtwin/js-client";

var stickersApi= new StickersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const stickers = await stickersApi.listStickers();
```
</CodeBlock>

</ExtendedCodeGroup>


</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type      | Description                                                                                                                                                                   |
|:----------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **color**       | string    | User-defined category of the sticker. It must be unique in the context of an account. It must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`. |
| **account**     | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Sticker.                                                    |
| **twin**        | string    | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin with the Sticker.                                                                  | 
| **note**     | string    | Note for the recipients of the Sticker. The maximum length of a note is 256 characters.                                                                                 |
| **validity_ts** | timestamp | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).           |
| **created_ts**  | timestamp | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).       | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "stickers": [
        {
            "color": "red",
            "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
            "twin": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
            "note": "This is an urgent request to replace parts of the device.",
            "validity_ts": 1665382457.0,
            "created_ts": 1664973830.463
        }
    ]
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
