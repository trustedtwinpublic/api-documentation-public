---
tags:
  - workflow tags
---

# Get Sticker

<div class="row">
<div class="column">

This endpoint retrieves the details of a given Sticker of a given Twin.

</div>
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation*  |
|:-------|:-------------------------------|:------------|
| GET    | /twins/{twin}/stickers/{color} | get_sticker |
</EndpointsTable>


::: footnote *
In order for a user to perform the "get_sticker" operation, the "get_sticker" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                  | Type   | In   | Description                                                                                                                           |
|:---------------------------|:-------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}**<br/> *required* | string | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                        |
| **color**<br/> *required*  | string | path | User-defined category of the Sticker. Must be unique in the context of an account. Must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`.   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/stickers/yellow' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_sticker("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", "yellow")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { StickersApi, Configuration } from "@trustedtwin/js-client";

var stickersApi= new StickersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const sticker = await stickersApi.getSticker({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    color: "yellow"
});
```
</CodeBlock>


</ExtendedCodeGroup>


</div>
</div>


## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type            | Description                                                                                                                                                                                                                                                                                                                                                            |
|:----------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **note**     | string          | Note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                          |
| **recipients**  | list of strings | List of user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), and account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that should be able to access and remove the sticker. The maximum number of items on the list is 100. |
| **validity_ts** | timestamp       | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                    |
| **created_ts**  | timestamp       | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                | 
| **publish** <br/> *optional***    | dictionary, keys are `"on_put"`, `"on_remove"` or `"on_expire"` | List of events that trigger a notification message for the Sticker with the subscription topics that the notifications are to be sent to. The key is the event. The value is the notification topics or a list of notifications topics. The event can be one of the following:<br/> - `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>  - `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/> - `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. A notification message is triggered up to 60 seconds from the time a Sticker has expired.<br/> The value is the name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. | 
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "note": "Please examine the device.",
    "recipients": [
        "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
        "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
        "5fb44305-da08-4948-b9b9-f5feef9ba94c"
    ],
    "validity_ts": 1667632457.0,
    "created_ts": 1664974052.126,
    "publish": {
        "on_put": "new-task-request",
        "on_remove": "task-completed",
        "on_expire": "task-expired"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
