---
tags:
  - workflow tags
---

# Put Sticker

<div class="row">
<div class="column">

This endpoint creates one or more Stickers and puts them on a given Twin. <br/>
<br/>

::: tip STICKER RECIPIENTS LIST
<Icon name="info-circle" :size="1.75" />
Each Sticker has a `"recipients"` list. A user whose user, role or account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) are included on the `"recipients"` list can view the Sticker in the response to the [list_stickers](./list-stickers.md) endpoint and remove the Sticker from the Twin through the [remove_sticker](./remove-sticker.md) endpoint.
:::


</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| POST   | /twins/{twin}/stickers | put_sticker |
</EndpointsTable>

::: footnote *
In order for a user to perform the "put_sticker" operation, the "put_sticker" permission must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                   | Type       | In   | Description                                                                                                                                                                                                                   |
|:----------------------------|:-----------|:-----|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{twin}** <br/> *required* | string     | path | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the Twin on which the Sticker is to be put.                                                                                                     | 
| **stickers**                | dictionary | body | Key-value pairs:<br/>- key (Sticker **color**): alphanumeric string unique within an account. It must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`. <br/>- value: Valid JSON data type. | 
</TableWrapper>

### Sticker parameters

<TableWrapper>
| Parameter                         | Type                                                                   | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |                                                       
|:----------------------------------|:-----------------------------------------------------------------------|:-----|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **note**<br/> *optional*       | string                                                                 | body | Note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                                                                                                                                                     |
| **recipients**<br/> *required*    | list of strings                                                        | body | List of user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), and account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that should be able to access and remove the sticker. The maximum number of items on the list is 100.                                                                                                                            |
| **validity_ts** <br/> *optional** | timestamp                                                              | body | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). The maximum validity of a Sticker is one year after the creation of a Sticker (`now()` + 1 Year). If the `"validity_ts"` is not provided in the request, the `"validity_ts"` is set to its maximum value (`now()` + 1 Year).                                                                                                  | 
| **publish** <br/> *optional***    | dictionary, keys are `"on_put"`, `"on_remove"` or `"on_expire"` | body | List of events that trigger a notification message for the Sticker with the subscription topics that the notifications are to be sent to. The key is the event. The value is the notification topics or a list of notifications topics. The event can be one of the following:<br/> - `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>  - `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/> - `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. A notification message is triggered up to 60 seconds from the time a Sticker has expired.<br/> The value is the name of the subscription topic. The `"topic"` value must match the [regular expression](https://regexr.com/).<code>^[0-9A-Za-z\-_]{0,127}(?:@[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})?$</code>. | 
</TableWrapper>


::: footnote *
If `"validity_ts"` with the value `null` is provided in the request body, a Sticker will not be created and the request will return an error.<br/>
If the request body does not contain `"validity_ts"`, the value of `"validity_ts"` will be set to its maximum and default value which corresponds one year from now (`now()` + 1 year).
:::

::: footnote **
The `"publish"` parameter is optional. If the `"publish"` parameter is not provided in the request, a notification will not be set for the Sticker of the given color.
:::


<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "stickers":{
        "red":{
            "note":"This is an urgent request to replace parts of the device.",
            "recipients":[
                "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
            ],
            "validity_ts":1665382457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        "yellow":{
            "note":"Please examine the device.",
            "recipients":[
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts":1667632457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


</div>
<div class="column">


<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins/f63ce1df-4643-49b2-9d34-38f4b35b9c7a/stickers' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "stickers":{
        "red":{
            "note":"This is an urgent request to replace parts of the device.",
            "recipients":[
                "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
            ],
            "validity_ts":1665382457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        "yellow":{
            "note":"Please examine the device.",
            "recipients":[
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts":1667632457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
    }
}'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

stickers_body = {
    "stickers":{
        "red":{
            "note":"This is an urgent request to replace parts of the device.",
            "recipients":[
                "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
            ],
            "validity_ts":1665382457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        "yellow":{
            "note":"Please examine the device.",
            "recipients":[
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts":1667632457,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
    }
}

status, response = TT_SERVICE.put_sticker("f63ce1df-4643-49b2-9d34-38f4b35b9c7a", body=stickers_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { StickersApi, Configuration } from "@trustedtwin/js-client";

var stickersApi= new StickersApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

  const response = await stickersApi.putSticker({
    twin: "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    createStickersBody: {
      stickers: {
        red: {
          note: "This is an urgent request to replace parts of the device",
          recipients: [
            "9891264d-4a77-4fa2-ae7f-84c9af14ae3b"
          ],
          validityTs: 1665382457,
          "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        yellow: {
          note: "Please examine the device.",
          recipients: [
            "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
            "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
            "5fb44305-da08-4948-b9b9-f5feef9ba94c"
          ],
          validityTs: 1667632457,
          "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
      }
    }
  })

```
</CodeBlock>

</ExtendedCodeGroup>


</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute       | Type            | Description                                                                                                                                                                                                                                                                                                                                                            |
|:----------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **note**     | string          | Note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                          |
| **recipients**  | list of strings | List of user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), and account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that should be able to access and remove the sticker. The maximum number of items on the list is 100. |
| **validity_ts** | timestamp       | Time at which the Sticker expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                    |
| **created_ts**  | timestamp       | Time at which the Sticker was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                | 
| **publish**   | dictionary, keys are `"on_put"`, `"on_remove"` or `"on_expire"` | List of events that trigger a notification message for the Sticker with the subscription topics that the notifications are to be sent to. The key is the event. The value is the notification topics or a list of notifications topics. The event can be one of the following:<br/> - `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>  - `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/> - `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. A notification message is triggered up to 60 seconds from the time a Sticker has expired.<br/> The value is the name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. | 
</TableWrapper>

</div>
<div class="column">


<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">

```json
{
    "stickers": {
        "red": {
            "note": "This is an urgent request to replace parts of the device.",
            "recipients": [
                "d6779760-effa-4d2b-a955-1fb4428af117"
            ],
            "validity_ts": 1665382457,
            "created_ts": 1664973830.463,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        },
        "yellow": {
            "note": "Please examine the device.",
            "recipients": [
                "e81ccbd6-ff3c-46e6-9a95-f3aff5b95896",
                "6b1b7f09-4538-461b-9477-0a6bc1406ea6",
                "5fb44305-da08-4948-b9b9-f5feef9ba94c"
            ],
            "validity_ts": 1667632457,
            "created_ts": 1664973830.463,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#status-codes) section.
