---
tags:
  - workflow tags
  - sticker
  - stickers
---

# Stickers

<div class="row">
<div class="column">

## About
Stickers (workflow tags) on the Trusted Twin platform are a functionality designed to facilitate workflow processes. They support passing ownership of workflow items (e.g., tasks to be done) between multiple users. These users can be users belonging the same account where a Sticker was created or to a foreign account. Stickers are attached to Twins. <br/>

The Sticker functionality is inspired by sticky notes that can be attached to a surface and removed afterwards. A sticky note of a given color can be attached by one person (i.e., the Sticker creator) with a task for another person (i.e., the Sticker recipient) and then removed by the recipient of the Sticker (e.g., when the task is completed). <br/>

## Available operations

<TableWrapper>
| Operation | Description                                                                                                                                                                                                                                                                   |
|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [put_sticker](./put-sticker.md) | Creates one or more Stickers and puts it on a given Twin.                                                                                                                                                                                                                     |
| [get_sticker](./get-sticker.md) | Retrieves the details of a given Sticker attached to a given Twin based on its unique `"color"` attribute. The response will return a Sticker if the calling user is the recipient of the given Sticker or if they belong to the account where the given Sticker was created. |
| [get_stickers](./get-stickers.md)| Retrieves a list of Stickers attached to a given Twin. The response will return a list of Stickers attached to the given Twin where the caller is on the recipients list.                                                                                                     |
| [list_stickers](./list-stickers.md) | Retrieves a list of all Stickers where the caller is on the recipients list.                                                                                                                                                                                                  |
| [remove_sticker](./remove-sticker.md) | Removes a given Sticker from a given Twin. Only a user who is included on the recipients list of the given Sticker can remove it.                                                                                                                                             |
</TableWrapper>

## Sticker attributes


A Sticker has the following attributes:

<TableWrapper>
| Attribute       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|:----------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **color**       | User-defined category of the sticker. It must be unique in the context of an account. It must match the [regular expression](https://regexr.com/) `[0-9A-Za-z\-]{3,48}`.                                                                                                                                                                                                                                                                             |
| **note**     | Optional note for the recipients of the Sticker. The maximum length of a note is 512 characters.                                                                                                                                                                                                                                                                                                                                                        |
| **recipients**  | List of user [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), role [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids), and account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) that should be able to access the sticker and remove the sticker. The maximum number of items on the `"recipients"` list is 100 per Sticker. To learn more, see [Sticker recipients](./index.md#sticker-recipients) | 
| **validity_ts** | Time at which the Sticker expires. The maximum validity of a Sticker is one year after the creation of a Sticker (`now()` + 1 Year). If the `"validity_ts"` is not provided in the request, the `"validity_ts"` is set to its maximum value (`now()` + 1 Year).                                                                                                                                                                                               |
| **publish**     | Dictionary with events that should trigger a notification message (key) and the name of the subscription topic (value). The keys define the event triggering the notification. It can be one of the following:<br/>- `"on_put"`: A notification message is triggered when a Sticker of the given color is put on a given Twin.<br/>- `"on_remove"`: A notification message is triggered when a Sticker of a given color is removed from the given Twin.<br/>- `"on_expire"`: A notification message is triggered when the `"validity_ts"` of a Sticker is reached and the Sticker expires. The value is the name of the subscription topic. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z\-]{3,48}$`. A notification message is triggered up to 60 seconds from the time a Sticker has expired. |
</TableWrapper>
  

## Sticker recipients

When putting a Sticker on a Twin, the calling user defines the recipients of the Stickers in the `"recipients"` field of a request body. The `"recipients"` field contains a list of [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) defining who are the recipients of a given Sticker:


<TableWrapper>
| UUID type                                                                      | Description                                                                                                                                                                                                                                                                                             |
|:-------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Account UUID** | User(s) belonging to the account(s) with the [UUID(s)](../../overview/trusted-twin-api.md#trusted-twin-ids) will be able to see the Sticker on the Stickers' list ([list_stickers](./list-stickers.md)) amd remove the Sticker ([remove_sticker](./remove-a-sticker.md)) from the given Twin. |
| **Role UUID**    | User(s) with the role(s) with the [UUID(s)](../../overview/trusted-twin-api.md#trusted-twin-ids) will be able to see the Sticker on the Sticker's list ([list_stickers](./list-stickers.md)) amd remove the Sticker ([remove_sticker](./remove-a-sticker.md)) from the given Twin.            | 
| **User UUID**    | User(s) with the user [UUID(s)](../../overview/trusted-twin-api.md#trusted-twin-ids) will be able to see the Sticker on the Sticker's list ([list_stickers](./list-stickers.md)) amd remove the Sticker ([remove_sticker](./remove-a-sticker.md)) from the given Twin.                        |
</TableWrapper>

The recipients of a Sticker do not have to belong to the account where the Sticker was created. Only users who are the recipients of a Sticker can remove the Sticker. The Sticker removal operation is atomic (i.e., only one user can remove a given Sticker). Any user who will try to remove the Sticker after it has been removed will receive a response stating that the Sticker does not exist.

## Example scenario
- Company A is working with three subcontractors who provide repair services for devices managed by the company.
- Company A has a Trusted Twin account ([UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) "2acaa5e2-52c2-4cf9-8ce5-db17a2aebd02"). 
- Subcontractors 1, 2 and 3 have three separate Trusted Twin accounts (with account [UUIDs](../../overview/trusted-twin-api.md#trusted-twin-ids) "8753da2d-d95f-4195-be73-4ee174532eb8", "0038ff4a-cb4b-42cf-aa64-09ca23cf8476", "e506f83e-aac1-4b21-9788-03179e164103", respectively).
- Company A creates a Sticker and puts it on one of their Twins (representing devices) that needs to be repaired. 
- The Sticker is of a previously-agreed color (e.g. `"yellow_id_56272d"`). 
- All the subcontractors' accounts are included on the recipients list (`"recipients": ["8753da2d-d95f-4195-be73-4ee174532eb8", "0038ff4a-cb4b-42cf-aa64-09ca23cf8476", "e506f83e-aac1-4b21-9788-03179e164103"`]).
- The `"yellow_id_56272d"` Sticker is visible when calling the [list_stickers](./list-stickers.md) endpoint to all the subcontractors.
- Subcontractor 2 removes the Sticker ([remove_sticker](./remove-sticker.md) endpoint) as they decide to complete the task.
- The Sticker is no longer visible to subcontractors 1 and 3.

## Notifications

You can set up notifications for when a Sticker of a certain color is "put" on a Twin, removed from it, or when the `"validity_ts"` of a Sticker is exceeded and the Sticker expires. For more details, please consult the [Notifications](../../reference/notifications/index.md) section.
</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation*                        |
|:-------|:-------|:----------------------------------|
| POST   | /twins/{twin}/stickers | [put_sticker](./put-sticker.md) |
| GET  | /twins/{twin}/stickers/{color} | [get_sticker](./get-sticker.md)    |
| GET | /twins/{twin}/stickers/ | [get_stickers](./get-stickers.md)    |
| GET | /stickers | [list_stickers](./list-stickers.md)  |
| DELETE | /twins/{twin}/stickers/{color} | [remove_sticker](./remove-sticker.md) |
</EndpointsTable>

::: footnote *
In order for a user to perform all operations on Stickers, the respective permissions "put_sticker", "get_sticker", "get_stickers", "list_stickers", and "remove_sticker" must be included in the list of allowed actions in the statement of the user's [role](../roles/index.md).
:::

</div>
</div>



