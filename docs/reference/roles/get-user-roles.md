# Get user roles

<div class="row">
<div class="column">

This endpoint retrieves roles for the account of the calling user. 

</div>
<div class="column">

<EndpointsTable>
| Method | Path    | Operation*   |
|:-------|:--------|:-------------|
| GET    | /roles | get_user_roles |
</EndpointsTable>

::: footnote *
In order for a user to perform the "get_user_roles" operation, the "get_user_roles" permission must be included in the list of allowed actions in the statement of the user's [role](./index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

No parameters.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>
<CodeBlock title="cURL">

```bash
curl --location --request GET 'https://rest.trustedtwin.com/roles' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.get_user_roles()
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var rolesApi = new RolesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const roles = await rolesApi.getUserRoles();
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                               |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**       | string    | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                              |
| **name**       | string    | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account. |
| **created_ts** | timestamp | Time at which the role was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                       |
| **updated_ts** | timestamp | Time at which the role was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                  |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "roles": {
        "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c": {
            "name": "Read only",
            "created_ts": 1646307775.089,
            "updated_ts": 1646307775.089
        },
        "cba1a586-b5b9-46f5-a99b-76f70404508f": {
            "name": "Administrator",
            "created_ts": 1646307910.852,
            "updated_ts": 1646307910.852
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
