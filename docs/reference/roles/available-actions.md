---
tags:
  - actions
  - permissions
  - user permissions
---

# Available actions

Below you can find a list of actions available on the Trusted Twin platform. They are used in the role `"statement"` to `"allow"` or `"deny"` a user to perform a given action. The names of actions correspond with the names of endpoint operations.

Actions are divided into two categories:
1. Actions that require authorization with a User Secret (API key). For a user to perform an action belonging to this category, the action must be included in the `"statement"` of a user's role (if we `"allow"` to perform actions in the `"statement"`), or it must not be included in the `"statement"` of a user's role (if we `"deny"` to perform actions in the `"statement"`) . 
2. Actions that don't require authorization with a User Secret (API key). Actions that don't require authorization with a User Secret (API key) don't need to be included in the `"statement"` of a user's role, because all users with access to the Trusted Twin platform are able to perform them.

## Actions that require authorization with API key

<TableWrapper>
| Object/service                               | Actions                                                                                                                                                                                   |
|:---------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Account](../account/index.md)               | "get_account", "update_account"                                                                                                                                                           |
| [Batches](../batches/index.md)               | "create_batch", "get_batch", "get_batches", "update_batch", "delete_batch"                                                                                                                |
| [Cache](../cache/index.md)                   | "create_upload_url", "invalidate_upload_url"                                                                                                                                              |
| [Databases](../databases/index.md)*           | "get_database", "get_databases", "update_database", "get_database_access", "update_database_user_access"                                                                                  |
| [Docs](../docs/index.md)                     | "attach_twin_doc", "get_twin_doc", "update_twin_doc", "delete_twin_doc", "get_twin_docs", "delete_twin_docs"                                                                              | 
| [History](../history/index.md)               | "get_twin_ledger_entry_history"                                                                                                                                                           |
| [Identities](../identities/index.md)         | "create_twin_identity", "get_twin_identity", "update_twin_identity", "delete_twin_identity", "get_twin_identities", "resolve_twin_identity"                                               | 
| [Indexes](../indexes/index.md)*            | "create_indexes_table", "get_indexes_tables", "update_indexes_access", "get_indexes_table", "update_indexes_table", "delete_indexes_table", "truncate_indexes_table"                      |
| [Ledgers](../ledgers/index.md)               | "add_twin_ledger_entry", "get_twin_ledger_entry_value", "get_twin_ledger_entry", "update_twin_ledger_entry", "update_twin_ledger_entry_value", "delete_twin_ledger_entry"                 | 
| [Log](../log/index.md)                       | "get_log"                                                                                                                                                                                 |
| [Notifications](../notifications/index.md)   | "webhook_subscribe", "get_notifications_access", "update_notifications_access"                                                                                                                                                                       | 
| [Roles](../roles/index.md)                   | "create_user_role", "get_user_role", "get_user_roles", "delete_user_role", "update_user_role"                                                                                             |
| [Stickers](../stickers/index.md)             | "put_sticker", "get_sticker", "get_stickers", "list_stickers", "remove_sticker"                                                                                                           | 
| [Timeseries](../timeseries/index.md)*        | "create_timeseries_table", "get_timeseries_tables", "update_timeseries_access", "get_timeseries_table", "update_timeseries_table", "delete_timeseries_table", "truncate_timeseries_table" | 
| [Trace](../trace/index.md)                   | "trace"                                                                                                                                                                                   |
| [Twins](../twins/index.md)                   | "create_twin", "get_twin", "scan_twins", "update_twin", "terminate_twin"                                                                                                                  |
| [Usage](../usage/index.md)                   | "get_account_usage", "get_user_usage"                                                                                                                                                     | 
| [User](../users/index.md)                    | "create_user", "get_user", "get_users", "update_user", "delete_user"                                                                                                                      | 
| [User Secret (API key)](../secrets/index.md) | "create_user_secret_pin", "get_user_secret", "update_user_secret", "delete_user_secret"                                                                                                   |
| [User Token](../token/index.md)              | "create_user_token", "refresh_user_token"                                                                                                                                                 |
| [Who am I](../whoami/index.md)               | "who_am_i"                                                                                                                                                                                | 
</TableWrapper>

::: footnote *
The [Databases](../databases/index.md) service needs to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::


## Actions that do not require authorization with User Secret (API key)

The below actions do not require authorization with User Secret (APi key). Therefore, they do not need to be included in a role. 

<TableWrapper>
| Object/service | Actions |
|:-------|:-----|
| [Notifications](../notifications/index.md) | "webhook_confirm_subscription", "webhook_refresh_subscription", "webhook_unsubscribe" |
| [User Secrets (API keys)](../secrets/index.md) |  "create_user_secret" |
</TableWrapper>

