# Delete user role

<div class="row">
<div class="column">

This endpoint deletes a given role. It is not possible to delete the role of the calling user.

::: tip WARNING
<Icon name="info-circle" :size="1.75" />
It is possible to delete a role assigned to a user. The user will then not be able to access the Trusted Twin platform without a role assigned. 
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation*     | 
|:-------|:--------------|:---------------|
| DELETE | /roles/{role} | delete_user_role |
</EndpointsTable>

::: footnote *
In order for a user to perform the "delete_user_role" operation, the "delete_user_role" permission must be included in the list of allowed actions in the statement of the user's [role](./index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                 | Type   | In   | Description                                                                   |
|:--------------------------|:-------|:-----|:------------------------------------------------------------------------------|
| **{role}**<br/>*required* | string | path | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).  |
</TableWrapper>

In our example, we want to delete the role with the role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) `"3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c"`.

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request DELETE 'https://rest.trustedtwin.com/roles/3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

status, response = TT_SERVICE.delete_user_role("3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c")
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var rolesApi = new RolesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const deletedRole = await rolesApi.deleteUserRole({
    role: "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c",
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|:---------------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**       | string    | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| **name**       | string    | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |                                                                                                                                                                                                                                                                                              |
| **rules**      | dictionary    | Access [rules](../../overview/rules.md) that control access to a `"twin"`, an `"entry"` and/or an `"identity"`. If a rule evaluates to `True`, it allows to access the given resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **statement**  | dictionary    | <details> <ExtendedSummary markdown="span">Lists `"actions"` that correspond with the names of [endpoint operations](../../overview/api-reference-index.md) that a user with the role is allowed (`"effect": "allow"`) or not allowed (`"effect": "deny"`) to perform.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**effect**</td>  <td> string, value is `"allow"` or `"deny"` </td>  <td>Permission to perform an `"action"` applied at the level of the account. </td>  </tr>  <tr>  <td>  **actions** </td>  <td> list </td>  <td>List of actions that we grant the user with the role the permission to perform. For a list of available actions, please consult the [Available actions](./available-actions.md) section.</td>  </tr>  </tbody>  </table></TableWrapper></details> |
| **created_ts** | timestamp | Time at which the role was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **updated_ts** | timestamp | Time at which the role was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c",
    "name": "Read only",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "rules": {
        "twin": "TWIN.company == 'Best Shoes'",      
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_identity",
            "get_user_role",
            "get_twin",
            "get_user",
            "get_twin_identities",
            "get_twin_ledger_entry"
        ]
    },
    "created_ts": 1646307775.089,
    "updated_ts": 1646308240.806
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.


