# Update user role

<div class="row">
<div class="column">

This endpoint updates a given role. Any parameters not provided in the request will be left unchanged.

</div>
<div class="column">

<EndpointsTable>
| Method | Path          | Operation*     |
|:-------|:--------------|:---------------|
| PATCH  | /roles/{role} | update_user_role |
</EndpointsTable>

::: footnote *
In order for a user to perform the "update_user_role" operation, the "update_user_role" permission must be included in the list of allowed actions in the statement of the user's [role](./index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                      | Type       | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|:-------------------------------|:-----------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **{role}**<br/>*required*      | string     | path | role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **name**<br/>*optional* *      | string     | body | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **rules**<br/>*optional* *     | dictionary | body | [Rules](../../overview/rules.md) control access to a `"twin"`, an `"entry"` or an `"identity"`. If a rule evaluates to `True`, it allows to access the given resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **statement**<br/>*optional* * | dictionary | body | <details> <ExtendedSummary markdown="span"> Lists `"actions"` corresponding with the names of [endpoint operations](../../overview/api-reference-index.md) a user with the role is allowed (`"effect": "allow"`) or not allowed (`"effect": "deny"`) to perform. </ExtendedSummary> <TableWrapper> <table> <thead> <tr>  <th>Parameter</th>  <th>Type</th> <th> In </th> <th>Description</th> </tr> </thead> <tbody> <tr> <td>**effect**<br/>*required* </td>  <td> string, value is `"allow"` or `"deny"` </td> <td> body </td>  <td> Permission to perform an `"action"` applied at the level of the account.  </td> </tr> <tr> <td>**actions**<br/>*required*</td>  <td> list </td> <td> body </td>  <td> List of `"actions"` that we grant the user with the role the permission to perform. For a list of available actions, please consult the [Available actions](./available-actions.md) section. </td> </tr> </tbody> </table></TableWrapper> </details> |
</TableWrapper>

::: footnote *
Any optional parameters not provided in the request will be left unchanged. Any optional parameters provided in the request will replace current parameters.
:::

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "name": "Read only",
  "rules": {
    "twin": "TWIN.company == 'Best Shoes'"
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request PATCH 'https://rest.trustedtwin.com/roles/3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
        "name": "Read only ",
        "rules": {
        "twin": "TWIN.company == '\'We Produce the Best Shoes'\'"
    }
    }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_role_update_body = {
    "name": "Read only ",
    "rules": {
        "twins": "TWIN.company == '\'Best Shoes'\'"
    }
}

status, response = TT_SERVICE.update_user_role("3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c", body=user_role_update_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var rolesApi = new RolesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const updatedRole = await rolesApi.updateUserRole({
    role: "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c",
    updateRole: {
        name: "Read only",
        rules: {
            twinRule: "TWIN.company == 'Best Shoes'",
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|:---------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**       | string    | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **name**       | string    | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | 
| **account**    | string    | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |                                                                                                                                                                                                                                                                                              |
| **rules**      | dictionary    | [Rules](../../overview/rules.md) control access to a `"twin"`, an `"entry"` or an `"identity"`. If a rule evaluates to `True`, it allows to access the given resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| **statement**  | dictionary    | <details> <summary markdown="span">Lists `"actions"` that correspond with the names of [endpoint operations](../../overview/api-reference-index.md) that a user with the role is allowed or not allowed to perform.</summary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**effect**</td>  <td> string, value is `"allow"` or `"deny"` </td>  <td>Permission or  to perform an `"action"` applied at the level of the account. </td>  </tr>  <tr>  <td>  **actions** </td>  <td> list </td>  <td>List of actions that we grant the user with the role the permission to perform. For a list of available actions, please consult the [Available actions](./available-actions.md) section.</td>  </tr>  </tbody>  </table></TableWrapper></details> |
| **created_ts** | timestamp | Time at which the role was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **updated_ts** | timestamp | Time at which the role was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c",
    "name": "Read only",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "rules": {
        "twin": "TWIN.company == 'Best Shoes'",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_identity",
            "get_user_role",
            "get_twin",
            "get_user",
            "get_twin_identities",
            "get_twin_ledger_entry"
        ]
    },
    "created_ts": 1646307775.089,
    "updated_ts": 1646308240.806
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
