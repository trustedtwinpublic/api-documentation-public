# Create user role

<div class="row">
<div class="column">

This endpoint creates a new role. A single role can be applied to multiple users.

::: tip NOTE
<Icon name="info-circle" :size="1.75" />
A user can only create a role that either matches their permissions or where permissions of the role that they are trying to create are lower than the permissions of the user who creates the role. A user cannot grant permissions they don't have themselves.
:::

</div>
<div class="column">

<EndpointsTable>
| Method | Path   | Operation*     |
|:-------|:-------|:---------------|
| POST   | /roles | create_user_role 
</EndpointsTable>

::: footnote *
In order for a user to perform the "create_user_role" operation, the "create_user_role" permission must be included in the list of allowed actions in the statement of the user's [role](./index.md).
:::

</div>
</div>

## Request

<div class="row">
<div class="column">

<TableWrapper>
| Parameter                    | Type                   | In   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:-----------------------------|:-----------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **name**<br/>*optional*      | string, `DEFAULT="User Role Name"`                 | body | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of an account.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| **rules**<br/>*optional**   | dictionary, `DEFAULT=null` | body | Access [rule](../../overview/rules.md) for a `"twin"`, an `"entry"` and/or an `"identity"`. If the rule evaluates to `True`, it allows to access the given resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **statement**<br/>*optional* ** | dictionary, DEFAULT=`{"effect": "deny", "actions": null}`               | body | <details> <ExtendedSummary markdown="span"> Lists `"actions"` that correspond with the names of [endpoint operations](../../overview/api-reference-index.md) that a user with the role is allowed (`"effect": "allow"`) or not allowed (`"effect": "deny"`) to perform. </ExtendedSummary> <TableWrapper><table> <thead> <tr>  <th>Attribute</th>  <th>Type</th> <th>In</th> <th>Description</th> </tr> </thead> <tr> <td>**effect**<br/>*required* </td>  <td> string, value is `"allow"` or `"deny"` </td> <td> body </td>  <td> Permission to perform an `"action"` applied at the level of the account.  </td> </tr> <tr> <td>**actions**<br/>*required*</td>  <td> list </td> <td> body </td>  <td> List of `"actions"` that we grant the user with the role the permission to perform. For a list of available actions, please consult the [Available actions](./available-actions.md) section. </td> </tr> </table></TableWrapper> </details> |
</TableWrapper>

::: footnote *
The `"rules"` parameter is optional and does not need to be included in the request body when creating a role. If a rule for a Twin, an Entry or an Identity is not included in the request, its default value (`null`) is used. 
:::

::: footnote **
The `"statement"` attribute is optional. If not provided, a role is created that allows the user to perform all available `"actions"` (`"effect": "deny", "actions": null`).
:::

In our example, we create a role with the name `"Read only"`:
- We create one rule to access the `"twin"` resource ("TWIN.company == USER.company"). If the rule evaluates to `True`, the user with the role will be able to access the Twin.
- We allow (`"effect": "allow"`) the following `"actions"`: `"get_twin_ledger_entry"`, `"get_twin_identities"`, `"get_user_role"`, `"get_twin_identity"`, `"get_user"`, and `"get_twin"`. These actions are held in the `"statement"` attribute of the role.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "name": "Read only",
  "rules": {
    "twin": "TWIN.company == USER.company"                                             
  },
  "statement": {
    "effect": "allow",
    "actions": [
      "get_twin",
      "get_twin_identity",
      "get_twin_identities",
      "get_twin_ledger_entry",
      "get_user_role",
      "get_user"
    ]
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
<div class="column">

<ExtendedCodeGroup title="Request" switchable copyable>

<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/roles' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "name": "Read only",
    "rules": {
      "twin": "TWIN.company == USER.company"
    },
    "statement": {
      "effect": "allow",
      "actions": [
        "get_twin", "get_twin_identity", "get_twin_identities",  "get_twin_ledger_entry", "get_user_role", "get_user"
      ]
    }
  }'
```
</CodeBlock>

<CodeBlock title="Python">

```python
from trustedtwin.tt_api import TTRESTService

TT_SERVICE = TTRESTService(
    tt_auth="VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"
)

user_role_body = {
    "name": "Read only",
    "rules": {
        "twin": "TWIN.company == USER.company"
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin", "get_twin_identity", "get_twin_identities", "get_twin_ledger_entry", "get_user_role", "get_user"
        ]
    }
}

status, response = TT_SERVICE.create_user_role(body=user_role_body)
```
</CodeBlock>

<CodeBlock title="Node.js">

```js
import { RolesApi, Configuration } from "@trustedtwin/js-client";

var rolesApi = new RolesApi(new Configuration({apiKey: "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"}))

const newRole = await rolesApi.createUserRole({
    postNewRole: {
        name: "Read only",
        rules: {
            twinRule: "TWIN.company == USER.company",
        },
        statement: {
            effect: "allow",
            actions: [
                "get_twin",
                "get_twin_identity",
                "get_twin_identities",
                "get_twin_ledger_entry",
                "get_user_role",
                "get_user",
            ],
        },
    },
});
```
</CodeBlock>

</ExtendedCodeGroup>

</div>
</div>

## Response

<div class="row">
<div class="column">

<TableWrapper>
| Attribute      | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:---------------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **uuid**       | string     | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| **name**       | string     | Name of the role. It must match the [regular expression](https://regexr.com/) `^[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]$`. It does not need to be unique in the context of the account.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 
| **account**    | string     | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |                                                                                                                                                                                                                                                                                              |
| **rules**      | dictionary | Access [rules](../../overview/rules.md) that control access to a `"twin"`, an `"entry"` and/or an `"identity"`. If a rule evaluates to `True`, it allows to access the given resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **statement**  | dictionary | <details> <ExtendedSummary markdown="span">Lists `"actions"` that correspond with the names of [endpoint operations](../../overview/api-reference-index.md) that a user with the role is allowed (`"effect": "allow"`) or not allowed (`"effect": "deny"`) to perform.</ExtendedSummary> <TableWrapper><table>  <thead>  <tr>  <th>Attribute</th>  <th>Type</th> <th>Description</th> </tr>  </thead>  <tbody>  <tr>  <td>**effect**</td>  <td> string, value is `"allow"` or `"deny"` </td>  <td>Permission or  to perform an `"action"` applied at the level of the account. </td>  </tr>  <tr>  <td>  **actions** </td>  <td> list </td>  <td>List of actions that we grant the user with the role the permission to perform. For a list of available actions, please consult the [Available actions](./available-actions.md) section.</td>  </tr>  </tbody>  </table></TableWrapper></details> |          
| **created_ts** | timestamp  | Time at which the role was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **updated_ts** | timestamp  | Time at which the role was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
</TableWrapper>

The response returns the role `"Read only"` with the role UUID `"3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c"`:
- The role has a rule that allows the user with the role to access the `"twin"` resource if the rule `"TWIN.company == USER.company"` evaluates to `True`.
- The role does not have an `"entry"` rule.
- The role does not have an `"identity"` rule.
- The rule allows (`"effect": "allow"`) the user to perform the following `"actions"`: `"get_twin_ledger_entry"`, `"get_twin_identities"`, `"get_user_role"`, `"get_twin_identity"`, `"get_user"`, and `"get_twin"` `"actions"`. These actions are held in the `"statement"` attribute of the role.

</div>
<div class="column">

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "uuid": "3d4c3ec0-6c5f-4d32-ab23-4df8c69f142c",
    "name": "Read only",
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "rules": {
        "twin": "TWIN.company == USER.company",
        "entry": null,
        "identity": null
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "get_twin_ledger_entry",
            "get_twin_identities",
            "get_user_role",
            "get_twin_identity",
            "get_user",
            "get_twin"
        ]
    },
    "created_ts": 1646307775.089,
    "updated_ts": 1646307775.089
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Status codes

Requests to this endpoint result in generic status codes. For a comprehensive list of status codes, please consult the [Status codes](../../overview/status-codes.md#http-status-codes) section.
