---
tags:
  - uri
  - url
---

# Trusted Twin API

## API Reference

The Trusted Twin API follows [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) principles. It is designed to have predictable, resource-oriented URLs.

## Trusted Twin REST URI

All Trusted Twin REST API calls should be made to the [https://rest.trustedtwin.com](https://rest.trustedtwin.com) base domain. Please note that the Trusted Twin API is not accessible via plain HTTP requests.

## Requests

<div class="row">
<div class="column">

Trusted Twin resources are accessed via standard HTTPS requests in UTF-8 format sent to the endpoints. The API uses appropriate HTTP verbs to define the method for each operation.

</div>
<div class="column">

<EndpointsTable>
| Method | Operation           |
|:-------|:--------------------|
| POST   | Create a resource   |
| GET    | Retrieve a resource |
| PATCH  | Update a resource   |
| DELETE | Delete a resource   |
</EndpointsTable>

</div>
</div>

### Request body

<div class="row">
<div class="column">

If request parameters are provided via request body, the request body follows the JSON format.

</div>
<div class="column">

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
  "description":{
    "size":41,
    "colour":"black",
    "type":"electric hazard",
    "model":"Xp6",
    "production_year":2022,
    "qa_passed":true,
    "company":"Shoes",
    "certifications":[
      "xptI2021",
      "fht76j",
      "IPS1449"
    ]
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Responses

<div class="row">
<div class="column">

Trusted Twin REST API responses are [JSON-encoded](https://www.json.org/json-en.html) objects.

</div>
<div class="column">

<ExtendedCodeGroup title="Example response" copyable>
<CodeBlock title="json">
```json
{
  "owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
  "status": "alive",
  "updated_ts": 1646305579.969,
  "description": {
    "size": 41,
    "colour": "black",
    "type": "electric hazard",
    "model": "Xp6",
    "production_year": 2022,
    "qa_passed": true,
    "company": "Shoes",
    "certifications": [
      "xptI2021",
      "fht76j",
      "IPS1449"
    ]
  },
  "creation_certificate": {
    "uuid": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    "creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "created_ts": 1646305579.969
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Trusted Twin IDs

The IDs on the Trusted Twin platform are standardised and follow the below formats.

<TableWrapper>
| ID name                                    | Type   | Description                                                                                                                                                                      | Example                                     |
|:-------------------------------------------|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------|
| UUID                                       | string | Unique ID of Trusted Twin objects (account UUID, Twin UUID etc.). Follows the [UUID Version 4](https://en.wikipedia.org/wiki/Universally_unique_identifier) format.              | `e834214a-e13d-412b-ffd6-911ab58c8733`      |
| [Identity](../reference/identities/index.md) | string | User-defined ID for an [Identity](../reference/identities/index.md). It must match the [regular expression](https://regexr.com/) `[A-Za-z_][0-9A-Za-z_]{0,7}#[0-9A-Za-z_=+-]{1,128}`. | `RFID#be744bdc-0f6d-4a00-8199-9f6a893c1bde` |
</TableWrapper>

## Description field

<div class="row">
<div class="column">

The description field is part of the user, Twin, and Doc objects. It allows for adding metadata to objects. It can be used to create [rules](./rules.md) or for indexing purposes.

<TableWrapper>
| Attribute       | Type       | Description                                                                                                                                                          | 
|:----------------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **description** | dictionary | User-defined key-value pairs:<br/> - key: It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: Valid JSON data type. |
</TableWrapper>


</div>
<div class="column">

<ExtendedCodeGroup title="Example description" copyable>
<CodeBlock title="json">
```json
{
  "description": {
    "size": 41,
    "colour": "black",
    "type": "electric hazard",
    "model": "Xp6",
    "production_year": 2022,
    "qa_passed": true,
    "company": "Shoes",
    "certifications": [ "xptI2021", "fht76j", "IPS1449" ]
  }
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

## Timestamps

The timestamps on the Trusted Twin platform are standard [Unix timestamps](https://en.wikipedia.org/wiki/Unix_time). They display the number of seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).

## Swagger

You can find the most recent version of the [Trusted Twin API in SwaggerHub](https://app.swaggerhub.com/apis/TrustedTwinDev/trusted-twin_api/3.17.00).
