---
tags:
  - rules
  - rule
  - access rules
  - selection rules
  - in-built rules
---

# Rules   

## Rules overview

A rule on the Trusted Twin platform is used to manage visibility and access of a user to a resource, to define conditions triggering a [notification](../reference/notifications/index.md) event or to select Twins for a given [Indexes](../reference/indexes/index.md) database table.   
    
Rules are used in:
- [Roles](../reference/roles/index.md): Rules control user's access to a `"twin"`, an `"identity"`, or an `"entry"` of a Leger. They are held in the `"rules"` attribute of a role.
- [Identities](../reference/identities/index.md): Rules control visibility of Identities for users from foreign accounts (Identities are by default visible to users from the account that created the Identity). They are held in the `"visibility"` attribute of an Identity.
- [Ledger](../reference/ledgers/index.md) Entries: 
  - Rules that control visibility of Entries of a Ledger. Such a rule is held in the `"visibility"` attribute of an Entry of a Ledger.
  - Rules that define the condition triggering a [notification](../reference/notifications/index.md) event. Such a rule is held in the `"publish"` attribute of an Entry of a Ledger.
- [Indexes](../reference/indexes/index.md) to select Twins for a given Indexes database table.
    
Rules can be `null`. The interpretation of `null` differs for role rules and for visibility rules:
- Role rule: If the rule is `null`, the user does not have access to Twins and Ledger Entries in foreign accounts.
- Visibility rule: If the rule is `null`, the Identity (or Entry of a Ledger) is only visible to users of the account that created the Identity (or Entry of a Ledger).

## Rule syntax
Rules follow the [Rule Engine Syntax](https://zerosteiner.github.io/rule-engine/syntax.html). 

## Rule variables
The set of variables available to create rules varies based on the object where the rules are created.

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
Missing variables (not set, not existing, or not available) resolve to `None`.
:::

### System variables

<TableWrapper>
| Variable name                                  | Type                                         | Description                                                                                                                                                                                                                                                                                                                                | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:-----------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| account                                        | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                             | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| twin                                           | string                                       | Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                   | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| identity                                       | string                                       | [Identity](../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                    |                  | &check;              |                   | &check;               |                           |                        |
| ledger**                                      | string                                       | Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids). The Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) is equal to the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) if the operation is performed by a user belonging to an account that owns the Ledger. |                  |                      | &check;           |                       | &check;                   | &check;                |
| user*                                          | string                                       | User [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                                | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| role*                                          | string                                       | Role [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                                | &check;          | &check;              | &check;           |                |                  |            |
| operation                                         | string                                       | Name of API operation. For a full list of operations, please see the [List of endpoints](../overview/api-reference-index.md) section.                                                                                                                                                                                                                               | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| cause                                            | string, value is `"user"` or `"system"`                           | Cause of a task. Value can be `"user"` (for tasks generated by user) or `"system"` (for system-generated tasks). | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| now                                            | timestamp (float)                            | Current time. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                             | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
</TableWrapper>

::: footnote *
Not available in system tasks.
:::

::: footnote **
Ledger UUID of the Ledger where the operation is being performed. It is available only for [Ledger](../reference/ledgers/index.md)-related operations.
:::

### Twin variables

<TableWrapper>
| Variable name   | Type                                         | Description                                                                                                                                                                                                                                                                                                                              | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:----------------|:---------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| twin_status     | string, value is `"alive"` or `"terminated"` | Status of the Twin. In case of alive Twins, the `"description"` can be updated by the owner of the Twin. In case of terminated Twins, the `"description"` cannot be updated. Ledger Entries, Identities and Docs can be attached to alive and terminated Twins by all users involved in the process.                                     | &check;          |                      | &check;           |                       | &check;                   | &check;                |
| twin_owner      | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that owns the Twin.                                                                                                                                                                                                                            | &check;          |                      | &check;           |                       | &check;                   | &check;                |
| twin_creator    | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Twin.                                                                                                                                                                                                                         | &check;          |                      | &check;           |                       | &check;                   | &check;                |
| twin_created_ts | timestamp (float)                            | Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                     | &check;          |                      | &check;           |                       | &check;                   | &check;                |
| twin_updated_ts | timestamp (float)                            | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                | &check;          |                      | &check;           |                       | &check;                   | &check;                |
| TWIN*           | dictionary                                   | Description of the Twin. It consists of user defined key-value pairs:<br/> - key: Must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. <br/> - value: Valid JSON data type. <br/>For more details consult the [description field](../overview/trusted-twin-api.md#description-field) section. | &check;          |                      | &check;           |                       | &check;                   | &check;                |
</TableWrapper>

::: footnote *
To access dictionary type variables, use the `.` separator.
:::

### Identity variables

<TableWrapper>
| Variable name                                  | Type                                         | Description                                                                                                                                                                                                                                                                                                                                | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:-----------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| identity_owner                                 | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that owns the Identity.                                                                                                                                                                                                                          |                  | &check;              |                   |                       |                           |                        |
</TableWrapper>

### Ledger variables

<TableWrapper>
| Variable name                                  | Type                                         | Description                                                                                                                                                                                                                                                                                                                                | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:-----------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| entry_name                                     | string                                       | Name of the Entry of a Ledger.                                                                                                                                                                                                                                                                                                             |                  |                      | &check;           |                       | &check;                   | &check;                |
| entry_value                                    | valid JSON data type                         | Value of the Entry of a Ledger.                                                                                                                                                                                                                                                                                                            |                  |                      | &check;           |                       | &check;                   | &check;                |
| entry_new_value                                | valid JSON data type                         | Value of the Entry of a Ledger after Entry update.                                                                                                                                                                                                                                                                                                            |                  |                      |                   |                       |                           | &check;                |
| entry_old_value                                | valid JSON data type                         | Value of the Entry of a Ledger before Entry update.                                                                                                                                                                                                                                                                                                            |                  |                      |                   |                       |                           | &check;                |
| entry_created_ts                               | timestamp (float)                            | Time at which the Entry of a Ledger was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                          |                  |                      | &check;           |                       | &check;                   | &check;                |
| entry_updated_ts                               | timestamp (float)                            | Time at which the `"visibility"`, `"history"`, `"timeseries"`, or `"publish"` property of an Entry was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | | | &check;           |                       | &check;                 |    &check;               |
| value_changed_ts                               | timestamp (float)                            | Time at which the value of the Entry of a Ledger was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                        |                  |                      | &check;           |                       | &check;                   | &check;                |
| LEDGER*                                       | dictionary                                   | The value of the key-value pair:<br/> - Key: alphanumeric string unique within the Ledger. It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: Valid JSON data type.                                                                                                                      |                  |                      | &check;           |                       | &check;                   | &check;                |
</TableWrapper>

::: footnote *
To access dictionary type variables, use the `.` separator.
:::

### User variables

<TableWrapper>
| Variable name                                  | Type                                         | Description                                                                                                                                                                                                                                                                                                                                | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:-----------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| user_name                                      | string                                       | Name of the user. It must match the [regular expression](https://regexr.com/) `[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]`. It does not need to be unique in the context of an account.                                                                                                                                                   | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| USER*                                         | dictionary                                   | Description of the user. It consists of user-defined key-value pairs:<br/> - key: Must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: Valid JSON data type.                                                                                                                                | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
| auth_type                                         | string, value is `"secret"` or `"token"` | Authorization type used by the calling user. Value can be `"secret"` (denoting authorization with a [User Secret](../reference/secrets/index.md) or a [User Token](../reference/token/index.md). | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
</TableWrapper>

::: footnote *
To access dictionary type variables, use the `.` separator.
:::


### Custom header variables

<TableWrapper>
| Variable name                                  | Type                                         | Description                                                                                                                                                                                                                                                                                                                                | Twin rule (Role) | Identity rule (Role) | Entry rule (Role) | Visibility (Identity) | Visibility (Ledger Entry) | Publish (Ledger Entry) |
|:-----------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:---------------------|:------------------|:----------------------|:--------------------------|:-----------------------|
| DICT*                                         | dictionary                                   | Value of the `X-TrustedTwin` header.                                                                                                                                                                                                                                                                                                       | &check;          | &check;              | &check;           | &check;               | &check;                   | &check;                |
</TableWrapper>

::: footnote *
To access dictionary type variables, use the `.` separator.
:::

## Built-in functions

The following built-in functions are available on the Trusted Twin platform:

<TableWrapper>
| Built-in function | Description                                                 | Example                                     | Result | 
|:--------------|:------------------------------------------------------------|:--------------------------------------------|:-------|
| DTFISO        | Converts string in ISO format into datetime object.         | $DTFISO['2023-04-03']                       |  2023-04-03 00:00:00      |
| DTFTS         | Converts timestamp into datetime object with UTC time zone. | $DTFTS[1680516155.471]                          |  2023-04-03 10:02:35 |
| TDFS          | Converts seconds into datetime object.                      | $TDFS[1680516155]                           |  19450 days, 10:02:35      |
| HASH |  Computes a hash value. Available parameters:<br/>-data (mandatory): String to be hashed.<br/>-key (optional): Hashing key (string).  | $HASH('name@example.com', 'key_123') | 9jq3MhUJ_J31avqSxX8beqbcCu-t |
| ABS | Returns the absolute value of the provided number. |  $ABS(-9)  | 9 |
| ANY | Returns the boolean 'True' if every member of the array argument is truthy. | $ANY([1,2,3,4,5,6,0]) | True | 
| ALL | Returns the boolean 'True' if any member of the array argument is truthy. | $ALL([1,2,3,4,5,6,0]) | False |
| SPLIT | Splits the string value into substrings. | $SPLIT('Lorem ipsum') | ('Lorem', 'ipsum') |
| MAX | Converts a list or a set to its maximum element.            | $MAX([1,2,3,4,5,6]) | 6.0  |
| MIN | Converts a list or a set to its minimum element.            | $MIN([1,2,3,4,5,6]) | 1    |
| SUM | Converts a list or a set to the sum of its elements.        | $SUM([1,2,3,4,5,6]) | 21.0 |
| AVR | Converts a list or a set to the average of its elements.    | $AVR([1,2,3,4,5,6]) | 3.5  |
| LEN | Converts a list or a set to the length of its elements.     | $LEN([1,2,3,4,5,6]) | 6    |
</TableWrapper> 

## Example usage 

For examples of usage of rules on the Trusted Twin platform, please see our feature guides:
- [Rules (Entry visibility)](../feature-guides/working-with-rules.md) feature guide
- [Rules (Twin rule)](../feature-guides/working-with-twin-rules.md) feature guide




