# Log messages

You can obtain system logs through the [get_log](../reference/log/get-log.md) endpoint or by using the [Log monitor](../tools/log_monitor.md) tool. The log can be of error ('ERR'), warning ('WARN'), or informational ('INFO') type.

## Timeseries

<TableWrapper>
| Category                  | Type | Message                                                                                                            | Additional information                                                                                                          |
|:--------------------------|:-----|:-------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Timeseries - General      | WARN | "Row potentially incomplete. Refer to reported errors: twin=[{}]."                                                 |                                                                                                                                 |
| Timeseries - General      | WARN | "Invalid Ledger configuration. Attempt to use a disabled Timeseries service: twin=[{}]."                           | The Timeseries database service needs to be enabled for an Account. See [Timeseries](../reference/timeseries/index.md) section. |
| Timeseries - General      | ERR  | "Failed to connect to Timeseries database. Operation aborted: twin=[{}]."                                          |                                                                                                                                 |
| Timeseries - General      | ERR  | "Failed to read Timeseries service configuration from database. Operation aborted: twin=[{}]."                     |                                                                                                                                 |
| Timeseries - General      | ERR  | "Invalid Timeseries table. Table does not exist. Operation aborted: twin=[{}], table=[{}]."                        | Please double-check the name of the [Timeseries](../reference/timeseries/index.md) table.                                       |
| Timeseries - General      | WARN | "Invalid data. Timeseries task processing has no correct data to insert. Operation aborted: twin=[{}]."            |                                                                                                                                 |
| Timeseries - General      | ERR  | "Unexpected error when inserting Timeseries data to table. Operation is retried: twin=[{}]."                       |                                                                                                                                 |
| Timeseries - General      | ERR  | "Unexpected error when processing Timeseries data. Operation is retried: twin=[{}]."                               |                                                                                                                                 |
| Timeseries  - Timestamp   | ERR  | "Failed to resolve timestamp column name. Invalid template. Default timestamp used: twin=[{}], template=[{}]."     | See [Templates](../overview/templates.md) section.                                                                          |
| Timeseries  - Timestamp   | ERR  | "Invalid timestamp format. Failed to convert. Default timestamp used: twin=[{}], timestamp=[{}]."                  |                                                                                                                                 |
| Timeseries  - Timestamp   | ERR  | "Invalid timestamp format. Must be JSON. Default timestamp used: twin=[{}], value=[{}]."                           |                                                                                                                                 |
| Timeseries  - Measurement | ERR  | "Failed to resolve measurement column name. Missing template. Measurement not stored: twin=[{}]."                  |                                                                                                                                 |
| Timeseries  - Measurement | ERR  | "Failed to resolve measurement column name. Invalid template. Measurement not stored: twin=[{}], template=[{}]."   | See [Templates](../overview/templates.md) section.                                                                          |
| Timeseries  - Measurement | WARN | "Failed to unpack measurement column names. Value is not a dict. Entry name used: twin=[{}], template=[{}]."       |                                                                                                                                 |
| Timeseries  - Measurement | ERR  | "Invalid measurement value type. Measurement not stored: twin=[{}], measurement=[{}]."                             |                                                                                                                                 |
| Timeseries  - Measurement | ERR  | "Invalid measurement column name. Column does not exist. Measurement not stored: twin=[{}], measurement=[{}]."     | Please double-check the name of the measurement column of the [Timeseries](../reference/timeseries/index.md) table.             |
| Timeseries  - Dimension   | ERR  | "Invalid dimension template. Dimension value not stored: twin=[{}], template=[{}], dimension=[{}]."                | See [Templates](../overview/templates.md) section.                                                                          |
| Timeseries  - Dimension   | ERR  | "Invalid dimension value type. Dimension value not stored: twin=[{}], value=[{}], dimension=[{}]."                 |                                                                                                                                 |
| Timeseries  - Dimension   | ERR  | "Invalid dimension value format. Must be JSON. Dimension value not stored: twin=[{}], value=[{}], dimension=[{}]." |                                                                                                                                 |
| Timeseries  - Dimension   | ERR  | "Failed to resolve dimension value. Missing template. Dimension value not stored: twin=[{}], dimension=[{}]."      |                                                                                                                                 |
| Timeseries  - Dimension   | WARN | "Unknown dimensions: twin=[{}], dimensions=[{}]."                                                                  |                                                                                                                                 |
</TableWrapper>


## Notifications

<TableWrapper>
| Category      | Type | Message                                                                                                                    | Additional information                                 |
|:--------------|:-----|:---------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------|
| Notifications | WARN | "Rule evaluation error. Rule evaluation result defaulted to False: twin=[{}], entry=[{}], rule=[{}]."                      | See [Rules](../overview/rules.md) section.         | 
| Notifications | ERR  | "Failed to resolve topic. Invalid template. Topic not published: twin=[{}], entry=[{}], template=[{}]."                    | See [Templates](../overview/templates.md) section. | 
| Notifications | WARN | "No one is listening. Topic not published: twin=[{}], topic=[{}]."                                                         |                                                        | 
| Notifications | ERR  | "Topic rejected by publish service. Invalid topic. Topic not published: twin=[{}], topic=[{}]."                            |                                                        | 
| Notifications | ERR  | "Internal publish service error. Topic not published: twin=[{}], topic=[{}]."                                              |                                                        | 
| Notifications | WARN | "Failed to invoke the webhook. Could not reach the endpoint. Will retry {} times: endpoint=[{}], topic=[{}], status=[{}]." |                                                        | 
| Notifications | WARN | "Failed to invoke the webhook. Endpoint returned an error. Will retry {} times: endpoint=[{}], topic=[{}], status=[{}]."   |                                                        | 
</TableWrapper>

## Ledger

<TableWrapper>
| Category                  | Type | Message                                                                                                                                  | Additional information                                                                                                          |
|:--------------------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Ledger - Entry                   | WARN | "Failed to fetch included value. Source does not exist or could not be obtained. Value defaulted to null: source=[{}]."                                           |                                                                                                                                 | 
| Ledger - Reference | WARN | "Reference to a non-existing Twin created in a ledger entry. Referenced value will never be available: twin=[{}]." | |
| Ledger - Reference | ERR | "Attempt to store a value older than value already stored in a reference type entry. Potentially a loop. Value not stored: twin=[{}], entry=[{}]." | | 
| Ledger - Quotas | ERR | "Ledger size quota violation. Ledger size=[{}], quota=[{}]." | |
</TableWrapper>

## Indexes

<TableWrapper>
| Category          | Type | Message                                                                                                                                    | Additional information                                 |
|:------------------|:-----|:-------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------|
| Indexes - General | ERR  | "Failed to connect to Indexes database. Operation aborted: twin=[{}]."                                                                     |                                                        | 
| Indexes - General | ERR  | "Failed to read Indexes service configuration from database. Operation aborted: twin=[{}]."                                                |                                                        | 
| Indexes - General | ERR  | "Unexpected error when updating Index table: twin=[{}]."                                                                                   |                                                        | 
| Indexes - General | ERR  | "Unexpected error when processing Index data: twin=[{}]."                                                                                  |                                                        | 
| Indexes - Update  | ERR  | "Invalid Index table. Table does not exist. Operation aborted: twin=[{}], table=[{}]."                                                     |                                                        | 
| Indexes - Update  | ERR  | "Failed to resolve property value. Invalid template. Index updated with empty value: twin=[{}], index=[{}], property=[{}], template=[{}]." | See [Templates](../overview/templates.md) section. | 
| Indexes - Update  | ERR  | "Invalid property value type. Property updated with empty value: twin=[{}], index=[{}], property=[{}], type=[{}], value=[{}]."             | See [Templates](../overview/templates.md) section. | 
| Indexes - Update  | WARN | "Record with newer timestamp already exists. Update skipped: twin=[{}], index=[{}]."                                                       |                                                        | 
| Indexes - Database index creation  | ERR | "Failed to connect to Indexes database. Operation aborted: table=[{}]."                                                       |                                                        | 
| Indexes - Database index creation  | INFO | "Started creating index for table[{}] property[{}]."                                                       |                                                        | 
| Indexes - Database index creation  | INFO | "Finished creating index for table[{}] property[{}]."                                                       |    
| Indexes - Database index creation  | ERR | "Failed to create index for table[{}] property[{}]. Operation aborted."                                                       |   
| Indexes - Database index creation  | WARN | "Column type {} is not btree indexable. Index on table column for property {} not created."                                                       |   


</TableWrapper>

## Trace

<TableWrapper>
| Category                  | Type | Message                                                                                                                                 | Additional information                                                                                                          |
|:--------------------------|:----------|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Trace                     | INFO | "Trace task execution. Request {} on [{}]."                                                                                             |                                                                                | 
| Trace                     | INFO | "Trace task execution. Task {} on [{}]."                                                                                                |                                                                               |
</TableWrapper>

## Limits

<TableWrapper>
| Category | Type | Message                                                                                                                                 | Additional information                                                                                                          |
|:---------|:-----|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Limits   | WARN | "Too many results for operation [{}]. Top [{}] results returned, remaining results skipped."   | 
</TableWrapper>

## User Activity Log

<TableWrapper>
| Category | Type | Message                                                                                                                                 | Additional information                                                                                                          |
|:---------|:-----|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| User Activity Log   | ERR | "Failed to send user activity log. Internal Error - contact support. User activity not logged."   | 
| User Activity Log   | WARN | "Failed to send user activity log. Timeseries Service not enabled for account. User activity not logged."   | 
</TableWrapper>

## Resource access log

<TableWrapper>
| Category | Type | Message                                                                                                                                 | Additional information                                                                                                          |
|:---------|:-----|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Resource access log   | ERR | "Failed to send resource access log. Internal Error - contact support. Resource access not logged."   | 
| Resource access log   | WARN | "Failed to send resource access log. Timeseries Service not enabled for account. Resource access not logged."   | 
</TableWrapper>

## Advanced database services

<TableWrapper>
| Category | Type | Message                                                                                                                                 | Additional information                                                                                                          |
|:---------|:-----|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Advanced database services   | ERR | "Failed to connect to Advanced Services Database. Operation aborted: database=[{}], modified user[{}]."   | 
| Advanced database services   | ERR | "Failed to update user in Advanced Services Database. Operation aborted: database=[{}], modified user[{}]."   | 
| Advanced database services   | ERR | "Failed to delete user from Advanced Services Database. Operation aborted: database=[{}], modified user[{}]."   | 
</TableWrapper>


