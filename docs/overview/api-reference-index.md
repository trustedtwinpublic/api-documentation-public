---
tags:
  - reference
  - endpoints
---

# API reference index

## Reference index

Below you can find a reference index of endpoints available on the Trusted Twin environment. Click on a category for a list of endpoints by category.

<TableWrapper>
| Path                                                                             | Category                                             | POST                                                                          | GET                                                                                        | PATCH                                                                                      | DELETE                                                                            |
|:---------------------------------------------------------------------------------|:-----------------------------------------------------|:------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------|
| /account                                                                         | [account](../reference/account/index.md)             |                                                                               | [get_account](../reference/account/get-account.md)                                         | [update_account](../reference/account/update-account.md)                                   |                                                                                   |
| /account/services/databases                                                      | [databases****](../reference/databases/index.md)     |                                                                               | [get_databases](../reference/databases/get-databases.md)                                   |                                                                                            |                                                                                   |
| /account/services/databases/{databases}                                          | [databases****](../reference/databases/index.md)     |                                                                               | [get_database](../reference/databases/get-database.md)                                     | [update_database](../reference/databases/update-database.md)                               |                                                                                   |
| /account/services/databases/{databases}/access | [databases****](../reference/databases/index.md)     |                                                                               | [get_database_access](../reference/databases/get-database-access.md)                       |  |    |
| /account/services/databases/{databases}/access/ips | [databases****](../reference/databases/index.md)     |          || [update_database_ip_access](../reference/databases/update-database-ip-access.md) |                                                      |
| /account/services/indexes                                                        | [indexes****](../reference/indexes/index.md)         | [create_indexes_table](../reference/indexes/create-indexes-table.md)          | [get_indexes_tables](../reference/indexes/get-indexes-tables.md)                           | [update_indexes_access](../reference/indexes/update-indexes-access.md)                     |                                                                                   |
| /account/services/indexes/{index}                                                | [indexes****](../reference/indexes/index.md)         |                                                                               | [get_indexes_table](../reference/indexes/get-indexes-table.md)                             | [update_indexes_table](../reference/indexes/update-indexes-table.md)                       | [delete_indexes_table](../reference/indexes/delete-indexes-table.md)              |
| /account/services/indexes/{index}/data                                           | [indexes****](../reference/indexes/index.md)         |                                                                               |                                                                                            |                                                                                            | [truncate_indexes_table](../reference/indexes/truncate-indexes-table.md)          |
| /account/services/timeseries                                                     | [timeseries****](../reference/timeseries/index.md)   | [create_timeseries_table](../reference/timeseries/create-timeseries-table.md) | [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md)                  | [update_timeseries_access](../reference/timeseries/update-timeseries-access.md)            |                                                                                   |
| /account/services/timeseries/{timeseries}                                        | [timeseries****](../reference/timeseries/index.md)   |                                                                               | [get_timeseries_table](../reference/timeseries/get-timeseries-table.md)                    | [update_timeseries_table](../reference/timeseries/update-timeseries-table.md)              | [delete_timeseries_table](../reference/timeseries/delete-timeseries-table.md)     |
| /account/services/timeseries/{timeseries}/data                                   | [timeseries****](../reference/timeseries/index.md)   |                                                                               |                                                                                            |                                                                                            | [truncate_timeseries_table](../reference/timeseries/truncate-timeseries-table.md) |
| /batches                                                                         | [batches](../reference/batches/index.md)             | [create_batch](../reference/batches/create-batch.md)                          | [get_batches](../reference/batches/get-batches.md)                                         |                                                                                            |                                                                                   |
| /batches/{batch}                                                                 | [batches](../reference/batches/index.md)             |                                                                               | [get_batch](../reference/batches/get-batch.md)                                             | [update_batch](../reference/batches/update-batch.md)                                       | [delete_batch](../reference/batches/delete-batch.md)                              |
| /cache                                                                           | [cache](../reference/cache/index.md)                 | [create_upload_url](../reference/cache/create-upload-url.md)                  |                                                                                            |                                                                                            |                                                                                   |
| /cache/{handler}                                                                 | [cache](../reference/cache/index.md)                 |                                                                               |                                                                                            |                                                                                            | [invalidate_upload_url](../reference/cache/invalidate-upload-url.md)              |
| /log                                                                             | [log](../reference/log/index.md)                     |                                                                               | [get_log](../reference/log/get-log.md)                                                     |                                                                                            |                                                                                   |
| /notifications/webhooks                                                          | [notifications](../reference/notifications/index.md) | [webhook_subscribe](../reference/notifications/webhook-subscribe.md)          |                                                                                            |                                                                                            |                                                                                   |
| /notifications/webhooks/access                                                          | [notifications](../reference/notifications/index.md) |            |  [get_notifications_access](../reference/notifications/get-notifications-access.md)                                                                                          |                                                                                            |                                                                                   |
| /notifications/webhooks/access/accounts/{account}                                                          | [notifications](../reference/notifications/index.md) |          |                                                                                            | [update_notifications_account_access](../reference/notifications/update-notifications-account-access.md)                                                                                           |                                                                                   | 
| /notifications/webhooks/{account}                                                | [notifications](../reference/notifications/index.md) |                                                                               | [webhook_confirm_subscription](../reference/notifications/webhook-confirm-subscription.md) | [webhook_refresh_subscription](../reference/notifications/webhook-refresh-subscription.md) | [webhook_unsubscribe](../reference/notifications/webhook-unsubscribe.md)          |
| /roles                                                                           | [role](../reference/roles/index.md)                  | [create_user_role](../reference/roles/create-user-role.md)                    | [get_user_roles](../reference/roles/get-user-roles.md)                                     |                                                                                            |                                                                                   | 
| /roles/{role}                                                                    | [role](../reference/roles/index.md)                  |                                                                               | [get_user_role](../reference/roles/get-user-role.md)                                       | [update_user_role](../reference/roles/update-user-role.md)                                 | [delete_user_role](../reference/roles/delete-user-role.md)                        |
| /secrets/{account}/{pin}                                                         | [user secret](../reference/secrets/index.md)         | [create_user_secret](../reference/secrets/create-user-secret.md)              |                                                                                            |                                                                                            |                                                                                   |
| /stickers                                                                        | [stickers](../reference/stickers/index.md)           |                                                                               | [list_stickers](../reference/stickers/list-stickers.md)                                    |                                                                                            |                                                                                   | 
| /token                                                                           | [user token](../reference/token/index.md)            | [create_user_token](../reference/token/create-user-token.md)                  |                                                                                            |                                                                                            |                                                                                   |
| /token/refresh                                                                   | [user token](../reference/token/index.md)            | [refresh_user_token](../reference/token/refresh-user-token.md)                |                                                                                            |                                                                                            |                                                                                   |
| /trace                                                                           | [trace](../reference/trace/index.md)                 | [trace](../reference/trace/trace.md)                                          |                                                                                            |                                                                                            |                                                                                   |
| /twins                                                                           | [twin](../reference/twins/index.md)                  | [create_twin](../reference/twins/create-twin.md)                              | [scan_twins](../reference/twins/scan-twins.md)                                             |                                                                                            |                                                                                   |
| /twins/{twin}                                                                    | [twin](../reference/twins/index.md)                  |                                                                               | [get_twin](../reference/twins/get-twin.md)                                                 | [update_twin](../reference/twins/update-twin.md)                                           | [terminate_twin](../reference/twins/terminate-twin.md)                            |
| /twins/{twin}/docs                                                               | [doc](../reference/docs/index.md)                    | [attach_twin_doc](../reference/docs/attach-twin-doc.md)                       | [get_twin_docs](../reference/docs/get-twin-docs.md)                                        |                                                                                            | [delete_twin_docs](../reference/docs/delete-twin-docs.md)                         |
| /twins/{twin}/docs/{doc_name}                                                    | [doc](../reference/docs/index.md)                    |                                                                               | [get_twin_doc](../reference/docs/get-twin-doc.md)                                          | [update_twin_doc](../reference/docs/update-twin-doc.md)                                    | [delete_twin_doc](../reference/docs/delete-twin-doc.md)                           |
| /twins/{twin}/identities                                                         | [identity](../reference/identities/index.md)         | [create_twin_identity](../reference/identities/create-twin-identity.md)       | [get_twin_identities](../reference/identities/get-twin-identities.md)                      |                                                                                            |                                                                                   |
| /twins/{twin}/identities/{identity}                                              | [identity](../reference/identities/index.md)         |                                                                               | [get_twin_identity](../reference/identities/get-twin-identity.md)                          | [update_twin_identity](../reference/identities/update-twin-identity.md)                    | [delete_twin_identity](../reference/identities/delete-twin-identity.md)           |
| /resolve/{identity}                                                              | [identity](../reference/identities/index.md)         |                                                                               | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)                  |                                                                                            |                                                                                   |
| /twins/{twin}/ledgers/{ledger}                                                   | [ledger](../reference/ledgers/index.md)              | [add_twin_ledger_entry*](../reference/ledgers/add-twin-ledger-entry.md)       | [get_twin_ledger_entry***](../reference/ledgers/get-twin-ledger-entry.md)                  | [update_twin_ledger_entry*](../reference/ledgers/update-twin-ledger-entry.md)              | [delete_twin_ledger_entry*](../reference/ledgers/delete-twin-ledger-entry.md)     |
| /twins/{twin}/ledgers/{ledger}/history                                           | [history](../reference/history/index.md)             |                                                                               | [get_twin_ledger_entry_history**](../reference/history/get-twin-ledger-entry-history.md)   |                                                                                            |                                                                                   |
| /twins/{twin}/ledgers/{ledger}/value                                             | [ledger](../reference/ledgers/index.md)              |                                                                               | [get_twin_ledger_entry_value**](../reference/ledgers/get-twin-ledger-entry-value.md)       | [update_twin_ledger_entry_value*](../reference/ledgers/update-twin-ledger-entry-value.md)  |                                                                                  |
| /twins/{twin}/stickers                                                           | [stickers](../reference/stickers/index.md)           | [put_sticker](../reference/stickers/put-sticker.md)                           | [get_stickers](../reference/stickers/get-stickers.md)                                      |                                                                                            |                                                                                   |
| /twins/{twin}/stickers/{color}                                                   | [stickers](../reference/stickers/index.md)           |                                                                               | [get_sticker](../reference/stickers/get-sticker.md)                                        |                                                                                            | [remove_sticker](../reference/stickers/remove-sticker.md)                         |                                                        |
| /usage                                                                           | [usage](../reference/usage/index.md)                 |                                                                               | [get_account_usage](../reference/usage/get-account-usage.md)                               |                                                                                            |                                                                                   | 
| /usage/{user}                                                                    | [usage](../reference/usage/index.md)                 |                                                                               | [get_user_usage](../reference/usage/get-user-usage.md)                                     |                                                                                            |                                                                                   | 
| /users                                                                           | [user](../reference/users/index.md)                  | [create_user](../reference/users/create-user.md)                              |              [get_users](../reference/users/get-users.md)                                                                              |                                                                                            |                                                                                   |
| /users/{user}                                                                    | [user](../reference/users/index.md)                  |                                                                               | [get_user](../reference/users/get-user.md)                                                 | [update_user](../reference/users/update-user.md)                                           | [delete_user](../reference/users/delete-user.md)                                  | 
| /users/{user}/secrets                                                            | [user secret](../reference/secrets/index.md)         | [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md)      | [get_user_secret](../reference/secrets/get-user-secret.md)                                 | [update_user_secret](../reference/secrets/update-user-secret.md)                           | [delete_user_secret](../reference/secrets/delete-user-secret.md)                  |
| /whoami                                                                          | [who am i](../reference/token/index.md)              |                                                                               | [who_am_i](../reference/whoami/who-am-i.md)                                                |                                                                                            |                                                                                   |
</TableWrapper>

::: footnote *
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../overview/rules.md) allows you to perform operations on the given [Twin](../reference/twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`).
:::

::: footnote **
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../overview/rules.md) allows you to perform operations on the given [Twin](../reference/twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). 
:::

::: footnote ***
The `"ledger"` parameter in the path of the request should point to the Ledger resource for requests based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids). Request based on the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) can be performed if the Twin [rule](../overview/rules.md) allows you to perform operations on the given [Twin](../reference/twins/index.md). For requests on personal Ledger, you can use `personal` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/personal`). For requests on Ledgers where you are the owner of the Twin, you can use `owner` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/owner`). For requests on Ledgers where you are the creator of the Twin, you can use `creator` instead of the Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request (`/twins/{twin}/ledgers/creator`).
:::

::: footnote ****
The advanced database services ([Databases](../reference/databases/index.md), [Timeseries](../reference/timeseries/index.md) and [Indexes](../reference/indexes/index.md)) need to be enabled for your account. Please contact <hello@trustedtwin.com> for more details.
:::


## Endpoints

Below you can find a breakdown of endpoints by category.
Click on the operation name for comprehensive information about the endpoint.

### Account

<EndpointsTable>
| Method | Path                     | Operation                                                                           |
|:-------|:-------------------------|:------------------------------------------------------------------------------------|
| GET   | /account   | [get_account](../reference/account/get-account.md) |
| PATCH  | /account | [update_account](../reference/account/update-account.md)        |
</EndpointsTable>

### Batches

<EndpointsTable>
| Method | Path                          | Operation                                                           |
|:-------|:------------------------------|:--------------------------------------------------------------------|
| POST   | /batches          | [create_batch](../reference/batches/create-batch.md)                   |
| GET    | /batches | [get_batches](../reference/batches/get-batches.md)                         |
| GET    | /batches/{batch}           | [get_batch](../reference/batches/get-batch.md)                         |
| PATCH  | /batches/{batch} | [update_batch](../reference/batches/update-batch.md)                   |
| DELETE | /batches/{batch} | [delete_batch](../reference/batches/delete-batch.md)                   | 
</EndpointsTable>

### Cache

<EndpointsTable>
| Method | Path                          | Operation                                                           |
|:-------|:------------------------------|:--------------------------------------------------------------------|
| POST   | /cache                        | [create_upload_url](../reference/cache/create-upload-url.md)         |
| DELETE | /cache/{handler}              | [invalidate_upload_url](../reference/cache/invalidate-upload-url.md) | 
</EndpointsTable>

### Databases*

<EndpointsTable>
| Method | Path                                | Operation*                                                        |
|:-------|:------------------------------------|:------------------------------------------------------------------|
| GET   | /account/services/databases           | [get_databases](../reference/databases/get-databases.md) |
| GET  | /account/services/databases/{database} | [get_database](../reference/databases/get-database.md)       |
| PATCH | /account/services/databases/{database} | [update_database](../reference/databases/update-database.md)      |
| GET  | /account/services/databases/{database}/access  | [get_database_access](../reference/databases/get-database-access.md) |
| PATCH | /account/services/databases/{database}/access/ips | [update_database_ip_access](../reference/databases/update-database-ip-access.md) |
| PATCH | /account/services/databases/{database}/access/users/{user} | [update_database_user_access](../reference/databases/update-database-user-access.md) |
</EndpointsTable>

::: footnote *
The Databases service needs to be enabled for your Account. Please contact <hello@trustedtwin.com> for more details.
:::

### Docs

<EndpointsTable>
| Method | Path                          | Operation                                                           |
|:-------|:------------------------------|:--------------------------------------------------------------------|
| POST   | /twins/{twin}/docs            | [attach_twin_doc](../reference/docs/attach-twin-doc.md)                   |
| GET    | /twins/{twin}/docs/{doc_name} | [get_twin_doc](../reference/docs/get-twin-doc.md)                         |
| GET    | /twins/{twin}/docs            | [get_twin_docs](../reference/docs/get-twin-docs.md)                         |
| PATCH  | /twins/{twin}/docs/{doc_name} | [update_twin_doc](../reference/docs/update-twin-doc.md)                   |
| DELETE | /twins/{twin}/docs/{doc_name} | [delete_twin_doc](../reference/docs/delete-twin-doc.md)                   |
| DELETE | /twins/{twin}/docs            | [delete_twin_docs](../reference/docs/delete-twin-docs.md)                   |        
</EndpointsTable>

### History

<EndpointsTable>
| Method | Path                                   | Operation                                                             |
|:-------|:---------------------------------------|:----------------------------------------------------------------------|
| GET    | /twins/{twin}/ledgers/{ledger}/history | [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md)       |
</EndpointsTable>

### Identities

<EndpointsTable>
| Method | Path                                | Operation                                                         |
|:-------|:------------------------------------|:------------------------------------------------------------------|
| POST   | /twins/{twin}/identities            | [create_twin_identity](../reference/identities/create-twin-identity.md) |
| GET    | /twins/{twin}/identities/{identity} | [get_twin_identity](../reference/identities/get-twin-identity.md)       |
| GET    | /twins/{twin}/identities            | [get_twin_identities](../reference/identities/get-twin-identities.md)      |
| PATCH  | /twins/{twin}/identities/{identity} | [update_twin_identity](../reference/identities/update-twin-identity.md) |
| DELETE | /twins/{twin}/identities/{identity} | [delete_twin_identity](../reference/identities/delete-twin-identity.md) |
| GET    | /resolve/{identity}                 | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)  |
</EndpointsTable>

### Indexes*

<EndpointsTable>
| Method | Path                                           | Operation                                                                         |
|:-------|:-----------------------------------------------|:----------------------------------------------------------------------------------|
| POST   | /account/services/indexes                   | [create_indexes_table](../reference/indexes/create-indexes-table.md)      |
| GET    | /account/services/indexes/{index}      | [get_indexes_table](../reference/indexes/get-indexes-table.md)           |
| GET    | /account/services/indexes                   | [get_indexes_tables](../reference/indexes/get-indexes-tables.md)          |
| PATCH  | /account/services/indexes/{index}      | [update_indexes_table](../reference/indexes/update-indexes-table.md)     |
| DELETE | /account/services/indexes/{index}/data | [truncate_indexes_table](../reference/indexes/truncate-indexes-table.md) |
| DELETE | /account/services/indexes/{index}      | [delete_indexes_table](../reference/indexes/delete-indexes-table.md)     |
</EndpointsTable>

::: footnote *
The Indexes service needs to be enabled for your Account. Please contact <hello@trustedtwin.com> for more details.
:::

### Ledgers

<EndpointsTable>
| Method | Path                                   | Operation                                                             |
|:-------|:---------------------------------------|:----------------------------------------------------------------------|
| POST   | /twins/{twin}/ledgers/{ledger}         | [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md)       |
| GET   | /twins/{twin}/ledgers/{ledger}/value | [get_twin_ledger_entry_value](../reference/ledgers/get-twin-ledger-entry-value.md) |
| GET    | /twins/{twin}/ledgers/{ledger}         | [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md)       |
| PATCH   | /twins/{twin}/ledgers/{ledger}/value | [update_twin_ledger_entry_value](../reference/ledgers/update-twin-ledger-entry-value.md) |
| PATCH  | /twins/{twin}/ledgers/{ledger}         | [update_twin_ledger_entry](../reference/ledgers/update-twin-ledger-entry.md) |
| DELETE | /twins/{twin}/ledgers/{ledger}         | [delete_twin_ledger_entry](../reference/ledgers/delete-twin-ledger-entry.md) |
</EndpointsTable>

### Log

<EndpointsTable>
| Method | Path | Operation                             |
|:-------|:-----|:--------------------------------------|
| GET    | /log | [get_log](../reference/log/get-log.md) |
</EndpointsTable>

### Notifications

<EndpointsTable>
| Method | Path | Operation*                                                          |
|:-------|:-----|:--------------------------------------------------------------------|
| POST    | /notifications/webhooks | [webhook_subscribe](../reference/notifications/webhook-subscribe.md)                   |
| GET    | /notifications/webhooks/{account}?token={token} | [webhook_confirm_subscription](../reference/notifications/webhook-confirm-subscription.md) |
| PATCH | /notifications/webhooks/{account}?token={token} | [webhook_refresh_subscription](../reference/notifications/webhook-refresh-subscription.md) |
| DELETE | /notifications/webhooks/{account}?token={token} | [webhook_unsubscribe](../reference/notifications/webhook-unsubscribe.md)              |
| PATCH | /notifications/webhooks/access/account/{account} | [update_notifications_account_access](../reference/notifications/update-notifications-account-access.md) |
| GET | /notifications/webhooks/access | [get_notifications_access](../reference/notifications/get-notifications-access.md) |

</EndpointsTable>

### Roles

<EndpointsTable>
| Method | Path          | Operation                                              |
|:-------|:--------------|:-------------------------------------------------------|
| POST   | /roles        | [create_user_role](../reference/roles/create-user-role.md)   |
| GET    | /roles/{role} | [get_user_role](../reference/roles/get-user-role.md)         |
| GET    | /roles/       | [get_user_roles](../reference/roles/get-user-roles.md) |
| PATCH  | /roles/{role} | [update_user_role](../reference/roles/update-user-role.md)   |
| DELETE | /roles/{role} | [delete_user_role](../reference/roles/delete-user-role.md)   |
</EndpointsTable>

### Stickers

<EndpointsTable>
| Method | Path   | Operation*                        |
|:-------|:-------|:----------------------------------|
| POST   | /twins/{twin}/stickers | [put_sticker](../reference/stickers/put-sticker.md) |
| GET  | /twins/{twin}/stickers/{color} | [get_sticker](../reference/stickers/get-sticker.md)    |
| GET | /twins/{twin}/stickers/ | [get_stickers](../reference/stickers/get-stickers.md)    |
| GET | /stickers | [list_stickers](../reference/stickers/list-stickers.md)  |
| DELETE | /twins/{twin}/stickers/{color} | [remove_sticker](../reference/stickers/remove-sticker.md) |
</EndpointsTable>

### Timeseries*

<EndpointsTable>
| Method | Path                                           | Operation*                                                                        |
|:-------|:-----------------------------------------------|:----------------------------------------------------------------------------------|
| POST   | /account/services/timeseries                   | [create_timeseries_table](../reference/timeseries/create-timeseries-table.md)     |
| GET    | /account/services/timeseries/{timeseries}      | [get_timeseries_table](../reference/timeseries/get-timeseries-table.md)           |
| GET    | /account/services/timeseries                   | [get_timeseries_tables](../reference/timeseries/get-timeseries-tables.md)           |
| PATCH  | /account/services/timeseries/{timeseries}      | [update_timeseries_table](../reference/timeseries/update-timeseries-table.md)     |
| DELETE | /account/services/timeseries/{timeseries}/data | [truncate_timeseries_table](../reference/timeseries/truncate-timeseries-table.md) |
| DELETE | /account/services/timeseries/{timeseries}      | [delete_timeseries_table](../reference/timeseries/delete-timeseries-table.md)     |
</EndpointsTable>

::: footnote *
The Timeseries service needs to be enabled for your Account. Please contact <hello@trustedtwin.com> for more details.
:::

### Trace

<EndpointsTable>
| Method | Path | Operation                             |
|:-------|:-----|:--------------------------------------|
| POST    | /trace | [trace](../reference/trace/trace.md) |
</EndpointsTable>

### Twins

<EndpointsTable>
| Method | Path           | Operation                                           |
|:-------|:---------------|:----------------------------------------------------|
| POST   | /twins         | [create_twin](../reference/twins/create-twin.md)    |
| GET    | /twins/{twin}  | [get_twin](../reference/twins/get-twin.md)          |
| GET    | /twins  | [scan_twins](../reference/twins/scan-twins.md)          |
| PATCH  | /twins/{twin}  | [update_twin](../reference/twins/update-twin.md)    |
| DELETE | /twins/{twin}  | [terminate_twin](../reference/twins/terminate-twin.md) |
</EndpointsTable>

### Usage

<EndpointsTable>
| Method | Path   | Operation* |
|:-------|:-------|:-----------|
| GET   | /usage | [get_account_usage](../reference/usage/get-account-usage.md) |
| GET | /usage/{usage} | [get_user_usage](../reference/usage/get-user-usage.md) |
</EndpointsTable>

### Users

<EndpointsTable>
| Method | Path          | Operation                                        |
|:-------|:--------------|:-------------------------------------------------|
| POST   | /users        | [create_user](../reference/users/create-user.md) |
| GET    | /users/{user} | [get_user](../reference/users/get-user.md)       |
| GET    | /users | [get_users](../reference/users/get-users.md)       |
| PATCH  | /users/{user} | [update_user](../reference/users/update-user.md) |
| DELETE | /users/{user} | [delete_user](../reference/users/delete-user.md) |
</EndpointsTable>

### User Secrets (API keys)

<EndpointsTable>
| Method | Path                     | Operation                                                                           |
|:-------|:-------------------------|:------------------------------------------------------------------------------------|
| POST   | /users/{user}/secrets    | [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md) |
| POST   | /secrets/{account}/{pin} | [create_user_secret](../reference/secrets/create-user-secret.md)        |
| GET    | /users/{user}/secrets   | [get_user_secret](../reference/secrets/get-user-secret.md)              |
| PATCH  | /users/{user}/secrets   | [update_user_secret](../reference/secrets/update-user-secret.md)        |
| DELETE | /users/{user}/secrets   | [delete_user_secret](../reference/secrets/delete-user-secret.md)        |
</EndpointsTable>

### User Token

<EndpointsTable>
| Method | Path                     | Operation                                                                           |
|:-------|:-------------------------|:------------------------------------------------------------------------------------|
| POST   | /token    | [create_user_token](../reference/token/create-user-token.md) |
| POST   | /token/refresh | [refresh_user_token](../reference/token/refresh-user-token.md)        |
</EndpointsTable>

### Who am I

<EndpointsTable>
| Method | Path          | Operation* |
|:-------|:--------------|:-----------|
| GET    | /whoami | [who_am_i](../reference/whoami/who-am-i.md)   |
</EndpointsTable>









