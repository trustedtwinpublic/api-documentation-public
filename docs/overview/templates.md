---
tags:
  - templates
  - template
  - timeseries templates
  - indexes templates
  - notifications templates
  - publish template
---

# Templates

## Templates overview

A template on the Trusted Twin platform is used every time when it is necessary to return a value or a name that is not known in advance.

Templates are used in:   
- [Timeseries](../reference/timeseries/index.md)*: `"timestamp"`, `"dimensions"`, and `"measurements"`.     
- [Indexes](../reference/indexes/index.md): `"properties"`.    
- [Notifications](../reference/notifications/index.md): `"topic"`.

::: footnote *
Available in general configuration and in [Ledger](../reference/ledgers/index.md) Entry configuration.
:::

## Template syntax

The templates on the Trusted Twin platform follow the Python string `str.format()` convention (see [Format String Syntax](https://docs.python.org/3/library/string.html#format-string-syntax)).       

In case of lists and objects, to access sub-items two interchangeable conventions can be used: 

<ExtendedCodeGroup title="Template example 1" copyable>
<CodeBlock title="json">
```json
"dimensions": {LEDGER.entry1}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="Template example 2" copyable>
<CodeBlock title="json">
```json
"dimensions": {LEDGER[entry1]}
```
</CodeBlock>
</ExtendedCodeGroup>

## Template types

There are two template types - value and name templates:
- Tokens used in a **value template** are replaced by a valid JSON representation of a given value of an item from the [Available variables](./templates.md#available-variables) dictionary.
- Tokens used in a **name template** are replaced by a valid string representation of a given value of an item from the [Available variables](./templates.md#available-variables) dictionary.

In most cases, string and JSON representations are similar. The only difference is the string type value. It is represented in quotation marks (`""`) for value templates and without quotation marks for name templates:

<TableWrapper>
| Type    | Example          | Value template         | Name template    |
|:--------|:-----------------|:-----------------------|:-----------------|
| string  | ``example name "x"``  | `` "example name \"x\"" `` | ``example name "x"``  |
| number  | ``123.45``            | ``123.45``                  | ``123.45``            | 
| object  | ``{"a": 1}``          | ``{"a": 1}``                | ``{"a": 1}``          |
| array   | ``[1, 2, 3]``         | ``[1, 2, 3]``               | ``[1, 2, 3]``         | 
| boolean | ``true``              | ``true``                    | ``true``              |
| null    | ``null``              | ``null``                    | ``null``              | 
</TableWrapper>

For example, a template `{LEDGER.str_entry}_{num_entry}` will be resolved to `Gdansk_123` in case of a name template or to `"Gdansk"_123` in case of a value template.

## Available templates

<TableWrapper>
| Name            | Template type  | Used in                                              | Description                                                                                            |
|:----------------|:---------------|:-----------------------------------------------------|:-------------------------------------------------------------------------------------------------------|
| `"timestamp"`   | Value template | [Timeseries](../reference/timeseries/index.md) and [Ledger](../reference/ledgers/index.md)       | Template that defines the value of the timestamp.                                                      |
| `"dimensions"`  | Value template | [Timeseries](../reference/timeseries/index.md) and [Ledger](../reference/ledgers/index.md)      | Template that defines the value for each of the dimensions.                                            |
| `"measurements"`| Name template  | [Timeseries](../reference/timeseries/index.md) and [Ledger](../reference/ledgers/index.md)      | Template that defines the name of the measurement column to store the Entry value.                     |
| `"properties"`  | Value template | [Indexes](../reference/indexes/index.md)             | Template containing a list of property names and a list of data types of the corresponding properties. | 
| `"topic" (entry)`       | Name template  | [Notifications](../reference/notifications/index.md) | Template that defines the name of the subscription topic.                                              | 
| `"topic" (Sticker)`       | Name template  | [Notifications](../reference/notifications/index.md) | Template that defines the name of the subscription topic.                                              | 
</TableWrapper>

## Available variables

Below you can find variables that can be used in templates:

### System variables

<TableWrapper>
| Variable name  | Type          | Description   | `"timestamp"` | `"dimensions"` | `"measurement"` | `"properties"` | `"topic" (entry)` | `"topic" (Sticker)` |
|:-----------|:---------------------------------------------|:---------|:--------------|:---------------|:----------------|:---------------|:----------|:---|
| account                                    | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                                     | &check;       | &check;        | &check;         | &check;        | &check;   | &check;   |
| twin                                       | string                                       | Twin [UUID](../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                                                                                                                                                                                                           | &check;       | &check;        | &check;         | &check;        | &check;   | &check;   |
| ledger                                     | string                                       | Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids). The Ledger [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) is equal to the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) if the operation is performed by a user belonging to an account that owns the Ledger. | &check;       | &check;        | &check;         | &check;        | &check;   |  |
| user*                                      | string                                       | User [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                                        | &check;       | &check;        | &check;         | &check;        | &check;   | &check;   |
| role*                                      | string                                       | Role [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) performing the given API operation.                                                                                                                                                                                                                                        | &check;       | &check;        | &check;         | &check;        | &check;   | &check;   |
| now                                        | timestamp (float)                            | Current time. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                                         | &check;       | &check;        | &check;         | &check;        | &check;   | &check;   |
</TableWrapper>

### Twin variables

<TableWrapper>
| Variable name                              | Type                                         | Description                                                                                                                                                                                                                                                                                                                                            | `"timestamp"` | `"dimensions"` | `"measurement"` | `"properties"` | `"topic"` |
|:-------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:---------------|:----------------|:---------------|:----------|
| twin_status                                | string, value is `"alive"` or `"terminated"` | Status of the Twin.                                                                                                                                                                                                                                                                                                                                    |&check;                 |&check;                  |&check;                   |&check;                  |&check;             |
| twin_owner                                 | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that owns the Twin.                                                                                                                                                                                                                                      |&check;                 |&check;                  |&check;                   |&check;                  |&check;             |
| twin_creator                               | string                                       | Account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Twin.                                                                                                                                                                                                                                   |&check;                 |&check;                  |&check;                   |&check;                  |&check;             |
| twin_created_ts                            | timestamp (float)                            | Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                                   |&check;                 |&check;                  |&check;                   |&check;                  |&check;             |
| twin_updated_ts                            | timestamp (float)                            | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                        &check;                                                                                                                                                       |&check;                 |&check;                  |&check;                   |&check;                  |&check;             |
| TWIN**                                     | dictionary                                   | Description of the Twin which consists of user-defined key-value pairs:<br/> - key: Must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`. <br/> - value: Valid JSON data type. <br/>For more details consult the [description field](../overview/trusted-twin-api.md#description-field) section.           |   &check;              |&check;                  |&check;                   |&check;                  |&check;             |
</TableWrapper>

### Ledger variables

<TableWrapper>
| Variable name                              | Type                                         | Description                                                                                                                                                                                                                                                                                                                                            | `"timestamp"` | `"dimensions"` | `"measurement"` | `"properties"` | `"topic" (entry)` | `"topic" (sticker)`|
|:-------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:---------------|:----------------|:---------------|:----------|:---------|
| entry_name                                 | string                                       | Name of the Entry of a Ledger.                                                                                                                                                                                                                                                                                                                         | &check;       | &check;        | &check;         | &check;        | &check;   |
| entry_value                                | valid JSON data type                         | Value of the Entry of a Ledger.                                                                                                                                                                                                                                                                                                                        | &check;       | &check;        | &check;         | &check;        | &check;   |
| entry_created_ts                           | timestamp (float)                            | Time at which the Entry of a Ledger was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                                      | &check;       | &check;        | &check;         | &check;        | &check;   |
| entry_updated_ts                           | timestamp (float)                            | Time at which the key of the Entry of a Ledger was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                      | &check;       | &check;        | &check;         | &check;        | &check;   |
| value_changed_ts                           | timestamp (float)                            | Time at which the value of the Entry of a Ledger was last changed. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                                                                                    | &check;       | &check;        | &check;         | &check;        | &check;   |
| entry_new_value                            | valid JSON data type                         | New value of the Entry (after the change to the Entry value).                                                                                                                                                                                                                                                                                          |               |                |                 |                | &check;   |
| entry_old_value                            | valid JSON data type                         | Old value of the Entry (before the change to the Entry value).                                                                                                                                                                                                                                                                                         |               |                |                 |                | &check;   |
| LEDGER**                                   | dictionary                                   | The value of the key-value pair:<br/> - Key: alphanumeric string unique within the Ledger. It must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: Valid JSON data type.                                                                                                                                  |   &check;              |    &check;              |      &check;             |   &check;               |     &check;        |
</TableWrapper>


### User variables

<TableWrapper>
| Variable name                              | Type                                         | Description                                                                                                                                                                                                                                                                                                                                            | `"timestamp"` | `"dimensions"` | `"measurement"` | `"properties"` | `"topic"` |
|:-------------------------------------------|:---------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:---------------|:----------------|:---------------|:----------|
| user_name*                                 | string                                       | Name of the user. It must match the [regular expression](https://regexr.com/) `[0-9A-Za-z][0-9A-Za-z_ \-]{0,30}[0-9A-Za-z]`. It does not need to be unique in the context of an account.                                                                                                                                                               |  &check;             |      &check;          |     &check;            |       &check;         |   &check;        |
| USER**                                     | dictionary                                   | Description of the user. It consists of user-defined key-value pairs:<br/> - key: Must match the [regular expression](https://regexr.com/) `^[a-z_][0-9a-z_]{0,63}$`.<br/> - value: Valid JSON data type.                                                                                                                                            |      &check;         |     &check;           |     &check;            |    &check;            |  &check;         |
</TableWrapper>

::: footnote *
Not available in system tasks.
:::

::: footnote **
To access dictionary type variables, use the `.` separator or `[]` brackets.
:::

<ExtendedCodeGroup title="Template variables examples" copyable>
<CodeBlock title="json">

```json
{
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "twin": "f63ce1df-4643-49b2-9d34-38f4b35b9c7a",
    "ledger": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user": "1e999f56-72cc-45b3-96a1-86c5e1f71e8f",
    "role": "e2e57dc3-47ac-4eba-9b79-e2451dba14b7",   
    "now": 1650453625.646,
    "twin_status": "alive",
    "twin_owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "twin_creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "twin_created_ts": 1650452074.932,
    "twin_updated_ts": 1650452074.932,
    "TWIN": {
        "descr_key1": 123,
        "descr_key2": {
            "sub_key1": 1,
            "sub_key2": 2
        }
    },
    "entry_name": "example_entry_name",
    "entry_value": 45.6,
    "entry_created_ts": 1650452176.188,
    "entry_updated_ts": 1650453625.646,
    "value_changed_ts": 1650453625.646,
    "entry_new_value": 45.6,
    "entry_old_value": 32.8,
    "LEDGER": {
        "entry_str": "Gdansk",
        "entry_num": 1234
    },
    "user_name": "test_user_1",
    "USER": {
        "type_of_user": {
            "test_field": "test value"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


### User Activity Log variables

<TableWrapper>
| Variable name    | Type       | Description                                                                                                                                                                                                                              | `"timestamp"` | `"dimensions"` | `"measurement"` |
|:-----------------|:-----------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:---------------|:----------------|
| request_uuid     | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the request.                                                                                                                                                       | &check;       | &check;        | &check;         |
| request_ts       | timestamp  | Time of the request. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                    | &check;       | &check;        | &check;         |
| user             | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the user making the request or of the user who generated the [User Token](../reference/token/index.md) used in the request.                                        | &check;       | &check;        | &check;         |
| operation        | string     | Endpoint operation (see [List of endpoints](../overview/api-reference-index.md)).                                                                                                                                                          | &check;       | &check;        | &check;         |
| auth_type        | string     | Authorization type. Value can be `"secret"` denoting that a [User Secret](../reference/secrets/index.md) was used in the request or `"token"` denoting that a [User Token](../reference/token/index.md) was used in the request.  | &check;       | &check;        | &check;         |
| auth_fingerprint | string     | 4 last characters of the User Secret used in the request or of the User Secret of the user who generated the [User Token](../reference/token/index.md) used in the request.                                                              | &check;       | &check;        | &check;         |
| auth_validity_ts | timestamp  | Time at which the [User Secret](../reference/secrets/index.md) used in the request expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | &check;       | &check;        | &check;         |
| account          | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account of the user making the request or of the user who generated the [User Token](../reference/token/index.md) used in the request.                         | &check;       | &check;        | &check;         |
| role             | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the role of the user making the request or of the user who generated the [User Token](../reference/token/index.md) used in the request.                            | &check;       | &check;        | &check;         |
| status_code      | integer    | Status code of the request (see [Status codes](../overview/status-codes.md)).                                                                                                                                                                  | &check;       | &check;        | &check;         |
| duration         | float      | Duration of the request.                                                                                                                                                                                                                 | &check;       | &check;        | &check;         |
| RESOURCES        | dictionary | Path parameters of the request.                                                                                                                                                                                                          | &check;       | &check;        | &check;         |
| PARAMS           | dictionary | Query string parameters of the request.                                                                                                                                                                                                  | &check;       | &check;        | &check;         |
| DICT             | dictionary | `DICT` passed in the [X-TrustedTwin custom header](../account-and-access/custom-headers.md) or `"secret_dict"` of the [User Token](../reference/token/index.md).                                                                         | &check;       | &check;        | &check;         |
</TableWrapper>

<ExtendedCodeGroup title="User activity log variables examples" copyable>
<CodeBlock title="json">

```json
{
    "request_uuid": "848cbff6-cb7a-4497-9501-aa8b836a8dfb",
    "request_ts": 1660745883.00,
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user": "1e999f56-72cc-45b3-96a1-86c5e1f71e8f",
    "role": "e2e57dc3-47ac-4eba-9b79-e2451dba14b7",
    "auth_type": "token",
    "auth_fingerprint": "xds8",
    "auth_validity": 1668694683.00,
    "operation": "create_twin",
    "status_code": 201,
    "duration": 0.1098921299,
    "RESOURCES": {
        "twin": "cb4e38ad-649b-46e1-9879-a6c7f9d8fa8b"
    },
    "PARAMS": {
        "show_terminated": "false"
    },
    "DICT": {
        "company": "company1"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


### Resource Access Log variables

<TableWrapper>
| Variable name    | Type       | Description                                                                                                                                                                                                                              | `"timestamp"` | `"dimensions"` | `"measurement"` |
|:-----------------|:-----------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:---------------|:----------------|
| request_uuid     | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the request.                                                                                                                                                       | &check;       | &check;        | &check;         |
| request_ts       | timestamp (float) | Time of the request. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).                                                                                    | &check;       | &check;        | &check;         |
| account          | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account requesting access to the resource.                         | &check;       | &check;        | &check;         |
| user             | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the user requesting access to the resource or of the user who generated the [User Token](../reference/token/index.md) used in the request.                                        | &check;       | &check;        | &check;         |
| role             | string     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the role of the user requesting access to the resource or of the user who generated the [User Token](../reference/token/index.md) used in the request.                            | &check;       | &check;        | &check;         |
| auth_type        | string     | Authorization type. Value can be `"secret"` denoting that a [User Secret](../reference/secrets/index.md) was used in the request or `"token"` denoting that a [User Token](../reference/token/index.md) was used in the request.  | &check;       | &check;        | &check;         |
| auth_fingerprint | string     | 4 last characters of the User Secret used in the request or of the User Secret of the user who generated the [User Token](../reference/token/index.md) used in the request.                                                              | &check;       | &check;        | &check;         |
| auth_validity_ts | timestamp (float) | Time at which the [User Secret](../reference/secrets/index.md) used in the request expires. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). | &check;       | &check;        | &check;         |
| operation        | string     | Endpoint operation (see [List of endpoints](../overview/api-reference-index.md)).                                                                                                                                                          | &check;       | &check;        | &check;         |
| status_code      | integer    | Status code of the request (see [Status codes](../overview/status-codes.md)).                                                                                                                                                                  | &check;       | &check;        | &check;         |
| RESOURCES        | dictionary | Path parameters of the request.                                                                                                                                                                                                          | &check;       | &check;        | &check;         |
| PARAMS           | dictionary | Query string parameters of the request.                                                                                                                                                                                                  | &check;       | &check;        | &check;         |
| twin             | string | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the accessed Twin. | &check;       | &check;        | &check;         |
| twin_status             | string, value is `"alive"` or `"terminated"` | Status of the Twin. | &check;       | &check;        | &check;         |
| twin_owner            | string | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account owning the Twin. | &check;       | &check;        | &check;         |
| twin_creator             | string | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Twin. | &check;       | &check;        | &check;         |
| twin_created_ts             | timestamp (float) | Time when the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).| &check;       | &check;        | &check;         |
| twin_updated_ts             | timestamp (float) | Time when the Twin was updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).| &check;       | &check;        | &check;         |
| LEDGER             | dictionary | Ledger Entries accessed. | &check;       | &check;        | &check;         |
| DOCS             | dictionary | Docs accessed. | &check;       | &check;        | &check;         |


</TableWrapper>

<ExtendedCodeGroup title="Resource Access Log variables examples" copyable>
<CodeBlock title="json">

```json
{
    "request_uuid": "848cbff6-cb7a-4497-9501-aa8b836a8dfb",
    "request_ts": 1660745883.00,
    "account": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "user": "1e999f56-72cc-45b3-96a1-86c5e1f71e8f",
    "role": "e2e57dc3-47ac-4eba-9b79-e2451dba14b7",
    "auth_type": "token",
    "auth_fingerprint": "xds8",
    "auth_validity": 1668694683.00,
    "operation": "get_twin",
    "status_code": 200,
    "RESOURCES": {
        "twin": "cb4e38ad-649b-46e1-9879-a6c7f9d8fa8b"
    },
    "PARAMS": {
        "show_terminated": "false"
    },
    "twin": "cb4e38ad-649b-46e1-9879-a6c7f9d8fa8b",
    "twin_status": "alive",
    "twin_creator": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "twin_owner": "9891264d-4a77-4fa2-ae7f-84c9af14ae3b",
    "twin_created_ts": "1702761358.00",
    "twin_updated_ts": "1702847758",
    "LEDGER": {
      "entry_1": "ok",
      "entry_2": "error"
    },
    "DOCS": {
      "doc_name_1": {
        "doc_created_ts": "1702934158.00",
        "doc_updated_ts": "1703020558.00"
      }
    }
    
}
```
</CodeBlock>
</ExtendedCodeGroup>
