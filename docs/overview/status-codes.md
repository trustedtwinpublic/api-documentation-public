---
tags:
  - status code
  - status codes
  - error code
  - error codes
  - error subcodes
  - error subcode
---

# Status codes

## HTTP status codes

The Trusted Twin API uses [HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) to communicate if a request has been successfully processed or not:
- Successful operations will return 2xx codes.
- Operations that result in an error due to a problem on the client's side will return 4xx codes.
- Operations that result in an error due to a problem in the Trusted Twin server will return 5xx codes.

<TableWrapper>
| Status code | Message                 | Description                                                                                      | 
|:------------|:------------------------|:-------------------------------------------------------------------------------------------------|
| 200         | OK                      | Successful request and response.                                                                 |                       
| 201         | Created                 | The resource was successfully created.                                                           |                       
| 400         | Bad request             | The request was unacceptable, often due to missing a required parameter or malformed parameters. |                       
| 401         | Authentication required | Authentication failed or was not provided.                                                       |                        
| 403         | Forbidden               | The user does not have permissions to perform the request.                                       |                       
| 404         | Not found               | The requested resource does not exist.                                                           |                       
| 500         | Internal error          | The server encountered an internal error and was not able to complete the request.               |
</TableWrapper>

## Error subcodes

All errors can be handled programmatically. The error response includes specific attributes and error subcodes that state the cause of the error.

<div class="row">
<div class="column">

<TableWrapper>
| Attribute   | Type   | Description                                                                                                                               | 
|:------------|:-------|:------------------------------------------------------------------------------------------------------------------------------------------|
| subcode     | string | Unique subcode of the error.                                                                                                              |                       
| trace       | string | String identifying the specific error instance. Please reference the trace when contacting our support team to ensure the fastest resolution. |                       
| description | string | Description that states the cause of the error.                                                                                           |                       
</TableWrapper>

</div>
<div class="column">

<ExtendedCodeGroup title="Example error response" copyable>
<CodeBlock title="json">
```json
{
  "subcode": 400003,
  "trace": "744cbb2c-112a-4013-a170-44cea35fa2fe",
  "description": "Twin already terminated."
}
```
</CodeBlock>
</ExtendedCodeGroup>

</div>
</div>

### Subcode examples

Below you will find a list of error subcodes. This list is not exhaustive.

<TableWrapper>
| Subcode | Message                   | Description                                                                                                                                                                                                                                         | 
|:--------|:--------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 400141  | quota_violation           | The body of the request or the Ledger exceeds 64 kB and is, as a result, too large to process.                                                                                                                                                      |                                                                                                                                                                                                                                                                                                                                     |            
| 400314  | invalid_characters        | Characters in the body of the request are not UTF-8 encoded. Example response: `"Request body contains invalid characters."`                                                                                                                        | 
| 400358  | idempotency_violation     | The operation has already been performed. Example response: `"Twin already terminated."`.                                                                                                                                                           |
| 400415  | not_a_valid_json          | The body of the request does not follow JSON syntax. You can use the [JSON formatter and validator](https://jsonformatter.curiousconcept.com/#) to check if the request is JSON compliant. Example response: `"Request body is not a valid JSON."`. |
| 400589  | request_format            | The format of the request is not correct. Example response: `"body::description->__key__: string does not match regex ^[a-z_][0-9a-z_]{0,63}$".`                                                                                                  |                                                                                                                                                                                                                                                                                                                                      |
| 400592  | operation_order_violation | Operations have not been conducted in correct order. Example response: `"Remove User before removing a Role."`.                                                                                                                                     |
| 400897  | operation_not_allowed     | A User cannot grant permissions that are not included in the `"statement"` of their own role. Example response: `"You cannot grant permissions that are not included in the 'statement' of your role: {}."`                                         |
| 400926  | field_required            | The request is missing a required field.                                                                                                                                                                                                            |
| 400979  | service_disabled          | The service is not enabled for the Account. Example response: `"Timeseries not enabled for Account"`. Please contact <hello@trustedtwin.com> for more details about enabling the service for your Account.                                                                                                                                 |
| 401000  | unauthorized              | The User could not be authenticated. Example responses: `"Invalid secret."`, `"Authorization header not provided."`, `"Operation not allowed."`.                                                                                                    |                                                                                                                                                                                                                                                                                                                                         | 
| 403535  | access_forbidden          | The User does not have permission to access the requested resource. Example response: `"No access to the resource(s): Not a Twin owner."`                                                                                                           |
| 404653  | not_found                 | The resource could not be found in the system.                                                                                                                                                                                                      |  
| 50000x  | internal_error            | The server encountered an internal error and was not able to complete the request.                                                                                                                                                                  | 
| 50x159  | response_timeout          | A server is taking too long to reply to a data request made from another device.                                                                                                                                                                    |
| 500314  | services_database_no_connection  | Connection could not be established to the Timeseries database. Example response: `"Could not connect to Timeseries database."`                                                                                                                     | 
</TableWrapper>
