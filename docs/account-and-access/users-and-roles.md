---
tags:
  - super admin
  - user
  - users
  - role
  - roles
meta:
    - name: Users and roles
      content: User and role creation best practices
---

# Users and roles

## Super Admin role and user

When you [create a Trusted Twin account](./account-creation.md) through our [Self-care panel](https://selfcare.trustedtwin.com/login/), a Super Admin user with a Super admin role is generated automatically, together with a Super Admin User Secret (Super Admin API key). The Super Admin role allows the Super Admin user to access all account-related objects created on the Trusted Twin platform in the given account. As well, it allows to call all API methods available on the platform. Therefore, a Super Admin user can be considered an equivalent of a root user in a typical IT system. 

We designed the Trusted Twin system in a way to ensure maximum security when providing the initial Super Admin User Secret (Super Admin API key). However, we strongly recommend generating a new User Secret (API key), especially for production environments, as the initial User Secret (API key) is delivered through the web interface. 

We strongly recommend not to use the Super Admin User Secret (API key) to perform daily tasks and only use it to create new roles and users.


## Creating a role

Before creating a user, you will need to create a role ([create_user_role](../reference/roles/create-user-role.md)). You will assign the role to a user when creating the user. A single role can be assigned to many users.

A role is a collection of permissions. The permissions determine the actions that a user can perform, and on which resources the user can perform such actions. 

We recommend that you use the [Principle of Least Privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege) when creating roles and assigning them to users. Permissions should be limited to essential actions and resources required for a given user in their function. 

Please consult the [Role](../reference/roles/index.md) section to learn more.

## Creating a user

After you have created a role, you can create a user ([create_user](../reference/users/create-user.md)) and assign the role to the user. Please consult the [User](../reference/users/index.md) section to learn more.

## Creating a User Secret (API key)

After creating a user, you need to create a User Secret (API key) for the user. 

Please continue to the [Authentication](./authentication.md) section to learn more.


