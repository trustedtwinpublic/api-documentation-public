---
tags:
  - account
  - create account
  - account creation
  - free account
  - super admin
meta:
    - name: Account creation
      content: How to create an account on the Trusted Twin platform
---

# Account creation

Please contact <hello@trustedtwin.com> for more details about creating an account on the Trusted Twin platform.

