---
tags:
  - database access
  - access
  - timeseries access
  - indexes access
meta:
    - name: Database services access
      content: How to access the database services in the Trusted Twin platforms
---

# Database services access

The Trusted Twin platform offers advanced database services. They include:
- the [Database](../reference/databases/index.md) service,
- the [Indexes](../reference/indexes/index.md) database service,
- the [Timeseries](../reference/timeseries/index.md) database service.

## Enabling Timeseries and Indexes databases

The advanced database services need to be enabled for an account. Please get in touch with <hello@trustedtwin.com> to enable the advanced database services for your account.

## Accessing Timeseries and Indexes databases

### Granting and removing user access

If the database advanced database services are enabled for your account, you need to grant a given user `"read'` or `"write"` access to the [Timeseries](../reference/timeseries/index.md) tables, [Indexes](../reference/indexes/index.md) tables or to the customer schema of a given database. You can grant permissions through the [update_database_user_access](../reference/databases/update-database-user-access.md) endpoint.

The following permissions can be granted:

<TableWrapper>
| Access permission | Description                                                           
|:---------|:----------------------------------------------------------------------|
| **read** | The user is allowed to read from the given tables of a given database.     |
| **write** | The user is allowed to write to the given tables of a given database. |
</TableWrapper>

Please note that the "write" permission allows for write access only. If the user should also be allowed to read from the given tables of the given database, they should be assigned the "read" permission as well.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "timeseries": ["read"],
    "indexes": ["read"],
    "customer_data": ["read", "write"]

}
```

</CodeBlock>
</ExtendedCodeGroup>

To remove the access of a user to given tables of a given database, you need to include an pass an empty list as value for the given tables in the request body:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">

```json
{
    "customer_data": []

}
```

</CodeBlock>
</ExtendedCodeGroup>


### Granting and removing IP access 

In addition to granting a given user access to a [Timeseries](../reference/timeseries/index.md) tables, [Indexes](../reference/indexes/index.md) tables or to the customer schema of a given database, you need to grant IP address access to the given database. You can grant IP access through the [update_database_ip_access](../reference/databases/update-database-ip-access.md) endpoint.


### Accessing database services

First, you need to obtain the database UUID of the database where you want to update user access. You can obtain the database UUID through the [get_databases](../reference/databases/get-databases.md) endpoint. The response to the request looks as follows:

<ExtendedCodeGroup title="Example JSON response" copyable>
<CodeBlock title="json">
```json
{
    "default": "75f48545-2442-4491-81cd-7679f665fa27", 
    "databases": {
        "75f48545-2442-4491-81cd-7679f665fa27":{
            "status": "running",
            "created_ts": 1705570409,
            "updated_ts": 1705570409,
            "note": "database_x" 
        },
        "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c": {
            "status": "running",
            "created_ts": 1706521206,
            "updated_ts": 1706607606,
            "note": "database_y" 
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

The response returns attributes described below in the table. In our example, we can see that there are two databases in the account: the database with database UUID "75f48545-2442-4491-81cd-7679f665fa27" and the database with the database UUID "1c35c001-1c1f-4c82-9fae-88ea4c7aec4c".

<TableWrapper>
| Attribute         | Type                                                              | Description                                                                                                                                                                                                                                                     |
|:------------------|:------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **default**        | string | Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the [default database](./index.md#default-database) of the account. 
| **databases**          | dictionary                                                           | Dictionary containing databases with their attributes:<br/>- key: Database [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).<br/>- value: Database attributes. For more information, see [get_databases](../reference/databases/get-databases.md) endpoint.                                                                                                                                                                                            |
</TableWrapper>

Next, you can access the database services in the same way as a PostgreSQL database. The values of the required fields are as follows:

<TableWrapper>
| field        | value                                                                                                                                            |
|:-------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
| **host**     | `$DATABASE_UUID.database.trustedtwin.com`                                                                                                          |
| **port**     | `50649`                                                                                                                                           |
| **database** | `tt_customer`                                                                                                                           |
| **user**     | [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of the user logging into the database.                                                    |
| **password** * | [User Secret (API key)](../reference/secrets/index.md) of the user logging into the database.                                        |
</TableWrapper>

## Connecting selected data visualization tools

Below you can find instructions specific to selected data visualization tools where establishing a connection to Trusted Twin's advanced database services requires modifications to the process.       
       
Please bear in mind that the Trusted Twin advanced database services are based on the TimescaleDB [Apache 2.0 License](https://github.com/timescale/timescaledb/blob/main/LICENSE-APACHE) version. Please see the [Feature comparison](https://docs.timescale.com/about/latest/timescaledb-editions/) section for TimescaleDB features available in each TimescaleDB version. Should you require a different TimescaleDB version, please email <hello@trustedtwin.com>.

### Grafana

To connect to Trusted Twin's advanced database services using [Grafana](https://grafana.com/), you need to ensure that you select the "require" option in the TLS/SSl Mode section when adding a PostgreSQL data source or updating the settings of an existing PostgreSQL data source:

1. On the PostgreSQL page, go to the *PostgreSQL Connection* section.
2. In the *TLS/SSL Mode* field, select the "require" option from the drop-down menu.

When connecting to advanced services using Grafana, we also recommend to enable the PostgreSQL TimescaleDB* extension. If the extension is enabled, Grafana will use `time_bucket` and display TimescaleDB-specific aggregate functions in the query builder.

In order to enable the connector:

1. On the PostgreSQL page, go to the *PostgreSQL Details* section.
2. In the *TimescaleDB* field, enable the TimescaleDB extension using the toggle button.

::: footnote *
The Trusted Twin advanced database services are based on the TimescaleDB [Apache 2.0 License](https://github.com/timescale/timescaledb/blob/main/LICENSE-APACHE) version. Please see the [Feature comparison](https://docs.timescale.com/about/latest/timescaledb-editions/) section for TimescaleDB features available in each TimescaleDB version. Should you require a different TimescaleDB version, please email <hello@trustedtwin.com>.
:::

### Power BI

In order to establish a connection to Trusted Twin's advanced database services using Microsoft's [Power BI](https://powerbi.microsoft.com/en-us/), you need to install a certificate locally on your machine. 

1. Get in touch with the Trusted Twin team to obtain a certificate by sending an email to <hello@trustedtwin.com>.
2. Open the terminal. You will need to select the "Run as administrator" option. 
3. Install the certificate:


<ExtendedCodeGroup title="Certificate installation" copyable>
<CodeBlock title="bash">
```bash
certutil.exe -addstore -f "Root" $[FILE_NAME].crt
```
</CodeBlock>
</ExtendedCodeGroup>
