---
tags:
  - custom headers
  - custom header
meta:
    - name: Custom headers
      content: How to use optional custom HTTP headers in requests to the Trusted Twin API
---

# Custom headers

The Trusted Twin platform allows you to use optional custom HTTP headers in requests to the Trusted Twin API.

## X-TrustedTwin

The `X-TrustedTwin` header enhances the [rule](../overview/rules.md) functionality through using variables passed by the User in the request to the Trusted Twin API. These variables are accessed via the `DICT` [rule variable](../overview/rules.md#rule-variables).

### About

The `DICT` [rule variable](../overview/rules.md#rule-variables) in the request header follows the standard object description structure. The `DICT` variable must be encoded to be passed safely within the header.

### Encoding

If you are using the [Trusted Twin official Python library](../libraries/library-python.md), you only need to provide the `tt_dict` as a dictionary:

<ExtendedCodeGroup title="Header value provided as 'tt_dict'" copyable>
<CodeBlock title="Python">

```python
tt_dict = {"serial" : "123"}
TT_SERVICE = TTRESTService(tt_auth=$USER_SECRET, tt_dict=tt_dict)
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

In case you need to generate the value, you can use the Python function below:

<ExtendedCodeGroup title="Python function to generate 'tt_dict' value" copyable>
<CodeBlock title="Python">

```python
from base64 import b64encode
from json import dumps
from typing import Dict, Any

def http_header_encode(dictionary: Dict[str, Any]) -> str:
    return b64encode(dumps(dictionary).encode('utf-8')).decode('utf-8')

tt_dict = {"serial" : "123"}

encoded_dict = http_header_encode(tt_dict)
print(encoded_dict)
```

</CodeBlock>
</ExtendedCodeGroup>

<br/>

For other programming languages, use the same technique consisting of the following steps: 
- convert `DICT` into its JSON representation encoded in UTF-8,
- encode the string using base64 standard encoding.

### Example

Let's assume you want to pass the following dictionary in the header:

<ExtendedCodeGroup title="Dictionary example" copyable>
<CodeBlock title="json">

```json
{"serial": "b0f531c3-914b-4408-9895-b61f59c06b66"} 
```

</CodeBlock>
</ExtendedCodeGroup>

After encoding, the header will look as follows:

<ExtendedCodeGroup title="Header example" copyable>
<CodeBlock title="json">

```bash
'X-TrustedTwin: eyJzZXJpYWwiOiAiYjBmNTMxYzMtOTE0Yi00NDA4LTk4OTUtYjYxZjU5YzA2YjY2In0='
```

</CodeBlock>
</ExtendedCodeGroup>

### Example usage scenario

To explore example scenario of `X-TrustedTwin` header usage, please go to [Custom headers](../feature-guides/working-with-custom-headers.md) guide.







