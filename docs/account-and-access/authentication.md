---
tags:
  - user secret
  - api key
meta:
    - name: Authentication
      content: Authentication on the Trusted Twin platform
---

# Authentication (API key)

On the Trusted Twin platform, user authentication (the process of verifying the identity of a user) is based on User Secrets (API keys). 

## About the User Secret

A User Secret is an API key required to access the Trusted Twin API methods. It should be passed in the HTTP Authorization header of each request.

It is 64 characters long unique string of numbers and letters associated with the user for whom the User Secret is generated. There is exactly one User Secret for each user on the Trusted Twin platform.    

A User Secret has a `"validity_ts"` attribute. It holds a [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) with the time at which the User Secret expires. If the value of `"validity_ts"` is `null`, the User Secret does not expire.

### Example request

In our example, the User Secret `"VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"` is passed in the HTTP Authorization header of the request.

<ExtendedCodeGroup title="Example curl request" switchable copyable>
<CodeBlock title="cURL">
```bash
curl --location --request POST 'https://rest.trustedtwin.com/twins' \
--header 'Authorization: VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "description": {
            "size": 41,
            "colour": "black",
            "type": "electric hazard",
            "model": "Xp6",
            "production_year": 2022,
            "qa_passed": true,
            "company": "Shoes",
            "certifications": [ "xptI2021", "fht76j", "IPS1449" ]
        }
}'
```
</CodeBlock>
</ExtendedCodeGroup>


## Generation of a User Secret

The authentication scheme on the Trusted Twin platform consists of two steps:

1. Generation of a User Secret PIN through the [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md) endpoint. 
2. Generation of a User Secret through the [create_user_secret](../reference/secrets/create-user-secret.md) endpoint.

::: warning IMPORTANT NOTE
<Icon name="alert-circle" :size="1.75" />
The User Secret is displayed **only once**. It is not stored in the Trusted Twin system, and it **cannot be retrieved**. Keep your User Secret stored in a secure place. If you lose your User Secret, the User Secret generation process will need to be started from the beginning.
:::

The User Secret PIN is an additional safety lever used on the Trusted Twin platform. It was introduced as an additional safety measure as there can be more than one user involved in the process of generating a User Secret (see example scenario below). 

### Example scenario

In our scenario, two users are involved in the process of generating a User Secret:

- User A already who already has access to the Trusted Twin platform. 
- User B who does not have access to the Trusted Twin platform.    
     
User A wants to grant user B permission to access the Trusted Twin platform.

The scenario is as follows:
- User A sends a request to generate a User Secret PIN ([create_user_secret_pin](../reference/secrets/create-user-secret-pin.md)) for user B. User A includes the user [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) of user B in the path of the request as the User Secret is generated for a given user. 
- After the User Secret PIN has been generated, user A passes the User Secret PIN to user B. 
- User B sends a request to generate a User Secret ([create_user_secret](../reference/secrets/create-user-secret.md)) and adds the User Secret PIN in the path of the request. User B also includes the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request. The inclusion of the account [UUID](../overview/trusted-twin-api.md#trusted-twin-ids) in the path of the request is another safety measure for creating User Secrets.
- The User Secret is generated and displayed to user B. As the User Secret is displayed only once, user B has to take note of the User Secret and keep it safe. They cannot lose it as the User Secret is not stored in the Trusted Twin system.

For more information, please consult the [User authentication](../reference/secrets/index.md) section. The section contains descriptions of endpoints related to the process of user authentication.
