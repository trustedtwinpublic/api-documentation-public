---
tags:
  - resource access
  - access log
  - access logs
meta:
    - name: Resource Access Log
      content: The Trusted Twin platform allows you to log data about access to resources.
---


# Resource Access Log

## About

The Trusted Twin platform allows you to log data about access to resources in your account by your own account (personal account) as well as foreign accounts. The data is stored in the form of a user-defined [Timeseries](../reference/timeseries/index.md) table.

In order to use the Resource Access Log to log access to resources in your account:

1. Create a Timeseries table* where the Resource Access Log data is to be stored (see [Timeseries configuration](./resource-access-log.md#timeseries-configuration) section);
2. Configure the Resource Access Log represented by the previously defined Timeseries table for the account (see [Account configuration](./resource-access-log.md#account-configuration) section).

You can create multiple Timeseries tables to log the access to resources in your account. 

Each of the tables needs to be added to the account's configuration (through the [update_account](../reference/account/update-account.md) endpoint). For more details, please see the [Enabling Resource Access Log](./resource-access-log.md#account-configuration) section.

::: footnote *
Please note that Timeseries is a Trusted Twin advance database service, and it needs to be enabled for an account. Please get in touch with <hello@trustedtwin.com> to enable the advanced database services for your account.
:::

## Timeseries configuration

The Resource Access Log is stored in a user-defined [Timeseries](../reference/timeseries/index.md) table. You can create a Timeseries table via the [create_timeseries_table](../reference/timeseries/create-timeseries-table.md) endpoint.

Please refer to [Resource Access Log template variables](../overview/templates.md#resource-access-log-variables) for template variables available for the Resource Access Log templates.

### Example configuration

Below you can find an example of a request body used to create a [Timeseries](../reference/timeseries/index.md) table used to store the Resource Access Log:

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "timeseries": {
        "resource_access_log": {
            "dimensions": {
                "names": [
                    "request_uuid",
                    "request_ts",
                    "operation",
                    "status_code",
                    "resources",
                    "params",
                    "account",
                    "role",
                    "user",
                    "auth_type",
                    "auth_fingerprint",
                    "auth_validity_ts",
                    "twin",
                    "twin_status",
                    "twin_creator",
                    "twin_owner",
                    "twin_created_ts",
                    "twin_updated_ts",
                    "ledger",
                    "docs"
                ],
                "types": [
                    "uuid",
                    "numeric",
                    "varchar",
                    "int",
                    "jsonb",
                    "jsonb",
                    "uuid",
                    "uuid",
                    "uuid",
                    "varchar",
                    "varchar",
                    "numeric",
                    "uuid",
                    "varchar",
                    "uuid",
                    "uuid",
                    "numeric",
                    "numeric",
                    "jsonb",
                    "jsonb"
                ]
            },
        "measurements": {
            "names": [],
            "types": []
            },
        "defaults": {
            "dimensions": {
                "request_uuid": "{entry_value}",
                "request_ts": "{request_ts}",
                "operation": "{operation}",
                "status_code": "{status_code}",
                "resources": "{RESOURCES}",
                "params": "{PARAMS}",
                "account": "{account}",
                "role": "{role}",
                "user": "{user}",
                "auth_type": "{auth_type}",
                "auth_fingerprint": "{auth_fingerprint}",
                "auth_validity_ts": "{auth_validity_ts}",
                "twin": "{twin}",
                "twin_status": "{twin_status}",
                "twin_creator": "{twin_creator}",
                "twin_owner": "{twin_owner}",
                "twin_created_ts": "{twin_created_ts}",
                "twin_updated_ts": "{twin_updated_ts}",
                "ledger": "{LEDGER}",
                "docs": "{DOCS}"
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

In order to access the Timeseries database, please follow the steps in the [Database services access](../account-and-access/database-services-access.md) article.

## Template variables

In the below sections you can find variables available for the Resource Access Log divided into categories with examples. The examples are based on the Timeseries configuration provided in the [Example configuration](./resource-access-log#example-configuration) section above.

### Request variables

<TableWrapper>
| Variable         | Column name  | Description                                                                                                                                                |
|:-----------------|:-------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| **request_uuid** | request_uuid | Request [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids).                                                                                       |
| **request_ts**   | request_ts   | Timestamp of the request. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
| **operation**    | operation    | Name of the operation.                                                                                                                                     |
| **status_code**  | status_code  | Status of the request.                                                                                                                                     |
| **RESOURCES**    | resources    | Resource passed in the request.                                                                                                                            |
| **PARAMS**       | params       | Query string parameters passed in the request.                                                                                                             | 
</TableWrapper>

#### Example

<TableWrapper>
| request_uuid | request_ts | operation | status_code | resources | params |
|:------|:-----|:------|:------|:-----|:-----|:-----|
| 74239fad-816e-4e75-b6fd-d4a4c50df0d2 | 1704184875.00 | get_twin | 200 | {"twin": "78772552-f372-4cce-ac36-247af4bcb95c"} | {"show_terminated": "false"}
</TableWrapper>

### Requestor variables

<TableWrapper>
| Variable | Column name | Description                                                                           |
|:------|:------|:--------------------------------------------------------------------------------------|
| **account** | account | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the requestor. |
| **role** | role | Role [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the requestor.    |
| **user** | user | User [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the requestor.    |
</TableWrapper>

#### Example

<TableWrapper>
| account | role | user |
|:------|:-----|:-----|
| 20db3819-4cc2-44b5-bba5-fb270f105c07 | 27295dc1-bfbf-43bd-870c-bce2a1ff942e | ed32777c-efe6-4fac-b1fd-0c17b6a6c9ce |
</TableWrapper>

### Authentication variables

<TableWrapper>
| Variable | Column name | Description                                                                                                                                                                                                                                                                                                                                                                                                                                | 
|:------|:------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **auth_type** | auth_type | Denotes the authentication used. It can be either `secret` for authentication performed with a [user secret](../reference/secrets/index.md) or `token` for authentication performed with a [user token](../reference/token/index.md).                                                                                                                                                                                                           |
| **auth_fingerprint** | auth_fingerprint | 4 last characters of the [User Secret](../reference/secrets/index.md) of the calling user or the User Secret that was used to generate the [User Token](../reference/token/index.md) of the calling user.                                                                                                                                                                                                                                  |
| **auth_validity_ts** | auth_validity_ts |  Validity of the [User Secret](../reference/secrets/index.md) or the [User Token](../reference/token/index.md) of the calling user. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time). |
</TableWrapper>

#### Example

<TableWrapper>
| auth_type | auth_fingerprint | auth_validity_ts |
|:-----|:-----|:-----------------|
| user | xds8 | 1735821675.00 |
| token | xds8 | 1735821675.00 |
</TableWrapper>

### Twin variables

<TableWrapper>
| Variable | Column name | Description |
|:---|:----|:----|
| **twin** | twin | Twin [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the requestor. |
| **twin_status** | twin_status | Status of a Twin. Value can be `"alive"` or `"terminated"`. |
| **twin_creator** | twin_creator | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account that created the Twin.|
| **twin_owner** | twin_owner | Account [UUID](../../overview/trusted-twin-api.md#trusted-twin-ids) of the account which is the current owner of the Twin. |
| **twin_created_ts** | twin_created_ts | Time at which the Twin was created. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).  | 
| **twin_updated_ts** | twin_updated_ts | Time at which the Twin was last updated. Measured in seconds (to three decimal places) that have elapsed since the [Unix epoch](https://en.wikipedia.org/wiki/Unix_time).     |
</TableWrapper> 

#### Example

<TableWrapper>
| twin | twin_status | twin_creator | twin_owner | twin_created_ts | twin_updated_ts |
|:-----|:----|:-----|:-----|:-----|:-----|:-----|
| e2a2d605-8f87-40a9-a8c6-80b1309860ac | alive | a517cacf-6819-4c24-a4c9-934678f66c81 | a517cacf-6819-4c24-a4c9-934678f66c81 | 1722598875.00 | 1725461001.00 |
</TableWrapper>

### Ledger variables

<TableWrapper>
| Variable | Column name | Description |
|:-----|:-----|:----|
| **LEDGER** | ledger | Dictionary where keys are names of Entries and values denote whether the requestor could read the Entry's value (`"ok"`) or whether the value was not accessible to the requestor (`"error"`). |
</TableWrapper>

#### Example

<TableWrapper>
| ledger | 
|:-----|
| { "entry_1": "ok", "entry_2": "error"} |
</TableWrapper>

### Doc variables

<TableWrapper>
| Variable | Column name | Description                                                                                                                                                                                 |
|:-------|:-------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **DOCS** | docs | Dictionary where the key is the name of the Doc and the value is a dictionary containing Doc creation timestamp (`"doc_created_ts"`) and timestamp of last Doc update (`"doc_updated_ts"`). |
</TableWrapper>

#### Example

<TableWrapper>
| docs | 
|:-----|
|{ "doc_name_1": {"doc_created_ts": "1702934158.00", "doc_updated_ts": "1703020558.00"}}|
</TableWrapper>

## Resources and operations logged 

The Resource Access Log can be configured to log access to the following resources for the following operations:

<TableWrapper>
| Resource | Operation | Personal account | Foreign account | Template variables                                                                                                                                                                                                             |
|:--------|:-----------|:-----------------|:------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Twin | create_twin | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                            |
| Twin | get_twin| &check;  | &check;                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts         |
| Twin | update_twin | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                 |
| Twin | terminate_twin | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                 |
| Twin | create_twin_identity | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                 |
| Twin | get_twin_identity | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts         |
| Twin | update_twin_identity | &check;  |                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                 |
| Twin | delete_twin_identity | &check;  |               | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                 |
| Twin, Ledger | add_twin_ledger_entry | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts                  |
| Twin, Ledger, Entry | get_twin_ledger_entry | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin, Ledger, Entry | get_twin_ledger_entry_value | &check;  | &check;                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin, Ledger, Entry | update_twin_ledger_entry | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin, Ledger, Entry | update_twin_ledger_entry_value | &check;  |              | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin, Ledger, Entry | update_twin_ledger_entry_ref_value | &check;  |              | request_uuid, request_ts, account, operation, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin, Ledger, Entry | delete_twin_ledger_entry | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, LEDGER |
| Twin | attach_twin_doc | &check;  |                 | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, DOCS           |
| Twin, Doc | get_twin_doc | &check;  |                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, DOCS   |
| Twin, Doc | update_twin_doc | &check;  |                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, DOCS           |
| Twin, Doc | delete_twin_doc | &check;  |                | request_uuid, request_ts, account, role, user, auth_type, auth_fingerprint, auth_validity_ts, operation, status_code, RESOURCES, PARAMS, twin, twin_status, twin_owner, twin_creator, twin_created_ts, twin_updated_ts, DOCS   |
</TableWrapper>

## Account configuration

### Enabling Resource Access Log

The Resource Access Log is enabled at the account-level through adding the `"resource_access_log"` property when updating the account ([update_account](../reference/account/update-account.md)). 


<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "resource_access_log": {
        "account == '6727ff91-60f7-4526-b8f1-bb75de904e73'": {
            "resource_access_log_table_1": {}
        },
        "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
            "resource_access_log_table_2": {},
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Updating Resource Access log

The Resource Access Log can be updated through the [update_account](../reference/account/update-account.md) endpoint. The new request body will replace the content of the `"resource_access_log"` attribute.

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
    "resource_access_log": {
        "account == '2663ba4f-8c05-4e41-b34a-177702a99fa0'": {
            "resource_access_log_table_2": {},
            "resource_access_log_table_3": {}
        }
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


### Disabling Resource Access Log

The Resource Access Log functionality can be disabled by setting the value of the `"resource_access_log"` property to `"null"` through the [update_account](../reference/account/update-account.md) endpoint. 

<ExtendedCodeGroup title="Example request body" copyable>
<CodeBlock title="json">
```json
{
   "resource_access_log": null
}
```
</CodeBlock>
</ExtendedCodeGroup>








