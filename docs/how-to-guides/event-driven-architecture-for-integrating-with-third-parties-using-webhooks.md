---
tags:
  - architecture
  - stateless
  - design pattern
  - design patterns
---

# Event-driven architecture for integrating with third parties

The Trusted Twin platform as a hub for managing event-driven applications to unlock the power of stateless services.

## Scenario

On the Trusted Twin platform, we use a subscription-based payment model with several subscription plans. Our application should:
- Trigger a third party payment system to perform respective actions (e.g., subscription plan upgrade) if conditions defined by us are met. 
- Send notifications to customers using a third party system if they exceed the plan limits of their subscription plan or when they are nearing the limits of a subscription plan.
- Send a message to our internal customer relationship management (CRM) system to add a record that an email has been sent to the customer with a timestamp. 

To achieve this, we need to integrate two third party systems (email notification system and party payments system) and one internal system (CRM) with our platform.

## Structure

On the Trusted Twin platform, we use the digital twin model. It allows us to represent real and abstract objects. For example, we can use Twin objects to represent:
- each of the customer accounts (payment entities) => **Customer Twin**
- the billing, usage and storage data for each of the Customer Twins => **Billing Twin**
- subscription plans available on the Trusted Twin platform => **Subscription Plan Twin**

A Twin on the platform has a Ledger that stores information about the state of the Twin (i.e., usage and storage). A Ledger can have multiple Entries. For example, the Ledger Entries can contain:
- Customer Twin entries: information about the customer such as contact address, account creation time and the current subscription plan.
- Billing Twin entries: usage and storage data. The data is updated on a regular basis.
- Subscription Plan Twin entries: each of the Entries stores the details of a subscription plan available on the platform.

We can use the reference feature to create links between different Entries so that one Entry can reference the other Entry’s value. We can use these links to apply a logic that will trigger notification events in later steps. For example, we can create references between:
- Entries Access rules of the Billing Twin to reference the updated usage data to Entries of the Customer Twin.
- Entries of the Subscription Twin to reference the details of a subscription plan to the Customer Twin.
Read more: [Object linking](https://trustedtwin.com/academy/object-linking-concept/)

We can to use the rules feature to compare the values of the Entries and define the conditions that trigger notification events and publish notification messages to a topic. Such conditions defined through rules could be:
- The usage (referenced from the Billing Twin) is equal or greater to what is defined in the subscription plan.
- The value of the subscription plan field in the Customer Twin has changed, i.e. the subscription plan for a customer account has changed.
Read more: [Rules](https://trustedtwin.com/academy/access-rules-concept/)

If a rule resolves to `True`, a notification event is triggered and an HTTP notification message is published to a topic. In our example, we have created two topics:
- `"subscription_plan_exceeded"`,
- `"subscription_plan_changed"`.

## Flow
We can have several stateless lambdas in our system. The lambda logic for each lambda can be defined in a different technology, regardless of what is used in other lambdas. The lambdas work asynchronously. 

### Cron lambda
For the sake of our example, we are going to look at the payments lambda and the email notifications lambda in our system. The lambdas subscribe to the topics we created. There is also a separate lambda that refreshes the subscriptions periodically.

### Payment lambda
The payment lambda subscribes to the `"subscription_plan_exceeded"` topic we created. If the rule we defined resolves to `True` (i.e, the usage in a customer account is equal to or exceeds the usage defined in the given subscription plan), the lambda receives a notification message. This triggers the following actions:
1. The lambda sends a POST request to the third party payments system to perform an action. In our example it is the upgrade of the subscription plan.<br/>
2. The third party payments system performs the defined action. In our example, it upgrades the customer account to a higher subscription plan.<br/>
3. The third party payments system webhook sends back a POST request with information about the changes it performed, i.e., that it has upgraded the payment plan.<br/>
4. Upon receiving the information, the payment lambda sends a POST request to the Customer Twin Ledger to change the subscription plan for the customer in the relevant field.

### Email notification lambda
Another stateless lambda in our system is the third party email notification lambda. It subscribes to the `"subscription_plan_changed"` topic we created.  If the rule we defined resolves to `True` (i.e, the subscription plan has changed), the lambda receives a notification message. This triggers the following actions:<br/>
1. The lambda sends a POST request to the third party email notification system to perform an action. In our example case it sends an email to the customer informing them of the change to the subscription plan. <br/>
2. The third party email notification system performs the defined action. In our example, it sends the email to the customer.<br/>
3. The third party email notification system webhook sends back a POST request with information about the changes it performed, i.e., that an email about changes to the subscription plan has changed.<br/>
4. Upon receiving the information, the email notification lambda sends a POST request to the Customer Twin Ledger to add an Entry to indicate that a message of this type has been sent to the customer with a timestamp.


