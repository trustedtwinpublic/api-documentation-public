---
tags:
  - tool
  - tools
  - usage
---

# Usage monitor

## About

The usage monitor for Python allows you to monitor account and user [usage](../reference/usage/index.md).

## Requirements

The Usage monitor program requires:

Python 3.6 or above.
- The `trustedtwin` library (see [Trusted Twin Python library](../libraries/library-python.md)).
- The `requests` library.
- The `curses` Python module.
- For Unix-based OS: curses system library, e.g. `ncurses`.
- For Windows: additional Python library: `windows-curses`.

## Installation

To install the usage monitor program, download the `usage_monitor.py`, `usage.py`, and `usage_monitor.json` files from the [Trusted Twin Tools - Usage monitor](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Usage%20monitor) repository. Next, add the name of the `usage_monitor.py` file and your [User Secret (API key)](../reference/secrets/index.md) to install and run the program:

<ExtendedCodeGroup title="Usage monitor installation" copyable>
<CodeBlock title="python">
```python
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python usage_monitor.py $USER_SECRET
```
</CodeBlock>
</ExtendedCodeGroup>

## Example

<TableWrapper>
| [ACCOUNT: 1a303f70-43f9-4e5b-b790-be27b00474b3] |     |                                   |
|:------------------------------------------------|----:|----------------------------------:|
| **requests**                                    |     |                                   |
| request_count                                   |     |                                 1 |
| **tasks**                                       |     |                                   |
| task_history_append_count                       |   1 |                                69 |
| **system**                                      |     |                                   |
| task_queue_count                                |     |                                 1 |
| result_io_count                                 |     |                                 2 |
| **time**                                        |     |                                   |
| result_io_time                                  |     |                             0.002 |
| lambda_time                                     |     |                             0.003 |
|                                                 |     | Change: Fri Sept 16 14:54:34 2022 |
</TableWrapper>


<TableWrapper>
| [USER: 02f5f248-d35f-4a2c-b58f-5f123823065b] |        |                                   |
|:---------------------------------------------|-------:|----------------------------------:|
| **db**                                       |        |                                   |
| odb_write_cost                               |      2 |                               138 |
| hist_db_write_cost                           |      2 |                               138 |
| ts_db_read_cost                              |        |                                30 |
| ts_db_write_cost                             |        |                                 1 |
| ix_db_read_cost                              |        |                                 2 |
| ix_db_write_cost                             |        |                                 1 |
| **requests**                                 |        |                                   |
| rest_count                                   |     17 |                               863 |
| request_count                                |     17 |                               864 | 
| **tasks**                                    |        |                                   |
| task_whom_am_i_count                         |        |                                 1 |
| task_get_twin_ledger_entry_count             |      1 |                                68 |
| task_update_twin_ledger_entry_count          |      1 |                                69 |
| task_get_log_count                           |     13 |                               584 |
| task_get_account_usage_count                 |      1 |                                68 |
| task_get_user_usage_count                    |      1 |                                68 |
| task_create_indexes_table_count              |        |                                 2 |
| task_create_timeseries_table_count           |        |                                 3 |
| task_get_timeseries_table_count              |        |                                 1 |
| **storage**                                  |        |                                   |
| ledger_size_delta                            |        |                                 0 |
| **system**                                   |        |                                   |
| task_queue_count                             |      2 |                               143 |
| result_io_count                              |     34 |                             2,108 |
| log_io_count                                 |     13 |                               584 |
| **cache**                                    |        |                                   |
| l1_hit_count                                 |      2 |                               138 |
| l2_hit_count                                 |     61 |                             3,279 |
| **time**                                     |        |                                   |
| odb_io_time                                  |  0.012 |                             0.755 |
| l2_io_time                                   |  0.066 |                             3.009 |
| result_io_time                               |  0.035 |                             2.384 |
| log_io_time                                  |  0.015 |                             0.777 |
| lambda_time                                  |  0.310 |                            18.363 |
| **transfer**                                 |        |                                   |
| rest_outbound_traffic_delta                  | 21,690 |                         1,153,234 |
|                                              |        | Change: Fri Sept 16 14:54:34 2022 |
</TableWrapper>

## Resources

### Usage records

For available usage records with descriptions, consult the [Usage - Overview](../reference/usage/overview.md) section.

### Usage monitor repository

You can access the program in the [Trusted Twin Tools - Usage monitor](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Usage%20monitor) repository.
