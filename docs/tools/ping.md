---
tags:
  - tool
  - tools
---

# Ping

## About

The Ping program establishes the minimum and maximum processing times of the [get_twin](../reference/twins/get-twin.md) and [update_twin](../reference/twins/update-twin.md) operations as well as the processing time of the last operation through periodically performing the [get_twin](../reference/twins/get-twin.md) and [update_twin](../reference/twins/update-twin.md) operations. The requests are performed in a separate Twin created for the purpose of establishing the processing times of the operations.

## Requirements

The ping program requires:
- Python 3.6 or above.
- The `requests` library.

## Installation

To install the Ping program, download the `ping.py` file from the [Trusted Twin Tools - Ping](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Ping) repository. Next, add the name of the file and your [User Secret (API key)](../reference/secrets/index.md) to install and run the program:

<ExtendedCodeGroup title="Ping installation" copyable>
<CodeBlock title="python">
```python
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python ping.py $USER_SECRET
```
</CodeBlock>
</ExtendedCodeGroup>

## Optional arguments

It is possible to use additional, optional arguments.

<TableWrapper>
| Optional argument | Description                            | 
|:------------------|:---------------------------------------|
| `-u` or `--url`   | Uses the URL provided in the argument. |
</TableWrapper>

## Resources 

### Ping repository

You can access the program in the [Trusted Twin Tools - Ping](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Ping) repository.
