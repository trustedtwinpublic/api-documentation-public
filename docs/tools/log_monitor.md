---
tags:
  - tool
  - tools
---

# Log monitor

## About

The log monitor for Python is a program that allows you to gain direct access to the log content as it is recorded.

## Requirements

The Log monitor program requires:
- Python 3.6 or above.
- The `trustedtwin` library (see [Trusted Twin Python library](../libraries/library-python.md)).

## Installation

To install the log monitor program, download the `log_monitor.py` file from the [Trusted Twin Tools - Log monitor](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Log%20monitor) repository. Next, add the name of the file and your [User Secret (API key)](../reference/secrets/index.md) to install and run the program:

<ExtendedCodeGroup title="Log monitor installation" copyable>
<CodeBlock title="python">
```python
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python log_monitor.py $USER_SECRET
```
</CodeBlock>
</ExtendedCodeGroup>

## Optional arguments

It is possible to use additional, optional arguments.

<TableWrapper>
| Optional argument | Description                            | 
|:------------------|:---------------------------------------|
| `-u` or `--url`   | Uses the URL provided in the argument. |
| `-f` or `--file`  | Adds the logs to the file indicated.   | 
</TableWrapper>

## Resources

### Log messages

Consult the [Log messages](../overview/log-messages.md) section for a list of log messages with additional information regarding troubleshooting.

### Log monitor repository

You can access the program in the [Trusted Twin Tools - Log monitor](https://gitlab.com/trustedtwinpublic/trusted-twin-tools/-/tree/main/Log%20monitor) repository.




