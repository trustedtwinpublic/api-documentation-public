import OutboundLink from './theme/components/OutboundLink.vue'

export default ({ Vue }) => {
  Vue.component('OutboundLink', OutboundLink);
}
