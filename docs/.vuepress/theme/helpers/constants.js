const DOCS_NAMESPACE = 'trustedtwindocs';

export const ENV_SWITCHED_STORAGE_KEY = `${DOCS_NAMESPACE}/env`;
export const ENV_SWITCHED_EVENT = `${DOCS_NAMESPACE}envswitched`;

export const DARKMODE_STORAGE_KEY = `${DOCS_NAMESPACE}/dark-mode`;
