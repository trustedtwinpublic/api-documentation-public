---
tags:
  - object graphs
  - python
  - tutorial
  - tutorials
  - object linking
  - meter
  - water meter
---

# Create a digital twin of a meter [Python]

## Introduction

### About

In this tutorial, you are going to create a digital twin representing a meter. The digital twin will consist of a Twin, a Ledger, and Identities. <br/>
- You are going to set up your environment and create a free Trusted Twin account. With the User Secret (API key) provided upon creation of the account, you are going to create a new role, a new user and a new User Secret (API key) that you will use in this tutorial.<br/>
- You are going to create the objects of the digital twin of the meter one by one. <br/>
- You are going to combine these steps to a convenient method to create a digital twin of a meter.<br/>
- You are going to learn how to store and read measurements, retrieve measurement history, and change meter location. In case you don't have real meters that can provide measurements, we prepared test data based on open weather data from Gdańsk in Poland. <br/>
- You are going to use the reference feature to connect meters.<br/>

### Resources

::: tip TUTORIAL JUPYTER NOTEBOOK
<Icon name="info-circle" :size="1.75" />
This tutorial is available as a standalone Jupyter notebook. You can find it in the Trusted Twin public repository in the [Trusted Twin Tutorial Resources](https://gitlab.com/trustedtwinpublic/tutorial-resources) section.
:::

### Requirements

This tutorial requires:
- Python 3.6 or above.
- The [Trusted Twin Python library](../libraries/library-python.md).
- The `json` library.

### Install the Trusted Twin library

As first step, install the [Trusted Twin Python library](../libraries/library-python.md) using the pip package manager for Python.

<ExtendedCodeGroup title="INSTALL TRUSTED TWIN LIBRARY" copyable>
<CodeBlock title="bash">

```bash
pip install trustedtwin
```
</CodeBlock>
</ExtendedCodeGroup>

### Set up your environment

Next, set up your environment.

<ExtendedCodeGroup title="SET UP YOUR ENVIRONMENT" copyable>
<CodeBlock title="Python">

```python
import json

from trustedtwin.tt_api import TTRESTService
```
</CodeBlock>
</ExtendedCodeGroup>

### Get a Super Admin User Secret

On the Trusted Twin platform [User authentication](../account-and-access/authentication.md) is based on [User Secrets (API keys)](../reference/secrets/index.md).

In order to obtain a Super Admin User Secret (API key):
1. Go to the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/) and follow the instructions to [create an account](../account-and-access/account-creation.md).
2. Once you have created an account, a Super Admin User Secret (API key) for the Super Admin user of the account you created will be displayed.    

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
The User Secret (API key) is displayed **only once**. It is not stored in the Trusted Twin system, and it **cannot be retrieved**. Keep your User Secret stored in a secure place. If you lose your User Secret, the User Secret generation process will need to be started from the beginning.
:::

### Authenticate requests

Once you have your Super Admin User Secret, you can use it to authenticate requests.

<ExtendedCodeGroup title="AUTHENTICATE" copyable>
<CodeBlock title="Python">

```python
ADMIN_SECRET = "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"

ADMIN_TT_SERVICE = TTRESTService(tt_auth=ADMIN_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>


### Get account, user and role

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET    | /whoami | [who_am_i](../reference/whoami/who-am-i.md) |
| GET  | /roles/{role} |[get_user-role](../reference/roles/get-user-role.md) |
</EndpointsTable>

</div>
<div class="column">


</div>
</div>

When you create a Super Admin User Secret, a Super Admin Role and a Super Admin role are created automatically. Let's retrieve the UUIDs the account, user, and role that the Super Admin User Secret is associated with as well as the details of the Super Admin Role.

<ExtendedCodeGroup title="DISPLAY ROLE AND USER DETAILS" copyable>
<CodeBlock title="Python">

```python
print("... reading information about the users ...")
status, response = ADMIN_TT_SERVICE.who_am_i()
resp_whoami = json.loads(response)

_ACCOUNT = resp_whoami['account']
_USER = resp_whoami['user']
_ROLE = resp_whoami['role']

print("... reading information about the users permissions ...")
status, response = ADMIN_TT_SERVICE.get_user_role(_ROLE)
resp_role = json.loads(response)

_ROLE_NAME = resp_role['name']
_RULES = resp_role['rules']
_STATEMENT = resp_role['statement']

print()
print("ACCOUNT: \t[{}]\nUSER: \t\t[{}]\nROLE: \t\t[{}]".format(_ACCOUNT, _USER, _ROLE))
print("ROLE NAME:\t{}\nRULES: \t\t{}\nSTATEMENT: \t{}".format(_ROLE_NAME, _RULES, _STATEMENT))
print()
```
</CodeBlock>
</ExtendedCodeGroup>

The response to the `who_am_i` endpoint returns information associated with the User Secret (API key) - the account UUID, the user UUID, and the role UUID.<br/>

The `get_user_role` response returns the name of the role, information about the user's access to resources (Twins and Ledger Entries, in addition to the ones belonging to the account where the role was created), and a role statement defining the actions a user can perform on the resources.

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
A Super Admin Role allows the user to perform all actions available on the Trusted Twin platform. Also, when new actions related to new features are introduced, they are added by default to the Super Admin role.<br/>We highly recommend that you create roles for users with permissions that allow them to only perform actions required for their position in the organisation and on resources that they need access to.
:::

As next steps we are going to create a new role, user and generate a User Secret (API key) for this user. Once we have generated it, we are going to authenticate with this User Secret (API key) for the operations that we are going to perform in this tutorial.


### Create a role

Let's create a role to perform operations on the Trusted Twin platform.

A role is a collection of permissions:
- you can `"allow"` or `"deny"` `"actions"` that correspond with endpoint operations,
- you can grant permission to resources - specific `"twin"`, `"identity"` or `"ledger"` objects.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /roles | [create_user_role](../reference/roles/create-user-role.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

You are going to create a role with permissions to perform actions required for this tutorial (see `"actions"` in the request body below) and allow operations only on Twins that have an "application" field in the `"description"` where the value is set to `"Trusted Twin DEMO App"` (`"twin": "TWIN.application == 'Trusted Twin DEMO App'"`). Please note that the `"create"`, `"update"`, and `"delete"` actions related to operations on roles, users, and User Secrets should not be included in the list of `"actions`". It is because the role you are going to create should not be used for user management purposes.


<ExtendedCodeGroup title="CREATE ROLE" copyable>
<CodeBlock title="Python">

```python
_ROLE_BODY = {
    "name": "TT DEMO User Role",
    "rules": {
        "twins": "TWIN.application == 'Trusted Twin DEMO App'"
    },
    "statement": {
        "effect": "allow",
        "actions": [
           "who_am_i", "get_user_role", "get_user", "get_user_secret", "get_twin_ledger_entry_value", "create_twin", "get_twin", "update_twin", "terminate_twin", "create_twin_identity", "get_twin_identity", "update_twin_identity",  "resolve_twin_identity", "delete_twin_identity", "add_twin_ledger_entry", "get_twin_ledger_entry", "update_twin_ledger_entry", "delete_twin_ledger_entry", "get_twin_ledger_entry_history", "get_log"
        ]
    }
}

status, response = ADMIN_TT_SERVICE.create_user_role(body=_ROLE_BODY)
resp_role = json.loads(response)

_ROLE = resp_role["uuid"]

print("New Role: {}\n".format(resp_role))
```
</CodeBlock>
</ExtendedCodeGroup>


### Create a user

After creating a role, let's create a user with the above role.

A user is a person who utilizes the Trusted Twin platform. The permissions of a user to access, create, and manipulate Trusted Twin resources are controlled by the role assigned to the user.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users | [create_user](..reference/users/create-user.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CREATE USER" copyable>
<CodeBlock title="Python">

```python
_USER_BODY = {
    "name": "TT DEMO User",
    "role": _ROLE,
    "description": {
        "purpose": "Playing with Trusted Twin APIs"
    }
}

status, response = ADMIN_TT_SERVICE.create_user(body=_USER_BODY)
resp_user = json.loads(response)

_USER = resp_user['uuid']

print("New User: {}\n".format(resp_user))
```
</CodeBlock>
</ExtendedCodeGroup>


### Create a User Secret PIN

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users/{user}/secrets | [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

A user needs a User Secret (API key) to authenticate on the Trusted Twin platform.

Let's create a User Secret PIN. It will be required to create a User Secret (API key) in the next step. The User Secret PIN can be passed to a third party so that they can create a User Secret for themselves.

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
A User Secret PIN is valid for 10 minutes, and it can be used only once to generate a User Secret (API key). After a User Secret (API key) has been generated, the User Secret PIN automatically expires.
:::

<ExtendedCodeGroup title="CREATE USER SECRET PIN" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret_pin(_USER)
resp_pin = json.loads(response)

_PIN = resp_pin["pin"]

print("New PIN: {}\n".format(resp_pin))
```
</CodeBlock>
</ExtendedCodeGroup>


### Create a User Secret (API key)

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /secrets/{account}/{pin} | [create_user_secret](../reference/secrets/create-user-secret.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Once you have created a User Secret PIN, you can use it to generate a User Secret.

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
Please note that the User Secret is displayed only once. It is not stored in the Trusted Twin system, and it cannot be retrieved. Keep your User Secret stored in a secure place. If you lose your User Secret, you will need to start the User Secret generation process from the beginning.
:::


<ExtendedCodeGroup title="CREATE USER SECRET" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret(_ACCOUNT, _PIN)
resp_secret = json.loads(response)

_SECRET = resp_secret["secret"]

print("New User Secret: {}\n".format(resp_secret))
```
</CodeBlock>
</ExtendedCodeGroup>

The result is a User Secret (API key) that we will use throughout this tutorial. We are going to use it to authenticate requests.

<ExtendedCodeGroup title="USE USER SECRET TO AUTHENTICATE REQUESTS" copyable>
<CodeBlock title="Python">

```python
TT_SERVICE = TTRESTService(tt_auth=_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>

## Digital twin

### Create a Twin

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins | [create_twin](../reference/twins/create-twin.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

You are going to create a digital twin that will represent a water meter. The digital twin will consist of a Twin, a Ledger and Identities. First, you are going to create a Twin of a meter. A Twin is an object on the Trusted Twin platform that ties together all knowledge in the Digital Twin.

A Twin can have an optional description field. A description of a Twin is always publicly accessible. This means that anyone who knows the unique identifier (UUID) of the Twin will be able to view the description of the Twin. The Twin you are going to create will have a description field with information about the application name, object type, serial number, and website.

Please note that the description below contains a placeholder (ADD_YOUR_METER_SERIAL_HERE). You need to add a serial number for the meter in place of the placeholder. You can insert any serial number that works for you. In our example, the `"serial"` is `"12345"`.

<ExtendedCodeGroup title="METER DESCRIPTION" copyable>
<CodeBlock title="Python">

```python
_METER_DESCRIPTION_BODY = {
    "description": {
        "application": "Trusted Twin DEMO App",
        "object": "generic meter",
        "serial": "12334",
            "site": "https://trustedtwin.com/docs/tutorials/python-meter-tutorial.html"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

<ExtendedCodeGroup title="CREATE A TWIN" copyable>
<CodeBlock title="Python">

```python
print("... creating meter twins ...")
status, response = TT_SERVICE.create_twin(body=_METER_DESCRIPTION_BODY)
resp_twin = json.loads(response)

twin = resp_twin['creation_certificate']['uuid']
serial =  resp_twin['description']['serial']

print(resp_twin)
```
</CodeBlock>
</ExtendedCodeGroup>

As you can see in the response, you are the `"owner"` and `"creator"` of the Twin. The owner of a Twin can change the description of the Twin by sending a request to the update_twin endpoint if the status of the Twin is `"alive"`.


### Add Ledger Entries

Let's add Entries to the Twin's Ledger. A Ledger is an object that stores information about the state of a Twin. A Ledger contains Entries that are user-defined key-value pairs.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/ledgers/personal | [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

The Entries of your Ledger are going to contain the following information about the state of the Twin:

- current meter reading (`"reading"` Entry),
- battery level (`"battery"` Entry),
- log of errors raised through the meter software (`"log"` Entry),
- location of where the water meter is installed (`"location"` Entry).

<ExtendedCodeGroup title="ADD LEDGER ENTRIES" copyable>
<CodeBlock title="Python">

```python
_METER_LEDGER_BODY = {
    "entries": {
        "reading": {
            "value": 0.0,
            "history": "1M"
        },
        "battery": {
            "value": 1.0,
            "visibility": "account == LEDGER.service_order.contractor"
        },
        "log": {
            "value": {
                "level": "INFO",
                "message": "Meter digital twins created and activated!"
            }
        },
        "location": {
            "value": {
                "city": "",
                "street": ""
        },
        "visibility": "account == LEDGER.service_order.contractor"
        }
    }
}

print("... creating meter ledgers ...")
status, response = TT_SERVICE.add_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
resp_ledger = json.loads(response)

print(resp_ledger)
```
</CodeBlock>
</ExtendedCodeGroup>

Please note that in the above `"reading"` Entry, we used the `"visibility"` and `"history"` attributes:

- The `"visibility"` attribute defines the visibility of the data in the Entry for other accounts.
- The `"history"` attribute defines the time period for which the history of value changes is stored (in our case it is set to 1 month). The History service lets you store historical values of an Entry of a Ledger.<br/>
You will use these fields later on in this tutorial.

You can execute the above code multiple times to observe the API response for when a given Entry has been already added to the Ledger.

### Create an Identity

Let's create an Identity to identify the meter. Although Twins have system-defined unique IDs (UUIDs), you might need to use user-generated IDs specific to your business to identify Twin. You can use Identities for this purpose.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/identities | [create_twin_identity](../reference/identities/create-twin-identity.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

The Identity that you are going to create is going to be a combination of the unique identifier of the account (account UUID) and the serial number of the meter.

<ExtendedCodeGroup title="METER IDENTITY" copyable>
<CodeBlock title="Python">

```python
_METER_IDENTITY_BODY = {
    "identities": {
        "'METER#{}_{}'.format(_ACCOUNT, serial)": {}
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>


<ExtendedCodeGroup title="CREATE METER IDENTITY" copyable>
<CodeBlock title="Python">

```python
print("... creating meter identities ...")
status, response = TT_SERVICE.create_twin_identity(twin, body=_METER_IDENTITY_BODY)
resp_identity = json.loads(response)

print(resp_identity)
```
</CodeBlock>
</ExtendedCodeGroup>

The `"visibility"` attribute is set to null. It means that the Identity is private and not visible to other accounts. The `"validity_ts"` is set to null. It means that the Identity does not expire.

Your first digital twin representing a meter is ready!

### Digital twin

Let's connect all the above steps into a convenient `create_meter()` method that handles all the above operations and create a digital twin.

<ExtendedCodeGroup title="CREATE METER DIGITAL TWIN" copyable>
<CodeBlock title="Python">

```python
def create_meter(serial: str):
    """Creates a meter with a given serial number."""
    
    _METER_IDENTITY = 'METER#{}_{}'.format(_ACCOUNT, serial)
    
    _METER_IDENTITY_BODY = {
        "identities": {
            "'METER#{}_{}'.format(_ACCOUNT, serial)": {}
        }
    }
    
    _METER_DESCRIPTION_BODY = {
        "description": {
            "application": "Trusted Twin DEMO App",
            "object": "generic meter",
            "serial": serial,
            "site": "https://trustedtwin.com/docs/tutorials/python-meter-tutorial.html"
        }
    }
    
    _METER_LEDGER_BODY = {
        "entries": {
            "reading": {
            "value": 0.0,
            "history": "1M"
            },
            "battery": {
                "value": 1.0,
                "visibility": "account == LEDGER.service_order.contractor"
            },
            "log": {
                "value": {
                    "level": "INFO",
                    "message": "Meter digital twins created and activated!"
                }
            },
            "location": {
                "value": {
                    "city": "",
                    "street": ""
                },
                "visibility": "account == LEDGER.service_order.contractor"
            }
        }
    }
    
    # Check if meter already exists
    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if resp_resolve['twins']:
        return {}, {}, {}, resp_resolve['twins']
    
    # Create a new meter Twin
    print("... creating meter twins ...")
    status, response = TT_SERVICE.create_twin(body=_METER_DESCRIPTION_BODY)
    resp_twin = json.loads(response)
    
    twin = resp_twin['creation_certificate']['uuid']
    
    # Create a new private Identity for the meter
    print("... creating meter identities ...")
    status, response = TT_SERVICE.create_twin_identity(twin, body=_METER_IDENTITY_BODY)
    resp_identity = json.loads(response)
    
    # Create a Ledger for the meter
    print("... creating meter ledgers ...")
    status, response = TT_SERVICE.add_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
    resp_ledger = json.loads(response)
        
    return resp_twin, resp_identity, resp_ledger, []

meter_twin, meter_identity, meter_ledger, check = create_meter("160")

if check:
    print("Meter already exists: {}\n".format(check))
else:    
    print("Meter Twin: {}\n".format(json.dumps(meter_twin, indent=4)))
    print("Meter Identity: {}\n".format(json.dumps(meter_identity, indent=4)))
    print("Meter Ledger: {}\n".format(json.dumps(meter_ledger, indent=4)))
    
create_meter("200")
```
</CodeBlock>
</ExtendedCodeGroup>

## Store and read measurements, retrieve history, change location

Below you will find methods that let you:

- store meter measurements: `store_measurement()`,
- read current meter measurements: `read_measurement()`,
- read the history of meter measurements: `read_history()`,
- store the location of the meter: `set_location()`.

Each time a new meter reading is available, your meter can call the `store_measurement()` method. Your application can read that measurement by calling the `read_measurement()` method.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /resolve/{identity} | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)|
| PATCH | /twins/{twin}/ledgers/personal | [update_twin_ledger_entry](../reference/ledgers/update-twin-ledger-entry.md) |
| GET | /twins/{twin}/ledgers/personal | [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) |
| GET | /twins/{twin}/ledgers/personal/history | [get_twin_ledger_entry_history](../reference/history/get-twin-ledger-entry-history.md) |
</EndpointsTable>

</div>
<div class="column">


</div>
</div>

In the previous section of this tutorial, the `"reading"` Entry was configured to store `"history"` of changes. This way, the historical meter readings will be stored for the time period defined in the `"history"` attribute. You can call the `read_history()` method to read the history of these changes.

You will notice that each time you call a method, the Identity of the meter is resolved to a Twin UUID ([resolve_twin_identity](../reference/identities/resolve-twin-identity.md) endpoint). This can easily be cached as the meter serial number is not going to change.

Let's test the flow of the measurement data!

<ExtendedCodeGroup title="STORE AND READ METER MEASUREMENTS, RETRIEVE HISTORY, CHANGE LOCATION" copyable>
<CodeBlock title="Python">

```python
def store_measurement(serial: str, reading: float):
    """Stores new measurement"""
    
    _METER_IDENTITY = 'METER#{}_{}'.format(_ACCOUNT, serial)
    
    _METER_LEDGER_BODY = {
        "entries": {
            "reading": {
                "value": reading
            }
        }
    }

    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if not resp_resolve['twins']:
        return {}
    
    twin = resp_resolve['twins'][0]
    
    print("... updating meter ledgers ...")
    status, response = TT_SERVICE.update_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
    resp_update = json.loads(response)
    
    return resp_update
    
def read_measurement(serial: str):
    """Reads measurement"""
    
    _METER_IDENTITY = 'METER#{}_{}'.format(_ACCOUNT, serial)

    # Resolves meter identities
    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if not resp_resolve['twins']:
        return {}
    
    twin = resp_resolve['twins'][0]
    
    print("... reading meter ledgers ...")
    status, response = TT_SERVICE.get_twin_ledger_entry(twin, "personal", params={"entries": ["reading"]})
    resp_get = json.loads(response)
    
    return resp_get

def read_history(serial: str):
    """Reads measurement history"""
    
    _METER_IDENTITY = 'METER#{}_{}'.format(_ACCOUNT, serial)
    
    # Resolves meter identities
    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if not resp_resolve['twins']:
        return {}
    
    twin = resp_resolve['twins'][0]
    
    print("... reading meter measurement history ...")
    status, response = TT_SERVICE.get_twin_ledger_entry_history(twin, "personal", params={"entries": ["reading"]})
    resp_history = json.loads(response)
    
    return resp_history

def set_location(serial: str, city: str, street: str):
    """Stores location of the meter"""

    _METER_IDENTITY = "METER#{}_{}".format(_ACCOUNT, serial)
    
    _METER_LEDGER_BODY = {
        "entries": {
            "location": {
                "value": {
                    "city": city,
                    "street": street
                }
            }
        }
    }

    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if not resp_resolve['twins']:
        return {}
    
    twin = resp_resolve['twins'][0]
    
    print("... updating meter ledgers ...")
    status, response = TT_SERVICE.update_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
    resp_update = json.loads(response)
    
    return resp_update

before_update = read_measurement("YOUR_SERIAL")
after_update = store_measurement("YOUR_SERIAL", 13.34)
history_response = read_history("YOUR_SERIAL")

location_response = set_location("YOUR_SERIAL", "YOUR_CITY", "YOUR_STREET")

print()

if not before_update:
    print("Meter does not exist")
else:
    print("Before update: {}".format(json.dumps(before_update, indent=4)))
    print("After update: {}".format(json.dumps(after_update, indent=4)))
    print("Reading history: {}".format(json.dumps(history_response, indent=4)))
    
if not location_response:
    print("Meter does not exist")
else:
    print("Meter location: {}".format(json.dumps(location_response, indent=4)))
```
</CodeBlock>
</ExtendedCodeGroup>

::: tip ADVANCED DATABASE SERVICES
<Icon name="info-circle" :size="1.75" />
The Trusted Twin platform also offers advanced services to store and handle historical data and state of objects - [Timeseries](../reference/timeseries/index.md) and [Indexes](../reference/indexes/index.md).
:::

## Use open weather data

### About

In case you don't have real meters that can provide measurements for your test, we prepared test data for you based on open weather data from Gdańsk in Poland.

We created one digital twin of all the weather stations that provides information about temperature on an hourly basis. The Ledger was created by us, so it is owned the account with the below account UUID.

<ExtendedCodeGroup title="DEMO ACCOUNT UUID" copyable>
<CodeBlock title="Python">

```python
_TT_DEMO_ACCOUNT = "574a9445-267d-4d4d-946d-9313b137fcd8"
```
</CodeBlock>
</ExtendedCodeGroup>

Please note that you can access data stored in the Ledger Entries, although you belong to a different account.

Entries in a Ledger are visible by default to all users belonging to that account. However, you can set up the `"visibility"` of an Entry so that it is also visible to users belonging to other accounts (foreign accounts). To make it possible, the visibility rule needs to resolve to true for a user of a foreign account. If you set the `"visibility": "true"`, the Entry will be visible to users belonging to foreign accounts.

In the Ledger below, the Entry `"meters"` is a list of all available weather stations with their real IDs (e.g., 307) and serial numbers attached to them (e.g., `sn_0000000`). All other Entries are temperature readings. All Entries have visibility rules set to `"true"`. This means that the Entries are visible to everyone as the rule in each of the Entries always resolves to true.

### Using public Identity

We have also created a public Identity that is visible for everyone (visibility rule is set to `"true"`).

<ExtendedCodeGroup title="DEMO METER IDENTITY" copyable>
<CodeBlock title="Python">

```python
_TT_DEMO_DATA_ID = "DEMO#TT_METER_DEMO___LIVING_MEASUREMENTS"
```
</CodeBlock>
</ExtendedCodeGroup>

### Resolve public Identity and read measurements

To find out the UUID of the weather data Twin, you need to resolve the Identity. You have to set a context in which the Identity should be resolved (the account `_TT_DEMO_ACCOUNT`), because you are not the owner of that Identity.



<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /resolve/{identity} | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)|
| GET | /twins/{twin}/ledgers/{ledger}/value | [get_twin_ledger_entry_value](../reference/ledgers/get-twin-ledger-entry-value.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

There should be only one Twin UUID on the list after resolving the Identity.

<ExtendedCodeGroup title="RESOLVE TWIN IDENTITY" copyable>
<CodeBlock title="Python">

```python
print("... resolving identities ...", "\n")
status, response = TT_SERVICE.resolve_twin_identity(_TT_DEMO_DATA_ID, params={"context": _TT_DEMO_ACCOUNT})
resp_resolve = json.loads(response)

print(resp_resolve)
```
</CodeBlock>
</ExtendedCodeGroup>

Let's store the Twin UUID as `_TT_DEMO_DATA_TWIN` for future use.

<ExtendedCodeGroup title="STORE TWIN UUID" copyable>
<CodeBlock title="Python">

```python
_TT_DEMO_DATA_TWIN = resp_resolve['twins'][0]
```
</CodeBlock>
</ExtendedCodeGroup>

You can also check a single measurements with the timestamp of the last update of the measurement.

<ExtendedCodeGroup title="CHECK SINGLE MEASUREMENT" copyable>
<CodeBlock title="Python">

```python
print("... retrieving single ledgers entries of demo weather twins ...", "\n")
status, response = TT_SERVICE.get_twin_ledger_entry_value(_TT_DEMO_DATA_TWIN, _TT_DEMO_ACCOUNT, params={"entries": ["sn_0000000", "updated_ts"]})
resp_demo_data = json.loads(response)

print(resp_demo_data)
```
</CodeBlock>
</ExtendedCodeGroup>

## Use references to connect meters

### About

The Trusted Twin platform offers a functionality allowing you to reference any visible Entry from a different Ledger. This Ledger could be your own Ledger attached to a different Twin, or any Ledger owned by a different account. 

Reference structure: `twin://{twin_uuid}/{account_uuid}/{entry_name}`<br/>
Reference example: `twin://e5dc9775-2aed-4eb6-9f4c-56960b8858f2/574a9445-267d-4d4d-946d-9313b137fcd8/sn_0000000`.

Let's create a reference to demo meters. We are going to reference an Entry in the Twin ` _TT_DEMO_DATA_TWIN` and `_TT_DEMO_ACCOUNT`. The Entry is equal to the ID of the meter (meter_id, in our example above it is `"sn_0000000"`).

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /resolve/{identity} | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)|
| DELETE | /twins/{twin}/ledgers/personal | [delete_twin_ledger_entry](../reference/ledgers/delete-twin-ledger-entry.md) |
| ADD | /twins/{twin}/ledgers/personal | [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CONNECT DEMO METERS" copyable>
<CodeBlock title="Python">

```python
# Using reference type entry to connect to demo meters

def create_reference(serial: str, meter_id: str):
    """Add reference to read value from the demo data"""
    _METER_IDENTITY = "METER#{}_{}".format(_ACCOUNT, serial)
    
    _METER_LEDGER_BODY = {
        "entries": {
            "reading": {
                "ref": {
                    "source": "twins://{}/{}/{}".format(_TT_DEMO_DATA_TWIN, _TT_DEMO_ACCOUNT, meter_id)
                },
                "history": "1M"
            }
        }
    }

    print("... resolving identities ...")
    status, response = TT_SERVICE.resolve_twin_identity(_METER_IDENTITY)
    resp_resolve = json.loads(response)
    
    if not resp_resolve['twins']:
        return {}
    
    twin = resp_resolve['twins'][0]
    
    print("... deleting value entry ...")
    status, response = TT_SERVICE.delete_twin_ledger_entry(twin, "personal", params={"entries": ["reading"]})
    
    print("... adding reference entry ...")
    status, add_entry_response = TT_SERVICE.add_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
    resp_add_entry = json.loads(response)
    
    return resp_add_entry

ref_response = create_reference("YOUR_SERIAL", "sn_0000000")

print()
print(json.dumps(ref_response, indent=4))
```
</CodeBlock>
</ExtendedCodeGroup>

As you can see, the source is pointing to the right value.

### Read measurements after update

Our next call to the `read_measurement()` method will show that the value was initiated as expected.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /resolve/{identity} | [resolve_twin_identity](../reference/identities/resolve-twin-identity.md)|
| GET | /twins/{twin}/ledgers/personal | [get_twin_ledger_entry](../reference/ledgers/get-twin-ledger-entry.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="READ MEASUREMENT AFTER UPDATE" copyable>
<CodeBlock title="Python">

```python
after_update = read_measurement("YOUR_SERIAL")

print()
print(json.dumps(after_update, indent=4))
```
</CodeBlock>
</ExtendedCodeGroup>



