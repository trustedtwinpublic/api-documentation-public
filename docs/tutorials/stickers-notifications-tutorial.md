---
tags:
  - workflow tags
  - stickers
  - notifications
  - python
  - tutorial
  - tutorials
---

# Stickers (workflow tags) and notifications [Python]

## Introduction

### About

 This tutorial will guide you through the [Stickers (workflow tags)](../reference/stickers/index.md) and [Notifications](../reference/notifications/index.md) functionalities.

You are going to use the Stickers (workflow tags) functionality to pass workflow items between two companies. The workflow items will be meter repairs and technical reviews. The two companies will be a company owning water meters and a contractor company taking care of meter repairs and technical reviews. 

In addition, you are going to set up notifications so that both companies receive notification messages triggered by the addition of a Sticker (the meter company submitting a request) and the removal of a Sticker (the contractor company confirming that the request is completed).

### Resources

::: tip TUTORIAL JUPYTER NOTEBOOK
<Icon name="info-circle" :size="1.75" />
This tutorial is available as a standalone Jupyter notebook. You can find it in the Trusted Twin public repository in the [Trusted Twin Tutorial Resources](https://gitlab.com/trustedtwinpublic/tutorial-resources) section.
:::

### Outcomes

- You are going to set up the environment and create a free Trusted Twin account. Using the User Secret (API key) provided upon creation of the account, you are going to create a role, user and User Secret (API key) that you will use in this tutorial.
- You are going to create a digital twin consisting of a Twin and a Ledger.   
- You are going to create a user and a role as well as a User Secret (API key) for the contractor company.
- You are going to use the Stickers functionality from two perspectives - the meter company's perspective and contractor company's perspective.
- You are going to set up notifications using the Notifications feature so that the contractor company receives a notification when a repair or technical review request is created by the meter company, and the meter company receives a notification once the repair or technical review is completed.

## Preparation

### Requirements

This tutorial requires:
- Python 3.6 or above.
- The [Trusted Twin Python library](../libraries/library-python.md).
- The `json` library.

### Install Trusted Twin library

As first step, install the [Trusted Twin Python library](../libraries/library-python.md) using the pip package manager for Python.

<ExtendedCodeGroup title="INSTALL TRUSTED TWIN LIBRARY" copyable>
<CodeBlock title="bash">

```bash
pip install trustedtwin
```
</CodeBlock>
</ExtendedCodeGroup>

### Set up your environment

Next, set up your environment.

<ExtendedCodeGroup title="SET UP YOUR ENVIRONMENT" copyable>
<CodeBlock title="Python">

```python
import json

from trustedtwin.tt_api import TTRESTService
```
</CodeBlock>
</ExtendedCodeGroup>

### Get Super Admin User Secret

On the Trusted Twin platform [user authentication](../account-and-access/authentication.md) is based on [User Secrets (API keys)](../reference/secrets/index.md).

In order to obtain a Super Admin User Secret (API key):
1. Go to the [Trusted Twin Self-care panel](https://selfcare.trustedtwin.com/login/) and follow the instructions to [create an account](../account-and-access/account-creation.md).
2. Once you have created an account, a Super Admin User Secret (API key) for the Super Admin user of the account you created will be displayed.    

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
The User Secret (API key) is displayed **only once**. It is not stored in the Trusted Twin system, and it **cannot be retrieved**. Keep your User Secret stored in a secure place. If you lose your User Secret, the User Secret generation process will need to be started from the beginning.
:::

### Authenticate requests

Once you have your Super Admin User Secret, you can use it to authenticate requests.

<ExtendedCodeGroup title="AUTHENTICATE" copyable>
<CodeBlock title="Python">

```python
ADMIN_SECRET = "VVNSC3bOfkVsVt/NsuQMr8VVJ+i0GTefQAcGjiu9V1TpqopWR75fC1W0pa10R3Gg"

ADMIN_TT_SERVICE = TTRESTService(tt_auth=ADMIN_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>

### Get account, user and role

When you create a Super Admin User Secret, a Super Admin user and a Super Admin role are created automatically. You can retrieve the details of your account, your Super Admin user and your Super admin role by calling the `"who_am_i"` endpoint. 

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET    | /whoami | [who_am_i](../reference/whoami/who-am-i.md) |
| GET  | /roles/{role} |[get_user-role](../reference/roles/get-user-role.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Let's retrieve the UUIDs the account, user, and role that the Super Admin User Secret is associated with as well as the details of the Super Admin Role.

<ExtendedCodeGroup title="DISPLAY ROLE AND USER DETAILS" copyable>
<CodeBlock title="Python">

```python
print("... reading information about the users ...")
status, response = ADMIN_TT_SERVICE.who_am_i()

resp_whoami = json.loads(response)

_ACCOUNT = resp_whoami['account']
_USER = resp_whoami['user']
_ROLE = resp_whoami['role']

print("... reading information about the users permissions ...")
status, response = ADMIN_TT_SERVICE.get_user_role(_ROLE)

resp_role = json.loads(response)

_ROLE_NAME = resp_role['name']
_RULES = resp_role['rules']
_STATEMENT = resp_role['statement']

print()
print("ACCOUNT: \t[{}]\nUSER: \t\t[{}]\nROLE: \t\t[{}]".format(_ACCOUNT, _USER, _ROLE))
print("ROLE NAME:\t{}\nRULES: \t\t{}\nSTATEMENT: \t{}".format(_ROLE_NAME, _RULES, _STATEMENT))
print()
```
</CodeBlock>
</ExtendedCodeGroup>

The response to the `who_am_i` endpoint returns information associated with the User Secret (API key) - the account UUID, the user UUID and the role UUID.<br/>

The `get_user_role` response  returns the name of the role, information about the user's access to resources and a role statement defining the actions a user can perform on the resources.

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
A Super Admin role allows the user to perform all actions available on the Trusted Twin platform. Also, when new actions related to new features are introduced, they are added by default to the Super Admin role. Therefore, we highly recommend that you create roles for users with permissions that allow them to only perform actions required for their position in the organization and on resources they need to access.
:::

The next step is to create a new role, user and generate a User Secret (API key) for this user. Once they have been generated, you will use this User Secret (API key) moving forward in this tutorial.

### Create meter company role

Let's create a new role to perform operations required for this tutorial.

A role is a collection of permissions:
- you can `"allow"` or `"deny"` `"actions"` that correspond with endpoint operations (see [List of available actions](../reference/roles/available-actions.md)),
- you can grant permission to resources - specific `"twin"`, `"identity"` or `"ledger"` objects.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /roles | [create_user_role](../reference/roles/create-user-role.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

You are going to create a role with permissions to perform actions required for this tutorial (see `"actions"` in the request body below). Please note that the `"create"`, `"update"`, and `"delete"` actions related to operations on roles, users, and User Secrets should not be included in the list of `"actions`". It is because the role you are going to create should not be used for user management purposes.


<ExtendedCodeGroup title="CREATE METER COMPANY ROLE" copyable>
<CodeBlock title="Python">

```python
_COMPANY_ROLE_BODY = {
    "name": "TT DEMO User Role",
    "statement": {
        "effect": "allow",
        "actions": [
           "who_am_i", "get_user_role", "get_user", "get_user_secret", "get_twin_ledger_entry_value", "create_twin", "get_twin", "update_twin", "terminate_twin", "create_twin_identity", "get_twin_identity", "update_twin_identity",  "resolve_twin_identity", "delete_twin_identity", "add_twin_ledger_entry", "get_twin_ledger_entry", "update_twin_ledger_entry", "delete_twin_ledger_entry", "get_twin_ledger_entry_history", "get_log", "list_stickers", "get_stickers", "get_sticker", "put_sticker", "remove_sticker", "webhook_subscribe" 
        ]
    }
}

status, response = ADMIN_TT_SERVICE.create_user_role(body=_COMPANY_ROLE_BODY)
resp_role = json.loads(response)

_COMPANY_ROLE = resp_role["uuid"]

print("New Role: {}\n".format(resp_role))
```
</CodeBlock>
</ExtendedCodeGroup>

### Create meter company user

After creating a role, create a user with the role.

A user is a person who utilizes the Trusted Twin platform. The permissions of a user to access, create, and manipulate Trusted Twin resources are controlled by the role assigned to the user.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users | [create_user](..reference/users/create-user.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CREATE USER" copyable>
<CodeBlock title="Python">

```python
_COMPANY_USER_BODY = {
    "name": "TT DEMO User",
    "role": _COMPANY_ROLE,
    "description": {
        "purpose": "Playing with Trusted Twin APIs"
    }
}

status, response = ADMIN_TT_SERVICE.create_user(body=_COMPANY_USER_BODY)
resp_user = json.loads(response)

_COMPANY_USER = resp_user['uuid']

print("New User: {}\n".format(resp_user))
```
</CodeBlock>
</ExtendedCodeGroup>


### Create meter company PIN

A user needs a User Secret (API key) to authenticate on the Trusted Twin platform.

Let's create a User Secret PIN. It will be required to create a User Secret (API key) in the next step. The User Secret PIN can be passed to a third party so that they can create a User Secret for themselves.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users/{user}/secrets | [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>


::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
A User Secret PIN is valid for 10 minutes, and it can be used only once to generate a User Secret (API key). After a User Secret (API key) has been generated, the User Secret PIN automatically expires.
:::

<ExtendedCodeGroup title="CREATE USER SECRET PIN" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret_pin(_COMPANY_USER)
resp_pin = json.loads(response)

_COMPANY_PIN = resp_pin["pin"]

print("New PIN: {}\n".format(resp_pin))
```
</CodeBlock>
</ExtendedCodeGroup>

### Create meter company User Secret (API key)

Once you have created a User Secret PIN, you can use it to generate a User Secret (API key).

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /secrets/{account}/{pin} | [create_user_secret](../reference/secrets/create-user-secret.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

::: warning USER SECRET IS DISPLAYED ONLY ONCE
<Icon name="alert-circle" :size="1.75" />
Please note that the User Secret (API key) is displayed only once. It is not stored in the Trusted Twin system, and it cannot be retrieved. Keep your User Secret stored in a secure place. If you lose your User Secret, you will need to start the User Secret generation process from the beginning.
:::

<ExtendedCodeGroup title="CREATE USER SECRET" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret(_ACCOUNT, _COMPANY_PIN)
resp_secret = json.loads(response)

_COMPANY_SECRET = resp_secret["secret"]

print("New User Secret: {}\n".format(resp_secret))
```
</CodeBlock>
</ExtendedCodeGroup>

The result is a User Secret (API key) that we will use throughout this tutorial. We are going to use it to authenticate requests.

<ExtendedCodeGroup title="USE USER SECRET TO AUTHENTICATE REQUESTS" copyable>
<CodeBlock title="Python">

```python
COMPANY_TT_SERVICE = TTRESTService(tt_auth=_COMPANY_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>

## Digital twin

### Create a Twin

You are going to create a digital twin that will represent a water meter. The digital twin will represent a water meter. It will consist of a Twin and a Ledger. 

First, let's create a Twin. It is an object on the Trusted Twin platform that ties together all knowledge in the digital twin.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins | [create_twin](../reference/twins/create_twin.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

A Twin can have an optional description field. A description of a Twin is always publicly accessible. This means that anyone who knows the unique identifier (UUID) of the Twin will be able to view the description of the Twin.

The Twin you are going to create will have a description field with information about the application name, object type, serial number, and website.

<ExtendedCodeGroup title="METER DESCRIPTION" copyable>
<CodeBlock title="Python">

```python
_METER_DESCRIPTION_BODY = {
    "description": {
        "application": "Trusted Twin DEMO App",
        "object": "generic meter",
        "serial": "12334",
        "site": "https://trustedtwin.com/docs/tutorials/stickers-notifications-tutorial.html"
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

Please note that the description below contains a placeholder (ADD_YOUR_METER_SERIAL_HERE). You need to add a serial number for the meter in place of the placeholder. You can insert any serial number that works for you. In our example, the `"serial"` is `"12345"`.

<ExtendedCodeGroup title="CREATE A TWIN" copyable>
<CodeBlock title="Python">

```python
print("... creating meter twins ...")
status, response = COMPANY_TT_SERVICE.create_twin(body=_METER_DESCRIPTION_BODY)
resp_twin = json.loads(response)

twin = resp_twin['creation_certificate']['uuid']
serial = resp_twin['description']['serial']

print(json.dumps(resp_twin))
```
</CodeBlock>
</ExtendedCodeGroup>

As you can see in the response, you are the `"owner"` and `"creator"` of the Twin. The owner of a Twin can change the description of the Twin by sending a request to the `"update_twin"` endpoint if the status of the Twin is `"alive"`.

### Add Ledger Entries

Let's add Entries to the Twin's Ledger.

A Ledger is an object that stores information about the state of a Twin. It contains Entries. They are user-defined key-value pairs.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/ledgers/personal | [add_twin_ledger_entry](../reference/ledgers/add-twin-ledger-entry.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

The Entries of your Ledger are going to contain the following information about the state of the Twin:

- current meter reading (`"reading"` Entry),
- battery level (`"battery"` Entry),
- log of errors raised through the meter software (`"log"` Entry),
- location of where the water meter is installed (`"location"` Entry).

<ExtendedCodeGroup title="ADD TWIN LEDGER ENTRIES" copyable>
<CodeBlock title="Python">

```python
_METER_LEDGER_BODY = {
    "entries": {
        "reading": {
            "value": 0.0,
            "history": "1M"
        },
        "battery": {
            "value": 1.0,
            "visibility": "account == LEDGER.service_order.contractor"
        },
        "log": {
            "value": {
                "level": "INFO",
                "message": "Meter digital twins created and activated!"
            }
        },
        "location": {
            "value": {
                "city": "",
                "street": ""
            },
            "visibility": "account == LEDGER.service_order.contractor"
        }
    } 
}

print("... creating meter ledgers ...")
status, response = COMPANY_TT_SERVICE.add_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
resp_ledger = json.loads(response)

print(resp_ledger)
```
</CodeBlock>
</ExtendedCodeGroup>

Please note that in the above `"reading"` Entry, we used the `"visibility"` and `"history"` attributes:

- The `"visibility"` attribute defines the visibility of the data in the Entry for other accounts.
- The `"history"` attribute defines the time period for which the history of value changes is stored (in our case it is set to 1 month). The [History](../reference/history/index.md) service lets you store historical values of Ledger Entry.<br/>

You can execute the above code multiple times to observe the API response for when a given Entry has been already added to the Ledger.

### Digital twin

Let's connect all the above steps into a convenient `create_meter()` method. It handles all the above operations and creates a digital twin consisting of a Twin and a Ledger.

<ExtendedCodeGroup title="CREATE METER DIGITAL TWIN" copyable>
<CodeBlock title="Python">

```python
def create_meter(serial: str):
    """Creates a meter with a given serial number."""
    
    _METER_DESCRIPTION_BODY = {
        "description": {
            "application": "Trusted Twin DEMO App",
            "object": "generic meter",
            "serial": serial,
            "site": "https://trustedtwin.com/docs/tutorials/stickers-notifications-tutorial.html"
        }
    }
    
    _METER_LEDGER_BODY = {
        "entries": {
            "reading": {
                "value": 0.0,
                "history": "1M"
            },
            "battery": {
                "value": 1.0,
                "visibility": "account == LEDGER.service_order.contractor"
            },
            "log": {
                "value": {
                    "level": "INFO",
                    "message": "Meter digital twins created and activated!"
                }
            },
            "location": {
                "value": {
                    "city": "",
                    "street": ""
                },
                "visibility": "account == LEDGER.service_order.contractor"
            }
        }
    }
    
    # Create a new meter Twin
    print("... creating meter twins ...")
    status, response = COMPANY_TT_SERVICE.create_twin(body=_METER_DESCRIPTION_BODY)
    resp_twin = json.loads(response)
    
    twin = resp_twin['creation_certificate']['uuid']
    
    # Create a Ledger for the meter
    print("... creating meter ledgers ...\n")
    status, response = COMPANY_TT_SERVICE.add_twin_ledger_entry(twin, "personal", body=_METER_LEDGER_BODY)
    resp_ledger = json.loads(response)
        
    return resp_twin, resp_ledger

meter_twin, meter_ledger  = create_meter("170")
  
print("Meter Twin: {}\n".format(json.dumps(meter_twin, indent=4)))
print("Meter Ledger: {}\n".format(json.dumps(meter_ledger, indent=4)))
```
</CodeBlock>
</ExtendedCodeGroup>

## Stickers (Preparation)

### About

[Stickers (workflow tags)](../reference/stickers/index.md) on the Trusted Twin platform are a functionality designed to facilitate workflow processes. They support passing ownership of workflow items (e.g., tasks to be completed) between multiple users. These users can be users belonging the same account where a Sticker was created or to a foreign account. Stickers are attached to Twins.

In this tutorial we are going to demonstrate how the passing of such workflow items works on the example of the meter Twin that you've already created. We are going to assume that you are the owner of the meter and that you hire a third party to:<br/>
- service the meter in case there is a need to repair the meter,<br/>
- perform technical reviews of the meter.

To see how the process works also from the perspective of the contractor company, you are going to create another role, user and User Secret (API key) for the contractor company. The role will have permissions restricted to obtaining details of their own user, role and User Secret (API key) as well as permissions so view Stickers and remove Stickers as well as webhook-related permissions (we are going to use them later on in this tutorial when exploring the Notifications functionality).

### Create contractor role

First, let's create a role for the contractor.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/identities | [create_twin_identity](../reference/identities/create-twin-identity.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CREATE METER IDENTITY" copyable>
<CodeBlock title="Python">

```python
_CONTRACTOR_ROLE_BODY = {
    "name": "TT DEMO User Role",
    "rules": {
        "twins": "TWIN.application == 'Trusted Twin DEMO App'"
    },
    "statement": {
        "effect": "allow",
        "actions": [
            "who_am_i", "get_user_role", "get_user", "get_user_secret", 
            "get_twin", "get_twin_ledger_entry_value", "get_sticker", "get_stickers", "list_stickers", "remove_sticker", "webhook_subscribe"
        ]
    }
}

status, response = ADMIN_TT_SERVICE.create_user_role(body=_CONTRACTOR_ROLE_BODY)
resp_role = json.loads(response)

_CONTRACTOR_ROLE = resp_role["uuid"]

print("New Role: {}\n".format(resp_role))
```
</CodeBlock>
</ExtendedCodeGroup>

### Create contractor user

After creating a role, let's create a user with the above role.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users | [create_user](..reference/users/create-user.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CREATE CONTRACTOR USER" copyable>
<CodeBlock title="Python">

```python
_CONTRACTOR_USER_BODY = {
    "name": "TT DEMO User",
    "role": _CONTRACTOR_ROLE,
    "description": {
        "purpose": "Playing with Trusted Twin APIs"
    }
}

status, response = ADMIN_TT_SERVICE.create_user(body=_CONTRACTOR_USER_BODY)
resp_user = json.loads(response)

_CONTRACTOR_USER = resp_user['uuid']

print("New User: {}\n".format(resp_user))
```
</CodeBlock>
</ExtendedCodeGroup>

### Create contractor PIN

The contractor needs a User Secret (API key) to authenticate on the Trusted Twin platform.

Let's create a User Secret PIN for the contractor. The PIN will be required to create a User Secret (API key) in the next step.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /users/{user}/secrets | [create_user_secret_pin](../reference/secrets/create-user-secret-pin.md) |
</EndpointsTable>

</div>
<div class="column">

</div>
</div>


::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
A User Secret PIN is valid for 10 minutes, and it can be used only once to generate a User Secret (API key). After a User Secret (API key) has been generated, the User Secret PIN automatically expires.
:::

<ExtendedCodeGroup title="CREATE CONTRACTOR USER SECRET PIN" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret_pin(_CONTRACTOR_USER)
resp_pin = json.loads(response)

_CONTRACTOR_PIN = resp_pin["pin"]

print("New PIN: {}\n".format(resp_pin))
```
</CodeBlock>
</ExtendedCodeGroup>

### Create contractor User Secret

Now, create a User Secret (API key) for the contractor.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /secrets/{account}/{pin} | [create_user_secret](../reference/secrets/create-user-secret.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

::: warning WARNING
<Icon name="alert-circle" :size="1.75" />
Please note that the User Secret is displayed only once. It is not stored in the Trusted Twin system, and it cannot be retrieved. Keep your User Secret stored in a secure place. If you lose your User Secret, you will need to start the User Secret generation process from the beginning.
:::


<ExtendedCodeGroup title="CREATE CONTRACTOR USER SECRET" copyable>
<CodeBlock title="Python">

```python
status, response = ADMIN_TT_SERVICE.create_user_secret(_ACCOUNT, _CONTRACTOR_PIN)
resp_secret = json.loads(response)

_CONTRACTOR_SECRET = resp_secret["secret"]

print("New User Secret: {}\n".format(resp_secret))
```
</CodeBlock>
</ExtendedCodeGroup>

The result is a User Secret (API key) that we will use throughout this tutorial. We are going to use it to authenticate requests send by the contractor.

<ExtendedCodeGroup title="AUTHENTICATE AS CONTRACTOR" copyable>
<CodeBlock title="Python">

```python
CONTRACTOR_TT_SERVICE = TTRESTService(tt_auth=_CONTRACTOR_SECRET)
```
</CodeBlock>
</ExtendedCodeGroup>

## Stickers

### Get Twin Stickers (meter company)

Stickers (workflow tags) are attached to a Twin. First, we are going to check whether any Stickers are attached to the meter Twin. Please note that you can only see stickers where you are a recipient of the sticker (i.e., your UUID is included in the `"recipients"` list).

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /twins/{twin}/stickers/ | [get_stickers](../reference/stickers/get-stickers.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="GET STICKERS" copyable>
<CodeBlock title="Python">

```python
status, response = COMPANY_TT_SERVICE.get_stickers(twin)
resp_get_stickers = json.loads(response)

print("Stickers list: {}".format(resp_get_stickers))
```
</CodeBlock>
</ExtendedCodeGroup>

There should be no Stickers attached to the Twin yet.<br/>


### Put Stickers on Twin (meter company)

Let's create two Stickers to represent repair requests and assign them to the following users:

- `_CONTRACTOR_USER`: the user who should perform the repairs and technical reviews,
- `_COMPANY_USER`: the meter company user who is creating the Sticker. This will allow the meter company user to view this Sticker.

The validity of the Stickers will be set to one day from the moment the Stickers are created. Please note that we are adding as well the `"publish"` attribute. It will allow us to set notifications for the Stickers in the next chapter.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/stickers/ | [put_sticker](../reference/stickers/put-sticker.md)|
</EndpointsTable>


</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="PUT STICKERS ON TWIN" copyable>
<CodeBlock title="Python">

```python
import datetime

_VALIDITY = round((datetime.datetime.now() + datetime.timedelta(days=1)).timestamp(), 2)

_STICKERS_BODY = {
        "stickers": {
            "red": {
                "note": "This is an urgent repair request",
                "recipients": [
                    _COMPANY_USER,
                    _CONTRACTOR_USER
                ],
                "validity_ts": _VALIDITY,
                "publish": {
                    "on_put": "task-created", 
                    "on_remove": "task-completed",
                    "on_expire": "task-expired"
                }
            },
            "yellow": {
                "note": "This is an urgent repair request",
                "recipients": [
                    _COMPANY_USER, 
                    _CONTRACTOR_USER
                ],
                "validity_ts": _VALIDITY,
                "publish": {
                    "on_put": "task-created", 
                    "on_remove": "task-completed",
                    "on_expire": "task-expired"
                }
            }
        }
}

status, response = COMPANY_TT_SERVICE.put_sticker(twin, body=_STICKERS_BODY)
resp_stickers = json.loads(response)

print(resp_stickers)
```
</CodeBlock>
</ExtendedCodeGroup>

### Get Twin Stickers (meter company)

Let's check again if there are any Stickers attached to the Twin.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /twins/{twin}/stickers/ | [get_stickers](../reference/stickers/get-stickers.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="GET STICKERS" copyable>
<CodeBlock title="Python">

```python
status, response = COMPANY_TT_SERVICE.get_stickers(twin)  
resp_get_stickers = json.loads(response)

print("Stickers list: {}".format(resp_get_stickers))
```
</CodeBlock>
</ExtendedCodeGroup>

We can see that the Stickers we just created are attached to the Twin.

### List Stickers (contractor)

Let's change the perspective and get into the shoes of the contractor company. Check if you have any Stickers (requests) assigned.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /stickers | [get_stickers](../reference/stickers/list-stickers.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="LIST STICKERS (REQUESTS)" copyable>
<CodeBlock title="Python">

```python
status, response = CONTRACTOR_TT_SERVICE.list_stickers()
resp_list_stickers = json.loads(response)

print("Sticker requests list: {}".format(resp_list_stickers))
```
</CodeBlock>
</ExtendedCodeGroup>

The response should return two Stickers (requests) as there are two Stickers assigned to the contractor user. 

### Remove Sticker (contractor)

After completing the repair, remove a Sticker from the Twin. 

Please note that the operation of removing Stickers is atomic i.e., after one user has removed a Sticker from the Twin this Sticker is no longer attached to the Twin.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| DELETE   | /twins/{twin}/stickers/{color} | [remove_sticker](../reference/stickers/remove-sticker.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Let's remove the `"red"` Sticker from the Twin.

<ExtendedCodeGroup title="REMOVE STICKER" copyable>
<CodeBlock title="Python">

```python
_COLOR = "red"
    
status, response = CONTRACTOR_TT_SERVICE.remove_sticker(twin, _COLOR)
resp_removed_sticker = json.loads(response)

print("Removed Sticker: {}".format(resp_removed_sticker))
```
</CodeBlock>
</ExtendedCodeGroup>

### List Stickers (contractor)

Confirm (still as the contractor company) that the Sticker is no longer on your task list.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /stickers | [get_stickers](../reference/stickers/get-stickers.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Let's retrieve all Stickers assigned to you.

<ExtendedCodeGroup title="LIST STICKERS (REPAIR REQUESTS)" copyable>
<CodeBlock title="Python">

```python
status, response = CONTRACTOR_TT_SERVICE.list_stickers()
resp_list_stickers = json.loads(response)

print("Stickers list: {}".format(resp_list_stickers))
```
</CodeBlock>
</ExtendedCodeGroup>

### Get Sticker (meter company)

Let's change the perspective back to the perspective of the meter owner. Check whether the `"red"` Sticker is still attached to the Twin.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /twins/{twin}/stickers/{color} | [get_sticker](../reference/stickers/get-sticker.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="GET STICKER" copyable>
<CodeBlock title="Python">

```python
_COLOR = "red"
    
status, response = COMPANY_TT_SERVICE.get_sticker(twin, _COLOR)
resp_sticker = json.loads(response)

print("Sticker response: {}".format(resp_sticker))
```
</CodeBlock>
</ExtendedCodeGroup>

We should receive an error message stating that the Sticker of the given color was not found if you search for the `_COLOR` `"red"`.

## Notifications

### About

Let's automate our workflow process further. Our goal is that:
- the service company receives a notification when a service request is created,
- we receive a notification once the meter is serviced,
- we receive a notification once a service request expires.

### Subscribe to a webhook (contractor)

First, create a subscription as the contractor company to receive a notification every time a Sticker (request) is created and assigned to you.   

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /notifications/webhooks | [webhook_subscribe](../reference/notifications/webhook-subscribe.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Please note that you will have to provide a callback url to which the subscription confirmation and notification messages will be sent. You can use a temporary URL by creating a public bin on the [requestbin.com](https://requestbin.com/) website.

The value of the `"topic"` parameter must be equal to the `"color"` (name) of the Sticker. 

<ExtendedCodeGroup title="WEBHOOK SUBSCRIBE" copyable>
<CodeBlock title="Python">

```python
_NOTIFICATION_BODY = {
    "callback_url": "ADD_YOUR_CALLBACK_URL_HERE", # add a callback URL
    "topic": "new-task-request"
}
    
status, response = CONTRACTOR_TT_SERVICE.webhook_subscribe(body=_NOTIFICATION_BODY)
resp_webhook_subscription = json.loads(response)

print("Webhook subscription: {}".format(resp_webhook_subscription)) 
```
</CodeBlock>
</ExtendedCodeGroup>

### Confirmation message received (contractor)

After creating the subscription, you should receive a confirmation message similar to the message below to the callback URL.

<ExtendedCodeGroup title="CONFIRMATION MESSAGE">
<CodeBlock title="json">

```json
{
    "type": "Confirmation",
    "message": "fdc674bc-4d19-42cb-80c2-b662676a7042",
    "sent_ts": 1677772112.046,
    "confirmation_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=jJoxbadWx5F9cxOo9IUEz2bBVjj7iZmDZFgpMxuLaeMvHxzkSwLnwmMs9ot3u3hY6B2GunSGMTQhJKIalSRlEcULw43FkZgbLiNkCh67LlWIMatLTFnnlf4tS4hFXSPa9t7RJIsIL2LNA9qeot0hWOdjjfIIMKP5dKhduGtK5sUmTg3IxF5vzOPD6mhBdHiFg1aHYYtYzkmTQhssOEuovdu7tiduzaLCVvF4hzdYWs62FMQzDLNvTSxJAYcM5Vvl3yehisUgylxtXZKwlnWonqgFpSCGmgtoPWEHxv58ecE9hg0zacyQPzr19QV76MzA3vRGmu9fCCcQmfpYOPaSNLEHwsWJn5Vi9Jk74oJ0H14bgBeYUKB6MJRSdW0811nJsc1qrPzSclYlrXstmncobN2u35ri8TlSpD7APNWGWm1IgXq0c56z2qbyjHT3ZMx4ftfN4A5xu8ef1XDyavduoEsN5qoNq9KWCHr2hm9kgSSLvfxOvnyewVLlmiNMChOdgRnixGampHbMHErGY2m92c==",
    "subscription": {
        "topic": "new-task-request",
        "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
        "validity_ts": 1678376911.633
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Confirm webhook subscription (contractor)

As next step, you need to confirm the webhook subscription. 

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /notifications/webhooks/?token={token} | [webhook_confirm_subscription](../reference/notifications/webhook-confirm-subscription.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

Use the `token` from the `"confirmation_url"` (you will find it in the confirmation message) to confirm the webhook subscription.

<ExtendedCodeGroup title="WEBHOOK CONFIRM SUBSCRIPTION" copyable>
<CodeBlock title="Python">

```python
 _SUBSCRIPTION_TOKEN = "ADD_YOUR_CONFIRMATION_URL_TOKEN_HERE" # add your token from the confirmation_url in the confirmation message

status, response = CONTRACTOR_TT_SERVICE.webhook_confirm_subscription(_SUBSCRIPTION_TOKEN)
resp_confirmed_subscription = json.loads(response)

print("Confirmed webhook subscription: {}".format(resp_confirmed_subscription))      
```
</CodeBlock>
</ExtendedCodeGroup>

### Put Sticker on a Twin (meter company)

Now that we have subscribed to a webhook and confirmed the subscription, each time there is a new sticker (request) created for the contractor company, they will receive a notification message. 

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /twins/{twin}/stickers | [put_sticker](../reference/stickers/put-sticker.md)|
</EndpointsTable>


</div>
<div class="column">

</div>
</div>

Let's trigger a notification event by adding a Sticker of the given color to the Twin.

<ExtendedCodeGroup title="PUT STICKER" copyable>
<CodeBlock title="Python">

```python
_STICKER_BODY = {
    "stickers": {
        "red": {
            "note": "This is an urgent repair request",
            "recipients": [
                _COMPANY_USER, 
                _CONTRACTOR_USER
            ],
            "validity_ts": _VALIDITY,
            "publish": {
                "on_put": "new-task-request",
                "on_remove": "task-completed",
                "on_expire": "task-expired"
            }
        }  
    }
}
        
status, response = COMPANY_TT_SERVICE.put_sticker(twin, body=_STICKER_BODY)
resp_sticker = json.loads(response)
    
print("New Sticker: {}".format(resp_sticker))  
```
</CodeBlock>
</ExtendedCodeGroup>

### Notification message received (contractor)

You should have received a notification message for the contractor company to the callback URL you provided earlier. The notification message should look similar to the message below:

<ExtendedCodeGroup title="NOTIFICATION MESSAGE">
<CodeBlock title="Python">

```python
{
    "type": "Notification",
    "trigger": "stickers.put",
    "message": "b2cac688-d645-4040-b03b-504b3325e9db",
    "twins": "24158644-c440-4353-b734-e6cfcb5b23b7",
    "event": {
        "color": "red",
        "note": "This is an urgent repair request",
        "validity_ts": 1677858508.38,
        "created_ts": 1677772626.227,
        "put_by": {
            "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
            "role": "b2cac688-d645-4040-b03b-504b3325e9db",
            "user": "2aae89b6-471c-4e12-9a86-d454302c4138"
        }
    },
    "sent_ts": 1677772626.385,
    "subscription": {
        "topic": "red",
        "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
        "validity_ts": 1678376911.633,
        "unsubscribe_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=WixZrhEW8UVedcIVegVlqbP5j5uMSkcuGUX204IhiNFi091hblzndFbuuHnLqaiMfhd4IB4dKwVVG8OqhnfQRmuacjYWPhJEgAbMHn1oqpB8BQXWR9Dh3RN9C4Du5SfkldqYi1drOOOqSuwRi1zF0IJIXzk=",
        "refresh_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=WixZrhEW8UVedcIVegVlqbP5j5uMSkcuGUX204IhiNFi091hblzndFbuuHnLqaiMfhd4IB4dKwVVG8OqhnfQRmuacjYWPhJEgAbMHn1oqpB8BQXWR9Dh3RN9C4Du5SfkldqYi1drOOOqSuwRi1zF0IJIXzk="
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Subscribe to a webhook (meter company)

Let's change the perspective and create a subscription as if we were the company owning the meters. We want to get notified of any Stickers that will get removed from the Twin. We will create a subscription with the topic "task-completed".

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| POST   | /notifications/webhooks | [webhook_subscribe](../reference/notifications/webhook-subscribe.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="WEBHOOK SUBSCRIBE" copyable>
<CodeBlock title="Python">

```python
_NOTIFICATION_BODY = {
    "callback_url": "ADD_YOUR_CALLBACK_URL_HERE", # add your callback URL
    "topic": "task-completed"
}
    
status, response = COMPANY_TT_SERVICE.webhook_subscribe(body=_NOTIFICATION_BODY)
resp_webhook_subscription = json.loads(response)

print("Webhook subscription: {}".format(resp_webhook_subscription)) 
```
</CodeBlock>
</ExtendedCodeGroup>

### Confirmation message received (meter company)

After creating a subscription, you will receive a subscription confirmation to the `"callback_url"`. The confirmation will look similar to the message below.

<ExtendedCodeGroup title="CONFIRMATION MESSAGE">
<CodeBlock title="json">

```json
{
    "type": "Confirmation",
    "message": "0c847ed6-81d2-4de5-a1b3-b176caec571d",
    "sent_ts": 1677772112.046,
    "confirmation_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=m7c2f1jyCCSI6JoWshqhhBRYA5OnExGDPsfWkW7TltRHl2i5popIe6Wj2HEuXAg4Ujdef3560yC4kADSiYtra3CmTpI34tvKS77mJbV6N54HV1Gj7mYNJhudHvJ3dbaHp1tIiP3UxgHIvELGnuibV7ti11Qw==",
    "subscription": {
        "topic": "task-completed",
        "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
        "validity_ts": 1678376911.633
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>

### Confirm webhook subscription (meter company)

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| GET   | /notifications/webhooks/?token={token} | [webhook_confirm_subscription](../reference/notifications/webhook-confirm-subscription.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="CONFIRM WEBHOOK SUBSCRIPTION">
<CodeBlock title="Python">

```python
_SUBSCRIPTION_TOKEN = "ADD_YOUR_CONFIRMATION_URL_TOKEN_HERE" # add your token from the confirmation_url in the confirmation message

status, response = COMPANY_TT_SERVICE.webhook_confirm_subscription(_SUBSCRIPTION_TOKEN)
resp_confirmed_subscription = json.loads(response)

print("Confirmed webhook subscription: {}".format(resp_confirmed_subscription))   
```
</CodeBlock>
</ExtendedCodeGroup>


### Remove Sticker (contractor)

Let's remove the Sticker to see if it triggers a notification message.

<div class="row">
<div class="column">

<EndpointsTable>
| Method | Path                           | Operation  |
|:-------|:-------------------------------|:------------|
| DELETE   | /twins/{twin}/stickers/{color} | [remove_sticker](../reference/stickers/remove-sticker.md)|
</EndpointsTable>

</div>
<div class="column">

</div>
</div>

<ExtendedCodeGroup title="REMOVE STICKER" copyable>
<CodeBlock title="Python">

```python
_COLOR = "red"
    
status, response = CONTRACTOR_TT_SERVICE.remove_sticker(twin, _COLOR)
resp_removed_sticker = json.loads(response)

print("Removed Sticker: {}".format(resp_removed_sticker))
```
</CodeBlock>
</ExtendedCodeGroup>

### Notification message received (meter company)

<ExtendedCodeGroup title="NOTIFICATION MESSAGE">
<CodeBlock title="Python">

```python
{
    "type": "Notification",
    "trigger": "stickers.removed",
    "message": "b2cac688-d645-4040-b03b-504b3325e9db",
    "twins": "24158644-c440-4353-b734-e6cfcb5b23b7",
    "event": {
        "color": "red",
        "note": "This is an urgent repair request",
        "validity_ts": 1677858508.38,
        "created_ts": 1677772626.227,
        "removed_by": {
            "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
            "role": "2ceba088-1b8d-4b2f-adf8-73e29605271d",                                       
            "user": "c56f9f52-7bb5-4faf-8a7b-980fc010106e"
        }
    },
    "sent_ts": 1677772626.385,
    "subscription": {
        "topic": "task-completed",
        "account": "9346e528-f96f-45ee-bd2e-6d9bda6aec1c",
        "validity_ts": 1678376911.633,
        "unsubscribe_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=WixZrhEW8UVedcIVegVlqbP5j5uMSkcuGUX204IhiNFi091hblzndFbuuHnLqaiMfhd4IB4dKwVVG8OqhnfQRmuacjYWPhJEgAbMHn1oqpB8BQXWR9Dh3RN9C4Du5SfkldqYi1drOOOqSuwRi1zF0IJIXzk=",
        "refresh_url": "https://rest.trustedtwin.com/notifications/webhooks/9346e528-f96f-45ee-bd2e-6d9bda6aec1c?token=WixZrhEW8UVedcIVegVlqbP5j5uMSkcuGUX204IhiNFi091hblzndFbuuHnLqaiMfhd4IB4dKwVVG8OqhnfQRmuacjYWPhJEgAbMHn1oqpB8BQXWR9Dh3RN9C4Du5SfkldqYi1drOOOqSuwRi1zF0IJIXzk="
    }
}
```
</CodeBlock>
</ExtendedCodeGroup>



