# TrustedTwin Documentation

## Overview
TrustedTwin Docs uses [Vuepress](https://github.com/vuejs/vuepress) static site generator.
Its [guides](https://vuepress.vuejs.org/guide/) and [documentation](https://vuepress.vuejs.org/config/) are appropriate for this site configuration and customization.

## File structure
```
.
├── docs                            # Documentation-related files
│   ├── .vuepress                   # Vuepress-related files
│   │   ├── public                  # Static assets (e.g. images, favicons)
│   │   ├── theme                   # Theme-related files
│   │   │   ├── components          # Vue components
│   │   │   ├── global-components   # Global Vue components (e.g. to use in content)
│   │   │   ├── layouts             # Base HTML structure files
│   │   │   ├── styles              # CSS/Stylus files
│   │   │   └── utils               # Additional utility scripts
│   │   └── config.js               # Vuepress configuration file
│   ├── index.md                    # Homepage configuration
│   ├── README.md                   # Documentation content structure (not used in Vuepress build)
│   └── **/*.md                     # Documentation content files
├── README-DEVELOPMENT.md           # Documentation's technical details
└── README.md                       # Documentation content structure (not used in Vuepress build)
```

## NPM's commands

- `npm run docs:dev` - Starts Vuepress' local development environment
- `npm run docs:build` - Builds a production version into `/docs/.vuepress/dist` directory.

## Customizations
Documentation's theme extends `@vuepress/theme-default`. The custom theme overwrites the following default components:
- [`docs/.vuepress/theme/components/Navbar.vue`](docs/.vuepress/theme/components/Navbar.vue)
- [`docs/.vuepress/theme/components/Page.vue`](docs/.vuepress/theme/components/Page.vue)
- [`docs/.vuepress/theme/components/SearchBox.vue`](docs/.vuepress/theme/components/SearchBox.vue)
- [`docs/.vuepress/theme/components/SidebarGroup.vue`](docs/.vuepress/theme/components/SidebarGroup.vue)
- [`docs/.vuepress/theme/components/SidebarLink.vue`](docs/.vuepress/theme/components/SidebarLink.vue)

Additionally, the documentation's theme contains a bunch of new components:
- [`docs/.vuepress/theme/global-components/Button.vue`](docs/.vuepress/theme/global-components/Button.vue)
- [`docs/.vuepress/theme/global-components/EndpointsTable.vue`](docs/.vuepress/theme/global-components/EndpointsTable.vue)
- [`docs/.vuepress/theme/global-components/Env.vue`](docs/.vuepress/theme/global-components/Env.vue)
- [`docs/.vuepress/theme/global-components/ExtendedCodeGroup.vue`](docs/.vuepress/theme/global-components/ExtendedCodeGroup.vue)
- [`docs/.vuepress/theme/global-components/ExtendedSummary.vue`](docs/.vuepress/theme/global-components/ExtendedSummary.vue)
- [`docs/.vuepress/theme/global-components/Icon.vue`](docs/.vuepress/theme/global-components/Icon.vue)
- [`docs/.vuepress/theme/global-components/TableWrapper.vue`](docs/.vuepress/theme/global-components/TableWrapper.vue)

## Local development
This guide contains CLI commands. Some IDEs support the mentioned commands in their GUI.

### Prerequisites
A development environment has to contain:
- `git`
- `node@16`
- `npm@8`

These tools have to be available in CLI.

### Clone repository to the development environment
1. Clone repository to the development environment
   ```
   git clone git@gitlab.com:trustedtwinpublic/api-documentation-public.git
   ```
2. Go into the cloned directory
   ```
   cd ./api-documentation-public
   ```
3. Install `npm` dependencies
   ```
   $ npm i
   ```

### Introducing changes guideline
- New changes should be introduced on a separate branch based on the `main` branch or the other feature branch if necessary
  ```
  git checkout main
  git pull
  git checkout -b [new branch name]
  ```
- During development, run the vuepress development environment
  ```
  npm run docs:dev
  ```
  It will create `localhost` by default on the `8080` port. The development environment will auto-refresh open pages by applying saved changes.
- Before pushing changes to the repository check if the [production build](#npms-commands) doesn't throw errors
  ```
  npm run docs:build
  ```
- After pushing changes to the repository open a [new merge request](https://gitlab.com/trustedtwinpublic/api-documentation-public/-/merge_requests)
